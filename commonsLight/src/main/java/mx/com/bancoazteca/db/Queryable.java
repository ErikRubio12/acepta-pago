package mx.com.bancoazteca.db;

import java.util.List;

public interface Queryable <E extends Entity>{
	public List<E>queryAll();
	public E queryById(long id);
	public boolean delete(E entity);
	public boolean update(E entity);
	public boolean insert(E entity);
	public E query(String table, String[] columns, String selection,
			String[] selectionArgs, String groupBy, String having,
			String orderBy);
}
