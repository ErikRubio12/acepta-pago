package mx.com.bancoazteca.beans;

import java.io.Serializable;


public class Person implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NAME = "nomCliente";
	public static final String LAST_NAME = "apePCliente";
	public static final String LASTNAME2 = "apeMCliente";
	public static final String MAIL="correo";
	protected String name = "";
	protected String lastName = "";
	protected String lastName2 = "";
	protected AddressBean address;
	protected String completeName;
	protected String mail;
	protected String id="-1";
	
	public Person() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLasttName2() {
		return lastName2;
	}

	public void setLastName2(String lasttName2) {
		this.lastName2 = lasttName2;
	}

	public void setAddress(AddressBean address) {
		this.address = address;
	}

	public AddressBean getAddress() {
		return address;
	}
	public String getCompleteName() {
		
		return completeName;
	}
	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer res= new StringBuffer();
		res.append("nombre: ");
		res.append(name);
		res.append("\na paterno:");
		res.append(lastName);
		res.append("\na materno:");
		res.append(lastName2);
		res.append("\ncorreo:");
		res.append(mail);
		res.append("\n");
		res.append(address);
		return res.toString();
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if(o==null) return false;
		else if(o==this) return true;
		else if (((Person)o).id ==null || this.id==null) return false;
		else  return this.id.equalsIgnoreCase(((Person)o).id);
	}
}