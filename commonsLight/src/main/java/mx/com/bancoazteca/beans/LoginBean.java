package mx.com.bancoazteca.beans;

import android.text.TextUtils;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Hashtable;

public class LoginBean implements Serializable,KvmSerializable{
	public final static String LOGIN="login";
	private static final int FIELDS = 2;
	private static PropertyInfo pIUser= new PropertyInfo();
	private static PropertyInfo pIPasswor= new PropertyInfo();
	private static PropertyInfo listPI[]= new PropertyInfo[FIELDS];
	private static final long serialVersionUID = 1691562621529720249L;
	private static final String USER = "usuario";
	private static final String PASSWORD = "password";
	private String user;
	//private String password;
	private char[] password;
	static{
		
		pIUser= new PropertyInfo();
		pIUser.name=USER;
		pIUser.type=PropertyInfo.STRING_CLASS;
		
		pIPasswor= new PropertyInfo();
		pIPasswor.name=PASSWORD;
		pIPasswor.type=PropertyInfo.STRING_CLASS;
		
		listPI[0]=pIUser;
		listPI[1]=pIPasswor;
	}
	
	public LoginBean() {
		// TODO Auto-generated constructor stub
	}
	
	public LoginBean(String user, char[] password) {
		super();
		this.user = user;
		this.password = password;
	}

	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return Arrays.toString(password).replace(",","").replace("[","").replace("]","").replace(" ","").trim();
//		return TextUtils.join("",Arrays.toString(password));
	}
	public void setPassword(char[] password) {
		this.password = password;
	}

	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: return user;
			case 1: return password; 
			default: return null;
		}
	}

	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}

	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}

	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0: pIUser=(PropertyInfo) arg1; break;
		case 1: pIPasswor=(PropertyInfo) arg1; break;
		}
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer res= new StringBuffer("usuario: ");
		res.append(user);
		res.append("\ncontraseña: ");
		res.append(password);
		res.append("\n");
		return res.toString();
	}
}
