package mx.com.bancoazteca.beans;

import java.io.Serializable;

import mx.com.bancoazteca.db.Entity;

public class AddressBean extends Entity implements Serializable{
	public static final String STREET="calle";
	public static final String INTERNAL_NUMBER="numInt";
	public static final String EXTERNAL_NUMBER="numExt";
	public static final String COLONY="colonia";
	public static final String STATE="estado";
	public static final String ZIP_CODE="cp";
	public static final String TELEPHONE="numTel";
	public static final String LADA="numLada";
	public static final String EXTENSION="extensio";
	public static final String POBLACION="poblacion";
	public final static String ADDRESS="address";
	private static final long serialVersionUID = 8316066506588078077L;
	private String street="";
	private String internalNumber="";
	private String externalNumber="";
	private String colony="";
	private String state="";
	private String zipCode="";
	private String telephone="";
	private String lada="";
	private String extension="0";
	private String poblacion="";
	public AddressBean(AddressBean address) {
		// TODO Auto-generated constructor stub
		this.street=address.getStreet();
		this.internalNumber=address.getInternalNumber();
		this.externalNumber=address.getExternalNumber();
		this.colony=address.getColony();
		this.state=address.getState();
		this.zipCode=address.getZipCode();
		this.telephone=address.getTelephone();
		this.lada=address.getLada();
		this.extension=address.getExtention();
		this.poblacion=address.getPoblacion();
	}
	public AddressBean() {
		// TODO Auto-generated constructor stub
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getInternalNumber() {
		return internalNumber;
	}
	public void setInternalNumber(String internalNumber) {
		this.internalNumber = internalNumber;
	}
	public String getExternalNumber() {
		return externalNumber;
	}
	public void setExternalNumber(String externalNumber) {
		this.externalNumber = externalNumber;
	}
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setExtention(String extension) {
		this.extension = extension;
	}
	public String getExtention() {
		return extension;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setLada(String lada) {	
		this.lada = lada;
	}
	public String getLada() {
		return lada;
	}
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer output=new StringBuffer();
		output.append("numero interior: " );
		output.append(internalNumber);
		output.append("\nnumero exterior: ");
		output.append(externalNumber);
		output.append("\ncalle: ");
		output.append(street);
		output.append("\ncolonia: ");
		output.append(colony);
		output.append("\npoblacion: ");
		output.append(poblacion);
		output.append("\nestado: ");
		output.append(state);
		output.append("\ncp: ");
		output.append(zipCode);
		output.append("\nlada : ");
		output.append(lada);
		output.append("\ntelefono : ");
		output.append(telephone);
		output.append("\n");
		return output.toString();
	}
	
}
