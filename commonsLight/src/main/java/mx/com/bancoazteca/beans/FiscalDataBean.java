package mx.com.bancoazteca.beans;

import java.io.Serializable;


public class FiscalDataBean implements Serializable {
	private static final long serialVersionUID = 9146401745018193635L;

	public static final String FISCAL_DATA = "fiscal data";
	public static final String FISCAL_NAME = "nomNeg";
	public static final String BUSINESS_STREET = "calleNeg";
	public static final String BUSINESS_COLONY = "colNeg";
	public static final String BUSINESS_ZIPCODE = "cpNeg";
	public static final String BUSINESS_STATE = "edoNeg";
	public static final String BUSINESS_EXTENSION = "extNeg";
	public static final String BUSINESS_LADA = "ladaNeg";
	public static final String BUSINESS_TOWN = "munNeg";
	public static final String BUSINESS_RFC = "rfcNeg";
	public static final String BUSSINES_TELEPHONE = "telNeg";
	
	private AddressBean address;
	private String fiscalName="";
	private String rfc="";
	public AddressBean getAddress() {
		return address;
	}
	public void setAddress(AddressBean address) {
		this.address = address;
	}
	public String getFiscalName() {
		return fiscalName;
	}
	public void setFiscalName(String fiscalName) {
		this.fiscalName = fiscalName;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer output=new StringBuffer();
		output.append("direccion: ");
		output.append(address);
		output.append("\nrazon social: ");
		output.append(fiscalName);
		output.append("\nrfc: ");
		output.append(rfc);
		return output.toString();
	}
}
