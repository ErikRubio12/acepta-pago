package mx.com.bancoazteca.beans;

import java.io.Serializable;

public class CardBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2336965288752894298L;
	public final static String DEBIT="DEBITO";
	public final static String CREDIT="CREDITO";
	public final static String CREDIT_CARD="CREDIT_CARD";
	public final static String ADMIN="ADMIN";
	public static final String TRACK2 = "TRACK2";		
	public static int CARD_NUMBER_LENGTH=16;
	public static int MIN_SECURITY_LENGTH=3;
	public static int MAX_SECURITY_LENGTH=4;
	private String cardOwner=null;
	private String number;
	private String maskedPan=null;
	private String KSN;
	private String rawTracks=null;
	private int security;
	private int validity;
	private String strValidity;
	private int cardType;
	private String chipData=null;
	private String track1;
	private String track2;
	private boolean isFullBack=false;
	public static int READER_UNKNOWN=0;
	public static int READER_OCOM_SWIPED=1;
	public static int READER_OCOM_BLUE_CHIP=2;
	private int device=READER_UNKNOWN;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getSecurity() {
		return security;
	}
	public void setSecurity(int security) {
		this.security = security;
	}
	public int getValidity() {
		return validity;
	}
	public void setValidity(int validity) {
		this.validity = validity;
	}
	public void setCardOwner(String cardOwner) {
		this.cardOwner = cardOwner;
	}
	public String getCardOwner() {
		return cardOwner;
	}
	public void setCardType(int cardType) {
		this.cardType = cardType;
	}
	public int getCardType() {
		return cardType;
	}
	public String getStringValidity(){
		return this.strValidity;
	}
	public void setStringValidity(String string) {
		// TODO Auto-generated method stub
		this.strValidity=string;
	}

	public String getRawTracks() {
		return rawTracks;
	}
	public void setRawTracks(String rawTracks) {
		this.rawTracks = rawTracks;
	}
	public String getKSN() {
		return KSN;
	}
	public void setKSN(String kSN) {
		KSN = kSN;
	}
	public String getMaskedPan() {
		return maskedPan;
	}
	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}
	public String getChipData() {
		return chipData;
	}
	public void setChipData(String chipData) {
		this.chipData = chipData;
	}
//	public String getTrack2() {
//		return track2;
//	}
//	public void setTrack2(String track2) {
//		this.track2 = track2;
//	}
	public String getTrack1() {
		return track1;
	}
	public void setTrack1(String track1) {
		this.track1 = track1;
	}
	public boolean isFullBack() {
		return isFullBack;
	}
	public void setFullBack(boolean isFullBack) {
		this.isFullBack = isFullBack;
	}
	
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer output=new StringBuffer();
		output.append("numero de tarjeta: ");
		output.append(number);
		output.append(" dueño: ");
		output.append(cardOwner);
		output.append(" vigencia: ");
		output.append(validity);
		output.append(" mascara: ");
		output.append(maskedPan);
		output.append(" ksn: ");
		output.append(KSN);
		output.append(" tracks encriptados: ");
		output.append(rawTracks);
		output.append(" track 2 : ");
		output.append(track2);
		return output.toString();
	}
	public int getDevice() {
		return device;
	}
	public void setDevice(int device) {
		this.device = device;
	}
}
