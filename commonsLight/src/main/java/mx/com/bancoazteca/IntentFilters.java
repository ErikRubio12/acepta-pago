package mx.com.bancoazteca;


public class IntentFilters {
	public class Categories{
		public final static String APP_CATEGORY="mx.com.bancoazteca.SWIPER";
	}
	public class Actions{
		public static final String HEADSET_PLUG = "android.intent.action.HEADSET_PLUG";
		public static final String ERROR = "mx.com.bancoazteca.ERROR";
		public static final String SWIPER_DECODED = "mx.com.bancoazteca.SWIPER_DECODED";
		public static final String EXCEPTION = "mx.com.bancoazteca.EXEPCTION";
		public static final String RECORDER_FINISHED = "mx.com.bancoazteca.RECORDER_FINISHED";
		public static final String SWIPER_DECODING = "mx.com.bancoazteca.SWIPER_DECODING";
	}
}
