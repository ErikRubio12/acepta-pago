package mx.com.bancoazteca;

import java.io.Serializable;
import java.util.List;

import android.content.Context;


public interface Navigation {
	public void release();
	public void goForward(Class<?> className);
	public void  goForward(Class<?> className,Context context);
	public void goForward(Class<?> className,Serializable param,String paramName);
	public void goForward(Class<?> className,Context context,Serializable param,String paramName);
	public void goForwardForResult(Class<?> className, Context context,
			List<Serializable> params, List<String> paramNames,int code);
	void goForwardForResult(Class<?> className, int code);
	void goForwardForResult(Class<?> className, Context context, int code);
	void goForwardForResult(Class<?> className, Serializable param,String paramName, int code);
	void goForwardForResult(Class<?> className, Context context, Serializable param,
			String paramName, int code);
	void goForward(Class<?> className, Context context, List<Serializable> params,
			List<String> paramNames);
}
