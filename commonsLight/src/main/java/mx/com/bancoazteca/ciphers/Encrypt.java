	package mx.com.bancoazteca.ciphers;


import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;
import mx.com.bancoazteca.hw.emvreader.emv.EMVResponse;
import mx.com.bancoazteca.util.Utility;


public class Encrypt {

	public static final String salt = "saltIsGoodForYou";
	public static final String pass = "thePassword";
	public static final String saltWS = "$1$3dr$ORHESVlaB6NFVB6qX3DpJ/";
	public static final String passWS = "$1$3dr$ZS.gv5E5gK4.kOcZFuAlh.";
	public static EMVResponse parseTracks(byte[] data){
		int offset = 5; 
		EMVResponse response= new EMVResponse();
		response.setCardType(data[1] & 0xff);
		response.setTrack1Len(data[3] & 0xff);
		response.setTrack2Len(data[4] & 0xff);
		response.setTrack3Len(data[5] & 0xff);
		if (response.getTrack1Len() > 0) {
			response.setMaskedT1(new String(data, offset,response.getTrack1Len()));
			offset += response.getTrack1Len();
			
		}
		
		if (response.getTrack2Len() > 0) {
			response.setMaskedT2(new String(data, offset,response.getTrack2Len()));
			offset += response.getTrack2Len();
			
		}
		
		if (response.getTrack3Len() > 0) {
			response.setMaskedT3(new String(data, offset,response.getTrack3Len()));
			offset += response.getTrack3Len();			
		}
		return response;
	}
	public static String [] decryptDUKPTBlock(byte[] data) {
		/*
		 * DATA[0]:	CARD TYPE: 0x0 - payment card
		 * DATA[1]:	TRACK FLAGS
		 * DATA[2]:	TRACK 1 LENGTH
		 * DATA[3]:	TRACK 2 LENGTH
		 * DATA[4]:	TRACK 3 LENGTH
		 * DATA[??]: TRACK 1 DATA MASKED
		 * DATA[??]: TRACK 2 DATA MASKED
		 * DATA[??]: TRACK 3 DATA
		 * DATA[??]: TRACK 1 AND TRACK 2 TDES ENCRYPTED
		 * DATA[??]: TRACK 1 SHA1 (0x14 BYTES)
		 * DATA[??]: TRACK 2 SHA1 (0x14 BYTES)
		 * DATA[??]: DUKPT SERIAL AND COUNTER (0x0A BYTES)
		 */
//		 String tk=HexUtil.byteArrayToHexString(dukptKey);
//     	 String td=HexUtil.byteArrayToHexString(data);
//         Log.v("Main activity", "despues llave: " + tk);
//         Log.v("Main activity", "despues datos: " + td);
		int cardType = data[0] & 0xff;
		int track1Len = data[2] & 0xff;
		int track2Len = data[3] & 0xff;
		int track3Len = data[4] & 0xff;
		int offset = 5; 
		String trakcs[]= new String [2];
		System.out.println(HexUtil.byteArrayToHexString(data));
		StringBuffer sb = new StringBuffer();
		sb.append("Card type: " + ((cardType == 0) ? "PAYMENT" : "UNKNOWN") + "\n");
		
		if (track1Len > 0) {
			sb.append("Track1(M): " + new String(data, offset, track1Len) + "\n");
			offset += track1Len; 
		}
		
		if (track2Len > 0) {
			sb.append("Track2(M): " + new String(data, offset, track2Len) + "\n");
			offset += track2Len; 
		}
		
		if (track3Len > 0) {
			sb.append("Track3: " + new String(data, offset, track3Len) + "\n");
			offset += track3Len; 
		}
		Log.v("Cipher", sb.toString());
		if ((track1Len + track2Len) > 0) {
			int blockSize = (track1Len + track2Len + 7) & 0xFFFFFF8;
			byte[] encrypted = new byte[blockSize];
			System.arraycopy(data, offset, encrypted, 0, encrypted.length);
			offset+= blockSize;
			
			byte[] track1Hash = null;
			if (track1Len > 0) {
				track1Hash = new byte[20];
				System.arraycopy(data, offset, track1Hash, 0, track1Hash.length);
				offset+= track1Hash.length;
			}
			
			byte[] track2Hash = null;
			if (track2Len > 0) {
				track2Hash = new byte[20];
				System.arraycopy(data, offset, track2Hash, 0, track2Hash.length);
				offset+= track2Hash.length;
			}
			
//			byte[] ipek = new byte[16];
//			System.arraycopy(dukptKey, 0, ipek, 0, ipek.length);
			//Creo esta seria la llave que deberia cambiarse ya que coincide con el valor que se por default en el demo para cambiar las llaves
			byte[] ipek = {(byte) 0x82,(byte) 0xDF,(byte) 0x8A,(byte) 0xC0,0x22,(byte) 0x91,0x62,(byte) 0xAF,0x04,0x0C,(byte) 0xF4,(byte) 0xD0,0x76,0x43,0x72,0x79};
			
			byte[] ksn = new byte[10];
			System.arraycopy(data, offset, ksn, 0, ksn.length);
			offset+= ksn.length;
			
			byte[] dataKey = CryptoUtil.calculateDataKy(ksn, ipek);
			byte[] decrypted = CryptoUtil.decrypt3DESCBC(dataKey, encrypted);			
			
			if (decrypted == null) throw new RuntimeException("Failed to decrypt");
				
			if (track1Hash != null) {
				if (!Arrays.equals(track1Hash, CryptoUtil.calculateSHA1(decrypted, 0, track1Len))) {
					throw new RuntimeException("Failed to decrypt");
				}
			}
			
			if (track2Hash != null) {
				if (!Arrays.equals(track2Hash, CryptoUtil.calculateSHA1(decrypted, track1Len, track2Len))) {
					throw new RuntimeException("Failed to decrypt");
				}
			}
			String track;
			if (track1Len > 0) {
				trakcs[0]=new String(decrypted, 0, track1Len);
				track="Track1: " + trakcs[0] + "\n";
				sb.append(track);
				Log.v("mAIN ACTIVITY", track);
			}

			if (track2Len > 0) {
				trakcs[1]=new String(decrypted, track1Len, track2Len);
				track="Track2: " + trakcs[1] + "\n";
				sb.append(track);
				Log.v("mAIN ACTIVITY", track);
			} 
		}
		
		return trakcs;
	}
	static public String encryptString(String input){
	    try {
			Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(pass, salt.getBytes("UTF8"));
			
		    SecretKeySpec skeySpec = new SecretKeySpec(rfc.getBytes(16), "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(rfc.getBytes(16));
		       
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
			byte[] encrypted = cipher.doFinal(input.getBytes("UTF8"));
			input =  Base64.encodeToString(encrypted,Base64.DEFAULT);
			
	    } catch (Exception e) {
			e.printStackTrace();
		} 
		return input;
	}
	
	static public String decryptString(String input){
		byte[] output = null;
		String value = null;
		try {
			output = Base64.decode(input,Base64.DEFAULT);
			
			Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(pass,salt.getBytes("UTF8"));
			
		    SecretKeySpec skeySpec = new SecretKeySpec(rfc.getBytes(16), "AES");
		    IvParameterSpec ivSpec = new IvParameterSpec(rfc.getBytes(16));
		       
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
			byte[] decrypted = cipher.doFinal(output);
			value = new String(decrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}
	
	static public String encryptStringWS(String input){
	    try {
	    	input=Utility.removeDeniedCharacters(input);
			Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(passWS, saltWS.getBytes("UTF8"));
			
		    SecretKeySpec skeySpec = new SecretKeySpec(rfc.getBytes(16), "AES");
		    IvParameterSpec ivSpec = new IvParameterSpec(rfc.getBytes(16));
		       
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
			byte[] encrypted = cipher.doFinal(input.getBytes("UTF8"));
			input =  Base64.encodeToString(encrypted,Base64.DEFAULT);
			
	    } catch (Exception e) {
			e.printStackTrace();
		} 
		return input;
	}
	
	static public String decryptStringWS(String input){
		byte[] output = null;
		String value = null;
		try {
			output = Base64.decode(input,Base64.DEFAULT);
			
			Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(passWS,saltWS.getBytes("UTF8"));
			
		    SecretKeySpec skeySpec = new SecretKeySpec(rfc.getBytes(16), "AES");
		    IvParameterSpec ivSpec = new IvParameterSpec(rfc.getBytes(16));
		       
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
			byte[] decrypted = cipher.doFinal(output);
			value = new String(decrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Utility.getBackDeniedCharacters(value);
	}
	
}
