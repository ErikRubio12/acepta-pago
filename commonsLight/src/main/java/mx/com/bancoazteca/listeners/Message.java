package mx.com.bancoazteca.listeners;

public  class Message {
	public final static int OK=4;
	public final static int OKKey=9;
	public final static int CANCEL=5;
	public final static int ERROR=6;
	public final static int UNKNOWN=7;
	public final static int EXCEPTION=8;
	public static final String MESSAGE = "mensaje";
	
	private int type;
	private int textMessage;
	
	public Message() {
		// TODO Auto-generated constructor stub
	}
	public Message(int type, int textMessage) {
		super();
		this.type = type;
		this.textMessage = textMessage;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getTextMessage() {
		return textMessage;
	}

	public void setTextMessage(int textMessage) {
		this.textMessage = textMessage;
	}
	
}
