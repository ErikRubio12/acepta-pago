package mx.com.bancoazteca.listeners;

import android.content.Context;


public interface MessageListener <M extends Message,P>{
	public  void sendMessage(Context ctx,M message,P parameter);
	public void sendStatus(Context ctx,int status);
	public void sendMessage(Context ctx,Message message, Object... parameters);
	
}
