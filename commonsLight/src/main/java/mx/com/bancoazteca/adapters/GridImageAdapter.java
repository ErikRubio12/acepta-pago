package mx.com.bancoazteca.adapters;

import java.util.ArrayList;

import mx.com.bancoazteca.ui.Item;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;

public class GridImageAdapter extends BaseAdapter implements ListAdapter{

	private Context context;
	private ArrayList<Item> images= new ArrayList<Item>();
	
	public GridImageAdapter(Context context) {
		// TODO Auto-generated constructor stub
		this.context= context;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return images.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return images.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return images.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View imageView;        
		if (convertView == null) {  
			imageView = new ImageView(context);
			imageView.setLayoutParams(new GridView.LayoutParams(85, 85));            
			((ImageView) imageView).setScaleType(ImageView.ScaleType.CENTER_CROP); 
			imageView.setPadding(8, 8, 8, 8);  
			((ImageView) imageView).setImageResource(images.get(position).getId());
		} 
		else {
			imageView = (ImageView) convertView;
			imageView.setBackgroundResource(images.get(position).getId());
		}
		
		return imageView;
		
	}
	public void addItem(Item item){
		images.add(item);
	}

}
