package mx.com.bancoazteca.adapters;

import java.util.ArrayList;
import java.util.List;
import mx.com.bancoazteca.beans.Person;
import mx.com.bancoazteca.filters.PersonFilter;
import mx.com.bancoazteca.util.Utility;
import mx.commons.R;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PersonAdapter <P extends Person> extends BaseAdapter implements ListAdapter, Filterable{
	private List<P> clients;
	private Context context;
	private List<P> completeClients;
	private boolean isContextMenu=false;
	private boolean isEditingMode=false;
	private OnClickListener listener;
	private PersonFilter<BaseAdapter, P> filter;
	public PersonAdapter(Context context, List<?> completeClientList) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.completeClients=(List<P>) completeClientList;
		clients=new ArrayList<P>();
		clients.addAll(completeClients);
		filter = new PersonFilter<BaseAdapter, P>(clients, this);
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return completeClients.size();
	}
	
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return completeClients.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	
	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		
		
		Person currentClient=(Person) getItem(index);
		TextView nameClient;
		TextView mailClient;
		// TODO Auto-generated method stub       
		if (convertView == null) {  
			convertView=(RelativeLayout) Utility.inflateLayout(context, R.layout.client_row, null);
			convertView.setTag(index);
			nameClient=(TextView) convertView.findViewById(R.id.name);
			mailClient=(TextView) convertView.findViewById(R.id.mail);
			StringBuilder completeName=new StringBuilder(currentClient.getCompleteName());
			nameClient.setText(completeName.toString());
			mailClient.setText(currentClient.getMail());
			convertView.setOnClickListener(listener);
			View v;
			if(isEditingMode){
				v=convertView.findViewById(R.id.content);
				v.setVisibility(View.VISIBLE);
//				v.setTag(index);
//				v.findViewById(R.id.edit).setTag(index);
//				v.findViewById(R.id.delete).setTag(index);
			}
				
			else{
				v=convertView.findViewById(R.id.content);
				v.setVisibility(View.VISIBLE);
			}
			v.setTag(index);
			v.findViewById(R.id.edit).setTag(index);
			v.findViewById(R.id.delete).setTag(index);
		} 
		else {
			convertView.setTag(index);
			nameClient=(TextView) convertView.findViewById(R.id.name);
			mailClient=(TextView) convertView.findViewById(R.id.mail);
			StringBuilder completeName=new StringBuilder(currentClient.getCompleteName());
			nameClient.setText(completeName.toString());
			mailClient.setText(currentClient.getMail());
			convertView.setOnClickListener(listener);
			View v;
			if(isEditingMode){
				v=convertView.findViewById(R.id.content);
				v.setVisibility(View.VISIBLE);
//				v.setTag(index);
//				v.findViewById(R.id.edit).setTag(index);
//				v.findViewById(R.id.delete).setTag(index);
			}
				
			else{
				v=convertView.findViewById(R.id.content);
				v.setVisibility(View.VISIBLE);
			}
			v.setTag(index);
			v.findViewById(R.id.edit).setTag(index);
			v.findViewById(R.id.delete).setTag(index);
		}
		if (isContextMenu) {
			((Activity)context).registerForContextMenu(convertView);
		}
		
		return convertView;
	}
	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null) {
			filter = new PersonFilter<BaseAdapter, P>(clients, this);
		}
		else{
			completeClients.clear();
			completeClients.addAll(clients);
			((PersonFilter<BaseAdapter, Person>)filter).setList((ArrayList<Person>) completeClients);
		} 
		return filter;
	}
	public void setContextMenu(boolean isContextMenu) {
		this.isContextMenu = isContextMenu;
	}
	public void deleteAllComplete(){
		completeClients.clear();
		clients.clear();
	}
	
	public void deleteComplete(int id){
		Person clientToRemove=completeClients.get(id);
		completeClients.remove(id);
		clients.remove(clientToRemove);
	}
	public void setEditingMode(boolean isEditingMode) {
		this.isEditingMode = isEditingMode;
	}
	public void setLsitener(OnClickListener lsitener) {
		this.listener = lsitener;
	}

}
