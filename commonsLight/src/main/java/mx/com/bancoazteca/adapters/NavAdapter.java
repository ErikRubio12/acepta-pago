package mx.com.bancoazteca.adapters;

import mx.com.bancoazteca.util.Utility;
import mx.commons.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NavAdapter extends BaseAdapter {
	private String[] titles;
	private Integer[] images;
	private Context context;
	public NavAdapter(Context context,String [] titles,Integer[] images) {
		// TODO Auto-generated constructor stub
		this.titles=titles;
		this.images=images;
		this.context=context;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.length<images.length ? titles.length : images.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return titles[arg0];
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView imageNav;
		TextView textNav;
		// TODO Auto-generated method stub       
		if (convertView == null) {  
			convertView=(LinearLayout) Utility.inflateLayout(context, R.layout.nav_row, null);
			convertView.setTag(index);
			textNav=(TextView) convertView.findViewById(R.id.textNav);
			imageNav=(ImageView) convertView.findViewById(R.id.imageNav);
			textNav.setText(titles[index]);
			imageNav.setImageResource(images[index]);
		} 
		else {
			convertView.setTag(index);
			textNav=(TextView) convertView.findViewById(R.id.textNav);
			imageNav=(ImageView) convertView.findViewById(R.id.imageNav);
			textNav.setText(titles[index]);
			imageNav.setImageResource(images[index]);
			
		}
		
		return convertView;
	}
}
