package mx.com.bancoazteca.data.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import mx.com.bancoazteca.util.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.util.Log;

public class FileManager {
	/**
	 * Environment.getDataDirectory()			= 	/data
	 * Environment.getDownloadCacheDirectory()	= 	/cache
	 * Environment.getRootDirectory()			=	/system
	 * Environment.getExternalStorageDirectory()=	/sdcard
	 * context.getFilesDir()					=	/data/data/packageName/files
	 * context.getCacheDir()					=	/data/data/packageName/cache
	 */
	public final static String separator=File.separator;
	public final static int PNG=10;
	public final static int JPEG=20;
	public final static int EXTERNAL=30;
	public final static int INTERNAL=40;
	public final static int CACHE=50;
	protected final static File DATA_PATH = new File(Environment.getDataDirectory(),"data");
	protected final static File EXTERNAL_PATH= Environment.getExternalStorageDirectory();
	private final static String PNG_FORMAT=".png";
	protected final static String FILE_DIR="files"+ File.separator;
	private final static String JPEG_FORMAT=".jpg";
	protected static final int NO_COMPRESSION = 100;
	public static final short MODE_RW = 2;
	public static final short MODE_W = 3;
//	public static final short MODE_R = 1;
	public static final short MODE_APPEND = 6;
	private static final String FILE_MANAGER = "FILE MANAGER";
	private static String appDirectory=".PAZ";
	public static File openFile(Context context, String fileName,
			int source, int mode) {
		try {
			File path=null;
			if(source==EXTERNAL){
				path= new File(EXTERNAL_PATH,appDirectory+ "/");
				
				if(!path.exists() || !path.isDirectory()){
					path.mkdirs();
				}
				if(path==null || fileName ==null)
					return null;
				path= new File(path,fileName);
				
				if(checkSDStatus()!=3)
					return null;
			}
			else if(source==INTERNAL){
				if (context==null)
					context=Utility.getInitialContext();
				path=context.getDir(appDirectory, Context.MODE_PRIVATE);
				path= new File(path,fileName);
			}
			else
				return null;
			
			if(!path.exists()){
				if(!path.createNewFile())
					return null;
			}
			
			if (mode==MODE_RW || mode==MODE_W){
				if(path.exists())
					path.delete();
				path.createNewFile();
				return path;
			}
			else if (mode==MODE_APPEND ) 
				return path;
			else
				return path;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.v("FILE MANAGER", "IOException: "+ e);
			return null;
		}
		catch (Exception e) {
			// TODO: handle exception
			Log.v("FILE MANAGER", "Exception: "+ e);
			return null;
		}
		
	}
	public static File openFile(Context context, String fileName) {
		// TODO Auto-generated method stub
		return openFile(context,fileName,EXTERNAL,MODE_RW);
	}
	public static File openFile(String fileName) {
		// TODO Auto-generated method stub
		return openFile(null,fileName,EXTERNAL,MODE_RW);
	}
	public static File openFile(String fileName,int mode) {
		// TODO Auto-generated method stub
		return openFile(null,fileName,EXTERNAL,mode);
	}
	public static File openFile(Context context, String fileName,int mode) {
		// TODO Auto-generated method stub
		return openFile(context,fileName,EXTERNAL,mode);
	}
	public static boolean write(File destiny,CharSequence line){
		FileWriter file=null;
		BufferedWriter writer=null;
		try {
			file = new FileWriter(destiny,true);
			writer= new BufferedWriter(file);
			writer.write(line.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}finally{
			try {
				if(writer!= null){
					writer.close();
					file.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return false;
			}
		}
		return true;
	}
	private static short checkSDStatus() {
		// TODO Auto-generated method stub
		byte mExternalStorageAvailable = 0;
		byte mExternalStorageWriteable = 0;
		String state = Environment.getExternalStorageState();
		
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = 2;
		    mExternalStorageWriteable = 1;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
		    mExternalStorageAvailable = 2;
		    mExternalStorageWriteable = 2;
		}
		return (short)(mExternalStorageAvailable ^ mExternalStorageWriteable);
	}
	
	
	public static File saveBitmap(Bitmap image, String fileName,int percentage,int compressFormat, int source)  {
		return saveBitmap(image, Utility.getInitialContext(), fileName, percentage, compressFormat, source);
	}
	public static File saveBitmap(Bitmap image, String fileName,int percentage,int compressFormat)  {
		return saveBitmap(image, Utility.getInitialContext(), fileName, percentage, compressFormat, EXTERNAL);
	}
	public static File saveBitmap(Bitmap image, String fileName,int percentage)  {
		return saveBitmap(image, Utility.getInitialContext(), fileName, percentage, JPEG, EXTERNAL);
	}
	public static File saveBitmap(Bitmap image,Context context, String fileName,int percentage,int compressFormat, int source)  {
		// TODO Auto-generated method stub	
		return saveBitmap(image, Utility.getInitialContext(), fileName, percentage, compressFormat, source,MODE_RW);
	}
	public static File saveBitmap(Bitmap image,Context context, String fileName,int percentage,int compressFormat, int source,int mode)  {
		File dst=null;
		if(source==EXTERNAL){
			dst= new File(EXTERNAL_PATH,appDirectory);
			if(!dst.exists() || !dst.isDirectory()){
				if(dst.mkdirs())
					Log.v("FILE MANAGER", "Se pudo crear el directorio");
			}
				
			dst= new File(dst,fileName);
			Log.v("FILE MANAGER", "directorion de la imagen: "+ dst.getAbsolutePath());
			if(checkSDStatus()!=3)
				return null;
		}
		else if(source==INTERNAL){
			dst=Utility.getInitialContext().getDir("PAZ", Context.MODE_PRIVATE);
			dst=new File(dst, fileName);
		}
		
		if(checkSDStatus()==0)
			return null;
		
		Log.v(FILE_MANAGER, "paso 1");
		if(dst != null ){
			Log.v(FILE_MANAGER, "paso 2: " +dst.getAbsolutePath());
			StringBuffer path=new StringBuffer(dst.getPath());
			path.append(compressFormat==PNG ? PNG_FORMAT : JPEG_FORMAT);
			dst = new File(path.toString());
			Log.v(FILE_MANAGER, "paso 3: " +dst.getAbsolutePath());
			FileOutputStream fo=null;
			try {
				if(mode != MODE_APPEND){
					if(dst.exists())
						dst.delete();
				}
				
				Log.v(FILE_MANAGER, "paso 4");
				dst.createNewFile();
				fo= new FileOutputStream(dst);
				image.compress(compressFormat==PNG ? CompressFormat.PNG : CompressFormat.JPEG, percentage, fo);
				
			} catch (IOException e) { 
				Log.v(FILE_MANAGER, e.getMessage());
				return null;
			}
			finally{
				try {
					fo.flush();
					fo.close();
					
				} catch (IOException e) {
					Log.v(FILE_MANAGER, e.getMessage());
					return null;	
				}
			}
		}
		else
			return null;
		return dst;
	}
	public static CharSequence match(File src, CharSequence line) {
		// TODO Auto-generated method stub
		BufferedReader buff = null;
		try {
			String currentLine;
			FileReader fr= new FileReader(src);
			buff= new BufferedReader(fr);
			while((currentLine=buff.readLine())!=null){
				if(currentLine.indexOf(line.toString())>=0)
					return currentLine;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
		finally{
			try {
				buff.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return null;
			}
		}
		return null;	
	}
	
	
	public static boolean isSdPresent(){
		return android.os.Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}

	public static File saveRawData(byte[] data,String fileName, int source)	 {
		// TODO Auto-generated method stub
		File dst;
		if (source==EXTERNAL) {
			dst= new File(EXTERNAL_PATH,appDirectory+ "/");
			if(!dst.exists() || !dst.isDirectory());
				dst.mkdirs();
			dst= new File(dst,fileName);
			Log.v("FILE MANAGER", "directorion de la imagen: "+ dst.getAbsolutePath());
			if(checkSDStatus()!=3)
				return null;
		}
		else{
			dst=Utility.getInitialContext().getDir("PAZ", Context.MODE_PRIVATE);
			dst=new File(dst, fileName);
		}
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(dst);
			os.write(data);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e(FILE_MANAGER, e.getMessage());
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(FILE_MANAGER, e.getMessage());
			return null;
		}finally{
			try {
				os.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e(FILE_MANAGER, e.getMessage());
			}
		}
		os=null;
		data=null;
		return dst;
	}
	public static boolean createDirectory(String dir,int source){
		File dst;
		if (source==EXTERNAL) {
			dst= new File(EXTERNAL_PATH,appDirectory+ "/");
			if(!dst.exists() || !dst.isDirectory());
				dst.mkdirs();
			dst= new File(dst,dir);
			if(checkSDStatus()!=3)
				return false;
		}
		else{
			dst=Utility.getInitialContext().getDir("PAZ", Context.MODE_PRIVATE);
			dst=new File(dst, dir);
		}
		Log.v("FILE MANAGER", "directorion a crear: "+ dst.getAbsolutePath());
		if(!dst.exists())
			return dst.mkdirs();
		else
			return true;
	}
	public static String getAppDirectory() {
		return appDirectory;
	}
	public static void setAppDirectory(String appDirectory) {
		FileManager.appDirectory = appDirectory;
	}
}
