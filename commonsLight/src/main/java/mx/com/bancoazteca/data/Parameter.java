package mx.com.bancoazteca.data;

public class Parameter <K ,V> {
	protected K key;
	protected V value=null;
	public Parameter() {
		// TODO Auto-generated constructor stub
	}
	
	public Parameter(K key, V value) {
		super();
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}
	public void setKey(K key) {
		this.key = key;
	}
	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}
	public void setParams(K key,V Value){
		this.key=key;
	}
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Parameter<?,?> other = (Parameter<?,?>) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} 
		else if (!key.equals(other.key))
			return false;
		
		return true;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer out= new StringBuffer();
		out.append(key==null ? "" : key.toString());
		out.append("=");
		out.append(value==null ? "" : value.toString());
		return out.toString();
	}
	
}
