package mx.com.bancoazteca.filters;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.Locale;

import mx.com.bancoazteca.util.Utility;

public class TextFilter implements InputFilter{
	public final static int TYPE_ALPHA_NUMERIC=1;
	public final static int TYPE_NUMERIC=2;	
	public final static int TYPE_CHARACTERS=3;
	public final static int TYPE_EMAIL=4;
	public final static int TYPE_LADA=5;
	public final static int TYPE_ALLOW_QUOTS=11;
	protected int filterType=TYPE_ALPHA_NUMERIC;
	protected int length;
	protected char [] extraChars=null;
	public TextFilter(int characters) {
		// TODO Auto-generated constructor stub
		filterType=characters;
	}
	public TextFilter(int characters,int length) {
		// TODO Auto-generated constructor stub
		filterType=characters;
		this.length=length;
	}
	public TextFilter(int characters,char [] extraChars) {
		// TODO Auto-generated constructor stub
		filterType=characters;
		this.extraChars=extraChars;
	}
	public TextFilter() {
		// TODO Auto-generated constructor stub
		filterType=TYPE_ALPHA_NUMERIC;
	}
	public static String filterText(String text){
		text=text.toLowerCase(Locale.getDefault());
		text=text.replace('Á', 'A');
		text=text.replace('É', 'E');
		text=text.replace('Í', 'I');
		text=text.replace('Ó', 'O');
		text=text.replace('Ú', 'U');
		text=text.replace('À', 'A');
		text=text.replace('È', 'E');
		text=text.replace('Ì', 'I');
		text=text.replace('Ò', 'O');
		text=text.replace('Ù', 'U');
		char currentChar=' ';
		for (int i = 0; i < text.length(); i++) {
			currentChar=text.charAt(i);
			if(!Utility.isLetterOrDigit(currentChar) &&
			   !Character.isWhitespace(currentChar)  &&
			   currentChar != '_' &&
			   currentChar != '-' ){
				text=text.replace(currentChar, ',');
			}
		}
		return text;
	}
	@Override
    public CharSequence filter(CharSequence source, int start, int end,
            Spanned dest, int dstart, int dend) {
		if(filterType==TYPE_ALPHA_NUMERIC){//Letras numeros y espacios
			 for (int i = start; i < end; i++) {
				 if(extraChars==null){
					 if(!Utility.isLetterOrDigit(Character.toLowerCase(source.charAt(i))) && 
						!Character.isWhitespace(source.charAt(i)))
						 return "";
				 }
				 else{
					 if(!Utility.isLetterOrDigit(Character.toLowerCase(source.charAt(i))) && 
						!Utility.isvalidChar(Character.toLowerCase(source.charAt(i)), extraChars) &&
						!Character.isWhitespace(source.charAt(i)))
						 return "";
				 }
				 
			 }
			 return null;
		}
		else if(filterType==TYPE_LADA){//Solo numeros
			char currentChar='|';
			for (int i = start; i < end; i++) {
				 if(extraChars==null){
					 currentChar=source.charAt(i);
					 if(!Character.isDigit(currentChar))
		       		 		return "";
					
				 }
				 else{
					 currentChar=source.charAt(i);
					 if(!Character.isDigit(currentChar) &&
						!Utility.isvalidChar(Character.toLowerCase(currentChar), extraChars))
		       		 		return "";
				 }
       	 	}
			
			 if(dest.length()==2 && source.length()>0 && ((dest.charAt(0)=='3' && dest.charAt(1)=='3') || (dest.charAt(0)=='5' && dest.charAt(1)=='5') || (dest.charAt(0)=='6' && dest.charAt(1)=='6'))){
				 if(source.charAt(0)==dest.charAt(0))
				 return "";
			 }
					
			return null;
		}
		else if(filterType==TYPE_NUMERIC){//Solo numeros
			for (int i = start; i < end; i++) {
				 if(extraChars==null){
					 if(!Character.isDigit(source.charAt(i)))
		       		 		return "";
				 }
				 else{
					 if(!Character.isDigit(source.charAt(i)) &&
						!Utility.isvalidChar(Character.toLowerCase(source.charAt(i)), extraChars))
		       		 		return "";
				 }
       		 	
       	 	}
			return null;
		}
		
		else if(filterType==TYPE_CHARACTERS || filterType==TYPE_EMAIL){
			for (int i = start; i < end; i++) {//Caracteres especiales, espacio en blanco y en el caso del correo "@"
				if(filterType==TYPE_EMAIL){
					if(extraChars==null){
						if(source.charAt(i) != '@' &&
						   !Utility.isvalidChar(Character.toLowerCase(source.charAt(i))) && 
						   !Character.isWhitespace(source.charAt(i)))
									return "";
					}
					else{
						if(source.charAt(i) != '@' &&
						   !Utility.isvalidChar(Character.toLowerCase(source.charAt(i))) && 
						   !Utility.isvalidChar(Character.toLowerCase(source.charAt(i)), extraChars) &&
						   !Character.isWhitespace(source.charAt(i)))
									return "";
					}
					
				}
				else{
					if(extraChars==null){
						if(!Utility.isvalidChar(Character.toLowerCase(source.charAt(i))) && 
					       !Character.isWhitespace(source.charAt(i)))
								return "";
					}
					else{
						if(!Utility.isvalidChar(Character.toLowerCase(source.charAt(i))) && 
						   !Utility.isvalidChar(Character.toLowerCase(source.charAt(i)), extraChars) &&
						   !Character.isWhitespace(source.charAt(i)))
										return "";
					}
				}
       		 	
       	 	}
			return null;
		}
		else if(filterType==TYPE_ALLOW_QUOTS){ //Igual que "TYPE_ALPHA_NUMERIC" mas acentos
			 for (int i = start; i < end; i++) {
				 if(extraChars==null){
					 if(!Utility.isLetterOrDigit(Character.toLowerCase(source.charAt(i))) && 
						!Utility.isQuot(source.charAt(i)) && 
						!Character.isWhitespace(source.charAt(i)))
						return "";
				 }
				 
			 }
			 return null;
		}
		else//Permite cualquier caracter
			return null;
		
		
    }
}
