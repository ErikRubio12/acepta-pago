package mx.com.bancoazteca.filters;

import android.text.Spanned;
import android.text.method.DigitsKeyListener;

public class PercentageFilter extends DigitsKeyListener {
	public PercentageFilter(boolean decimal) {
        super(false, decimal);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end,
            Spanned dest, int dstart, int dend) {
        CharSequence out = super.filter(source, start, end, dest, dstart, dend);
        if(dest.toString().length()>2)
        	return "";
        else{
        	 if(out==null && dest.toString().length()==2){
             	if(dest.toString().equals("10"))
             		return out;
             	else
             		return "";
             }
             else
             	return out;
        }
    }
}
