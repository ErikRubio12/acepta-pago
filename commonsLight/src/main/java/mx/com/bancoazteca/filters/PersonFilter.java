package mx.com.bancoazteca.filters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import mx.com.bancoazteca.beans.Person;

import android.widget.BaseAdapter;
import android.widget.Filter;

public class PersonFilter <T extends BaseAdapter ,P extends Person>  extends Filter{
	public static final String FILTER = "Filter";
	private List<P> list;
	private T adapter; 
	
	public PersonFilter(List<P> persons,T adapter) {
		// TODO Auto-generated constructor stub
		this.list=persons;
		this.adapter=adapter;
		
	}
	public void setList(ArrayList<P> list ){
		this.list=list;
	}
	@Override
	protected FilterResults performFiltering(CharSequence constraint) {
		// TODO Auto-generated method stub
		FilterResults results= new FilterResults();
		List<Person> nPersonList = new ArrayList<Person>();
		if (constraint != null && constraint.length() != 0) {
			for (Person p : list) {
				if (p.getCompleteName().startsWith(constraint.toString().toUpperCase(Locale.getDefault()))){
					nPersonList.add(p);
				}
			}
		}
		else{
			nPersonList= new ArrayList<Person>(list);
		}
		results.values = nPersonList;
		results.count = nPersonList.size();
		return results;
	}

	@Override
	protected void publishResults(CharSequence constraint, FilterResults results) {
		// TODO Auto-generated method stub
		
//		if (results.count == 0)
//	        adapter.notifyDataSetInvalidated();
//	    else {
		if(results != null && results.values!= null){
			list.clear();
	    	list.addAll((Collection<P>) results.values);
	        adapter.notifyDataSetChanged();
		}
	    	
//	    }
	}
	

}
