package mx.com.bancoazteca.social;


import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.Callback;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.facebook.widget.WebDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;
import mx.commons.R;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.media.ImageUpload;
import twitter4j.media.ImageUploadFactory;
import twitter4j.media.MediaProvider;


public class BaseSocialMapActivity extends BaseActivity {
	private static final String BASE_MAP_ACTIVITY_TAG = "BASE SOCIAL MAP ACTIVITY";			
	static final String TWITTER_CONSUMER_KEY="3DOepn57vqFHxxHC2OzG5OhLp";
	static final String TWITTER_CONSUMER_SECRET = "5m5wIPifJaAI1qE0VtVJ9Pppm0CmCmNxv5jBmFQTTYAMCx6JmZ";
	public final static String TWITPIC="e62dc379d124e98afefb0d561e01e25f";
	private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
//    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "login";
    public static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	protected static final int TWITTER_CODE = 30;
	private static String verifier;
	private UiLifecycleHelper uiHelper;
    private Twitter twitter;
	private RequestToken requestToken;
	//private PlusClient mGoogleApiClient;
	public static final int GOOGLE_SERVICES_CODE = 100;
	public static final int GOOGLE_COONNECTION_FAILED_CODE =200;
	public static final int GOOGLE_CONNECTED_CODE = 300;
	public static final int FB_POST_CODE = 400;
	private String image;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		this_=this;
		loadSocialNetwork();
	}
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	};
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		uiHelper.onStop();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	// Debe ejecutarse en background
	protected void checkTwitterRedirect() {
		// TODO Auto-generated method stub
		if (!isTwitterLoggedInAlready()) {
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Redirecting...");
			// oAuth verifier
			// Get the access token					
			AccessToken accessToken;
			try {
				if(twitter != null && requestToken!=null && verifier!= null){
					accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
					// Shared Preferences
					Editor e = getPreferences(MODE_PRIVATE).edit();

					// After getting access token, access token secret
					// store them in application preferences
					e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
//					e.putString(PREF_KEY_OAUTH_SECRET,accessToken.getTokenSecret());
					e.putString(getString(R.string.PREF_KY_OAUTH_SECRET),accessToken.getTokenSecret());
					// Store login status - true
					e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
					e.commit(); // save changes
				}
				
				
			} catch (TwitterException e1) {
				// TODO Auto-generated catch block
				Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, "> " + e1.getMessage());
				runOnUiThread(new Runnable() {
					public void run() {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.errorAuthTwitt));
						dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);

					}
				});						
			}
		}
	}
	protected boolean isTwitterLoggedInAlready() {
        // return twitter login status from Shared Preferences
        return getPreferences(MODE_PRIVATE).getBoolean(PREF_KEY_TWITTER_LOGIN, false);
    }
	public static String getVerifier() {
		return verifier;
	}
	public static void setVerifier(String verifier) {
		BaseSocialMapActivity.verifier = verifier;
	}
	protected void loadSocialNetwork(){
		//Para Gmail
		
		/*if(mGoogleApiClient==null){
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "GPlus loaded");
			mGoogleApiClient = new PlusClient.Builder(this, connectionCallBack, connectionFailedListener).setActions("http://schemas.google.com/AddActivity").setScopes(Scopes.PLUS_LOGIN).build();
		}*/
			
		//Para FaceBook
		if (uiHelper==null) {
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Facebook loaded");
			uiHelper= new UiLifecycleHelper(this, fbCallBack);
			uiHelper.onCreate(getIntent().getExtras());
			
		}		
		//Para twitter
		if (twitter==null) {
			ConfigurationBuilder config= new ConfigurationBuilder();
			config.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			config.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
			config.setMediaProviderAPIKey(TWITPIC);
			if (!isTwitterLoggedInAlready()){
				Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Twitter loaded");
				twitter = new TwitterFactory(config.build()).getInstance();
			}
			else{
				String access_token = getPreferences(MODE_PRIVATE).getString(PREF_KEY_OAUTH_TOKEN, "");
//				String access_token_secret = getPreferences(MODE_PRIVATE).getString(PREF_KEY_OAUTH_SECRET, "");
				String access_token_secret = getPreferences(MODE_PRIVATE).getString(getString(R.string.PREF_KY_OAUTH_SECRET), "");
				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				twitter = new TwitterFactory(config.build()).getInstance(accessToken);
			}
		}		
	}
	/*public void gplusConnect(){
		if(mGoogleApiClient!= null)
			mGoogleApiClient.connect();
	}
	public void gplusDisconnect(){
		if(mGoogleApiClient!= null)
			mGoogleApiClient.disconnect();
	}*/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		uiHelper.onActivityResult(requestCode, resultCode, data,fbDialogCallBack);
		switch (requestCode) {
		case FB_POST_CODE:
			if(resultCode==RESULT_OK){
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.fbPosted));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
			}
			break;
		case GOOGLE_CONNECTED_CODE:
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "GOOGLE_CONNECTED_CODE");
			if (resultCode==RESULT_OK) {
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.gplusPosted));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
			}
			break;
		}
	}
	
	//***********************      LOGIN      ************************************
	protected void loginTwitter(final View v,final Class <?> clase) {// Debe ejecutarse en background
		// TODO Auto-generated method stub
		if (!isTwitterLoggedInAlready()) {
			try {
				requestToken = twitter.getOAuthRequestToken(null);
				
				v.post(new Runnable() {
					public void run() {
						removeDialog(POPUP_MESSAGES_PROGRESS);
						Intent in=new Intent(BaseSocialMapActivity.this, clase);
						in.putExtra("url", requestToken.getAuthenticationURL());
						startActivityForResult(in,TWITTER_CODE);
					}
				});
				
			} catch (TwitterException e) {
				Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, e.getMessage());
				v.post(new Runnable() {
					public void run() {
						removeDialog(POPUP_MESSAGES_PROGRESS);
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.errorConnectingTwitter));
						dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
					}
				});
			}
		} 
		
			
		
	}
	protected void logoutFromTwitter() {
        // Clear the shared preferences
        Editor e = getPreferences(MODE_PRIVATE).edit();
        e.remove(PREF_KEY_OAUTH_TOKEN);
        e.remove(getString(R.string.PREF_KY_OAUTH_SECRET));
        e.remove(PREF_KEY_TWITTER_LOGIN);
        e.commit();     
    }

	protected void loginFb(View v) {
		// TODO Auto-generated method stub
		if (FacebookDialog.canPresentShareDialog(getApplicationContext(), FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
			FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this).
											setApplicationName("Acepta Pago").
											setLink("http://www.bancoazteca.com.mx").
											setCaption("Banco Azteca").
											setName("Acepta Pago").
											setDescription("Yo ya acepto pagos con tarjeta desde mi celular con Acepta Pago, Seguro, Rápido y Móvil").
											setPicture("http://ibravot.com/paz.jpg").
											setRequestCode(FB_POST_CODE).
											build();
											
			uiHelper.trackPendingDialogCall(shareDialog.present());
		}
		else{
			Bundle params= new Bundle();
			params.putString("app_id","218150135061280");
			params.putString("link", "http://www.bancoazteca.com.mx");
			params.putString("name", "Acepta Pago");
			params.putString("caption", "Banco Azteca");
			params.putString("description", "Yo ya acepto pagos con tarjeta desde mi celular con Acepta Pago, Seguro, Rápido y Móvil");
			params.putString("picture", "http://ibravot.com/paz.jpg");
			
			WebDialog feedDialog= new WebDialog.FeedDialogBuilder(this, getString(R.string.app_id), params).setOnCompleteListener(new WebDialog.OnCompleteListener() {
				public void onComplete(Bundle values, FacebookException error) {
					if (error==null) {
						final String postId=values.getString("post_id");
						if (postId != null) {
							Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Posted story, id: " + postId);
						}
						else{//Publish cancelled
							Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Posted cancelled");
						}
					}
					else if (error instanceof FacebookOperationCanceledException){
						//User clicked the x button
						Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Posted cancelled x button");
					}
					else{// Network error
						Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Network error");
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.errorConnectingFb));
						dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
					}
				}
			}).build();
			feedDialog.show();
		}
	}
	
	//***********************       POST	  ************************************	  
	protected void postTwitter(String text){
		if (isTwitterLoggedInAlready()) {
			new updateTwitterStatus().execute(text);
		}
		 
	}
	public void setImage(String image) {
		this.image = image;
	}
	private class updateTwitterStatus extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			popupMsg= R.string.updatingTwitter;
			popupTitle= R.string.popUpMessage;
			showDialog(POPUP_MESSAGES_PROGRESS);
		}
		protected String doInBackground(String... args) {
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Tweet Text" + "> " + args[0]);
			String statusTxt = args[0];
			StringBuffer bf= new StringBuffer("Yo ya acepto pagos con tarjeta desde mi celular con Acepta Pago, Seguro, Rápido y Móvil.\n");
			bf.append(statusTxt);
			image="imgPaz.jpg";
			if (image!= null && image.length()>0) {
				FileOutputStream fos = null;
				File file= FileManager.openFile(BaseSocialMapActivity.this, image);
				try {
					
					InputStream stream= getAssets().open(image);		
					byte[] data = new byte[2048];
			        int nbread = 0;
			        fos = new FileOutputStream(file);
			        while((nbread=stream.read(data))>-1){
			            fos.write(data,0,nbread);               
			        }
			        ConfigurationBuilder config= new ConfigurationBuilder();
					config.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
					config.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
					config.setOAuthAccessToken(getPreferences(MODE_PRIVATE).getString(PREF_KEY_OAUTH_TOKEN, ""));
					config.setOAuthAccessTokenSecret(getPreferences(MODE_PRIVATE).getString(getString(R.string.PREF_KY_OAUTH_SECRET), ""));
			        ImageUpload upload = new ImageUploadFactory(config.build()).getInstance(MediaProvider.TWITTER); //
			        String url = upload.upload(file,bf.toString());
			        Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "result tweet : " + url);
			       return url;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, e1.getMessage());
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, e.getMessage());
				}catch (Exception e) {
					// TODO: handle exception
					Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, e.getMessage());
				}
				finally{
					if(fos!= null){
						try {
							fos.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, e.getMessage());
						}
					}
				}
				return null;
			}
			else{
				StatusUpdate s= new StatusUpdate(bf.toString());
				try {				
					twitter4j.Status response=twitter.updateStatus(s);
					return response.getText();
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					Logger.log(Logger.EXCEPTION, BASE_MAP_ACTIVITY_TAG, e.getMessage());
				}
				return null;
			}
		}
		protected void onPostExecute(String param) {
			removeDialog(POPUP_MESSAGES_PROGRESS);
			 Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, param);
			if (param==null) {
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.twittingError));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);			}
			else{
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.tweetPosted));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
				hideTwitterScreen();
			}
		}	 
	}
	protected void  hideTwitterScreen(){
		
	}

	//*******************************************************************************
	//*********************** FACEBOOK LISTENERS ************************************
	private StatusCallback fbCallBack= new StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			// TODO Auto-generated method stub
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "State " + state.toString());
			if (session.isOpened()) {
				
			}
		}
	};
	private Callback fbDialogCallBack=new  FacebookDialog.Callback() {
		
		@Override
		public void onError(PendingCall pendingCall, Exception error, Bundle data) {
			// TODO Auto-generated method stub
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Error: " + error.toString());
		}

		@Override
		public void onComplete(PendingCall pendingCall, Bundle data) {
			// TODO Auto-generated method stub
			Logger.log(Logger.MESSAGE, BASE_MAP_ACTIVITY_TAG, "Succes");
		}
	};
}
