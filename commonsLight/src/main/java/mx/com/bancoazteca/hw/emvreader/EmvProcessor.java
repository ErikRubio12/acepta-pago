package mx.com.bancoazteca.hw.emvreader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.bancoazteca.hw.emvreader.backend.EMVProcessor.TransactionResponse;
import mx.com.bancoazteca.hw.emvreader.backend.EMVProcessorCallback;
import mx.com.bancoazteca.hw.emvreader.emv.PrivateTags;
import mx.com.bancoazteca.hw.emvreader.tlv.BerTlv;
import mx.com.bancoazteca.hw.emvreader.tlv.Tag;
import mx.com.bancoazteca.util.Logger;
import mx.commons.R;
import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class EmvProcessor implements EMVProcessorCallback {
	protected  String TAG_PROCESSOR_CALLBACK="Processor";
	protected Activity activity;
	protected SharedPreferences mPrefs;
	private static final String PREF_TRANSACTION_SEQUENCE = "transaction_sequence";
	public EmvProcessor(Activity ctx) {
		// TODO Auto-generated constructor stub
		this.activity=ctx;
		mPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
	}
	@Override
	public byte[] onPanCheckingRequest(byte[] data) {
		// TODO Auto-generated method stub
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"onPanCheckingRequest PAN checking\n");
        //Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,BerTlv.createList(data).toString());
        List<BerTlv> result = new ArrayList<BerTlv>();
        result.add(EMVProcessorHelper.createTlv(PrivateTags.TAG_CC_PAN_BASED_PROCESSING, "01"));
        //Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK," Return PAN checkin result\n");
        //Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,logData(result).toString());  
        //Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"Termino onPanCheckingRequest");
        return BerTlv.listToByteArray(result);   
        
	}

	@Override
	public byte[] onOnlineProcessingRequest(byte[] data) {
		// TODO Auto-generated method stub
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"onOnlineProcessingRequest");  
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,logData(BerTlv.createList(data)).toString());  
		

		final String EMV_OL_SUCCESS_TC = "0000"; // Transaction is permit by host (Tag 8A is 3030)
		//final String EMV_OL_SUCCESS_AAC = "0001"; // Transaction is denied
		//final String EMV_OL_FAILED = "0002"; // Failed to connect to server.
		//final String EMV_OL_INVALID = "0003"; // Invalid transaction result.

		// Simulate host answer
		List<BerTlv> result = new ArrayList<BerTlv>();                            
		result.add(EMVProcessorHelper.createTlv(PrivateTags.TAG_C7_ONLINE_AUTHORIZATION_PROCESSING_RESULT, EMV_OL_SUCCESS_TC));
		result.add(EMVProcessorHelper.createTlv(EMVTags.TAG_8A_AUTH_RESP_CODE, "3030"));
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK, "Return processing result extra tags\n");
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,logData(result).toString());
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"Termino onOnlineProcessingRequest");  
		return BerTlv.listToByteArray(result); 
	}

	@Override
	public void onConfirmOrReverseOnlineRequest(byte[] data) {
		// TODO Auto-generated method stub
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"onConfirmOrReverseOnlineRequest: "+data);  
		
		int transactionResult = EMVProcessorHelper.decodeInt(BerTlv.find(data, PrivateTags.TAG_CB_TRANSACTION_RESULT).getValue());
        boolean reverseOnline = true;
        
        // Depends of TAG_CB_TRANSACTION_RESULT the user can confirm or decline the transaction.
        if (transactionResult == TransactionResponse.AUTHORIZED || transactionResult == TransactionResponse.AUTHORIZED_SIGNATURE) {
            reverseOnline = false;
        }
        
        if (reverseOnline) {
        	Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK, "  � Reverse online\n");
        } else {
        	Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK, "  � Confirm online\n");
        }                                   
        //Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,logData(BerTlv.createList(data)).toString());
        Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"Termino onConfirmOrReverseOnlineRequest");  ;
	}

	@Override
	public byte[] onCardHolderSelectionRequest(byte[] data) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"onCardHolderSelectionRequest:");  
		Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"  � Process card holder selection\n");  
		//Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,logData(BerTlv.createList(data)).toString());
        
        int selectionIndex = -1; 
        
        // Find tag that contains list of candidates.
        List<BerTlv> listOfCandidates = null;              
        for (BerTlv tlv: BerTlv.createList(data)) {
            if (tlv.getTag().toIntValue() == PrivateTags.TAG_E1_LIST_OF_CANDIDATES) {            
                listOfCandidates = BerTlv.createList(tlv.getValue());
            }                                   
        }
        
        List<BerTlv> result = new ArrayList<BerTlv>();                            
        if (listOfCandidates != null) {
            List<String> listOfAppLabels = new ArrayList<String>();
            List<Integer> listOfAppIndexes = new ArrayList<Integer>();
            int applicationIndex = 0;
            for (BerTlv tlv: listOfCandidates) {
                if (tlv.getTag().toIntValue() ==  PrivateTags.TAG_E4_LIST_OF_CANDIDATES_ENTRY) {
                    Map<Tag, byte[]> appMap = BerTlv.createMap(tlv.getValue(),0);
                    
                    String appLabel = null;
                    
                    if (appMap.containsKey(new Tag(EMVTags.TAG_9F12_APP_PREFERRED_NAME))) {
                        // If no Issuer Code Table Index is available, or the Issuer Code Table Index has 
                        // a value != �01�, then ignore the Application Preferred Name
                        if (appMap.containsKey(new Tag(EMVTags.TAG_9F11_ISSUER_CODE_TABLE_INDEX))) {
                            int issuerCodeTable = EMVProcessorHelper.decodeInt(appMap.get(new Tag(EMVTags.TAG_9F11_ISSUER_CODE_TABLE_INDEX)));
                            
                            if (issuerCodeTable == 0x01) {
                                appLabel = EMVProcessorHelper.decodeAscii(appMap.get(new Tag(EMVTags.TAG_9F12_APP_PREFERRED_NAME)));
                            }
                        }
                    }
                    
                    if (appLabel == null) {
                        if (appMap.containsKey(new Tag(EMVTags.TAG_50_APP_LABEL))) {
                            appLabel = EMVProcessorHelper.decodeAscii(appMap.get(new Tag(EMVTags.TAG_50_APP_LABEL)));
                        }
                    }
                    
                    if (appLabel == null) {
                        if (appMap.containsKey(new Tag(EMVTags.TAG_84_DF_NAME))) {
                            appLabel = EMVProcessorHelper.decodeHex(appMap.get(new Tag(EMVTags.TAG_84_DF_NAME)));
                        }
                    }
                    
                    if (appLabel == null) {
                        if (appMap.containsKey(new Tag(PrivateTags.TAG_C2_APPLICATION_LABEL_DEFAULT))) {
                            appLabel = EMVProcessorHelper.decodeHex(appMap.get(new Tag(PrivateTags.TAG_C2_APPLICATION_LABEL_DEFAULT)));
                        }
                    }
                    
                    if (appLabel != null) {
                        listOfAppLabels.add(appLabel);
                        listOfAppIndexes.add(Integer.valueOf(applicationIndex));
                    }
                    applicationIndex++;
                }
            }
            
            if (listOfAppLabels != null && listOfAppLabels.size() > 0) {
                String[] applications = listOfAppLabels.toArray(new String[listOfAppLabels.size()]);                                  
                selectionIndex = UiHelper.selectDialogItemOnThread(activity, R.string.select_application, applications);                                                                  
                if (selectionIndex < 0) {
                	Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"    Selection is cancelled\n");  
                } else {
                    int resultIndex = listOfAppIndexes.get(selectionIndex);
                    Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"    Application: " + applications[selectionIndex] + "\n");
                    result.add(new BerTlv(PrivateTags.TAG_C5_SELECTED_INDEX, new byte[] { (byte)resultIndex })); // select
                }
            }
        }
        
        if (selectionIndex < 0) {                               
            result.add(new BerTlv(PrivateTags.TAG_C5_SELECTED_INDEX, new byte[] { (byte)0xFF })); // abort
        }
        
        // Add transaction date & time
        //result.add(EmvProcessorHelper.createTlv(EMVTags.TAG_9A_TRANSACTION_DATE, EmvProcessorHelper.encodeTransactionDate(Calendar.getInstance())));
        //result.add(EmvProcessorHelper.createTlv(EMVTags.TAG_9F21_TRANSACTION_TIME, EmvProcessorHelper.encodeTransactionTime(Calendar.getInstance())));
                                    
        Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"   � Return card holder selection result\n");
       // Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,logData(result).toString());
        Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,"Termina onCardHolderSelectionRequest:");  
        return BerTlv.listToByteArray(result);
	}
	public StringBuffer logData(List<BerTlv> data) {
		StringBuffer result= new StringBuffer();
		BerTlv tlv=null;
		for (int i = 0; i < data.size(); i++) {
			tlv=data.get(i);
            Tag tag = tlv.getTag();
            
            if (tag.isConstructed()) {
                String s = Integer.toHexString(tag.toIntValue()) + "(C): ";   
                Logger.log(Logger.MESSAGE, TAG_PROCESSOR_CALLBACK,  s + "\n");
                
                try {
                    List<BerTlv> list = BerTlv.createList(tlv.getValue());
                    result.append(":");
                    result.append(logData(list)) ;   
                } catch (Exception e) {
                	Logger.log(Logger.EXCEPTION, TAG_PROCESSOR_CALLBACK, s  + HexUtil.byteArrayToHexString(tlv.getValue()) + "\n" + e.getMessage());
                	break;
                	
                }
            } else {
                StringBuffer s = new StringBuffer();
                s.append(Integer.toHexString(tag.toIntValue()));
                s.append(",");
                s.append(tlv.getValueAsHexString());
                s.append( ",");
                s.append(HexUtil.byteArrayToHexString(tlv.getLengthBytes()));
                if(i<(data.size() -1))
                	s.append(":");
                result.append(s);
            }                       
        }
        return result;
    }
	public int getTransactionSequence() {
		int value = (mPrefs.getInt(PREF_TRANSACTION_SEQUENCE, 0) + 1) % 1000000;
		mPrefs.edit().putInt(PREF_TRANSACTION_SEQUENCE, value).commit();
		return value;        
	}
}
