package mx.com.bancoazteca.hw;

import java.util.Timer;
import java.util.TimerTask;

import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;
import mx.commons.R;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

public class LocationThread extends Thread implements LocationListener{
	public static final int LOCATION_CHANGED = 0;
	public static final int PROVIDER_DISABLE = 1;
	public static final int PROVIDER_ENABLE = 2;
	public static final int STATUS_CHANGED = 3;
	public static final int GPS_DISABLE = 4;
	public final static String LOCATION="location";
	public final static String  GPS=LocationManager.GPS_PROVIDER;
	public final static String  NETWORK=LocationManager.NETWORK_PROVIDER;
	protected static LocationManager manager=null;
	protected LocationListener locationListener;
	protected Context appContext;
	private String provider=null;
	private LocationCallBackParameters params=null;
	protected MessageListener<Message,LocationCallBackParameters> messageListener=null;
	protected Criteria criteria=null;
	protected final static int 	SEG=1000;
	public final static int DEFAULT_TIME_OUT=60;
	private static final String LOCATION_THREAD = "LocationThread";
	private int timeOut=DEFAULT_TIME_OUT*SEG;
	private Message message= new Message();
	private boolean located=false;
	private boolean isOneShoot=true;
	private Timer scheduler;
	private SchedulerTask task;
	private Location lastLocation;
	public LocationThread(LocationListener listener, Context context, String provider) {
		// TODO Auto-generated constructor stub
		this.locationListener=listener;
		this.appContext=context;
		this.provider=provider;
	}
	
	public LocationThread(MessageListener<Message,LocationCallBackParameters> listener,Context context,String provider) {
		// TODO Auto-generated constructor stub
		this.messageListener=listener;
		this.appContext=context;
		this.provider=provider;
		locationListener=this;
	}
	public LocationThread(LocationListener listener, Context context, Criteria criteria,String provider) {
		// TODO Auto-generated constructor stub
		this.locationListener=listener;
		this.appContext=context;
		this.criteria=criteria;
		this.provider=provider;
	}
	
	public LocationThread(MessageListener<Message,LocationCallBackParameters> listener,Context context,Criteria criteria,String provider) {
		// TODO Auto-generated constructor stub
		this.messageListener=listener;
		this.appContext=context;
		this.criteria=criteria;
		locationListener=this;
		this.provider=provider;
	}
	public void setOneShoot(boolean isOneShoot) {
		this.isOneShoot = isOneShoot;
	}

	public boolean isOneShoot() {
		return isOneShoot;
	}

	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Looper.prepare();
		if(criteria==null){
			if(startLocation())
				scheduleTimer();
			else{
				sendParams(null, new Message(GPS_DISABLE,R.string.gpsDisabled));
			}
		}
		else{
			if(startLocationByCriteria())
				scheduleTimer();
		}
		Looper.loop();
	}
	private void scheduleTimer() {
		// TODO Auto-generated method stub
		if(isOneShoot){
			task=new SchedulerTask();
			scheduler = new Timer();
			scheduler.schedule(task, timeOut);
		}
	}

	public void stopGps(){
		located=true;
		manager.removeUpdates(locationListener);
		if(scheduler != null)
			scheduler.cancel();
	}
	protected void stopLocation(){
		manager.removeUpdates(locationListener);
		scheduler.cancel();
	}
	public Location getLastLocation(){
		lastLocation= manager.getLastKnownLocation(provider);
		return lastLocation;
	}
	protected boolean startLocation(){
		
		if(manager== null)
			manager=Utility.getLocationManager(appContext);
		if(!manager.isProviderEnabled(provider))
			return false;
		else{
			manager.requestLocationUpdates(provider, 0, 0, locationListener);
			return true;
		}
	}
	protected boolean startLocationByCriteria(){
		
		if(manager== null)
			manager=Utility.getLocationManager(appContext);
		
		provider=manager.getBestProvider(criteria, true);
		if(provider!=null && provider.length()>0){
			if(!manager.isProviderEnabled(provider))
				return false;
			else{
				manager.requestLocationUpdates(provider, 0, 0, locationListener);
				return true;
			}
		}
			
		else
			return false;
		
	}
	public static void stopLocation(LocationListener locationListener){
		if(manager != null){
			manager.removeUpdates(locationListener);
		}
	}
	private synchronized void sendParams(LocationCallBackParameters params, Message mesg) {
		// TODO Auto-generated method stub
		
		if(messageListener==null){
			switch (mesg.getType()) {
			case LOCATION_CHANGED:
					locationListener.onLocationChanged(params.getLocation());
				break;
			case PROVIDER_DISABLE:
				locationListener.onProviderDisabled(params.getProvider());
			break;
			case PROVIDER_ENABLE:
				locationListener.onProviderEnabled(params.getProvider());
			break;

			default:
				break;
			}
			
		}
		else
			messageListener.sendMessage(null,mesg, params);
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut*SEG;
	}

	public int getTimeOut() {
		return timeOut/SEG;
	}
	//**************** MESSAGE LISTENERS******************
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_THREAD, "En onLocationChanged: " + location);
		if(!located){
			located=isOneShoot ? !located : false;
			stopLocation();
			params= new LocationCallBackParameters();
			params.setLocation(location);
			message.setTextMessage(R.string.locationReceived);
			message.setType(LOCATION_CHANGED);
			sendParams(params, message);
		}
	}

	
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_THREAD, "En onProviderDisabled: " + provider);
		params= new LocationCallBackParameters();
		params.setProvider(provider);
		message.setTextMessage(R.string.gpsTurnedOff);
		message.setType(PROVIDER_DISABLE);
		sendParams(params, message);
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_THREAD, "En onProviderEnabled: "+ provider);
		params= new LocationCallBackParameters();
		params.setProvider(provider);
		message.setTextMessage(R.string.gpsTurnedOn);
		message.setType(PROVIDER_ENABLE);
		sendParams(params, message);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_THREAD, "En onStatusChanged: "+ provider);
		params= new LocationCallBackParameters();
		params.setProvider(provider);
		params.setParams(extras);
		params.setStatus(status);
		message.setTextMessage(R.string.gpsStatusChanged);
		message.setType(STATUS_CHANGED);
		sendParams(params, message);
	}

	

	//*************+INNER CALSS*************************
	private final class SchedulerTask extends TimerTask{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			onLocationChanged(new Location(provider));
		}
		
	}
}
