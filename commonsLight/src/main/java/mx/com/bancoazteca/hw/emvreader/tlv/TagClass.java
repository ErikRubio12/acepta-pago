package mx.com.bancoazteca.hw.emvreader.tlv;

/**
 * Tag class type
 */
public enum TagClass {
    UNIVERSAL, 
    APPLICATION, 
    CONTEXT_SPECIFIC, 
    PRIVATE
}