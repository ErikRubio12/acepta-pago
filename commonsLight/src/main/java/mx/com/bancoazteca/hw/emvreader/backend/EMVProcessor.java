package mx.com.bancoazteca.hw.emvreader.backend;

import android.util.Log;

import com.datecs.audioreader.AudioReader;
import com.datecs.audioreader.AudioReaderException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import mx.com.bancoazteca.hw.emvreader.emv.PrivateTags;
import mx.com.bancoazteca.hw.emvreader.tlv.BerTlv;
import mx.com.bancoazteca.hw.emvreader.tlv.Tag;
import mx.com.bancoazteca.hw.emvreader.tlv.TagTwo;
import mx.com.bancoazteca.util.Logger;

/**
 * This class that manage EMV processing.
 */
public class EMVProcessor {

    // The private module
    public static byte[] privateKeyModule = {
        (byte)0x77, (byte)0x6C, (byte)0xD1, (byte)0xEF, (byte)0x62, (byte)0xE9,
        (byte)0x8D, (byte)0x8F, (byte)0x19, (byte)0xB3, (byte)0x4F, (byte)0xDA,
        (byte)0xD2, (byte)0x41, (byte)0x1C, (byte)0x7A, (byte)0xC7, (byte)0xE8,
        (byte)0xD9, (byte)0x5D, (byte)0x10, (byte)0xD4, (byte)0xF4, (byte)0xA5,
        (byte)0x68, (byte)0x26, (byte)0xAA, (byte)0x2C, (byte)0xCE, (byte)0x8E,
        (byte)0xA3, (byte)0x3C, (byte)0x05, (byte)0xEA, (byte)0x1B, (byte)0x81,
        (byte)0x6A, (byte)0x39, (byte)0xDE, (byte)0x34, (byte)0x7B, (byte)0x23,
        (byte)0xC5, (byte)0xE6, (byte)0x25, (byte)0x50, (byte)0x73, (byte)0x55,
        (byte)0xD8, (byte)0x3F, (byte)0x3F, (byte)0x33, (byte)0x2E, (byte)0x5B,
        (byte)0x28, (byte)0xB9, (byte)0xFE, (byte)0x4C, (byte)0x40, (byte)0xAA,
        (byte)0xD2, (byte)0x40, (byte)0x1A, (byte)0xE4, (byte)0x37, (byte)0x85,
        (byte)0x33, (byte)0x87, (byte)0x32, (byte)0x46, (byte)0xAE, (byte)0xCE,
        (byte)0x54, (byte)0x03, (byte)0xD2, (byte)0xAD, (byte)0x8A, (byte)0xE0,
        (byte)0xAF, (byte)0x27, (byte)0xC6, (byte)0x03, (byte)0x7C, (byte)0xCF,
        (byte)0x78, (byte)0x96, (byte)0x17, (byte)0xF5, (byte)0x5A, (byte)0x2D,
        (byte)0x38, (byte)0x94, (byte)0x28, (byte)0x2B, (byte)0x6F, (byte)0xD9,
        (byte)0xEC, (byte)0xA0, (byte)0x7C, (byte)0x5F, (byte)0xDE, (byte)0x20,
        (byte)0xE8, (byte)0x2F, (byte)0xA6, (byte)0x51, (byte)0x07, (byte)0xCD,
        (byte)0xD5, (byte)0xB0, (byte)0xC8, (byte)0xB0, (byte)0x44, (byte)0xB0,
        (byte)0x4A, (byte)0xE2, (byte)0x5B, (byte)0xD7, (byte)0xC4, (byte)0x99,
        (byte)0x22, (byte)0x98, (byte)0xAC, (byte)0x95, (byte)0x75, (byte)0x99,
        (byte)0x5D, (byte)0xEB, (byte)0xBB, (byte)0x97, (byte)0x22, (byte)0x82,
        (byte)0xC0, (byte)0xF4, (byte)0x6A, (byte)0x4E, (byte)0x0E, (byte)0x74,
        (byte)0xE3, (byte)0xA8, (byte)0x11, (byte)0x17, (byte)0xBA, (byte)0x0F,
        (byte)0xD1, (byte)0x47, (byte)0x7E, (byte)0x38, (byte)0x96, (byte)0xA0,
        (byte)0xDA, (byte)0x5F, (byte)0x99, (byte)0x1B, (byte)0x6B, (byte)0x68,
        (byte)0x76, (byte)0x46, (byte)0x9C, (byte)0xED, (byte)0x6A, (byte)0x5F,
        (byte)0xE3, (byte)0x3A, (byte)0xA0, (byte)0x03, (byte)0x5D, (byte)0xBC,
        (byte)0x27, (byte)0x2B, (byte)0x45, (byte)0xC1, (byte)0x29, (byte)0xBA,
        (byte)0x6D, (byte)0x6B, (byte)0xF0, (byte)0xBF, (byte)0x8A, (byte)0x93,
        (byte)0xBB, (byte)0x9C, (byte)0x34, (byte)0xB6, (byte)0xB1, (byte)0xC9,
        (byte)0x33, (byte)0xC8, (byte)0x3B, (byte)0x53, (byte)0xE2, (byte)0xE7,
        (byte)0x40, (byte)0xF7, (byte)0x30, (byte)0x74, (byte)0x98, (byte)0xF1,
        (byte)0x7D, (byte)0xB5, (byte)0x60, (byte)0x7C, (byte)0x55, (byte)0x28,
        (byte)0x73, (byte)0x19, (byte)0x5C, (byte)0x74, (byte)0x22, (byte)0xB7,
        (byte)0xB8, (byte)0x65, (byte)0xFC, (byte)0xA1, (byte)0xBA, (byte)0x3A,
        (byte)0xC3, (byte)0x4D, (byte)0x70, (byte)0x75, (byte)0xFE, (byte)0x95,
        (byte)0x6A, (byte)0x96, (byte)0x0F, (byte)0xC2, (byte)0x75, (byte)0x86,
        (byte)0xB1, (byte)0x26, (byte)0x00, (byte)0x07, (byte)0x20, (byte)0x02,
        (byte)0x35, (byte)0x50, (byte)0x23, (byte)0xA0, (byte)0x94, (byte)0x47,
        (byte)0xC7, (byte)0x1D, (byte)0x4F, (byte)0x72, (byte)0x77, (byte)0xBE,
        (byte)0xAA, (byte)0x6B, (byte)0xAA, (byte)0xFB, (byte)0xDC, (byte)0x28,
        (byte)0xB6, (byte)0x48, (byte)0xE1, (byte)0xC7
    };

    // The private exponent
    public static byte[] privateKeyExponent = {
        (byte)0x50, (byte)0x9B, (byte)0xF7, (byte)0x28, (byte)0x2A, (byte)0x0F,
        (byte)0x93, (byte)0x29, (byte)0x60, (byte)0x23, (byte)0x94, (byte)0x67,
        (byte)0x13, (byte)0x3C, (byte)0x37, (byte)0xC8, (byte)0xF8, (byte)0x5E,
        (byte)0xC7, (byte)0x38, (byte)0xF6, (byte)0x3F, (byte)0x87, (byte)0xD2,
        (byte)0x8D, (byte)0xF6, (byte)0x6B, (byte)0x2F, (byte)0x4B, (byte)0x4D,
        (byte)0x24, (byte)0x09, (byte)0x43, (byte)0xC4, (byte)0xBD, (byte)0x44,
        (byte)0x21, (byte)0x3B, (byte)0x66, (byte)0x2C, (byte)0xEE, (byte)0x61,
        (byte)0x3B, (byte)0x17, (byte)0x19, (byte)0x60, (byte)0xB0, (byte)0x38,
        (byte)0xE5, (byte)0x79, (byte)0xEB, (byte)0x62, (byte)0xD4, (byte)0x8B,
        (byte)0x5B, (byte)0x76, (byte)0x0F, (byte)0x9B, (byte)0xD0, (byte)0x9A,
        (byte)0x7C, (byte)0xC8, (byte)0x20, (byte)0x5E, (byte)0xA2, (byte)0xCB,
        (byte)0x19, (byte)0xF8, (byte)0xCB, (byte)0x8A, (byte)0xC2, (byte)0x3B,
        (byte)0x2A, (byte)0xA2, (byte)0x59, (byte)0xF6, (byte)0x21, (byte)0xA3,
        (byte)0x7F, (byte)0x16, (byte)0xCD, (byte)0xA5, (byte)0x54, (byte)0xFD,
        (byte)0x85, (byte)0x5B, (byte)0x6A, (byte)0x58, (byte)0x85, (byte)0xC1,
        (byte)0xB8, (byte)0x4A, (byte)0xE8, (byte)0xC2, (byte)0x49, (byte)0x01,
        (byte)0x43, (byte)0xA3, (byte)0x1F, (byte)0xD0, (byte)0x65, (byte)0xD2,
        (byte)0xB8, (byte)0x66, (byte)0x51, (byte)0x50, (byte)0xA8, (byte)0x7F,
        (byte)0xDB, (byte)0x19, (byte)0x34, (byte)0x9D, (byte)0x26, (byte)0x00,
        (byte)0x08, (byte)0xCB, (byte)0xB9, (byte)0x4A, (byte)0x6E, (byte)0xBD,
        (byte)0x1E, (byte)0x89, (byte)0x07, (byte)0x14, (byte)0xEB, (byte)0x07,
        (byte)0xD6, (byte)0x48, (byte)0x6D, (byte)0x11, (byte)0xA8, (byte)0x14,
        (byte)0xC3, (byte)0xB3, (byte)0x14, (byte)0x2A, (byte)0x63, (byte)0x63,
        (byte)0x51, (byte)0x0C, (byte)0xF5, (byte)0x93, (byte)0x1C, (byte)0x94,
        (byte)0x73, (byte)0xD3, (byte)0x7B, (byte)0x21, (byte)0xA3, (byte)0xE7,
        (byte)0x57, (byte)0x22, (byte)0xC1, (byte)0x60, (byte)0x62, (byte)0xB9,
        (byte)0xBA, (byte)0x2E, (byte)0x40, (byte)0xFC, (byte)0xF3, (byte)0x35,
        (byte)0x1F, (byte)0xEC, (byte)0x7C, (byte)0x10, (byte)0xE7, (byte)0x27,
        (byte)0x13, (byte)0x1C, (byte)0x32, (byte)0x4C, (byte)0xA1, (byte)0x58,
        (byte)0x85, (byte)0x5D, (byte)0xEB, (byte)0x0D, (byte)0xFC, (byte)0x91,
        (byte)0x5B, (byte)0xD4, (byte)0xFF, (byte)0x46, (byte)0xF4, (byte)0x8F,
        (byte)0x2A, (byte)0x98, (byte)0x05, (byte)0x1D, (byte)0xE6, (byte)0xAE,
        (byte)0xCC, (byte)0x24, (byte)0xFE, (byte)0xD7, (byte)0xCC, (byte)0x40,
        (byte)0xE7, (byte)0xF9, (byte)0x22, (byte)0xD0, (byte)0x02, (byte)0x29,
        (byte)0xA5, (byte)0x65, (byte)0x7A, (byte)0x54, (byte)0x9A, (byte)0xDB,
        (byte)0xCC, (byte)0x4C, (byte)0x83, (byte)0x59, (byte)0xD6, (byte)0xDB,
        (byte)0xE8, (byte)0x8C, (byte)0xD9, (byte)0xE1, (byte)0x75, (byte)0x57,
        (byte)0x43, (byte)0xAB, (byte)0xDC, (byte)0x66, (byte)0x80, (byte)0xA1,
        (byte)0x9D, (byte)0x9B, (byte)0x5B, (byte)0xBC, (byte)0xB1, (byte)0x0C,
        (byte)0x84, (byte)0x30, (byte)0xDF, (byte)0x91, (byte)0x48, (byte)0xA4,
        (byte)0xA3, (byte)0x5D, (byte)0xF9, (byte)0xEF, (byte)0x9F, (byte)0xFF,
        (byte)0x28, (byte)0xA5, (byte)0xA9, (byte)0x28, (byte)0x00, (byte)0xFE,
        (byte)0xDA, (byte)0xD0, (byte)0x4A, (byte)0xE1
    };

    // The reader instance
    private final AudioReader mReader;

    // Session key used to encrypt/decrypt packet data
    private final byte[] mSessionKey;

    //EGR

    public static ArrayList<BerTlv> mArrayBerTlv = new ArrayList<>();

//    private ArrayListMultimap<Tag,byte[]> map1;
//    private ArrayListMultimap<Tag,byte[]> map2;

    /**
     * Constructs a new instance of this class from given {@link AudioReader} object.
     *
     * @param reader instance of {@link AudioReader} object.
     *
     * @throws AudioReaderException if a reader error occurs.
     * @throws IOException if an I/O error occurs.
     */
    public EMVProcessor(AudioReader reader) throws AudioReaderException, IOException {
        this.mReader = reader;
        this.mSessionKey = loadSessionKey();
    }

    // Helper method for converting byte array to int
    private static int byteArrayToInt(byte[] buf, int offset, int length) {
        int result = 0;

        while ((length--) > 0) {
            result <<= 8; // Shift value with 8 bits.
            result += buf[offset++] & 0xff;
        }

        return result;
    }

    // Helper method for extracting sub array from array
    private static byte[] byteArrayToSubArray(byte[] buf, int offset, int length) {
        byte[] result = new byte[length];
        System.arraycopy(buf, offset, result, 0, length);
        return result;
    }

    // Helper method to convert byte array to hex string
    private static final String byteArrayToHexString(byte[] data, int offset, int length) {
        final char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char[] buf = new char[length * 2];
        int offs = 0;

        for (int i = 0; i < length; i++) {
            buf[offs++] = hex[(data[offset + i] >> 4) & 0xf];
            buf[offs++] = hex[(data[offset + i] >> 0) & 0xf];
        }

        return new String(buf, 0, offs);
    }

    // Helper method to convert byte array to hex string
    private static final String byteArrayToHexString(byte[] data) {
        return byteArrayToHexString(data, 0, data.length);
    }

    // Helper method to do the encryption
    private static final byte[] encryptWithAES(byte[] keyValue, byte[] data) {
        try {
            final SecretKey key = new SecretKeySpec(keyValue, "AES");
            final IvParameterSpec iv = new IvParameterSpec(new byte[16]);
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            final byte[] cipherData = cipher.doFinal(data);
            return cipherData;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Helper method to do data decryption
    private static final byte[] decryptWithAES(byte[] keyValue, byte[] data) {
        try {
            final SecretKey key = new SecretKeySpec(keyValue, "AES");
            final IvParameterSpec iv = new IvParameterSpec(new byte[16]);
            final Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            final byte[] cipherData = cipher.doFinal(data);
            return cipherData;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Helper method to decrypt content of RSA block
    private static final byte[] decryptRSABlock(byte[] data) {
        byte[] result = null;

        try {
            BigInteger modulus = new BigInteger(1, privateKeyModule);
            BigInteger exponent = new BigInteger(1, privateKeyExponent);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            Cipher cipher = Cipher.getInstance("RSA");

            RSAPrivateKeySpec privateSpec = new RSAPrivateKeySpec(modulus, exponent);
            PrivateKey privateKey = factory.generatePrivate(privateSpec);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            result = cipher.doFinal(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    // Helper class that represent 
    private static class Message {
        public final boolean encrypted;
        //public final boolean rfu;
        public final byte[] data;

        public Message(boolean encrypted, boolean rfu, byte[] data) {
            this.encrypted = encrypted;
            //this.rfu = rfu;
            this.data = data;
        }

        public byte[] toArray() {
            byte[] buffer = new byte[data.length + 2];
            buffer[0] = (byte)((encrypted ? 0x80 : 0) | (data.length >> 8));
            buffer[1] = (byte)(data.length & 0xFF);
            System.arraycopy(data, 0, buffer, 2, data.length);
            return buffer;
        }

        public static Message parse(byte[] data) {
            if (data.length < 2 || (((data[0] & 0x3F) << 8) + (data[1] & 0xff)) != (data.length - 2)) {
                throw new IllegalArgumentException("Invalid data content length");
            }

            boolean encrypted = (data[0] & 0x80) != 0;
            byte[] buffer = new byte[data.length - 2];
            System.arraycopy(data, 2, buffer, 0, buffer.length);
            return new Message(encrypted, false, buffer);
        }
    }

    private synchronized byte[] processMessage(byte[] data) throws AudioReaderException, IOException {
        final boolean ENCRYPTED_INTERFACE = true;
        Message input = null;

        if (ENCRYPTED_INTERFACE) {
            final ByteBuffer buffer = ByteBuffer.allocate(data.length + 2);
            final int crc = AudioReader.crcccit(data, 0, data.length);
            buffer.put(data);
            buffer.put((byte)(crc >> 8));
            buffer.put((byte)(crc));

            final byte[] enc = encryptWithAES(mSessionKey, buffer.array());
            if (enc == null) {
                throw new RuntimeException("Error in data encryption");
            }

            input = new Message(true, false, enc);
        } else {
            input = new Message(false, false, data);
        }

        final byte[] result = mReader.processMessage(input.toArray());
        final Message output = Message.parse(result);

        // Check result
        if (output.encrypted != input.encrypted) {
            throw new RuntimeException("Invalid response encryption mode");
        }

        if (output.encrypted) {
            final byte[] dec = decryptWithAES(mSessionKey, output.data);

            // Check decrypted packet
            int length = ((dec[2] & 0xff) << 8) + (dec[3] & 0xff);
            if (dec.length < (length + 4 + 2)) {
                throw new RuntimeException("Invalid content length");
            }

            int calCrc = AudioReader.crcccit(dec, 0, length + 4);
            int rcvCrc = ((dec[length + 4] & 0xff) << 8) + (dec[length + 5] & 0xff);
            if (calCrc != rcvCrc) {
                throw new RuntimeException("Invalid content checksum");
            }

            final byte[] buffer = new byte[length + 4];
            System.arraycopy(dec, 0, buffer, 0, buffer.length);
            return buffer;
        } else {
            return output.data;
        }
    }

    private byte[] loadSessionKey() throws AudioReaderException, IOException {
        final byte[] buffer = mReader.enterProtectedMode();
        if (byteArrayToInt(buffer, 0, 2) != 0) {
            throw new RuntimeException("Error on session key loading");
        }

        final int rsaOffset = 2 + 16 + 8;
        final byte[] rsa = byteArrayToSubArray(buffer, rsaOffset, 256);
        final byte[] dec = decryptRSABlock(rsa);
        if (dec == null) {
            throw new RuntimeException("Error in RSA block decryption");
        }

        byte[] key = new byte[16];
        System.arraycopy(dec, 207, key, 0, key.length);
        return key;
    }

    /**
     * The class that represents response from data transmit between backend and the reader.
     */
    public static class Response {
        /**
         * The command identifier
         */
        public final int cid;
        /**
         * Reserved for future use.
         */
        public final int reserved;
        /**
         * Response data.
         */
        public final byte[] data;

        private Response(int cid, int reserved, byte[] data) {
            this.cid = cid;
            this.reserved = reserved;
            this.data = data;
        }

        static Response parse(byte[] input) {
            int cid = byteArrayToInt(input, 0, 1);
            int reserved = byteArrayToInt(input, 1, 1);
            int length =  byteArrayToInt(input, 2, 2);
            byte[] data = byteArrayToSubArray(input, 4, length);
            return new Response(cid, reserved, data);
        }

        @Override
        public String toString() {
            return "Response [cid=" + cid + ", data(" + data.length + ")=" + byteArrayToHexString(data) + "]";
        }
    }

    /**
     * Send command to audio reader.
     *
     * @param check check the response for valid content.
     * @param cid the command identifier.
     * @param data the message data buffer.
     * @param offset the offset in buffer.
     * @param length the data length.
     *
     * @return the result data.
     *
     * @throws IOException if an I/O error occurs.
     */
    private synchronized Response transmit(boolean check, int cid, byte[] data, int offset, int length) throws AudioReaderException,
            IOException {
        ByteArrayOutputStream o = new ByteArrayOutputStream();
        o.write(cid);
        o.write(0);
        o.write(length >> 8);
        o.write(length);
        o.write(data, offset, length);

        byte[] input = o.toByteArray();
        byte[] output = processMessage(input);
        Response result = Response.parse(output);

        if (check) {
            if (result.cid != cid) {
                throw new RuntimeException("Response with wrong cid: " + result.cid + " (must be " + cid + ")");
            }
        }

        return result;
    }

    /**
     * Send command to audio reader.
     *
     * @param cid the command identifier.
     * @param data the message data.
     *
     * @return the result data.
     *
     * @throws IOException if an I/O error occurs.
     */
    private synchronized Response transmit(boolean check, int cid, byte[] data) throws AudioReaderException,
            IOException {
        return transmit(check, cid, data, 0, data.length);
    }

    /**
     * Send command to audio reader.
     *
     * @param check check the response for valid content.
     * @param cid the command identifier.
     *
     * @return the result data.
     *
     * @throws IOException if an I/O error occurs.
     */
    private synchronized Response transmit(boolean check, int cid) throws AudioReaderException, IOException {
        return transmit(check, cid, new byte[] { });
    }

    /**
     * The class that represents reset parameters response data.
     */
    public static class ResetParametersResponse {
        /**
         * Processing result.
         */
        public final int result;
        /**
         * Reader state
         */
        public final int state;
        /**
         * Max configuration message size.
         */
        public final int maxSize;

        private ResetParametersResponse(byte[] data) {
            this.result = byteArrayToInt(data, 0, 2);
            this.state = byteArrayToInt(data, 2, 1);
            this.maxSize = byteArrayToInt(data, 3, 2);
        }

        public static ResetParametersResponse parse(byte[] data) {
            return new ResetParametersResponse(data);
        }

        @Override
        public String toString() {
            return "ResetParametersResponse [result=" + result + ", state=" + state + ", maxSize=" + maxSize + "]";
        }
    }

    /**
     * Reset the reader into state BORN and eliminate all stored parameters.
     * @return the reset parameters response data.
     * @throws AudioReaderException if a reader error occurs.
     * @throws IOException IOException if an I/O error occurs.
     */
    public synchronized ResetParametersResponse resetParameters() throws AudioReaderException, IOException {
        Response response = transmit(true, 0x01);
        ResetParametersResponse result = ResetParametersResponse.parse(response.data);
        return result;
    }

    /**
     * The class that represents load parameters response data.
     */
    public static class LoadParametersResponse {
        /**
         * Processing result.
         */
        public int result;
        /**
         * Reader state
         */
        public int state;
        /**
         * The accumulated amount of data already received.
         */
        public int dataReceived;

        @Override
        public String toString() {
            return String.format(Locale.US, "LoadParametersResponse [result=0x%X, state=0x%X, dataReceived=%d]", result, state, dataReceived);
        }
    }

    /**
     * Load the parameters as defined in documentation into the Reader.
     *
     * @param data the parameters data.
     * @return the load parameters response data.
     * @throws AudioReaderException if a reader error occurs.
     * @throws IOException IOException if an I/O error occurs.
     */
    private synchronized LoadParametersResponse loadParametersChunk(int msgControl, int configSize, int offset, byte[] data) throws AudioReaderException, IOException {
        ByteArrayOutputStream o = new ByteArrayOutputStream();

        o.write(msgControl);
        o.write(configSize >> 8);
        o.write(configSize);
        o.write(offset >> 8);
        o.write(offset);
        o.write(data);

        Response response = transmit(true, 0x02, o.toByteArray());
        LoadParametersResponse result = new LoadParametersResponse();
        result.result = byteArrayToInt(response.data, 0, 2);
        result.state = byteArrayToInt(response.data, 2, 1);
        result.dataReceived = byteArrayToInt(response.data, 3, 2);
        return result;
    }

    /**
     * Load the parameters as defined in documentation into the Reader.
     *
     * @param data the configuration parameters data in BER TLV data structure.
     *
     * @return the load parameters response data.
     * @throws AudioReaderException if a reader error occurs.
     * @throws IOException IOException if an I/O error occurs.
     */
    public synchronized LoadParametersResponse loadParameters(byte[] data, int chunkSize) throws AudioReaderException, IOException {
        LoadParametersResponse result = null;
        int offset = 0;

        do {
            int length = Math.min(data.length - offset, chunkSize);
            byte[] chunk = byteArrayToSubArray(data, offset, length);
            int msgControl = ((data.length - offset) <= chunkSize) ? 0x01 : 0x02;
            result = loadParametersChunk(msgControl, data.length, offset, chunk);
            offset = result.dataReceived;
        } while (result.result == 0 && offset < data.length);

        return result;
    }

    /**
     * The class that represents get configuration parameters response data.
     */
    public static class ConfigrationParametersResponse {
        /**
         * Processing result.
         */
        public int result;
        /**
         * Reader state
         */
        public int state;
        /**
         * Configuration Data Set Version Number.
         */
        public int version = 0xffffffff;

        /**
         * Max Configuration Message Size.
         */
        public int maxSize = 0xffffffff;

        @Override
        public String toString() {
            return "ConfigrationParameters [result=" + result + ", state=" + state + ", version=" + version + ", maxSize=" + maxSize + "]";
        }
    }

    public synchronized ConfigrationParametersResponse getConfigrationParameters() throws AudioReaderException, IOException {
        byte[] response = mReader.getConfigurationParameters();
        List<BerTlv> tlvs = BerTlv.createList(response);

        ConfigrationParametersResponse result = new ConfigrationParametersResponse();
        for (BerTlv tlv: tlvs) {
            if (tlv.getTag().toIntValue() == PrivateTags.TAG_C1_PROCESSING_RESULT) {
                result.result = byteArrayToInt(tlv.getValue(), 0, tlv.getValue().length);
            }
            if (tlv.getTag().toIntValue() == PrivateTags.TAG_C6_READER_STATE) {
                result.state = byteArrayToInt(tlv.getValue(), 0, tlv.getValue().length);
            }
            if (tlv.getTag().toIntValue() == PrivateTags.TAG_DF10_CONFIGURATION_DATA_SET_VERSION_NUMBER) {
                result.version = byteArrayToInt(tlv.getValue(), 0, tlv.getValue().length);
            }
            if (tlv.getTag().toIntValue() == PrivateTags.TAG_DF11_MAX_CONFIGURATION_MESSAGE_SIZE) {
                result.maxSize = byteArrayToInt(tlv.getValue(), 0, tlv.getValue().length);
            }
        }

        return result;
    }

    /**
     * A class that represents final transaction response.
     */
    public static class TransactionResponse {
        public static final int DECLINED = 0x00;

        public static final int AUTHORIZED = 0x01;

        public static final int ABORTED = 0x02;

        public static final int NOT_ACCEPTED = 0x03;

        public static final int AUTHORIZED_SIGNATURE = 0x81;

        /**
         * The complete list of transaction tags.
         */
        public final Map<Tag, byte[]> tags;

        // Constructs a new instance of this class
        TransactionResponse(Map<Tag, byte[]> tags) {
            this.tags = tags;
        }
       
//        public byte[] getValue(Tag tag) {
//            if (tags.containsKey(tag)) {
//                return tags.get(tag);
//            }
//            return null;
//        }

//        public byte[] getValue(Tag tag) {
//            for(Map.Entry<Tag,byte[]> entry: tags.entrySet()){
//                Tag tmpTag = entry.getKey();
//                if(tmpTag.equals(tag)){
//                    byte[] tmpValBytesEntry = entry.getValue();

        public byte[] getValue(Tag tag) {
//            if (tags.containsKey(tag)) {
//                return tags.get(tag);
//            }
            //EGR
            for(Map.Entry<Tag,byte[]> entry: tags.entrySet()){
                Tag tmpTag = entry.getKey();
                if(tmpTag.equals(tag)){
//                    byte[] tmpVal = entry.getValue();


                    byte[] tmpValBytesEntry = entry.getValue();
                    byte[] tmpValBytesTag = tag.getBytes();
                    byte[] tmpValBytes = tags.get(entry.getKey());
                    byte[] tmpVale1 = entry.getValue();
                    byte[] tmpVale2 = entry.getValue();
                    return tmpValBytesEntry;
                }
            }
            return null;
        }

        public byte[] getValue(int tag) {
            return getValue(new Tag(tag));
        }

        public int getProcessingResult() {
            byte[] value = getValue(PrivateTags.TAG_C1_PROCESSING_RESULT);

            if (value != null) {
                int entero = byteArrayToInt(value, 0, value.length);
                return entero;
            }

            return -1;
        }

        public int getTransactonResult() {
            byte[] value = getValue(PrivateTags.TAG_CB_TRANSACTION_RESULT);

            if (value != null) {
                return byteArrayToInt(value, 0, value.length);
            }

            return -1;
        }

        public String getTransactonResultDescription() {
            int value = getTransactonResult();

            switch (value) {
                case DECLINED: return "Declined";
                case AUTHORIZED: return "Authorized, no signature required";
                case ABORTED: return "Aborted";
                case NOT_ACCEPTED: return "Not Accepted";
                case AUTHORIZED_SIGNATURE: return "Authorized, signature required";
                default: return "Unknown transaction result " + value;
            }
        }

        public boolean isSignatureRequired() {
            int result = getTransactonResult();

            if (result == AUTHORIZED_SIGNATURE) {
                return true;
            }

            return false;
        }

        @Override
        public String toString() {
            return "TransactionResponse [processingResult=" + getProcessingResult() + ", transactionResult=" + getTransactonResultDescription();
        }
    }

    private void startThreadAndWaitToFinish(Thread t) throws AudioReaderException, IOException {
        t.start();
        do {
            try {
                t.join(250);
            } catch (InterruptedException e) {
            }
        } while (t.isAlive());
    }

    private Response responseToCardHolderSelection(byte[] data) throws AudioReaderException, IOException {
        Response response = transmit(false, 0x10, data);
        return response;
    }

    private Response responseToCheckForPAN(byte[] data) throws AudioReaderException, IOException {
        Response response = transmit(false, 0x20, data);
        return response;
    }

    private Response responseToOnlineProcessing(byte[] data) throws AudioReaderException, IOException {
        Response response = transmit(false, 0x30, data);
        return response;
    }

    private void responseToConfigOrReject(byte[] data) {

    }

    private Response processCheckForPAN(final byte[] data, final EMVProcessorCallback callback) throws AudioReaderException, IOException {
        final Object[] result = new Object[1];
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                result[0] = callback.onPanCheckingRequest(data);
            }
        });
        startThreadAndWaitToFinish(t);

        return responseToCheckForPAN((byte[])result[0]);
    }

    private Response processCardHolderSelection(final byte[] data, final EMVProcessorCallback callback) throws AudioReaderException, IOException {
        final Object[] result = new Object[1];
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                result[0] = callback.onCardHolderSelectionRequest(data);
            }
        });
        startThreadAndWaitToFinish(t);

        return responseToCardHolderSelection((byte[]) result[0]);
    }

    private Response processOnline(final byte[] data, final EMVProcessorCallback callback) throws AudioReaderException, IOException {
        final Object[] result = new Object[1];
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                result[0] = callback.onOnlineProcessingRequest(data);
            }
        });
        startThreadAndWaitToFinish(t);

        Response response = responseToOnlineProcessing((byte[])result[0]);
        processConfirmOrReject(response.data, callback);
        return response;
    }

    private void processConfirmOrReject(final byte[] data, final EMVProcessorCallback callback) throws AudioReaderException, IOException {
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                callback.onConfirmOrReverseOnlineRequest(data);
            }
        });
        startThreadAndWaitToFinish(t);

        responseToConfigOrReject(new byte[0]);
    }

    /**
     * Start EMV transaction.
     *
     * @param data the input data representing by BER-TLV objects encoded into byte array.
     * @param callback the callback that will run during transaction processing.
     * @return the transaction response object.
     * @throws AudioReaderException if a reader error occurs.
     * @throws IOException if an I/O error occurs.
     */
    public TransactionResponse initEMVProcessing(final byte[] data, final EMVProcessorCallback callback) throws AudioReaderException, IOException {
        Response response = transmit(false, 0x04, data);
        Logger.log(Logger.MESSAGE, "emvProcessor"," *****---------*******"+response.toString());
//

        mArrayBerTlv = new ArrayList<>();
        Map<Tag, byte[]> tags = BerTlv.createMap(data,1);
        //EGR
//        ByteBuffer buf = ByteBuffer.wrap(data);
//        ArrayListMultimap<Tag, byte[]> tags = BerTlv.createArrayListMultiMap(buf);

        while (response.cid != 0x04) {
            // We need to collect all TLV data that comes from reader + initial data.            
            try {


//                tags.putAll(BerTlv.createMap(response.data,tags));
                tags.putAll(BerTlv.createMap(response.data, 1));
                //EGR
//                map1 = BerTlv.createArrayMultiMap(response.data);
//                tags.putAll(map1);
//                Map<TagTwo,byte[]> mapTemp = new HashMap<>();
//                ByteBuffer buffer = ByteBuffer.wrap(data);
//                while (buffer.hasRemaining()) {
//                    TagTwo tagTwo = TagTwo.create(buffer);
//                    int len = decodeLength(buffer);
//                    byte[] val = new byte[len];
//                    //CREAR OTRA CLASE BERTLV
//                    mapTemp.put(tlv.getTag(), tlv.getValue());
//                }

                switch (response.cid) {
                    case 0x10: {
                        response = processCardHolderSelection(response.data, callback);
//                        tags.putAll(BerTlv.createMap(response.data));
                        break;
                    }
                    case 0x20: {
                        response = processCheckForPAN(response.data, callback);
//                        tags.putAll(BerTlv.createMap(response.data));
                        break;
                    }
                    case 0x30: {
                        response = processOnline(response.data, callback);
//                        tags.putAll(BerTlv.createMap(response.data));
                        break;
                    }
                    default: {
                        throw new RuntimeException("Response with invalid cid: " + Integer.toHexString(response.cid));
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException("Failed to parse TLV data", e);
            }
        }
        tags.putAll(BerTlv.createMap(response.data,tags));

        TransactionResponse result = new TransactionResponse(tags);
        return result;
    }

//    @SafeVarargs
//    public final Map<Tag, byte[]> mergeSumOfMaps(ArrayListMultimap<Tag, byte[]>... maps) {
//        final ArrayListMultimap<Tag, byte[]> resultMap = ArrayListMultimap.create();
//        for (final ArrayListMultimap<Tag, byte[]> map : maps) {
//            for (final Tag key : map.keySet()) {
//                final byte[] value;
////                if (!resultMap.containsKey(key)) {
//                value = map.get(key);
//                resultMap.put(key, value);
////                }
//            }
//        }
//        return resultMap;
//    }

    public static int decodeLength(ByteBuffer data) {
        int length = (int) data.get() & 0xff;

        if ((length & 0x80) != 0) {
            int numberOfBytes = length & 0x7F;

            length = 0;
            while (numberOfBytes > 0) {
                length = (length << 8) + ((int) data.get() & 0xff);
                numberOfBytes--;
            }
        }

        return length;
    }
}
