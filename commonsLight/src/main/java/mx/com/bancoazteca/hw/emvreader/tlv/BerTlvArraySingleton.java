package mx.com.bancoazteca.hw.emvreader.tlv;

import java.util.ArrayList;

/**
 * Created by B931724 on 05/04/18.
 */

public class BerTlvArraySingleton {

    private static BerTlvArraySingleton mInstance = null;
    private ArrayList<BerTlv> mArraySingleton;

    private BerTlvArraySingleton(){
        mArraySingleton = new ArrayList<BerTlv>();
    }

    public static BerTlvArraySingleton getmInstance(){
        if(mInstance==null)
            mInstance = new BerTlvArraySingleton();
        return mInstance;
    }

    public ArrayList<BerTlv> getmArraySingleton() {
        return mArraySingleton;
    }

    public void setmArraySingleton(ArrayList<BerTlv> mArraySingleton) {
        this.mArraySingleton = mArraySingleton;
    }

    public void addValue(BerTlv berTlv){
        mArraySingleton.add(berTlv);
    }
}
