package mx.com.bancoazteca.hw;

public interface HeadsetListener {
	public void headsetPlugged(int microphoneState);
	public void headsetUnplugged(int microphoneState);
}
