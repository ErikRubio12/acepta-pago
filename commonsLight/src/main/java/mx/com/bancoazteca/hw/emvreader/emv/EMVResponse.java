package mx.com.bancoazteca.hw.emvreader.emv;

public class EMVResponse {

	private int cardType;
	private int track1Len;
	private int track2Len;
	private int track3Len;
	private String maskedT1;
	private String maskedT2;
	private String maskedT3;
	public int getCardType() {
		return cardType;
	}
	public void setCardType(int cardType) {
		this.cardType = cardType;
	}
	public int getTrack1Len() {
		return track1Len;
	}
	public void setTrack1Len(int track1Len) {
		this.track1Len = track1Len;
	}
	public int getTrack2Len() {
		return track2Len;
	}
	public void setTrack2Len(int track2Len) {
		this.track2Len = track2Len;
	}
	public int getTrack3Len() {
		return track3Len;
	}
	public void setTrack3Len(int track3Len) {
		this.track3Len = track3Len;
	}
	public String getMaskedT1() {
		return maskedT1;
	}
	public void setMaskedT1(String maskedT1) {
		this.maskedT1 = maskedT1;
	}
	public String getMaskedT2() {
		return maskedT2;
	}
	public void setMaskedT2(String maskedT2) {
		this.maskedT2 = maskedT2;
	}
	public String getMaskedT3() {
		return maskedT3;
	}
	public void setMaskedT3(String maskedT3) {
		this.maskedT3 = maskedT3;
	}
	
}
