package mx.com.bancoazteca.hw;

import android.location.Location;
import android.os.Bundle;

public class LocationCallBackParameters {
	private String provider;
	private int status;
	private Bundle params;
	private Location location;
	
	
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Bundle getParams() {
		return params;
	}
	public void setParams(Bundle params) {
		this.params = params;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public Location getLocation() {
		return location;
	}
	
	
}
