package mx.com.bancoazteca.hw.emvreader.backend;

public interface ReaderState {
	public static final int BORN = 0x00;
	public static final int CONFIGURATION_IN_PROGRESS = 0x10;
	public static final int IDLE = 0x20;
	public static final int SELECTION = 0x21;
	public static final int FINAL_SELECT = 0x22;
	public static final int CARD_HOLDER = 0x30;
	public static final int TRANSACTION = 0x31;
	public static final int PAN_CHECK = 0x40;
	public static final int ONLINE = 0x50;
	public static final int COMPLETION = 0x51;
}
