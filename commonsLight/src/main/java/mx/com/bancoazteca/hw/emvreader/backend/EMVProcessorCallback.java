package mx.com.bancoazteca.hw.emvreader.backend;

/**
 * An interface that defines processor callback methods. 
 */
public interface EMVProcessorCallback {    
    
    public byte[] onCardHolderSelectionRequest(byte[] data);
    
    public byte[] onPanCheckingRequest(byte[] data);
    
    public byte[] onOnlineProcessingRequest(byte[] data);
    
    public void onConfirmOrReverseOnlineRequest(byte[] data);
    
}