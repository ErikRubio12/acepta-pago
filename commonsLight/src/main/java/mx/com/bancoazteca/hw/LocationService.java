package mx.com.bancoazteca.hw;

import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.util.Logger;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;

public class LocationService extends Service implements MessageListener<Message,  LocationCallBackParameters>{
	private String LOCATION_SERVICE="Location Service";
	private final IBinder mBinder = new LocationBinder();
	private Thread backgroundThread;
	private static Location mostRecentLocation=null;
	private boolean isFirstShoot=true;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En  onCreate() LocationService");
	}
	@Override
	public ComponentName startService(Intent service) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En  startService() LocationService : "+ service);
		return super.startService(service);
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En  onStartCommand LocationService: intent - "+ intent + "  flags - " + flags + " startId - "+ startId);
		backgroundThread= new LocationThread(this, this, LocationThread.NETWORK);
		((LocationThread) backgroundThread).setTimeOut(LocationThread.DEFAULT_TIME_OUT);
		backgroundThread.start();
		return START_STICKY;
	}
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En onBind LocationService: " + intent);
		return mBinder;
	}
	@Override
	public boolean stopService(Intent name) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En stopService LocationService: " + name);
		return super.stopService(name);
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En onDestroy" );
		super.onDestroy();
	}
	
	public class LocationBinder extends Binder {
       public LocationService getService() {
        	Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En getService" );
            return LocationService.this;
        }
    }

	@Override
	public void sendMessage(Context ctx,Message message,
			LocationCallBackParameters parameter) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En sendMessage" );
		if(message.getType()==LocationThread.PROVIDER_DISABLE){
			this.stopSelf();
		}
		else if(message.getType()==LocationThread.LOCATION_CHANGED){
			Location location=parameter.getLocation();
			Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"ubicacion: "+ location );
			if(location.getLatitude()==0 && isFirstShoot){
				isFirstShoot=false;
				backgroundThread= new LocationThread(this, this, LocationThread.GPS);
				((LocationThread) backgroundThread).setTimeOut(LocationThread.DEFAULT_TIME_OUT);
				backgroundThread.start();
			}else{
				if(location.getLatitude()==0)
					mostRecentLocation=((LocationThread) backgroundThread).getLastLocation();
				else{
					mostRecentLocation=location;
					Logger.log(Logger.MESSAGE, LOCATION_SERVICE,"En sendMessage" );
					this.stopSelf();
				}
			}
		}
		else if(message.getType()==LocationThread.GPS_DISABLE){
			this.stopSelf();
		}
		else{
			this.stopSelf();
		}
	}
	@Override
	public void sendStatus(Context ctx,int status) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
	public static Location getMostRecentLocation() {
		return mostRecentLocation;
	}

}
