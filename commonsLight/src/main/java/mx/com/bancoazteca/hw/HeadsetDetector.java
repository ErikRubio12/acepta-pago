package mx.com.bancoazteca.hw;

import mx.com.bancoazteca.IntentFilters;
import mx.com.bancoazteca.util.Logger;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class HeadsetDetector extends BroadcastReceiver {
	public static int MICROPHONE_PRESENT=1;
	public static int MICROPHONE_NOT_PRESENT=0;
	private HeadsetListener listener;
	boolean isDevicePlugged=false;
	public HeadsetDetector(HeadsetListener listener) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		
	}
	@Override
	public void onReceive(Context arg0, Intent intent) {
		// TODO Auto-generated method stub
		// TODO Put your code here
		/**
		 * headsetState    = 0 Unplugged
		 * headsetState    = 1 Plugged
		 * microphoneState = 1 Present
		 * microphoneState = 0 Not Present
		 */
		
		if(intent.getAction().equals(IntentFilters.Actions.HEADSET_PLUG)){
			int headsetState = intent.getExtras().getInt("state");
			int microphoneState = intent.getExtras().getInt("microphone");
			Logger.log("HeadsetDetector: "+ headsetState + " , " + microphoneState);
			if(headsetState== 1 ){
				isDevicePlugged=true;
				listener.headsetPlugged(microphoneState);
			}
							
			else if(headsetState==0){
				isDevicePlugged=false;
				listener.headsetUnplugged(microphoneState);
			}
		}

	}
	public boolean isDevicePlugged(){
		return isDevicePlugged;
	}
}

