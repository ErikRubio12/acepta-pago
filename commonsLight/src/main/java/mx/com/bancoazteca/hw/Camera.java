package mx.com.bancoazteca.hw;
import java.io.File;
import java.io.IOException;

import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.util.Logger;
import mx.commons.R;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.view.SurfaceHolder;

public class Camera  implements ShutterCallback, ErrorCallback,PictureCallback{
	protected MessageListener<Message, CameraCallBackParameters> listener;
	protected android.hardware.Camera camera;
	private SurfaceHolder holder;
	public final static String AUTO_FLASH=android.hardware.Camera.Parameters.FLASH_MODE_AUTO;
	public final static String AUTO_SCENE_MODE=android.hardware.Camera.Parameters.SCENE_MODE_AUTO;
	public final static String AUTO_WHITE_BALANDE=android.hardware.Camera.Parameters.WHITE_BALANCE_AUTO;
	public final static String NONE_EFFECT=android.hardware.Camera.Parameters.EFFECT_NONE;
	public final static String AUTO_FOCUS_MODE=android.hardware.Camera.Parameters.FOCUS_MODE_AUTO;
	private final static String AZTECA_CAMERA="CAMARA AZTECA";
	public final static int PICTURE_TAKEN=100;
	protected Message message;
	protected CameraCallBackParameters params=null;
	public final static String CAMERA="Camera";
	private static final String PHOTO_IMAGE_NAME = "Imagen.jpg";
	public Camera(MessageListener<Message, CameraCallBackParameters> listener, SurfaceHolder holder) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		this.holder=holder;
	}
	
	public  void open() throws IOException{
		camera =android.hardware.Camera.open();
		camera.setPreviewDisplay(holder);
		camera.setErrorCallback(this);
		
	}
	
	public static boolean isCameraAvailable(){
		android.hardware.Camera c;
		try {
			c=android.hardware.Camera.open();
			if(c==null)
				return false;
		} catch (RuntimeException e) {
			// TODO: handle exception
			return false;
		}
		c.release();
		return true;
	}
	public void close(){
		if(camera!=null)
			camera.release();
	}
	public void stopPreview(){
		camera.stopPreview();
	}
	public void startPreview(){
		camera.startPreview();
	}
	public void takeShoot(){
		if(camera != null)
			camera.takePicture(this,null, this);
	}
	public void setListener(MessageListener<Message, CameraCallBackParameters> listener) {
		this.listener = listener;
	}

	public MessageListener<Message, CameraCallBackParameters> getListener() {
		return listener;
	}
	
	public android.hardware.Camera.Parameters getParameters(){
		if(camera!=null)
			return camera.getParameters();
		else
			return null;
	}
	
	public void setParameters(android.hardware.Camera.Parameters params){
		
		camera.setParameters(params);
	}
	//**************** ACTION LISTENERS **********************
	@Override
	public void onError(int error, android.hardware.Camera camera) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE,AZTECA_CAMERA,"en onError");
	}

	@Override
	public void onShutter() {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE,AZTECA_CAMERA,"en onShutter");
	}

	
	@Override
	public void onPictureTaken( byte[] data, android.hardware.Camera camera) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE,AZTECA_CAMERA,"en onPictureTaken");
		message= new Message();
		message.setTextMessage(R.string.pictureTaken);
		message.setType(PICTURE_TAKEN);
		
		File imageFile=FileManager.saveRawData(data,PHOTO_IMAGE_NAME,FileManager.EXTERNAL);
		data=null;
		listener.sendMessage(null,message,imageFile==null ? "" : PHOTO_IMAGE_NAME);
		imageFile=null;
		System.gc();
	}
	
}
