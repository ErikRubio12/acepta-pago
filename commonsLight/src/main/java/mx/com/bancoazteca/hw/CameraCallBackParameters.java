package mx.com.bancoazteca.hw;
import android.hardware.Camera;

public class CameraCallBackParameters {

	private Camera camera;
	private static byte data[]=null;
	private boolean zoomStopped;
	private int zoomValue;
	private int errorType;
	public final static int UNKNOW_ERROR=Camera.CAMERA_ERROR_UNKNOWN;
	public final static int MEDIA_SERVICE_DIED_ERROR=Camera.CAMERA_ERROR_SERVER_DIED;
	public CameraCallBackParameters() {
		// TODO Auto-generated constructor stub
	}
	
	public CameraCallBackParameters(byte[] data) {
		super();
		CameraCallBackParameters.data = data;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	public void setData(byte[] data) {
		CameraCallBackParameters.data = data;
	}
	public void setZoomStopped(boolean zoomStopped) {
		this.zoomStopped = zoomStopped;
	}
	public void setZoomValue(int zoomValue) {
		this.zoomValue = zoomValue;
	}
	public void setErrorType(int errorType) {
		this.errorType = errorType;
	}
	public Camera getCamera() {
		return camera;
	}
	public byte[] getData() {
		return data;
	}
	public boolean isZoomStopped() {
		return zoomStopped;
	}
	public int getZoomValue() {
		return zoomValue;
	}
	public int getErrorType() {
		return errorType;
	}
	
	
}
