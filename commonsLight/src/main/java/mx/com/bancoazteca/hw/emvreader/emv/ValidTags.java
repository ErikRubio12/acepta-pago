package mx.com.bancoazteca.hw.emvreader.emv;

import java.util.ArrayList;

public class ValidTags {
	public final static String TAG_9A="9A";
	public final static String TAG_9F35="9F35";
	public final static String TAG_9F53="9F53";
	public final static String TAG_9C="9C";
	public final static String TAG_9F1A="9F1A";
	public final static String TAG_5F2A="5F2A";
	public final static String TAG_9F09="9F09";
	public final static String TAG_9F41="9F41";
	public final static String TAG_9F27="9F27";
	public final static String TAG_9F02="9F02";
	public final static String TAG_9F03="9F03";
	public final static String TAG_9F33="9F33";
	public final static String TAG_95="95";
	public final static String TAG_9F37="9F37";
	public final static String TAG_9F1E="9F1E";
	public final static String TAG_9F34="9F34";
	public final static String TAG_9F26="9F26";
	public final static String TAG_9F36="9F36";
	public final static String TAG_82="82";
	public final static String TAG_4F="4F";
	public final static String TAG_9F10="9F10";
	public final static String TAG_84="84";
	public final static String TAG_5F34="5F34";
	public final static String TAG_57="57";
	public final static String TAG_81="81";
	
	private static ArrayList<String> list;
	static{
		list= new ArrayList<String>();
		list.add(TAG_9A);
		list.add(TAG_9F35);
		list.add(TAG_9F53);
		list.add(TAG_9C);
		list.add(TAG_9F1A);
		list.add(TAG_5F2A);
		list.add(TAG_9F09);
		list.add(TAG_9F41);
		list.add(TAG_9F27);
		list.add(TAG_9F02);
		list.add(TAG_81);
		list.add(TAG_9F03);
		list.add(TAG_9F33);
		list.add(TAG_95);
		list.add(TAG_9F37);
		list.add(TAG_9F1E);
		list.add(TAG_9F34);
		list.add(TAG_9F26);
		list.add(TAG_9F36);
		list.add(TAG_82);
		list.add(TAG_4F);
		list.add(TAG_9F10);
		list.add(TAG_84);
		list.add(TAG_5F34);
		list.add(TAG_57);
	}
	public static ArrayList<String> getList() {
		return list;
	}	
}
