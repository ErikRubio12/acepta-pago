package mx.com.bancoazteca.hw.emvreader;

import java.util.Calendar;

import mx.com.bancoazteca.hw.emvreader.backend.ProcessingResult;
import mx.com.bancoazteca.hw.emvreader.backend.ReaderState;
import mx.com.bancoazteca.hw.emvreader.tlv.BerTlv;




public class EMVProcessorHelper {
    
    private static byte encodeToBCD(int value) {
        if (value > 99 || value < 0) {
            throw new IllegalArgumentException("Value must be between 0 and 99");      
        }
        return (byte)(((value / 10) << 4) | (value % 10)); 
    }

    public static byte[] encodeTransactionDate(Calendar c) {        
        byte[] result = new byte[3]; 
        result[0] = encodeToBCD(c.get(Calendar.YEAR) - 2000);
        result[1] = encodeToBCD(c.get(Calendar.MONTH) + 1);
        result[2] = encodeToBCD(c.get(Calendar.DAY_OF_MONTH));
        return result;
    }
        
    public static byte[] encodeTransactionTime(Calendar c) {        
        byte[] result = new byte[3]; 
        result[0] = encodeToBCD(c.get(Calendar.HOUR));
        result[1] = encodeToBCD(c.get(Calendar.MINUTE));
        result[2] = encodeToBCD(c.get(Calendar.SECOND));
        return result;
    }
    
    public static byte[] encodeTransactionSequence(int value) {                
        byte[] result = new byte[3]; 
        result[0] = encodeToBCD(value / 10000);
        result[1] = encodeToBCD((value / 100) % 100);
        result[2] = encodeToBCD(value % 100);
        return result;
    }
    
    public static byte[] encodeAmount(String amount) {
        int v = (int)(Double.parseDouble(amount) * 100 + 0.5);
        return new byte[] { (byte)(v >> 24), (byte)(v >> 16), (byte)(v >> 8), (byte)(v) };
    }
    
    public static String decodeNib(byte[] value) { 
        StringBuilder sb = new StringBuilder(value.length);
        
        for (byte b: value) {
            int v = (int)b & 0xff;
            int f = v >> 4;
            int s = v & 0xf;
            
            if (f > 9) {
                if (f != 0xf) throw new RuntimeException("Invalid value: " + Integer.toHexString(f));
            } else {
                sb.append((char)(48 + f));                              
            }
            
            if (s > 9) {
                if (s != 0xf) throw new RuntimeException("Invalid value: " + Integer.toHexString(s));
            } else {
                sb.append((char)(48 + s));                              
            }           
        }
        
        return sb.toString();
    }

    public static String decodeAscii(byte[] value) {
        StringBuilder sb = new StringBuilder();
        
        for (byte b: value) {
            if (b != 0) {
                sb.append((char)b);
            }
        }
        
        return sb.toString();
    }
    
    public static int decodeInt(byte[] value) {
        int n = 0;
        
        for (byte b: value) {
            n = (n << 8) + (b & 0xff);
        }
        
        return n;
    }
    
    public static String decodeDate(byte[] value) {
        String s = decodeNib(value);        
        return "20" + s.substring(0, 2) + "-" + s.substring(2, 4) + "-" + s.substring(4, 6);
    }
    
    public static String decodeTime(byte[] value) {
        String s = decodeNib(value); 
        return s.substring(0, 2) + ":" + s.substring(2, 4) + ":" + s.substring(4, 6);
    }   
    
    public static String decodeAmount(byte[] value) {
        long amount = 0;
        
        for (byte b: value) {
            amount = amount << 8;
            amount += (int)b & 0xff;            
        }
        
        return "" + String.valueOf(amount / 100) + "." + String.valueOf(100 + (amount % 100)).substring(1);
    }
    
    public static String decodeHex(byte[] value) {
        char[] hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] buf = new char[value.length * 2];
        
        for (int i = 0; i < value.length; i++) {
            buf[i * 2 + 0] = hex[(value[i] & 0xff) >> 4];
            buf[i * 2 + 1] = hex[(value[i] & 0x0f) >> 0];
        }
        
        return new String(buf);
    }
    
    public static String getMaskedString(String s, int unmaskedAtStart, int unmaskedAtEnd) {
        char[] tmp = s.toCharArray();
        
        for (int i = unmaskedAtStart; i < (tmp.length - unmaskedAtEnd); i++) {
            tmp[i] = '*';
        }
        
        return new String(tmp);
    }

    // Helper method for converting state to string HRI value
    public static String getReaderStateDescription(int state) {
        switch (state) {
            case ReaderState.BORN : return "BORN";
            case ReaderState.CONFIGURATION_IN_PROGRESS: return "CONFIGURATION IN PROGRESS";
            case ReaderState.IDLE: return "IDLE";
            case ReaderState.SELECTION: return "SELECTION";
            case ReaderState.FINAL_SELECT: return "FINAL SELECT";
            case ReaderState.CARD_HOLDER: return "CARD HOLDER";
            case ReaderState.TRANSACTION: return "TRANSACTION";
            case ReaderState.PAN_CHECK: return "PAN CHECK";
            case ReaderState.ONLINE: return "ONLINE";
            case ReaderState.COMPLETION: return "COMPLETION";
            default: return "UNKNOWN STATE: " + Integer.toHexString(state);
        }
    }

    // Helper method for converting result to string HRI value
    public static String getMessageResultDescription(int result) {
        switch (result) {
            case ProcessingResult.OK: return "OK";
            case ProcessingResult.CANCELLED: return "CANCELLED";
            case ProcessingResult.CARD_BLOCKED: return "CARD BLOCKED";
            case ProcessingResult.CARD_MISSING: return "CARD MISSING";
            case ProcessingResult.CHIP_ERROR: return "CHIP ERROR";
            case ProcessingResult.DATA_ERROR: return "DATA ERROR";
            case ProcessingResult.EMPTY_LIST: return "EMPTY LIST";
            case ProcessingResult.GPO6985: return "GPO6985";
            case ProcessingResult.MISSING_DATA: return "MISSING DATA";
            case ProcessingResult.NO_CARD_INSERTED: return "NO CARD INSERTED";
            case ProcessingResult.NO_PROFILE: return "NO PROFILE";
            case ProcessingResult.NOT_ACCEPTED: return "NOT ACCEPTED";
            case ProcessingResult.TIMEOUT: return "TIMEOUT";
            case ProcessingResult.ABORTED: return "ABORTED";
            case ProcessingResult.FALLBACK_PROHIBITED: return "FALLBACK PROHIBITED";
            case ProcessingResult.CONFIGURATION_ERROR: return "CONFIGURATION ERROR";
            case ProcessingResult.EMV_LIB: return "EMV LIB ERROR";
            case ProcessingResult.FLOW_CONTROL: return "FLOW CONTROL";
            case ProcessingResult.INTERNAL_ERROR: return "INTERNAL ERROR"; 
            case ProcessingResult.RESELECT: return "RESELECT";
            case ProcessingResult.SECURITY: return "SECURITY";
            case ProcessingResult.INPUT_DATA_ERROR: return "INPUT DATA ERROR";
            case ProcessingResult.OUT_OF_MEMORY: return "OUT OF MEMORY";
            default: return "UNKNOWN RESULT: " + Integer.toHexString(result);
        }
    }

    public static BerTlv createTlv(int tag, String value) {
        return new BerTlv(tag, HexUtil.hexStringToByteArray(value));
    }
    
    public static BerTlv createTlv(int tag, byte[] value) {
        return new BerTlv(tag, value);
    }
    
}
