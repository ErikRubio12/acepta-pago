package mx.com.bancoazteca.util;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera.CameraInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class HandsetInfo {
	public final static int DENSITY_DEFAULT=DisplayMetrics.DENSITY_DEFAULT;
	public final static int DENSITY_HIGH=DisplayMetrics.DENSITY_HIGH;
	public final static int DENSITY_LOW=DisplayMetrics.DENSITY_LOW;
	public final static int DENSITY_MEDIUM=DisplayMetrics.DENSITY_MEDIUM;
	public final static int DENSITY_TV=213;	//DisplayMetrics.DENSITY_TV;
	public final static int DENSITY_XHIGH=320; //DisplayMetrics.DENSITY_XHIGH;
	public final static int DENSITY_XXHIGH=480; //DisplayMetrics.DENSITY_XXHIGH;
	
	public static int getScreenHeight(Activity currentActivity){
		return currentActivity.getWindowManager().getDefaultDisplay().getHeight();
	}
	public static int getScreenWidth(Activity currentActivity){
		return currentActivity.getWindowManager().getDefaultDisplay().getWidth();
	}
	public static InputMethodManager getKeyboardManager(Context ctx){
		return (InputMethodManager)ctx.getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			
	}
	public static String getIMEI(Context context) {
		// TODO Auto-generated method stub
		String imei=((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		return imei;
	}	
	public static String getIMEI() {
		// TODO Auto-generated method stub
		Context context= Utility.getInitialContext();
		if(context !=null){
			String imei=((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
			return imei==null ? "NA" : imei;
		}
		else
			return "NA";
	}
	public static void dismissKeyboard(Context ctx,EditText edit){
		InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}
	public static void showKeyboard(Context ctx,EditText edit){
		InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(edit, 0);
	}
	public static void setFullScreen(Activity context){
		context.requestWindowFeature(Window.FEATURE_NO_TITLE);
	}
	public static int getPhoneNetworkType(Context ctx ){
		return ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkType();
	}
	public static int getPhoneType(Context ctx ){
		return ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getPhoneType();
	}
	public static int getDataState(Context ctx ){
		return ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getDataState();
	}
	public static boolean isOnline(Context appContext) {
		// TODO Auto-generated method stub
		ConnectivityManager cm = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo network=cm.getActiveNetworkInfo();
		if (network != null)
			return network.isConnectedOrConnecting();
		else
			return false;
	}
	public static String getDataNetworkType(Context ctx){
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo network=cm.getActiveNetworkInfo();
		return network.getTypeName();
	}
	public static int getOrientation(Activity activity){

		return activity.getWindowManager().getDefaultDisplay().getOrientation();
	}
	public static int getAndroidVersion(){
		return Build.VERSION.SDK_INT;
	}
	public static boolean supportPhoneCall(Context ctx){
		try {
			TelephonyManager tm= (TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE);
		       if(tm.getPhoneType()==TelephonyManager.PHONE_TYPE_NONE){
		        //No calling functionality
		    	   return false;
		       }
		       else
		       {
		       //calling functionality
		    	   return true;
		       }
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
	}
	public static int getScreenDensity(Activity activity){
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics.densityDpi;
	}
	public static int getWidthPixels(Activity activity){
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics.widthPixels;
	}
	public static int getHeight(Activity activity){
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics.heightPixels;
	}
	public static int getFrontCameraId() {
	    CameraInfo ci = new CameraInfo();
	    for (int i = 0 ; i < android.hardware.Camera.getNumberOfCameras(); i++) {
	    	android.hardware.Camera.getCameraInfo(i, ci);
	        if (ci.facing == CameraInfo.CAMERA_FACING_FRONT) return i;
	    }
	    return -1;
	}
}
