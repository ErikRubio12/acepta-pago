package mx.com.bancoazteca.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mx.com.bancoazteca.data.files.FileManager;

public class Utility {
	
	private static Context initContext;
	private final static String UTILITY="UTILITY";
	private static char []vocals={'A','E','I','O','U'};
	private static char []quots={'Á','É','Í','Ó','Ú'};
	private static char []noQuots={'A','E','I','O','U'};
	protected final static char[] lettersDigits= {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z','0',
	    '1','2','3','4','5','6','7','8','9'};
	protected final static char[] chars= {'á','é','í','ó','ú','a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z','0',
    '1','2','3','4','5','6','7','8','9','_','-',':',';',',','=','.','/','#','$','@','%','*','+','(',')','?','¿','!','¡','\''};
	private static final String TAG_UTIL = "UTILITY";
	private static String []articulos={"DEL","Y","LAS","DE","LA","A","MC","LOS","VON","VAN"};
	public static View inflateLayout(Context context, int idLayout, ViewGroup viewGroup){
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return inflater.inflate(idLayout, viewGroup);
	}
	public static String fixSoapResponse(String response){
		if(response !=null){
			response=response.replace("&quot;","");
			response=response.replace("&lt;", "<");
			response=response.replace("&gt;", ">");
		}
		
		return response;
	}
	public static boolean isAppInstalled(Context ctx, String uri) {
		PackageManager pm = ctx.getPackageManager();
		boolean app_installed = false;
		try {
			PackageInfo info= pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			Logger.log(Logger.MESSAGE, TAG_UTIL, info.packageName + " , " + info.sharedUserLabel + " , " +info.applicationInfo.name);
			app_installed = true;
		}
		catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed ;
	}
	public static void launchApp(Context context, String uri){
		context.startActivity(context.getPackageManager().getLaunchIntentForPackage(uri));
	}

	public static LocationManager getLocationManager(Context appContext){
		return (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
	}
	public static void setFullScreen(Activity cameraPreviewScreen) {
		// TODO Auto-generated method stub
		cameraPreviewScreen.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	public static Bitmap scaleImage(Bitmap originalImage,float width,float height) {
		// TODO Auto-generated method stub
		return Bitmap.createScaledBitmap(
                                         originalImage,     // bitmap to resize
                                         (int)width,   // new width
                                         (int)height,  // new height
                                         true); // bilinear filtering
	}
	public static void setInitialContext(Context context){
		
		initContext=context;
	}
	public static String removeDeniedCharacters(String cadena){
		HashMap<Character, String> charactersNotAllowed= new HashMap<Character, String>();
		charactersNotAllowed.put(Character.valueOf('á'), "~a");
		charactersNotAllowed.put(Character.valueOf('é'), "~e");
		charactersNotAllowed.put(Character.valueOf('í'), "~i");
		charactersNotAllowed.put(Character.valueOf('ó'), "~o");
		charactersNotAllowed.put(Character.valueOf('ú'), "~u");
		charactersNotAllowed.put(Character.valueOf('Á'), "~A");
		charactersNotAllowed.put(Character.valueOf('É'), "~E");
		charactersNotAllowed.put(Character.valueOf('Í'), "~I");
		charactersNotAllowed.put(Character.valueOf('Ó'), "~O");
		charactersNotAllowed.put(Character.valueOf('Ú'), "~U");
		charactersNotAllowed.put(Character.valueOf('Ñ'), "~N");
		charactersNotAllowed.put(Character.valueOf('ñ'), "~n");
//		charactersNotAllowed.put(Character.valueOf(','), "~coma");
		for (Map.Entry<Character, String> value : charactersNotAllowed.entrySet()) {
            cadena= cadena.replace(value.getKey().toString(),value.getValue() );
		}
		return cadena;
		
	}
	public static String getBackDeniedCharacters(String cadena){
		HashMap<Character,String> charactersNotAllowed= new HashMap<Character, String>();
		charactersNotAllowed.put(Character.valueOf('á'), "~a");
		charactersNotAllowed.put(Character.valueOf('é'), "~e");
		charactersNotAllowed.put(Character.valueOf('í'), "~i");
		charactersNotAllowed.put(Character.valueOf('ó'), "~o");
		charactersNotAllowed.put(Character.valueOf('ú'), "~u");
		charactersNotAllowed.put(Character.valueOf('Á'), "~A");
		charactersNotAllowed.put(Character.valueOf('É'), "~E");
		charactersNotAllowed.put(Character.valueOf('Í'), "~I");
		charactersNotAllowed.put(Character.valueOf('Ó'), "~O");
		charactersNotAllowed.put(Character.valueOf('Ú'), "~U");
		charactersNotAllowed.put(Character.valueOf('Ñ'), "~N");
		charactersNotAllowed.put(Character.valueOf('ñ'), "~n");
//		charactersNotAllowed.put(Character.valueOf(','), "~coma");
		if(cadena!=null) {
			for (Map.Entry<Character, String> value : charactersNotAllowed.entrySet()) {
				cadena = cadena.replace(value.getValue(), value.getKey().toString());
			}
			return cadena;
		}
		return cadena=null;
		
	}
	public static Context getInitialContext(){
		return initContext;
	}
	
	public static Bitmap decodeImage(int scaleFactor,byte[] image) {
		// TODO Auto-generated method stub
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inDensity=DisplayMetrics.DENSITY_LOW;
		options.inTargetDensity=DisplayMetrics.DENSITY_LOW;
		options.inTempStorage = new byte[16*1024];
		options.inPurgeable=true;
		options.inInputShareable=false;
		options.inSampleSize=scaleFactor;
        
		return BitmapFactory.decodeByteArray(image, 0, image.length,options);
	}
	public static Bitmap decodeImage(int scaleFactor,byte[] image,int width,int height) {
		// TODO Auto-generated method stub
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inTempStorage = new byte[16*1024];
		options.inPurgeable=true;
		options.inInputShareable=true;
		options.inSampleSize=scaleFactor;
		options.outHeight=height;
		options.outWidth=width;
		return BitmapFactory.decodeByteArray(image, 0, image.length,options);
	}
	
	public static Bitmap roundCornerImage(Bitmap src, float round) {
		  // Source image size
		  int width = src.getWidth();
		  int height = src.getHeight();
		  // create result bitmap output
		  Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		  // set canvas for painting
		  Canvas canvas = new Canvas(result);
		  canvas.drawARGB(0, 0, 0, 0);
		 
		  // configure paint
		  final Paint paint = new Paint();
		  paint.setAntiAlias(true);
		  paint.setColor(Color.BLACK);
		 
		  // configure rectangle for embedding
		  final Rect rect = new Rect(0, 0, width, height);
		  final RectF rectF = new RectF(rect);
		 
		  // draw Round rectangle to canvas
		  canvas.drawRoundRect(rectF, round, round, paint);
		 
		  // create Xfer mode
		  paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		  // draw source image to canvas
		  canvas.drawBitmap(src, rect, rect, paint);
		 
		  // return final image
		  return result;
		 }
	public static Bitmap decodeImage(Context ctx,int scaleFactor,String imagePath,int source,int width,int height) {
		// TODO Auto-generated method stub
		File f=FileManager.openFile(ctx,imagePath,source,FileManager.MODE_APPEND ^ FileManager.MODE_RW);
		if(f != null){
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inTempStorage = new byte[16*1024];	
			options.inSampleSize=scaleFactor;
			options.outHeight=height;
			options.outWidth=width;
			return BitmapFactory.decodeFile(f.getAbsolutePath(), options);
		}
		else 
			return null;
	}
	public static Bitmap decodeImage(Context ctx,String imagePath,int source,int width,int height) {
		// TODO Auto-generated method stub
		File f=FileManager.openFile(ctx,imagePath,source,FileManager.MODE_APPEND ^ FileManager.MODE_RW);
		if(f != null){
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inTempStorage = new byte[16*1024];	
			options.outHeight=height;
			options.outWidth=width;
			return BitmapFactory.decodeFile(f.getAbsolutePath(), options);
		}
		else 
			return null;
	}
	
	
	public static InputStream loadFileAsset(Context context,String fileName) throws IOException {
		// TODO Auto-generated method stub
		AssetManager assets=context.getAssets();
		return assets.open(fileName);
	}
    
	public static void gpsSettings(Activity activity,int code) {
		// TODO Auto-generated method stub
		activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), code);
	}
    
	public static String getPrefix() {
		// TODO Auto-generated method stub
		return "02";
	}
	public static String generateRfc(String name,String lastName,String lastName2,Date date){
		StringBuffer rfc= new StringBuffer();
		StringBuffer completeName= new StringBuffer();
		SimpleDateFormat format= new SimpleDateFormat("yyMMdd",Locale.getDefault());
		String _date=format.format(date);
		
		name=name.toUpperCase(Locale.getDefault());
		lastName=lastName.toUpperCase(Locale.getDefault());
		lastName2=lastName2.toUpperCase(Locale.getDefault());
		
		String oldName=name,oldLastName=lastName,oldLastName2=lastName2;
		
		name=quitarArticulos(name);
		lastName=quitarArticulos(lastName);
		lastName2=quitarArticulos(lastName2);
		// Los espacios que se quitan son de los costados
		name=name.trim();
		lastName=lastName.trim();
		lastName2=lastName2.trim();
		
		name=removeQuots(name);
		lastName=removeQuots(lastName);
		lastName2=removeQuots(lastName2);
		//Se debe considerar mas espacios entre los nombres si es que el control
		//permite la entrada de mas de un espacio entre cada palabra
		
		if(name.indexOf("MARIA JOSE")>=0)
			name=name.replace("MARIA JOSE", "JOSE");
		if(name.indexOf("JOSE MARIA")>=0)
			name=name.replace("JOSE MARIA", "MARIA");
		if(name.indexOf("JOSE LUIS")>=0)
			name=name.replace("JOSE LUIS", "LUIS");
		//Generalizar para todos los jose
		if(name.trim().indexOf("JOSE")==0  && name.trim().indexOf(" ")>=0){
			name=name.trim().substring(name.trim().indexOf(" ")+1);
		}
		char firstLetterLastName=lastName.charAt(0);
		rfc.append(firstLetterLastName);
		for (int i = 0; i < lastName.length(); i++) {
			if(isVocal(lastName.charAt(i))){
				if(i==0)
					continue;
				rfc.append(lastName.charAt(i));
				break;
			}
		}
		rfc.append(lastName2.charAt(0));
		rfc.append(name.charAt(0));
		rfc=removeBadWords(rfc);
		rfc.append(_date);
		
		
		completeName.append(oldName);
		completeName.append(" ");
		completeName.append(oldLastName);
		completeName.append(" ");
		completeName.append(oldLastName2);
//		return generateKey(completeName,rfc);
		return  rfc.toString();
		
	}
	private static String generateKey(StringBuffer completeName,StringBuffer rfc) {
		// TODO Auto-generated method stub
		//******************Generar caracteres de homoclave*************************
		StringBuffer strNumero= new StringBuffer("0");
		StringBuffer homoclave= new StringBuffer();
		char character;
		int intAux, nombreInt = 0, intDivide = 0, intModulo = 0, intValHom = 0, intDigNum = 0, intDigPar = 0;
		for (int i = 0; i < completeName.length(); i++) {
            character = completeName.charAt(i);
			if ((int)character >=(int)'A' && (int)character <=(int)'I') {
				intAux = (int)character - 54;
				strNumero.append(intAux);
			}else if ((int)character >=(int)'J' && (int)character <=(int)'R'){
				intAux = (int)character - 53;
				strNumero.append(intAux);
			}else if ((int)character >=(int)'S' && (int)character <=(int)'Z'){
				intAux = (int)character- 51;
				strNumero.append(intAux);
			}else if ((int)character >=(int)'0' && (int)character <=(int)'9'){
				strNumero.append(character);
			}else {
                if ( ((int)character == (int)'&') || (int)completeName.charAt(i) == (int)'�' ){
                    strNumero.append("10");
                }else {
                    strNumero.append("00");
                }
			}
		}
		for (int i = 0; i < strNumero.length()-1; i++) {
			character = strNumero.charAt(i);
			nombreInt += (Integer.parseInt(character+"")*10+ Integer.parseInt(strNumero.charAt(i+1)+"")) * Integer.parseInt(strNumero.charAt(i+1)+"");
			
		}
		
		//Calcular cociente y residuo
		intDivide	= nombreInt % 1000;
		intModulo	= intDivide % 34;
		intDivide	= (intDivide - intModulo) / 34;
		
		
		for (int i = 0; i < 2; i++) {
			if (i == 0)
				intValHom = intDivide;
			else
				intValHom = intModulo;
            
			if ( (intValHom >= 0) && (intValHom <=8) )
				homoclave.append((char)(intValHom + 49));
			else if ( (intValHom >= 9) && (intValHom <= 22) )
				homoclave.append((char)(intValHom + 56));
			else if ( (intValHom >= 23) && (intValHom <= 32) )
				homoclave.append((char)(intValHom + 57));
			else
				homoclave.append("Z");
		}
		//Obtener dígito verificador
		rfc.append(homoclave);
		
		for (int i = 0; i < rfc.length(); i++) {
			character = rfc.charAt(i);
			
			if ((int)character >=(int)'A' && (int)character <=(int)'N')
				intDigNum = (int)character - 55;
			else if ((int)character >=(int)'O' && (int)character <=(int)'Z')
				intDigNum = (int)character - 54;
			else if ((int)character >=(int)'0' && (int)character <=(int)'9')
				intDigNum = Integer.parseInt(character+"");
			else if (character == ' ')
				intDigNum = 37;
			else
				intDigNum = 0;
			intDigPar = intDigPar + (intDigNum * (14 - (i + 1)));
		}
		intModulo = intDigPar % 11;
		if (intModulo == 0)
			rfc.append("0");
		else
		{
			intDigPar = 11 - intModulo;
			if (intDigPar == 10)
				rfc.append("A");
			else
				rfc.append(intDigPar);
		}
		
		//////////////////////////////////////////////////////////////////////
		
		return rfc.toString();
	}
    
	private static StringBuffer removeBadWords(StringBuffer rfc) {
		// TODO Auto-generated method stub
		
		String badWords[]={"BUEI","BUEY","CACA","CACO","CAGA","CAGO","CAKA","COGE","COJA","COJE","COJI","COJO","CULO","CAKO"/*nueva*/	,
            "FETO","GUEY","JOTO","KACA","KACO","KAGA","KAGO","KAKA","KOGE","KOJO","KULO","LOCA","LOCO","LOKA","LOKO","MAME",
            "MAMO","MEAR","MEAS","MEON","MION","MOCO","MULA","PEDA","PEDO","PENE","PITO","PUTA","PUTO","QULO","RATA","RUIN"};
		
		for (int i = 0; i < badWords.length; i++) {
			if (rfc.equals(badWords[i]) ) {
				rfc.append("X");
				break;
			}
		}
		return rfc;
	}
    
	private static String quitarArticulos(String word) {
		StringTokenizer tokens= new StringTokenizer(word, " ");
		String temp;
		boolean articuloFlag=false;
		StringBuffer newWord= new StringBuffer();
		while(tokens.hasMoreTokens()){
			temp= tokens.nextToken();
			for (int i = 0; i < articulos.length; i++) {
				if(articulos[i].equals(temp)){
					articuloFlag=true;
					break;
				}
			}
			if(!articuloFlag){
				newWord.append(temp);
				newWord.append(" ");
			}
			articuloFlag=false;
		}
		return newWord.toString();
	}
	private static String removeQuots(String name) {
		// TODO Auto-generated method stub
		char[]chars= name.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			for (int j = 0; j < quots.length; j++) {
				if(chars[i]==quots[j]){
					chars[i]=noQuots[j];
					break;
				}
			}
		}
		return new String(chars);
	}
    
	private static boolean isVocal(char charAt) {
		// TODO Auto-generated method stub
		for (int i = 0; i < vocals.length; i++) {
			if(vocals[i]==charAt)
				return true;
		}
		return false;
	}
    
	
    
	public static void launchNetworkSettings(Context ctx) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
		//intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
		ctx.startActivity(intent);
	}
	public static Document createDom(String res,String encoding) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder =  factory.newDocumentBuilder();
			InputStream is;
			is = new ByteArrayInputStream(res.getBytes(encoding));
			return builder.parse(is);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static Document createDom(String res) {
		// TODO Auto-generated method stub
		return createDom(res, "ISO-8859-1");
	}
    
	public static boolean isDigits(String number) {
		// TODO Auto-generated method stub
		char val;
		for (int i = 0; i < number.length(); i++) {
			val=number.charAt(i);
			if(!Character.isDigit(val))
				return false;
		}
		return true;
	}
    
	public static String formatDate(Date bornDate, String pattern) {
		// TODO Auto-generated method stub
		SimpleDateFormat formatter= new SimpleDateFormat(pattern,Locale.getDefault());
		return formatter.format(bornDate);
	}
	public static Date dateFromFormat(String date, String pattern) {
		// TODO Auto-generated method stub
		SimpleDateFormat formatter= new SimpleDateFormat(pattern,Locale.getDefault());
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
    public static StringBuffer validCharsToString(){
    	StringBuffer characters= new StringBuffer();
    	for (int i = 0; i < chars.length; i++) {
    		characters.append(chars[i]);
    		if(i<chars.length - 1)
    			characters.append(" , ");
    	}
			
    	return characters;
		
    }
	public static boolean isvalidChar(char character) {
		// TODO Auto-generated method stub
		for (int i = 0; i < chars.length; i++) {
			if(character==chars[i])
				return true;
		}
		return false;
	}
	public static boolean isvalidChar(char character, char [] chars) {
		// TODO Auto-generated method stub
		for (int i = 0; i < chars.length; i++) {
			if(character==chars[i])
				return true;
		}
		return false;
	}
	public static boolean isvalidCharacters(String character) {
		character=character.toLowerCase(Locale.getDefault());
		// TODO Auto-generated method stub
		for (int i = 0; i < character.length(); i++) {
			if(!Utility.isvalidChar(character.charAt(i))){
				return false;
			}
		}
		return true;
	}
	public static boolean isValidCustomCharacters(String character,char [] characters){
		character=character.toLowerCase(Locale.getDefault());
		// TODO Auto-generated method stub
		for (int i = 0; i < character.length(); i++) {
			for (int j = 0; j < characters.length; j++) {
				if(character.charAt(i)==characters[j])
					break;
				else{
					if(j==(characters.length-1))
						return false;
				}
			}
		}
		return true;
	}
	public static boolean isvalidCharacters(String character,char [] extraCharacters) {
		// TODO Auto-generated method stub
		character=character.toLowerCase(Locale.getDefault());
		for (int i = 0; i < character.length(); i++) {
			if(!Utility.isvalidChar(character.charAt(i))){
				for (int j2 = 0; j2 < extraCharacters.length; j2++) {
					if(character.charAt(i)==extraCharacters[j2])
						return true;
				}
				return false;
			}
		}
		return true;
	}
	public static boolean isQuot(char character) {
		// TODO Auto-generated method stub
		for (int i = 0; i < quots.length; i++) {
			if(Character.toUpperCase(character)==quots[i])
				return true;
		}
		return false;
	}
    
//	04-23 20:09:37.140: V/LOG(24937):  folio preapertura = 020100072484600763 rfc SAMJ860627 path: http://200.38.122.86/img13/digitalizacion_web/2013/04/23/PAGOAZTECA/843236-018072/843236-018072-1-1/516/56680001.jpeg

	public static Bitmap loadBitmap(String url, int width, int height, int sf) {
	    InputStream urlStream=null;
	    Logger.log(Logger.MESSAGE, UTILITY,"urlStream: " + url );
    	try {
			urlStream=new URL(url).openStream();
			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inSampleSize=sf;
			options.outHeight=height;
			options.outWidth=width;
			Bitmap b=BitmapFactory.decodeStream(urlStream,null,options);
			urlStream.close();
			return b;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, UTILITY,"urlStream: " + e.getMessage() );
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, UTILITY,"urlStream: " + e.getMessage() );
			return null;
		}
    	
    	
	}
	public static Integer getAppVersionCode(Context ctx){
		if(ctx!=null){
			try {
				if(ctx.getPackageName()!= null){
					if(ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0) != null)
						return Integer.valueOf(ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionCode);					
					else 
						return 000;
					
				}
				else
					return 000;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				return 000;
			}
			catch (Exception e) {
				// TODO: handle exception
				return 000;
			}
		}
		else
			return 000;
		
	}
	public static String getAppVersionName(Context ctx){
		
		if(ctx!=null){
			try {
				return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				return "00000";
			}
		}
		else
			return "00000";
	}
	public static void goToPlayStore(String uri, Context appContext) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("market://details?id="+uri));
		appContext.startActivity(intent);
	}
	public static long nextLong(Random rng,long l){
		if (l<=0)
            throw new IllegalArgumentException("n must be positive");
		long bits, val;
		   do {
		      bits = (rng.nextLong() << 1) >>> 1;
		      val = bits % l;
		   } while (bits-val+(l-1) < 0L);
		   return val;
		
	}
	public static boolean isLetterOrDigit(char character) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lettersDigits.length; i++) {
			if(character==lettersDigits[i])
				return true;
		}
		return false;
	}
	
}
