package mx.com.bancoazteca.util;



import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;
import java.io.File;
import mx.com.bancoazteca.data.files.FileManager;
import android.util.Log;


public class Logger {
private static final String LOG = "LOG";
public static String WARNING="advertencia";
public static String ERROR="error";
public static String EXCEPTION="EXCEPTION";
public static String MESSAGE="message";
protected static String LOG_FILE_NAME="log.txt";
private static boolean  DEBUG=true;
protected static SimpleDateFormat FORMATTER= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss | ",Locale.getDefault());
	public static void log(String type,String tag,String message){
		if(DEBUG){
			File destiny = FileManager.openFile(LOG_FILE_NAME,FileManager.MODE_APPEND);
			if(destiny !=null){
				StringBuilder log= new StringBuilder("");
				Date currentDate= new Date();
				log.append("\n");
				log.append(FORMATTER.format(currentDate));
				log.append(tag);
				log.append(" | ");
				log.append(type);
				log.append(" | ");
				log.append(message);
				log.append("\n");
				FileManager.write(destiny,log.toString());
				
			}
		}
		if(type== MESSAGE)
			Log.v(tag, message);
		else if(type==WARNING)
			Log.w(tag, message);
		else if(type==EXCEPTION)
			Log.e(tag, message);
		else 
			Log.e(tag, message);
		
	}
	public static void setNameLog(String name){
		LOG_FILE_NAME=name;
	}
	public static void log(String type,String message){
		log(type, LOG, message);
	}
	
	public static void log(String message){
		log(MESSAGE, LOG, message);
	}
	public static void setDebug(boolean isDebug){
		DEBUG=isDebug;
	}
	public static boolean isDebug(){
		return DEBUG;
	}
}
