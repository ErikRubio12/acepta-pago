package mx.com.bancoazteca.util;

public class GenericException extends Exception {
	private static final long serialVersionUID = 5088353614053517953L;
	private int code=-1;
	private String cause;
	public GenericException(String message, int code, String cause) {
		// TODO Auto-generated constructor stub
		super(message);
		this.cause=cause;
		this.code=code;
	}
	public GenericException(String message,String cause) {
		// TODO Auto-generated constructor stub
		this(message,-1,cause);
	}
	public String getClassCause(){
		return cause;
	}
	public int getCode(){
		return code;
	}
	@Override
	public String toString() {
		return getClassCause()+" [code=" + getCode()+ ", message=" + getMessage()
				+ "]";
	}
}
