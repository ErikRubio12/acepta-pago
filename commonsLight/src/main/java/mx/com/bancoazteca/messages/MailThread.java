package mx.com.bancoazteca.messages;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import android.content.Context;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;
import mx.commons.R;

public class MailThread extends Thread implements MessageListener<Message, String>{
	private static final String MAIL_THREAD_TAG = "MAIL THREAD";
	private MessageListener<Message, String> listener;
	private Context context;
	private EmailBean email;
	private Message message;
	public MailThread(MessageListener<Message, String> listener, Context context) {
		// TODO Auto-generated constructor stub
		if(listener!=null)
			this.listener=listener;
		else
			this.listener=this;
		this.context=context;
		this.message= new Message();
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		Mail mail;
		try {
			if(email==null){
				message.setType(Message.CANCEL);
				message.setTextMessage(R.string.wrongEmail);
				listener.sendMessage(null,message);
				return;
			}
				
			mail = new Mail(context,email);
			if(!mail.send()){
				message.setType(Message.CANCEL);
				message.setTextMessage(R.string.unableToSendMail);
				listener.sendMessage(null,message);
			}
				
			else{
				message.setType(Message.OK);
				message.setTextMessage(R.string.sentSuccess);
				listener.sendMessage(null,message);
			}
				
		} catch (GenericException e1) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, MAIL_THREAD_TAG,e1.getMessage());
			message.setType(Message.EXCEPTION);
			message.setTextMessage(R.string.mailConfigError);
			listener.sendMessage(null,message);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, MAIL_THREAD_TAG,e.getMessage());
			message.setType(Message.CANCEL);
			message.setTextMessage(R.string.unableToSendMail);
			listener.sendMessage(null,message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, MAIL_THREAD_TAG,e.getMessage());
			message.setType(Message.CANCEL);
			message.setTextMessage(R.string.unableToSendMail);
			listener.sendMessage(null,message);
		} 
	}
	public void setEmail(EmailBean email) {
		this.email = email;
	}
	@Override
	public void sendMessage(Context ctx,Message message, String parameter) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, MAIL_THREAD_TAG,"sendMessage");
	}
	@Override
	public void sendStatus(Context ctx,int status) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, MAIL_THREAD_TAG,"sendStatus");
	}
	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, MAIL_THREAD_TAG,"sendMessage");
	}
}
