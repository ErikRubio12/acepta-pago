 package mx.com.bancoazteca.messages;

import java.io.File;
import java.io.IOException;
import java.util.Date; 
import java.util.Properties; 
import javax.activation.CommandMap; 
import javax.activation.DataHandler;
import javax.activation.DataSource; 
import javax.activation.FileDataSource; 
import javax.activation.MailcapCommandMap; 
import javax.mail.BodyPart; 
import javax.mail.MessagingException;
import javax.mail.Multipart; 
import javax.mail.PasswordAuthentication;
import javax.mail.Session; 
import javax.mail.Transport; 
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress; 
import javax.mail.internet.MimeBodyPart; 
import javax.mail.internet.MimeMessage; 
import javax.mail.internet.MimeMultipart; 


import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;
import mx.commons.R;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;

public class Mail extends javax.mail.Authenticator { 
  
  private boolean auth=true; 
  private EmailBean email;
  private Multipart multipart; 
  public final static String DEFAULT_SMTP_SERVER="smtpServer";
  public final static String DEFAULT_SMTP_PORT="smtpPort";
  public final static String DEFAULT_SOCKET_PORT="socketPort";
  public final static String USER="user";
  public final static String PASSWORD="password";
  public final static String HTML="html";
  public final static String XML="xml";
  public final static String PLAIN="plain";
  public static String IMAGE_TYPE="image/jpeg";
  public static String TEXT_RFC882_TYPE="message/rfc882";
  public final static String MULTIPART="multipart";
  public final static String MESSAGE="message";
  private static final String MAIL_CONFIG = "mailConfig.properties";
  private Properties configProps= new Properties();	
  public final static String UTF_8="UTF-8";
  public final static String ISO_8859_1="ISO-8859-1";
  public final static String US_ASCII="US-ASCII";
  private static final String MAIL_TAG = "MAIL";
private static final String MAIL = "Mail";
  public Mail(Context context,EmailBean email) throws GenericException { 
	  this.email=email;
	  auth = true; // smtp authentication - default on 
	  multipart = new MimeMultipart(); 
	  try {
		  configProps.load(Utility.loadFileAsset(context,MAIL_CONFIG));
		  // There is something wrong with MailCap, javamail can not find a handler for the multipart/mixed part, so this bit needs to be added. 
		  MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap(); 
		  PreferenceManager.getDefaultSharedPreferences(context);
		  mc.addMailcap(configProps.getProperty(HTML)); 
		  mc.addMailcap(configProps.getProperty(XML)); 
		  mc.addMailcap(configProps.getProperty(PLAIN)); 
		  mc.addMailcap(configProps.getProperty(MULTIPART)); 
		  mc.addMailcap(configProps.getProperty(MESSAGE)); 
		  CommandMap.setDefaultCommandMap(mc); 
		  this.email.setEmailFrom(configProps.getProperty(USER));
	  } catch (IOException e) {
		  // TODO Auto-generated catch block
		  throw new GenericException(e.getMessage(), MAIL_CONFIG + " doesnt exist");
	  }
	  
	 
  } 
  public synchronized static void sendByIntent(EmailBean email, Context context) throws GenericException{

	  Logger.log(Logger.MESSAGE, MAIL, email.toString() );
	  if(email.getAttachedCount()>EmailBean.MAX_ATTACHED_ELEMENTS)
		  throw new GenericException("The max of attached elemnts its: "+ EmailBean.MAX_ATTACHED_ELEMENTS, "mail");
	  else{
		  Intent intent = new Intent(Intent.ACTION_SEND); 
		  intent.setData(Uri.parse("mailto:"));
//		  intent.setType("message/rfc822");
		  String[] tos= new String[email.getDestiniesCount()];
		  for (int i=0;i<email.getDestiniesCount();i++) {
			  tos[i]=email.getDestiny(i);
		  }

		  intent.putExtra(Intent.EXTRA_EMAIL, tos); 
		  intent.putExtra(Intent.EXTRA_TITLE, context.getResources().getString(R.string.chooseEmail)); 
		  intent.putExtra(Intent.EXTRA_TEXT, email.getMessage()); 
		  intent.putExtra(Intent.EXTRA_SUBJECT, email.getSubject()); 
		  intent.setType(email.getType());
		  if(!email.isAttachedEmpty()){
			  for (int i = 0; i < email.getAttachedCount(); i++) {
				  Uri uri=Uri.fromFile(email.getAttached(i));
				  intent.putExtra(Intent.EXTRA_STREAM,uri); 
			  }
		  }
		  context.startActivity( Intent.createChooser(intent, context.getResources().getString(R.string.chooseEmail)));
//		  context.startActivity(intent);
	  }
	 
  }
  public synchronized boolean send() throws AddressException,MessagingException{ 
    Properties props = setMailProperties(); 
 
    if(!configProps.getProperty(USER).equals("") && !configProps.getProperty(PASSWORD).equals("") && email.getDestinies().size() > 0 /*&& !email.getEmailFrom().equals("") */) {  
    	Session session = Session.getInstance(props, this); 
    	MimeMessage msg = new MimeMessage(session); 
    	
		msg.setFrom(new InternetAddress(email.getEmailFrom()));
	
    	InternetAddress[] addressTo = new InternetAddress[email.getDestinies().size()]; 
    	for (int i = 0; i < email.getDestinies().size(); i++) 
    		addressTo[i] = new InternetAddress(email.getDestiny(i)); 
    	
    	for (int i = 0; i < email.getAttachedCount(); i++){ 
			try {
				addAttachment(email.getAttached(i));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Logger.log(Logger.EXCEPTION,MAIL_TAG, "Error al adjuntar archivo: " +e.getMessage());
				
			}
    	}
		
    	msg.setRecipients(MimeMessage.RecipientType.TO, addressTo); 

    	msg.setSubject(email.getSubject()); 
    	msg.setSentDate(new Date()); 
    	
    	// setup message body 
    	BodyPart messageBodyPart = new MimeBodyPart(); 
    	messageBodyPart.setText(email.getMessage()); 
    	multipart.addBodyPart(messageBodyPart); 

    	// Put parts in message 
    	msg.setContent(multipart); 

    	// send email 
    	Transport.send(msg); 

    	return true; 
    } else 
    	return false; 
    
  } 
 
  private void addAttachment(File filename) throws Exception { 
    BodyPart messageBodyPart = new MimeBodyPart(); 
    DataSource source = new FileDataSource(filename); 
    messageBodyPart.setDataHandler(new DataHandler(source)); 
    messageBodyPart.setFileName(filename.getName()); 
    multipart.addBodyPart(messageBodyPart); 
  } 
 
  @Override 
  public PasswordAuthentication getPasswordAuthentication() { 
    return new PasswordAuthentication(configProps.getProperty(USER), configProps.getProperty(PASSWORD)); 
  } 
 
  private Properties setMailProperties() { 
    Properties props = new Properties(); 
    props.put("mail.smtp.host", configProps.getProperty(DEFAULT_SMTP_SERVER)); 
    if(auth)
      props.put("mail.smtp.auth", "true"); 
    props.put("mail.smtp.port", configProps.getProperty(DEFAULT_SMTP_PORT)); 
    props.put("mail.smtp.socketFactory.port", configProps.getProperty(DEFAULT_SOCKET_PORT)); 
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); 
    props.put("mail.smtp.socketFactory.fallback", "false"); 
 
    return props; 
  }

} 