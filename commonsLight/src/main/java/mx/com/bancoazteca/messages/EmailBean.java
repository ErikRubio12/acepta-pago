package mx.com.bancoazteca.messages;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;

public class EmailBean implements Serializable{
	private static final long serialVersionUID = 1L;
	public final static int DEFAULT_DESTINIES_ELEMENTS=3;
	public final static int MAX_ATTACHED_ELEMENTS=3;
	private List<String> emailTo=new ArrayList<String>(DEFAULT_DESTINIES_ELEMENTS);
	private String emailFrom;
	private String subject="";
	private String message="";
	private String type="text/plain";
	public static final String EMAIL = "Mail";
	protected static final int MIN_LENGTH = 3;
	private  List<File> pathList = new ArrayList<File>(MAX_ATTACHED_ELEMENTS);
	
	public EmailBean() {}
	
	public EmailBean(String emailFrom, String subject, String message) throws GenericException {
		super();
		this.emailFrom = emailFrom;
		this.subject = subject;
		this.message = message;
		if(!validEmail(emailFrom)){
			StringBuffer messageException= new StringBuffer("Email: ");
			messageException.append(emailFrom);
			messageException.append(" ");
			messageException.append("invalido");
			throw new GenericException(messageException.toString(), EMAIL);
		}
			
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer b= new StringBuffer();
		b.append("de: ");
		b.append(emailFrom);
		b.append(" , ");
		b.append("subject: ");
		b.append(subject);
		b.append(" , ");
		b.append("tipo: ");
		b.append(type);
		b.append(" , ");
		b.append("para: ");
		for (int i = 0; i < emailTo.size(); i++) {
			b.append(emailTo.get(i));
			b.append(" , ");
		}
		b.append("\nArchivos: ");
		for (int i = 0; i < pathList.size(); i++) {
			b.append(pathList.get(i));
			b.append(" , ");
		}
		return b.toString();
	}
	public List<String> getDestinies() {
		return emailTo;
	}
	public String getDestiny(int index){
		return emailTo.get(index);
	}
	public int getDestiniesCount(){
		return emailTo.size();
	}
	public void removeDestinies(){
		if(emailTo.size()>0)
			emailTo.clear();
	}
	public void addDestiny(String destiny) throws GenericException{
		if(!validEmail(destiny)){
			StringBuffer messageException= new StringBuffer("Email: ");
			messageException.append(emailFrom);
			messageException.append(" ");
			messageException.append("invalido");
			throw new GenericException(messageException.toString(), EMAIL);
		}
		emailTo.add(destiny.toLowerCase(Locale.getDefault()));
	}
	public void setDestinies(List<String>  destinies) throws GenericException {
		for (String currentDestiny : destinies) {
			
			if(!validEmail(currentDestiny)){
				StringBuffer messageException= new StringBuffer("Email: ");
				messageException.append(emailFrom);
				messageException.append(" ");
				messageException.append("invalido");
				throw new GenericException(messageException.toString(), EMAIL);
			}
			emailTo.add(currentDestiny.toLowerCase(Locale.getDefault()));
		}
			
		
		this.emailTo = destinies;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) throws GenericException {
		
		if(!validEmail(emailFrom)){
			StringBuffer messageException= new StringBuffer("Email: ");
			messageException.append(emailFrom);
			messageException.append(" ");
			messageException.append("invalido");
			throw new GenericException(messageException.toString(), EMAIL);
		}
		this.emailFrom = emailFrom.toLowerCase(Locale.getDefault());
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public File getAttached(int index) {
		return pathList.get(index);
	}
	public void addAttached(File atached) {
		this.pathList.add(atached);
	}
	public boolean isAttachedEmpty(){
		return pathList.isEmpty();
	}
	public int getAttachedCount() {
		// TODO Auto-generated method stub
		return pathList.size();
	}
	public static boolean validEmail(String email){
		// TODO Auto-generated method stub
		int index=-1;
		String dominio="";
	
		if(email==null){
			Logger.log("Pas 1");
			return false;
		}
			
		else if(email.length()<MIN_LENGTH){
			Logger.log("Pas 2");
			return false;
		}
		else if((index=email.indexOf("@"))<0){
			Logger.log("Pas 3");
			return false;
		}
		else if(email.subSequence(0, index).length()==0){
			Logger.log("Pas 4: " + index);
			return false;
		}
		else if((index=((dominio=email.substring(index +1)).indexOf("."))) <0){
			Logger.log("Pas 5: "+ index + "  " + dominio);
			return false;	
		}
		else if(dominio.substring(0, index).length()==0){
			Logger.log("Pas 6: "+ index + "  " + dominio);
			return false;	
		}
		else
			return true;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
}
