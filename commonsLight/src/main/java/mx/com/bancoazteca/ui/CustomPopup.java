package mx.com.bancoazteca.ui;


import mx.com.bancoazteca.util.Utility;
import mx.commons.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CustomPopup extends GenericPopUp {
	private LinearLayout popupContainer;
	private LinearLayout buttonContainer;
	private TextView popupTitle;
	private TextView popupMessage;
	private ImageView popupIcon;
	private ProgressBar progressBar;
	
	
	public CustomPopup(Context appContext,int title,String message) {
		super(appContext,title,message);
	}
	public CustomPopup(Context appContext,int title, int message,int icon) {
		// TODO Auto-generated constructor stub
		super(appContext,title,message,icon);
	}
	public CustomPopup(Context appContext,int title, int message) {
		// TODO Auto-generated constructor stub
		super(appContext,title,message);
	}
	public CustomPopup(int title, int message,int icon) {
		// TODO Auto-generated constructor stub
		super(title,message,icon);
	}
	public CustomPopup(Context ctx) {
		// TODO Auto-generated constructor stub
		super(ctx);
	}
	@Override
	public Displayable create() {
		// TODO Auto-generated method stub
		if(popupContent<0){
			super.create();
		}
		else{
			dialog= new Dialog(appContext);
			dialog.setCancelable(isCancelable);
			dialog.setContentView(popupContent);
			dialog.setOnCancelListener(cancelListener);
			if(title>0)
				dialog.setTitle(title);
		}
		return this;
	}
	@Override
	protected void generate() {
		// TODO Auto-generated method stub
	}
	@Override
	protected void initPopup() {
		// TODO Auto-generated method stub
		dialog= new Dialog(appContext);
		dialog.setCancelable(isCancelable);
		
		popupContainer=(LinearLayout) Utility.inflateLayout(appContext, R.layout.popup_progress, null);
		buttonContainer=(LinearLayout) popupContainer.findViewById(R.id.buttonsContainer);
		popupTitle=(TextView) popupContainer.findViewById(R.id.popupTitle);
		popupMessage=(TextView) popupContainer.findViewById(R.id.popupMessage);
		progressBar=(ProgressBar) popupContainer.findViewById(R.id.popupProgressBar);
		popupIcon=(ImageView) popupContainer.findViewById(R.id.popupIcon);
		optionList=(ListView) popupContainer.findViewById(R.id.selection);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(popupContainer);
		dialog.setOnCancelListener(cancelListener);
		if(type!=PROGRESS_INFINIT)
			progressBar.setVisibility(View.GONE);
		else if(type == SELECTION && adapter != null){
			progressBar.setVisibility(View.GONE);
			popupMessage.setVisibility(View.GONE);
			optionList.setVisibility(View.VISIBLE);
			optionList.setAdapter(adapter);
			if(clickItemListener!= null)
				optionList.setOnItemClickListener(clickItemListener);
		}
	}
	@Override
	public void updateText() {
		// TODO Auto-generated method stub
		super.updateText();
	}
	@Override
	protected void addListeners() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void addIcon() {
		// TODO Auto-generated method stub
		if(icon>0)
			popupIcon.setImageResource(getIcon());
		else if (icon==WARNING)
			popupIcon.setImageResource(android.R.id.icon);
		else if(icon==ERROR)
			popupIcon.setImageResource(android.R.id.icon1);
	}
	
	@Override
	protected void setText() {
		// TODO Auto-generated method stub
		if(getTitle()>0)
				popupTitle.setText(getTitle());
		else if (getTitle()==WARNING)
			popupTitle.setText(R.string.defaultWarningPopUpTitle);
		else if(getTitle()==ERROR)
			popupTitle.setText(R.string.defaultErrorPopUpTitle);
		else if(getTitle()==MESSAGE)
			popupTitle.setText(R.string.defaultMessagePopUpTitle);
		else
			popupTitle.setText("");
		if(messageString ==null){
			if(getMessage()>0)
				popupMessage.setText(getMessage());
			else if (getMessage()==WARNING)
				popupMessage.setText(R.string.defaultWarningPopUpMessage);
			else if(getMessage()==ERROR)
				popupMessage.setText(R.string.defaultErrorPopUpMessage);
			else
				popupMessage.setText("");

		}
		else
			popupMessage.setText(messageString);
	}
	@Override
	protected void setButtons() {
		// TODO Auto-generated method stub
		Button viewButton;
		if(buttons.size()>2 || layout==VERTICAL_LAYOUT)
			buttonContainer.setOrientation(LinearLayout.VERTICAL);
		
		for (GenericButton currentButton : buttons) {
			viewButton= new Button(appContext);
			viewButton.setText(currentButton.getText());
			viewButton.setOnClickListener(((ViewButton)currentButton).getListener());
			if(currentButton.getTag() != null)
				viewButton.setTag(currentButton.getTag());
			if(layout==VERTICAL_LAYOUT){
				viewButton.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			}
			
			buttonContainer.addView(viewButton);
		}
	}
	public void setType(int type) {
		if(dialog!=null && progressBar !=null){
			if(type==DEFAULT)
				progressBar.setVisibility(View.GONE);
		}
		this.type = type;
	}
	public int getType() {
		return type;
	}
	@Override
	public void show() {
		// TODO Auto-generated method stub
		dialog.show();
	}
}
