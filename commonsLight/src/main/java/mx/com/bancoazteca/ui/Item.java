package mx.com.bancoazteca.ui;

import android.view.View.OnClickListener;

public class Item {

	protected int id;
	public Item() {
		super();
	}
	
	public Item(int id, OnClickListener clickListener) {
		super();
		this.id = id;
	
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	
}