package mx.com.bancoazteca.ui;

import android.view.View.OnClickListener;

public class ViewButton extends GenericButton{
	private OnClickListener listener;
	public ViewButton() {
		// TODO Auto-generated constructor stub
	}
	public ViewButton(OnClickListener listener) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
	}
	public void setListener(OnClickListener listener) {
		this.listener = listener;
	}

	public OnClickListener getListener() {
		return listener;
	}
}
