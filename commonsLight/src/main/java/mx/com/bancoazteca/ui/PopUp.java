package mx.com.bancoazteca.ui;

import mx.commons.R;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;

public class PopUp extends GenericPopUp{
	private Builder builder;
	protected OnCancelListener cancelListener;
	
	public PopUp(Context appContext,int title,String message) {
		super(appContext,title,message);
	}
	public PopUp(Context appContext,int title, int message,int icon) {
		// TODO Auto-generated constructor stub
		super(appContext,title,message,icon);
	}
	public PopUp(Context appContext,int title, int message) {
		// TODO Auto-generated constructor stub
		super(appContext,title,message);
	}
	public PopUp(int title, int message,int icon) {
		// TODO Auto-generated constructor stub
		super(title,message,icon);
	}
	public PopUp(Context ctx) {
		// TODO Auto-generated constructor stub
		super(ctx);
	}
	public Displayable create() {
		// TODO Auto-generated method stubB
		initPopup();
		setButtons();
		setText();
		addIcon();
		addListeners();
		generate();
		return this;
	}
	
	protected void generate() {
		// TODO Auto-generated method stub
		if(this.type!=PROGRESS_INFINIT)
			dialog=builder.create();	
	}
	@Override
	public void show() {
		// TODO Auto-generated method stub
		dialog.show();
	}
	protected void initPopup() {
		// TODO Auto-generated method stub
		if(type==PROGRESS_INFINIT){
			dialog= new ProgressDialog(appContext);
			((ProgressDialog)dialog).setIndeterminate(true);
			dialog.setCancelable(isCancelable);
		}
			
		else{
			builder= new AlertDialog.Builder(appContext);
			builder.setCancelable(isCancelable);
		}
		
		
	}
	protected void addListeners() {
		// TODO Auto-generated method stub
		if(this.type!= PROGRESS_INFINIT){
			if(cancelListener!=null)
				builder.setOnCancelListener(cancelListener);
		}
		else{
			if(cancelListener!=null)
				((ProgressDialog)dialog).setOnCancelListener(cancelListener);
		}
	}
	protected void addIcon() {
		// TODO Auto-generated method stub
		if(this.type!=PROGRESS_INFINIT){
			if(icon>0)
				builder.setIcon(getIcon());
			else if (icon==WARNING)
				builder.setIcon(android.R.id.icon);
			else if(icon==ERROR)
				builder.setIcon(android.R.id.icon1);
		}
		else{
			if(icon>0)
				((ProgressDialog)dialog).setIcon(getIcon());
			else if (icon==WARNING)
				((ProgressDialog)dialog).setIcon(android.R.id.icon);
			else if(icon==ERROR)
				((ProgressDialog)dialog).setIcon(android.R.id.icon1);
		}
		
	}
	protected void setText() {
		// TODO Auto-generated method stub
		if(type==PROGRESS_INFINIT){
			if(getTitle()>0)
				((ProgressDialog)dialog).setTitle(getTitle());
			else if (getTitle()==WARNING)
				((ProgressDialog)dialog).setTitle(R.string.defaultWarningPopUpTitle);
			else if(getTitle()==ERROR)
				((ProgressDialog)dialog).setTitle(R.string.defaultErrorPopUpTitle);
			else if(getTitle()==MESSAGE)
				((ProgressDialog)dialog).setTitle(R.string.defaultMessagePopUpTitle);
			if(messageString ==null){
				((ProgressDialog)dialog).setMessage("");
				if(getMessage()>0)
					((ProgressDialog)dialog).setMessage(appContext.getResources().getString(getMessage()));
				else if (getMessage()==WARNING)
					((ProgressDialog)dialog).setMessage(appContext.getResources().getString(R.string.defaultWarningPopUpMessage));
				else if(getMessage()==ERROR)
					((ProgressDialog)dialog).setMessage(appContext.getResources().getString(R.string.defaultErrorPopUpMessage));
				
			}
			else
				((ProgressDialog)dialog).setMessage(messageString);
				
		}
		else{
			if(getTitle()>0)
				builder.setTitle(getTitle());
			else if (getTitle()==WARNING)
				builder.setTitle(R.string.defaultWarningPopUpTitle);
			else if(getTitle()==ERROR)
				builder.setTitle(R.string.defaultErrorPopUpTitle);
			else if(getTitle()==MESSAGE)
				builder.setTitle(R.string.defaultMessagePopUpTitle);
			if(messageString ==null){
				if(getMessage()>0)
					builder.setMessage(getMessage());
				else if (getMessage()==WARNING)
					builder.setMessage(R.string.defaultWarningPopUpMessage);
				else if(getMessage()==ERROR)
					builder.setMessage(R.string.defaultErrorPopUpMessage);
				
			}
			else
				builder.setMessage(messageString);
		}
		
	}
	protected void setButtons() {
		// TODO Auto-generated method stub
		for (GenericButton currentButton : buttons) {
			int type=(currentButton).getType();
			if(this.type!=PROGRESS_INFINIT){
				if(type==GenericButton.NEGATIVE)
					builder.setNegativeButton(currentButton.getText(), ((DialogButton) currentButton).getPressEvent());
				else
					builder.setPositiveButton(currentButton.getText(), ((DialogButton) currentButton).getPressEvent());
			}
			else{
				((ProgressDialog)dialog).setButton(appContext.getResources().getString(currentButton.getText()),  ((DialogButton) currentButton).getPressEvent());
			}
			
				
		}
	}
	public void setCancelListener(OnCancelListener cancelListener) {
		this.cancelListener = cancelListener;
	}
}
