package mx.com.bancoazteca.ui;

public interface Displayable {

	public abstract void show();

	public abstract void hide();

	public abstract void dismiss();

	public abstract boolean isVisible();

}