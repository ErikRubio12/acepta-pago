package mx.com.bancoazteca.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class DialogButton  extends GenericButton{
	private OnClickListener listener;
	public DialogButton() {
		// TODO Auto-generated constructor stub
	}
	public DialogButton(DialogInterface.OnClickListener pressEvent) {
		// TODO Auto-generated constructor stub
		this.listener=pressEvent;
	}
	public OnClickListener getPressEvent() {
		return listener;
	}
	public void setListener(DialogInterface.OnClickListener pressEvent) {
		this.listener = pressEvent;
	}
	
}
