package mx.com.bancoazteca.ui;

public class GenericButton extends Item {
	public static int POSITIVE=0;
	public static int NEGATIVE=1;
	protected int type=POSITIVE;
	protected int text;
	private String tag;
	public GenericButton() {
		super();
	}

	public int getText() {
		return text;
	}

	public void setText(int text) {
		this.text = text;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
}