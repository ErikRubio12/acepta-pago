package mx.com.bancoazteca.ui.customviews;

import mx.com.bancoazteca.filters.MascaraDinero;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextDinero extends EditText {
	public static String inicial = "$0.00";

	public EditTextDinero(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setText(inicial);
		this.addTextChangedListener(new MascaraDinero(this));
	}

	public EditTextDinero(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setText(inicial);
		this.addTextChangedListener(new MascaraDinero(this));
	}

	public EditTextDinero(Context context) {
		super(context);
		this.setText(inicial);
		this.addTextChangedListener(new MascaraDinero(this));
	}
	
	
}
