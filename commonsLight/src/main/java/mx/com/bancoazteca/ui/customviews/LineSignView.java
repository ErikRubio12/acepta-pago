package mx.com.bancoazteca.ui.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

public class LineSignView extends TextView {
	private static final int DEFAULT_LINE_HEIGHT = 2;
	private int lineHeight=DEFAULT_LINE_HEIGHT;
	private static int spaceBetweenLineAndText=2;
	private final static int SPACE=3;
	private float aboveBaseLine;
	private float underBaseLine;
	private float textHeight;
	private int lineSize=150;
	private Paint firmPaint;
    public LineSignView(Context context) {
        super(context);
        initParams();
    }
	
	public LineSignView(Context context, AttributeSet attrs) {
		// TODO Auto-generated constructor stub
    	super(context,attrs);
    	initParams();
	}
	public LineSignView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initParams();
	}
	private void initParams() {
		// TODO Auto-generated method stub
		
		firmPaint= new Paint();
		firmPaint.setAntiAlias(true);
		firmPaint.setDither(true);
		firmPaint.setColor(Color.BLACK);
		firmPaint.setStyle(Paint.Style.STROKE);
		firmPaint.setStrokeWidth(lineHeight);
	}
    @Override
    public void onDraw(Canvas canvas) {
    	float center= getWidth() -getPaint().measureText((String) getText());
    	center=center/2;
    	canvas.drawLine(0, SPACE, getWidth(),SPACE, firmPaint);
    	canvas.drawText((String) getText(),center, lineHeight + textHeight, getPaint());
    }
    
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		int width=measureWidth(widthMeasureSpec);
		int height= measureHeight(heightMeasureSpec);
		setMeasuredDimension(width,height);
		
	}
	private int measureHeight(int measureSpec) {
		// TODO Auto-generated method stub
		 float specMode = MeasureSpec.getMode(measureSpec); 
		 float specSize = MeasureSpec.getSize(measureSpec);
		 float result; 
		
		underBaseLine = getPaint().descent();
		aboveBaseLine = getPaint().ascent();
		
		 if (specMode == MeasureSpec.EXACTLY) 
			 result = specSize; 
		 else { 

			 textHeight=(-aboveBaseLine + underBaseLine) ;
			 result=textHeight + lineHeight + spaceBetweenLineAndText;

			 if (specMode == MeasureSpec.AT_MOST) 
				 result = specSize>0?Math.min(result, specSize) :result;
		 }
		return Math.round(result);
	
	}
	private int measureWidth(int measureSpec) {
		// TODO Auto-generated method stub
		 float result = 0;   
		 int specMode = MeasureSpec.getMode(measureSpec); 
		 int specSize = MeasureSpec.getSize(measureSpec);
		 
		 if (specMode == MeasureSpec.EXACTLY) 
			 result = specSize;       
		 
		 else { 
			 result = getPaint().measureText(getText().toString());
			 result=Math.max(result, lineSize);
			 if (specMode == MeasureSpec.AT_MOST)
				 result = Math.max(result, specSize); 
		}        
		return Math.round(result);
	}
	public void setLineHeight(int lineHeight) {
		this.lineHeight = lineHeight;
	}
	public int getLineHeight() {
		return lineHeight;
	}

	
}
