package mx.com.bancoazteca.ui;

public interface TouchEventListener {
	public void moveUp();
	public void moveDown(float xPoint,float yPoint);
	public void drag(float xPoint,float yPoint);
}
