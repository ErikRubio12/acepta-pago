package mx.com.bancoazteca.ui;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public abstract class GenericPopUp implements Displayable{
	public final static int PROGRESS_INFINIT=20;
	public final static int DEFAULT=30;
	public final static int SELECTION=40;
	protected int icon;
	protected int message;
	protected String messageString;
	protected int title;
	protected Dialog dialog;
	protected OnCancelListener cancelListener;
	protected OnItemClickListener clickItemListener;
	protected ArrayAdapter<String> adapter;
	public static int WARNING=-4;
	public static int ERROR=-1;
	public static int ADVICE=-2;
	public static int MESSAGE=-3;
	protected boolean isVisible;
	public static int HORIZONTAL_LAYOUT=22;
	public static int VERTICAL_LAYOUT=23;
	protected int layout=VERTICAL_LAYOUT;
	protected Context  appContext;
	protected boolean isCancelable=true;
	protected List<GenericButton> buttons= new ArrayList<GenericButton>();
	protected abstract void initPopup();
	protected abstract void setButtons();
	protected abstract void setText();
	protected abstract void addIcon();
	protected abstract void addListeners();
	protected abstract void generate();
	protected int type=DEFAULT;
	protected ListView optionList;
	protected int popupContent=-100;
	
	public void setPopupContent(int content){
		this.popupContent=content;
	}
	public void setCancelListener(OnCancelListener cancelListener) {
		this.cancelListener = cancelListener;
		if(dialog!= null)
			dialog.setOnCancelListener(cancelListener);
		
	}
	public void setOnItemClickListener(OnItemClickListener listener){
		this.clickItemListener=listener;
		if(optionList!= null)
			optionList.setOnItemClickListener(clickItemListener);
	}
	
	public void setLayout(int layout) {
		this.layout = layout;
	}
	public void updateText(){
		setText();
	}
	public Displayable create(){
		initPopup();
		setButtons();
		setText();
		addIcon();
		addListeners();
		generate();
		
		return this;
	}
	public GenericPopUp(Context appContext,int title,String message) {
		// TODO Auto-generated constructor stub
		this.appContext= appContext;
		this.title=title;
		this.messageString= message;
	}
	public GenericPopUp(Context appContext,int title, int message,int icon) {
		// TODO Auto-generated constructor stub
		this.title=title;
		this.message= message;
		this.appContext= appContext;
		this.icon=icon;
	}
	public GenericPopUp(Context appContext,int title, int message) {
		// TODO Auto-generated constructor stub
		this.title=title;
		this.message= message;
		this.appContext= appContext;
	}
	public GenericPopUp(int title, int message,int icon) {
		// TODO Auto-generated constructor stub
		this.title=title;
		this.message= message;
		this.icon=icon;
	}
	public GenericPopUp(Context ctx) {
		// TODO Auto-generated constructor stub
		this.appContext=ctx;
	}
	/* (non-Javadoc)
	 * @see mx.com.bancoazteca.ui.Displayable#show()
	 */
	public void show() {
		// TODO Auto-generated method stub
		dialog.show();
		isVisible=true;
	}
	/* (non-Javadoc)
	 * @see mx.com.bancoazteca.ui.Displayable#hide()
	 */
	public void hide(){
		dialog.hide();
	}
	/* (non-Javadoc)
	 * @see mx.com.bancoazteca.ui.Displayable#dismiss()
	 */
	public void dismiss(){
		if (dialog.isShowing()) 
			dialog.dismiss();
	}

	/* (non-Javadoc)
	 * @see mx.com.bancoazteca.ui.Displayable#isVisible()
	 */
	public boolean isVisible() {
		// TODO Auto-generated method stub
		return isVisible;
	}
	public void setAppContext(Activity appContext) {
		this.appContext = appContext;
	}
	public void setType(int type) {
		
		this.type = type;
	}
	public int getType() {
		return type;
	}
	
	public boolean isCancelable() {
		return isCancelable;
	}
	
	public void setCancelable(boolean isCancelable) {
		if(dialog!= null)
			dialog.setCancelable(isCancelable);
		this.isCancelable = isCancelable;
	}
	public void removeButtons(){
		buttons.clear();
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}

	public int getIcon() {
		return icon;
	}
	public void addButton(GenericButton button){
		buttons.add(button);
	}
	public void removeButton(int index){
		buttons.remove(index);
	}
	public GenericButton getButton(int index){
		return buttons.get(index);
	}
	public void setMessage(int message) {
		this.message = message;
	}
	public void setMessage(String message) {
		this.messageString = message;
	}
	public int getMessage() {
		return message;
	}
	public void setTitle(int title) {
		this.title = title;
	}
	public int getTitle() {
		return title;
	}
	public Dialog getDialog(){
		return dialog;
	}
	
	public void setAdapter(ArrayAdapter<String> adapter) {
		this.adapter = adapter;
		if(optionList!= null)
			optionList.setAdapter(adapter);
	}
}
