package mx.com.bancoazteca.ui.customviews;

import mx.com.bancoazteca.ui.TouchEventListener;
import mx.com.bancoazteca.util.Logger;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

public class SignView extends View implements TouchEventListener{
	private Path path;
	protected Paint firmPaint;
	protected float x,y;
	protected Bitmap bitmapImage=null;
	protected Drawable drawable;
	protected Canvas imageCanvas;
	protected static final float TOUCH_TOLERANCE = 4;
	protected static final String SIGN_VIEW = "SIGN VIEW";
	protected int defaultColor=Color.BLACK;
	protected final static int DEFAULT_THICKNESS_LINE=5;
	private int thickness=DEFAULT_THICKNESS_LINE;
	public SignView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		setInitParams();
	}
	public SignView(Context context, AttributeSet attrs, int arg0) {
		super(context, attrs,arg0);
		// TODO Auto-generated constructor stub
		setInitParams();
	}
	public SignView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		setInitParams();
	}
	private void setInitParams() {
		// TODO Auto-generated method stub
		firmPaint= new Paint();
		firmPaint.setAntiAlias(true);
		firmPaint.setDither(true);
		firmPaint.setColor(defaultColor);
		firmPaint.setStyle(Paint.Style.STROKE);
		firmPaint.setStrokeJoin(Paint.Join.ROUND);
		firmPaint.setStrokeCap(Paint.Cap.ROUND);
		firmPaint.setStrokeWidth(thickness);
       
		path= new Path();
	}
	public void changeColor(int color){
		firmPaint.setColor(color);
	}
	public void setPaint(Paint paint){
		this.firmPaint=paint;
	}
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		canvas.drawPath(path, firmPaint);
		
	}
	
	public void setDefaultColor(int defaultColor) {
		this.defaultColor = defaultColor;
	}
	public Bitmap getBitmap(int width , int height){
		Logger.log(Logger.MESSAGE, SIGN_VIEW, "ancho: "+ width +  " alto: " +height);
		bitmapImage= Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
		imageCanvas=new Canvas(bitmapImage);
		imageCanvas.clipPath(path);
		imageCanvas.drawPath(path, firmPaint);
		return bitmapImage;
	}
	public Bitmap getBitmap(){
		return getBitmap(getWidth(), getHeight());
	}
	public void setBitmap(Bitmap b){
		this.bitmapImage=b;
	}
//	isFirstTIme=false;
	@Override
	public void moveUp() {
		// TODO Auto-generated method stub
		path.lineTo(x, y);
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	@Override
	public void moveDown(float x, float y) {
		// TODO Auto-generated method stub
		path.moveTo(x, y);
		path.addCircle(x, y, 1f, Direction.CW);
		this.x = x;
		this.y = y;
		
	}
	@Override
	public void drag(float xPoint,float yPoint) {
		// TODO Auto-generated method stub
		 float dx = Math.abs(xPoint - x);
         float dy = Math.abs(yPoint - y);
         if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
             path.quadTo(x, y, (xPoint + x)/2, (yPoint + y)/2);
             x = xPoint;
             y = yPoint;
         }
	}
	public void cleanScreen(){
		path.reset();
	}
	public void setThickness(int thickness) {
		this.thickness = thickness;
		firmPaint.setStrokeWidth(thickness);
	}
	public int getThickness() {
		return thickness;
	}
	public boolean isEmpty() {
		return path.isEmpty();
	}
	public Path getPath() {
		return path;
	}
	public void setPath(Path path) {
		this.path = path;
	}
	
}
