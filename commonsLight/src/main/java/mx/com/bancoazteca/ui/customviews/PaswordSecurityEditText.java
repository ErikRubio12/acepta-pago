package mx.com.bancoazteca.ui.customviews;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.SeekBar;

public class PaswordSecurityEditText  extends EditText{
	public static int inicial = 0;
	private SeekBar securityBar=null;
	private RulePasswordWatcher watcher;
	public PaswordSecurityEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		watcher=new RulePasswordWatcher(securityBar);
		this.addTextChangedListener(watcher);
	}

	public PaswordSecurityEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		watcher=new RulePasswordWatcher(securityBar);
		this.addTextChangedListener(watcher);
	}

	public PaswordSecurityEditText(Context context) {
		super(context);
		watcher=new RulePasswordWatcher(securityBar);
		this.addTextChangedListener(watcher);
	}

	public void setSecurityBar(SeekBar securityBar) {
		this.securityBar = securityBar;
		this.securityBar.setMax(100);
		this.securityBar.setProgress(4);
		this.securityBar.setEnabled(false);
		this.securityBar.setFocusable(false);
		this.securityBar.setFocusableInTouchMode(false);
		this.watcher.setBar(securityBar);
	}
	public class RulePasswordWatcher implements TextWatcher {
		private final int MIN = 4;
		private final int MEDIUM = 50;
		private final int MAX = 96;
		private int maxLowLevel = 6;
		private int minLowLevel = 1;
		private int maxMediumLevel = 10;
		private int minMediumLevel = 7;
		private int highLevel = 11;
		private SeekBar bar;
		public RulePasswordWatcher(SeekBar bar) {
			this.bar=bar;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			int numChars=0;
			int numDigits=0;
			for (int i = 0; i < s.length(); i++) {
				if (Character.isLetter( s.charAt(i))) 
					numChars++;
				else if (Character.isDigit( s.charAt(i))) 
					numDigits++;
			}
			if (s.length()>=minLowLevel && s.length()<=maxLowLevel) {
				if (numChars==s.length() || numDigits==s.length()) 
					if(bar!=null)
						bar.setProgress(MIN);
			}
			else if (s.length()>=minMediumLevel && s.length()<=maxMediumLevel) {
				if (numChars>0 && numDigits>0) 
					if(bar!=null)
						bar.setProgress(MEDIUM);
			}
			else if (s.length()>=highLevel){
				if (numChars>0 && numDigits>0) 
					if(bar!=null)
						bar.setProgress(MAX);
			}
			else
				if(bar!=null)
					bar.setProgress(MIN);
		}

		@Override
		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub

		}

		

		public void setMinMediumLevel(int minMediumLevel) {
			this.minMediumLevel = minMediumLevel;
		}

		public void setMaxMediumLevel(int maxMediumLevel) {
			this.maxMediumLevel = maxMediumLevel;
		}

		public void setMinLowLevel(int minLowLevel) {
			this.minLowLevel = minLowLevel;
		}

		public void setMaxLowLevel(int maxLowLevel) {
			this.maxLowLevel = maxLowLevel;
		}

		public void setBar(SeekBar bar) {
			this.bar = bar;
		}

	}
}
