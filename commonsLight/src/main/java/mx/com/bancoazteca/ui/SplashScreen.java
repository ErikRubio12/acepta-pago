package mx.com.bancoazteca.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Class Activity that controls the progress dialog 
 * @author NFLORE
 *
 */
public class SplashScreen extends BaseActivity{
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	protected int frameIMageViewId;
	protected int textViewId=-1;
	protected int animResource;
	protected boolean animText=true;
	private ImageView frame;
	private TextView text;
	private String originalText;
	private  int position=0;
	private final static int CHANGE_TEXT=22;
	protected AnimationDrawable ani ;
	private final static int INTERVAL=1000;
	private int counter=1;
	private boolean updatedText=true;
	protected boolean puntos=false;
	private Message msg;
	private Timer textAnimator= new Timer();
	private Task task;
	private SplashMessageHandler handler= new SplashMessageHandler();

	protected void bringToFront(){
		loadSplashScreen();
		if(textViewId>-1)
			originalText=(String) text.getText();
		if(originalText.indexOf("...")>=0){
			puntos=true;
			position=originalText.indexOf("...");
		}
		
		frame.post(new Runnable() {
			public void run() {
				// TODO Auto-generated method stub
				ani.start();
			}
		});
		if(animText){
			textAnimator=new Timer();
			task= new Task();
		}
			
	}
	private void cleanAnim(){
		counter=1;
		if(textAnimator!=null){
			textAnimator.cancel();
			textAnimator=null;
		}
		if(task!= null){
			task.cancel();
			task = null;
		}
	}
	
	/**
	 * Load the animation shown in the progressDialog
	 */
	protected void loadSplashScreen() {
		
		frame = (ImageView) findViewById(frameIMageViewId);
		frame.setImageResource(animResource);
		if(textViewId>-1)
			text = (TextView) findViewById(textViewId);
		ani = (AnimationDrawable) frame.getDrawable();
		ani.setVisible(true, false);
		
	}
	
	
	/**
	 * Sets the configuration to the cycle that shows the animation
	 */
	private void animText(){
			textAnimator.schedule(task, 0, INTERVAL);
	}
	
	/**
	 * Change the text the progressDialog shows
	 * @param text String new text to show in the pop-up 
	 */
	protected void changeText(String text){
		msg = Message.obtain();
        msg.arg1=CHANGE_TEXT;
        msg.obj=text;
        handler.sendMessage(msg);
		
	}
	/**
	 * Inits the animation 
	 */
	protected void startAni(){
		if(animText)
			animText();
		
	}
	
	/**
	 * Stop playing the animation
	 */
	protected void stopAni() {
		if(animText){
			if(textAnimator != null)
				textAnimator.cancel();
		}
		if(ani!=null)
			ani.stop();
		cleanAnim();
		if (frame != null)
			frame.clearAnimation();
		
		
		ani=null;
		frame=null;
	}
	
	
	protected void setTextColor(int color){
		text.setTextColor(color);
	}
	protected void setBackgroundText(int color){
		text.setBackgroundColor(color);
	}
	protected void showSplash() {
		ani.setVisible(true, false);
		frame.setVisibility(View.VISIBLE);
		
	}
	protected void showText(){
		text.setVisibility(View.VISIBLE);
	}
	
	protected void hideSplash() {
		ani.setVisible(false, false);
		frame.setVisibility(View.GONE);
		
	}
	protected void hideText() {
		text.setVisibility(View.GONE);
	}
	public void setTextAnimation(boolean isAnimable){
		animText=isAnimable;
	}
	//*************INNER CLASS*************************+
	private final class Task extends TimerTask{
		private String tempText="";
		@Override
		public void run() {
			// TODO Auto-generated method stub
			 
			if(updatedText && puntos){
				updatedText=false;
				tempText=originalText.substring(0, position);
			}
			
			if(puntos){
				if(counter==1)
					tempText+=".";
				else if(counter==2)
					tempText+="..";
				else if(counter==3){
					tempText+="...";
					counter=-1;
				}
				counter++;	
				changeText(tempText);
			}
		}
		
	}
	private final class SplashMessageHandler extends Handler{
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.arg1) {
			case CHANGE_TEXT:
				if(msg.obj!= null)
					text.setText((CharSequence) msg.obj);
				updatedText=true;
				break;
			default:
				break;
			}
			
		}
	}
}