package mx.com.bancoazteca.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Class builder a pop-up (progressDialog) and lets configure it.
 * @author NFLORE
 *
 */
public class Splash {

	protected ProgressDialog progressDialog;
	protected Activity activity;
	private String text;
	private String title;
	private Context context;
	public final static int END=0;
	public final static int SHOW=1;
	public final static int HIDE=2;
	private boolean isCancelable=true;
	
	/**
	 * Class Constructor Init a progressDialog (pop-up)
	 * @param context Context activity
	 * @param text String text to show in the pop-up 
	 */
	public Splash(Activity context,String text) {
		// TODO Auto-generated constructor stub
		this.text= text;
		this.activity=context;
		this.context = context;
		progressDialog = new ProgressDialog(this.context){
			
			public void onBackPressed() {
				if(isCancelable)
					activity.finish();
			};
		};
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMax(100);
		progressDialog.setMessage(text);

	}
	
	/**
	 * Sets a progressDialog cancelable with the BACK key and the class flag 
	 * @param isCancelable boolean true: cancelable, false: NOT cancelable
	 */
	public void setCancelable(boolean isCancelable){
		this.isCancelable=isCancelable;
		progressDialog.setCancelable(isCancelable);
	}
	
	
	/**
	 * Add a Cancel button to the progress Dialog (pop-up) and don't let it be cancelable
	 * @param action onClickListener contains the action is going to  do the button when clicks
	 */
	public void addCancelButton(DialogInterface.OnClickListener action){
		progressDialog.setCancelable(false);
		progressDialog.setButton("Cancelar    ",action);
	}
	
	/**
	 * Sets the title to the pop-up
	 * @param title String with the title to the pop-up
	 */
	public void setTitle(String title) {
		this.title = title;
		progressDialog.setTitle(this.title);
	}
	
	/**
	 * Sets the message to the pop-up
	 * @param text String message is going to show's the pop-up
	 */
	public void setText(String text){
		this.text= text;
		progressDialog.setMessage(this.text);
	}
	
	/**
	 * Show the progress dialog
	 */
	public void show() {
		progressDialog.show();
		//ProgressDialog.show(context, title, text);
	}
	
	/**
	 * Hide the pop-up
	 */
	public void hide(){
		progressDialog.hide();
	}
	
	
	/**
	 * 	 Remove the dialog from the screen
	 */
	public void finish(){
		progressDialog.dismiss();
	}
	
	
	/**
	 * Tells if the dialog is showing himself
	 * @return boolean true: is showed, false: not show
	 */
	public boolean isVisible() {
		return progressDialog.isShowing();
	}
}