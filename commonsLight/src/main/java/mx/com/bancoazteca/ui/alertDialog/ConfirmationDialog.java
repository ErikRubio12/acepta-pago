package mx.com.bancoazteca.ui.alertDialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import mx.commons.R;

/**
 * Created by marcoantonio on 26/11/15.
 */
public class ConfirmationDialog extends DialogFragment{
    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_MESSAGE = "ARG_MESSAGE";
    private String mTitle;
    private String mMessage;
    private AcceptListener mAcceptListener;
    private CancelListener mCancelListener;
    private DismissListener mDismissListener;

    public static ConfirmationDialog newInstance(String title,String message) {
        
        Bundle args = new Bundle();
        args.putString(ARG_TITLE,title);
        args.putString(ARG_MESSAGE,message);
        ConfirmationDialog fragment = new ConfirmationDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments()!=null){
            mTitle=getArguments().getString(ARG_TITLE);
            mMessage=getArguments().getString(ARG_MESSAGE);
        }
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setPositiveButton(mx.commons.R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mAcceptListener != null) {
                            mAcceptListener.onAccept(dialog);
                        }else {
                            dialog.dismiss();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mCancelListener != null) {
                            mCancelListener.onCancel(dialog);
                        }else {
                            dialog.dismiss();
                        }
                    }
                })
                .setCancelable(true);
        AlertDialog dialog=builder.create();
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mDismissListener!=null){
            mDismissListener.onDismiss();
        }
    }


    public void setmAcceptListener(AcceptListener mAcceptListener) {
        this.mAcceptListener = mAcceptListener;
    }


    public void setmCancelListener(CancelListener mCancelListener) {
        this.mCancelListener = mCancelListener;
    }

    public void setmDismissListener(DismissListener mDismissListener) {
        this.mDismissListener = mDismissListener;
    }


    public interface AcceptListener{
        public void onAccept(DialogInterface dialog);
    }

    public interface CancelListener{
        public void onCancel(DialogInterface dialog);
    }

    public interface DismissListener{
        public void onDismiss();

    }
}
