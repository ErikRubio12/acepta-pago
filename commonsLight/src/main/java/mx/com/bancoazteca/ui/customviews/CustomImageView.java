package mx.com.bancoazteca.ui.customviews;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mx.commons.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CustomImageView extends ImageView {
	private String authorizationNumber="";
	private Date date= new Date();
	private Paint paint= new Paint();
	private boolean watermark=true;
	public CustomImageView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
	}
	
	public CustomImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if(watermark){
			SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
			String dateText=format.format(date);
			format= new SimpleDateFormat("HH:mm",Locale.getDefault());
			String hourText=format.format(date);
			
			paint.setColor(Color.BLACK);
			paint.setTextSize(getContext().getResources().getDimension(R.dimen.mediumFont));
			paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
			paint.setAlpha(25);
			float underBaseLine = paint.descent();
			float aboveBaseLine = paint.ascent();
			float textHeight=(-aboveBaseLine + underBaseLine) ;
			canvas.drawText(dateText, (getWidth() -paint.measureText(dateText))/2,65,paint);
			canvas.drawText(hourText, (getWidth() -paint.measureText(hourText))/2,textHeight +68,paint);
			canvas.drawText(authorizationNumber, (getWidth() -paint.measureText(authorizationNumber))/2,textHeight*2 +71,paint);
			
			canvas.drawText(dateText, (getWidth() -paint.measureText(dateText))/2,textHeight*3 +74,paint);
			canvas.drawText(hourText, (getWidth() -paint.measureText(hourText))/2,textHeight*4 +77,paint);
			canvas.drawText(authorizationNumber, (getWidth() -paint.measureText(authorizationNumber))/2,textHeight*5 +80 ,paint);
		}
		
	}
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	public void setWatermark(boolean watermark) {
		this.watermark = watermark;
	}
	
}
