package mx.com.bancoazteca.ui;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.Navigation;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.commons.R;

public class BaseActivity extends AppCompatActivity implements Navigation{
	private static final String BASE_ACTIVITY_TAG = "BASE ACTIVITY";
	protected  GenericPopUp popUp;
	protected Intent intent= new Intent();
	protected Thread backgroundThread;
	protected Bundle params;
	protected Activity this_=null;
	private int height;
	private int width;
	protected String popupMsgStr=null;
	protected int popupMsg=0;
	protected int popupTitle=0;
	protected final static int POPUP_CONNECTION_ERROR=2111;
	protected final static int POPUP_CONNECTION_ERROR_EXIT=2112;
	protected final static int POPUP_EMPTY_FORM=2113;
	protected final static int POPUP_MESSAGES=2114;
	protected final static int POPUP_MESSAGES_EXIT=2115;
	protected final static int POPUP_MESSAGES_PROGRESS=2116;
	private static final String POPUP_TITLE ="popupTitle";
	private static final String POPUP_MESSAGE = "popupMsg";
	private static final String POPUP_MESSAGE_STR = "popupMsgStr";
	private boolean isLandscape=false;
	
	@Override
	public void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		Log.v(BASE_ACTIVITY_TAG, "onAttachedToWindow");
	}
	public boolean isLandscape(){
		return isLandscape;
	}
	private void setOrientation(){
		int rotation=HandsetInfo.getOrientation(this);
		if(rotation==Surface.ROTATION_90 || rotation==Surface.ROTATION_270){
			Log.v(BASE_ACTIVITY_TAG, "cambio a landscape");
			isLandscape=true;
		}
		else if(rotation==Surface.ROTATION_0 || rotation==Surface.ROTATION_180){
			Log.v(BASE_ACTIVITY_TAG, "cambio a portrait");
			isLandscape=false;
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
		Logger.setDebug(true);
		height=HandsetInfo.getScreenHeight(this);
		width=HandsetInfo.getScreenWidth(this);
		Log.v(BASE_ACTIVITY_TAG, "ancho: "+ width +" alto: "+ height);
		setOrientation();
		Log.v(BASE_ACTIVITY_TAG, "onCreate");
		if(savedInstanceState==null){
			loadUiComponents();
			processData();
			startBackgroundProcess();
		}
		else
			handleBackToFront(savedInstanceState);
		Log.v(BASE_ACTIVITY_TAG, "final onCreate");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
			Log.i(BASE_ACTIVITY_TAG,"Back presed");
		}
		return super.onOptionsItemSelected(item);
	}

	public void back(View v){
		Log.v(BASE_ACTIVITY_TAG, "back");
		finish();
	}
	public void accept(View v){
	}
	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		Log.v(BASE_ACTIVITY_TAG, "startBackgroundProccess");
	}

	protected boolean validForm(){
		return true;
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.v(BASE_ACTIVITY_TAG, "onStart");
	}
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		Log.v(BASE_ACTIVITY_TAG, "onRestart");
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.v(BASE_ACTIVITY_TAG, "onStop");
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.v(BASE_ACTIVITY_TAG, "onResume");
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		Log.v(BASE_ACTIVITY_TAG, "onSavedInstance");
		outState.putInt(POPUP_TITLE, popupTitle);
		outState.putInt(POPUP_MESSAGE, popupMsg);
		outState.putBoolean("landscape", isLandscape);
		outState.putString(POPUP_MESSAGE_STR, popupMsgStr);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		Log.v(BASE_ACTIVITY_TAG, "onRestoreInstanceState");
		popupMsg=savedInstanceState.getInt(POPUP_MESSAGE);
		popupTitle=savedInstanceState.getInt(POPUP_TITLE);
		isLandscape=savedInstanceState.getBoolean("landscape");
		popupMsgStr=savedInstanceState.getString(POPUP_MESSAGE_STR);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.v(BASE_ACTIVITY_TAG, "onPause");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.v(BASE_ACTIVITY_TAG, "onDestroy");
		release();
	}
	protected void handleBackToFront(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.v(BASE_ACTIVITY_TAG, "handleBackToFront");
		loadUiComponents();
		processData();
		if(isLandscape())
			loadUILandscape();
		else
			loadUIPortrait();
	}
	protected void processData() {
		// TODO Auto-generated method stub
		Log.v(BASE_ACTIVITY_TAG, "proccess data");
		params=getIntent().getExtras();
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Log.v(BASE_ACTIVITY_TAG, "onBackPressed");
	
	}
	protected void loadUiComponents(){
		Log.v(BASE_ACTIVITY_TAG, "loadUiComponents");
	}
	@Override
	public void release() {
		// TODO Auto-generated method stub		
		Log.v(BASE_ACTIVITY_TAG, "release");
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.v(BASE_ACTIVITY_TAG, "onActivityResult");
	}
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}

	public void  goForward(Class<?> className){
		Log.v(BASE_ACTIVITY_TAG, "goForward");
		intent.setClass(this, className);
		startActivity(intent);
	}
	@Override
	public void  goForward(Class<?> className,Context context){
		Log.v(BASE_ACTIVITY_TAG, "goForward");
		intent.setClass(context, className);
		startActivity(intent);
	}
	@Override
	public void goForward(Class<?> className, Serializable param,
			String paramName) {
		// TODO Auto-generated method stub
		intent.setClass(this, className);
		intent.putExtra(paramName, param);
		startActivity(intent);
	}

	@Override
	public void goForward(Class<?> className, Context context,
			Serializable param, String paramName) {
		// TODO Auto-generated method stub
		intent.setClass(context, className);
		intent.putExtra(paramName, param);
		startActivity(intent);
	}
	@Override
	public void goForward(Class<?> className, Context context,
			List<Serializable> params, List<String> paramNames) {
		// TODO Auto-generated method stub
		intent.setClass(context, className);
		for (int i = 0; i < paramNames.size(); i++) 
			intent.putExtra(paramNames.get(i), params.get(i));
		
		startActivity(intent);
	}
	//*******
	
	@Override
	public void  goForwardForResult(Class<?> className, int code){
		Log.v(BASE_ACTIVITY_TAG, "goForward");
		intent.setClass(this, className);
		startActivityForResult(intent,code);
	}
	@Override
	public void  goForwardForResult(Class<?> className,Context context,int code){
		Log.v(BASE_ACTIVITY_TAG, "goForward");
		intent.setClass(context, className);
		startActivityForResult(intent, code);
	}
	@Override
	public void goForwardForResult(Class<?> className, Serializable param,
			String paramName,int code) {
		// TODO Auto-generated method stub
		intent.setClass(this, className);
		intent.putExtra(paramName, param);
		startActivityForResult(intent,code);
	}
	public void goForwardForResult(Class<?> className, Context context,
			List<Serializable> params, List<String> paramNames,int code) {
		// TODO Auto-generated method stub
		intent.setClass(this, className);
		for (int i = 0; i < paramNames.size(); i++) 
			intent.putExtra(paramNames.get(i), params.get(i));
		startActivityForResult(intent, code);
	}
	@Override
	public void goForwardForResult(Class<?> className, Context context,
			Serializable param, String paramName,int code) {
		// TODO Auto-generated method stub
		intent.setClass(context, className);
		intent.putExtra(paramName, param);
		startActivityForResult(intent,code);
	}
	
	protected void createPopup(int title,int message,int type){
		createPopup(title,message,type,null);
		popUp.setCancelable(true);
	}
	protected void createPopup(int type){
		createPopup(popupTitle,popupMsg,type,null);
		popUp.setCancelable(true);
	}
	
	protected void createPopup(){
		createPopup(popupTitle,popupMsg,CustomPopup.DEFAULT,null);
		popUp.setCancelable(true);
	}
	protected void createPopup(ViewButton btn){
		ArrayList<ViewButton>btns= new ArrayList<ViewButton>(1);
		btns.add(btn);
		createPopup(popupTitle,popupMsg,CustomPopup.DEFAULT,btns);
	}
	protected void createPopup(int title,int message, List<ViewButton> btns){
		createPopup(popupTitle,popupMsg,CustomPopup.DEFAULT,btns);
	}
	private void createPopup(int title, int message, int type, List<ViewButton> btns){
		this.popupMsg=message;
		this.popupTitle=title;
		popUp= new CustomPopup(this,title,message);
		popUp.setCancelable(false);
		popUp.setType(type);
		if (btns != null){
			for (ViewButton viewButton : btns) 
				popUp.addButton(viewButton);
		}
		popUp.create();
	}
	protected void hidePopup(){
		if(popUp != null){
			popUp.dismiss();
			popUp=null;
		}
	}
	protected void show(){
		if(popUp != null)
			popUp.create().show();
	}

	
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Log.v(BASE_ACTIVITY_TAG, "onCreateDialog: "+ id + "   " + (popUp != null ? popUp.getDialog() :  "null"));
		DialogButton btn=null;
		switch (id) {
		case POPUP_MESSAGES_PROGRESS:			
			popUp=new PopUp(this, popupTitle, popupMsg);			
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		case POPUP_CONNECTION_ERROR:
			btn= new DialogButton(new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					removeDialog(POPUP_CONNECTION_ERROR);
				}
			});
			btn.setText(R.string.accept);			
			popUp=new PopUp(this, R.string.popUpMessage, R.string.connectionFails);			
			popUp.setCancelable(false);
			popUp.addButton(btn);
			popUp.create();
			return popUp.getDialog();
		case POPUP_CONNECTION_ERROR_EXIT:
			btn= new DialogButton(new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					removeDialog(POPUP_CONNECTION_ERROR_EXIT);
					finish();
				}
			});
			btn.setText(R.string.accept);			
			popUp=new PopUp(this, R.string.popUpMessage, R.string.connectionFails);			
			popUp.setCancelable(false);
			popUp.addButton(btn);
			popUp.create();
			return popUp.getDialog();
		case POPUP_EMPTY_FORM:
			btn= new DialogButton(new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					removeDialog(POPUP_EMPTY_FORM);
				}
			});
			btn.setText(R.string.accept);			
			popUp=new PopUp(this, R.string.popUpMessage, R.string.emptyFields);			
			popUp.setCancelable(false);
			popUp.addButton(btn);
			popUp.create();
			return popUp.getDialog();
		case POPUP_MESSAGES:
			btn= new DialogButton(new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					removeDialog(POPUP_MESSAGES);
				}
			});
			btn.setText(R.string.accept);		
			if(popupMsgStr==null)
				popUp=new PopUp(this, popupTitle, popupMsg);
			else
				popUp=new PopUp(this, popupTitle, popupMsgStr);
			popUp.setCancelable(false);
			popUp.addButton(btn);
			popUp.create();
			popupMsgStr=null;
			return popUp.getDialog();
		case POPUP_MESSAGES_EXIT:
			btn= new DialogButton(new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					removeDialog(POPUP_MESSAGES_EXIT);
					finish();
				}
			});
			btn.setText(R.string.accept);		
			if(popupMsgStr==null)
				popUp=new PopUp(this, popupTitle, popupMsg);
			else
				popUp=new PopUp(this, popupTitle, popupMsgStr);
			popUp.setCancelable(false);
			popUp.addButton(btn);
			popUp.create();
			popupMsgStr=null;
			return popUp.getDialog();
		default: return null;
		}
	}

	public void setIntent(Intent intentToNextActivity) {
		this.intent = intentToNextActivity;
	}
	protected void loadUIPortrait() {
		Log.v(BASE_ACTIVITY_TAG, "loadUIPortrait");
	}
	protected void loadUILandscape() {
		Log.v(BASE_ACTIVITY_TAG, "loadUILandscape");
	}
}
