package mx.com.bancoazteca.ui;

import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BaseFragment extends Fragment {
	protected final static String TAG_FRAGMENT="BaseFragment";
	protected final static int POPUP_CONNECTION_ERROR=BaseFragmentActivity.POPUP_CONNECTION_ERROR; 
	protected final static int POPUP_CONNECTION_ERROR_EXIT=BaseFragmentActivity.POPUP_CONNECTION_ERROR_EXIT; 
	protected final static int POPUP_EMPTY_FORM=BaseFragmentActivity.POPUP_EMPTY_FORM; 
	protected final static int POPUP_MESSAGES=BaseFragmentActivity.POPUP_MESSAGES; 
	protected final static int POPUP_MESSAGES_EXIT=BaseFragmentActivity.POPUP_MESSAGES_EXIT; 
	protected final static int POPUP_MESSAGES_PROGRESS=BaseFragmentActivity.POPUP_MESSAGES_PROGRESS; 
	protected Bundle params;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onCreate: " + savedInstanceState);
		params=getArguments();
		if(params==null)
			params= new Bundle();		
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onCreateView: "+ savedInstanceState);
		View v =null;
		if(savedInstanceState ==null){			
			v=loadUiComponents(inflater,container);
			proccessData();
			startBackgroundProcess();
		}
		else{
			onRestoreInstanceState(savedInstanceState);
		}					
		return v;
	}
	protected boolean isValidForm() {
		return true;
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onSaveInstanceState");
		outState.putBoolean("showed", true);
	}
	protected View loadUiComponents(LayoutInflater inflater, ViewGroup container){
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment loadUiComponents");
		return null;
	}
	protected void proccessData(){
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment proccessData");
	}
	protected void startBackgroundProcess(){
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment startBackgroundProcess");
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onResume");
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onPause");
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		release();
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onPause");
	}
	public void release() {
		// TODO Auto-generated method stub		
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment release");
		
	}
	
	protected void showDialog(int dialog,int title,int msg,Bundle params){
		if(getActivity() instanceof BaseFragmentActivity ){
			((BaseFragmentActivity)getActivity()).popupTitle=title;
			((BaseFragmentActivity)getActivity()).popupMsg=msg;
		}
		else if(getActivity() instanceof BaseActionBarActivity){
			((BaseActionBarActivity)getActivity()).popupTitle=title;
			((BaseActionBarActivity)getActivity()).popupMsg=msg;
		}
		if(HandsetInfo.getAndroidVersion()<=7)
			getActivity().showDialog(dialog);
		else
			getActivity().showDialog(dialog,params);
	}
	protected void showDialog(int dialog,int title,	String msg,Bundle params){
		if(getActivity() instanceof BaseFragmentActivity){
			((BaseFragmentActivity)getActivity()).popupTitle=title;
			((BaseFragmentActivity)getActivity()).popupMsgStr=msg;
		}
		else if(getActivity() instanceof BaseActionBarActivity){
			((BaseActionBarActivity)getActivity()).popupTitle=title;
			((BaseActionBarActivity)getActivity()).popupMsgStr=msg;
		}
		if(HandsetInfo.getAndroidVersion()<=7)
			getActivity().showDialog(dialog);
		else
			getActivity().showDialog(dialog,params);
	}
	protected void removeDialog(int dialog){
		getActivity().removeDialog(dialog);
	}
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, TAG_FRAGMENT, "Fragment onRestoreInstanceState");
	}

	
}