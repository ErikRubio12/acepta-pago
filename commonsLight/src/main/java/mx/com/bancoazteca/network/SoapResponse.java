package mx.com.bancoazteca.network;

import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.HttpsTransportSE;

public class SoapResponse {
	protected Object response;
	protected HttpTransportSE connection;
	protected HttpsTransportSE secureConnection;
	public Object getResponse() {
		return response;
	}
	
	
}
