package mx.com.bancoazteca.network;

import android.content.Context;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;

public class BaseService <T extends Message,R>implements Runnable{
	protected static float TIME_OUT=2.0f;
	protected MessageListener<T, R> listener;
	protected String operationCode;
	protected String systemError;
	protected Context ctx;
	protected String url;
	
	public BaseService(){
		
	}
	public BaseService(MessageListener<T, R> listener,Context ctx) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		this.ctx=ctx;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	public static void setTimeOut(float t){
		TIME_OUT=t;
	}
}
