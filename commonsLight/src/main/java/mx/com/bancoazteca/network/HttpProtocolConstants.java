/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.com.bancoazteca.network;

/**
 *
 * @author iscm-
 */
public interface HttpProtocolConstants {
    public static String HEADER_CONTENT_LENGTH = "Content-Length" ;
    public static String GET = "GET" ;
    public static String POST = "POST";
}
