package mx.com.bancoazteca.network;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class Connections {
	private static final String CONNECTIONS = "Connections";
	private final static int MIN=60000;
	private static String error = "";
	public static String makeWrapSoapConnection(SoapRequest<String> req,float timeout) throws TimeoutException, IOException{
		
		StringBuilder envelope = null;
		if(req.getRequest()==null){
			StringBuilder content=Connections.addParameters(req.getNameSpace(),req.getParams());
			if (req.getNameSpace() != null && !"".equals(req.getNameSpace())){
				String prefix = req.getNameSpace().substring(7);
				envelope= new StringBuilder();
				envelope.append("<");
				envelope.append(prefix.substring(0, 3));
				envelope.append(":");
				envelope.append(req.getMethod());
				envelope.append(" xmlns:");
				envelope.append(prefix);
				envelope.append("=\"");
				envelope.append(req.getNameSpace());
				envelope.append("\">\r\n");
				envelope.append(content);
				envelope.append("\r\n");
				envelope.append("</");
				envelope.append(prefix.substring(0, 3));
				envelope.append(":");
				envelope.append(req.getMethod());
				envelope.append(">\r\n");
			}
			else{
				
				envelope= new StringBuilder();
				envelope.append("<");
				envelope.append(req.getMethod());
				envelope.append(">\r\n");
				envelope.append(content);
				envelope.append("\r\n");
				envelope.append("</");
				envelope.append(req.getMethod());
				envelope.append(">\r\n");
			}
			Connections.wrapRequest(envelope);
		}
		else
			envelope=new StringBuilder(req.getRequest());
		
		HttpHeaders soapHeader= new HttpHeaders();
		soapHeader.addProperty("Content-Type", "application/soap+xml;charset=UTF-8");
		soapHeader.addProperty("SOAPAction", req.getAction());
		
		HttpConnection conn= Connections.makeHttpConnection(req.getUrl(),soapHeader,envelope.toString().getBytes(),new ArrayList<Cookie>(),timeout);
		
		if (conn != null && conn.getResponse().getStatusLine()!=null && conn.getResponse().getStatusLine().getStatusCode()==HttpStatus.SC_OK) {
			String response= conn.getResponseContent();
			Connections.closeConnection(conn);
			return response;
		}
		
		else
			return null;
	}
	
	
	private static void wrapRequest(StringBuilder envelope) {
		// TODO Auto-generated method stub
		envelope.insert(0, "\r\n<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:end=\"http://endpoint.pagoazteca.ws.com\">\r\n<soapenv:Header/>\r\n<soapenv:Body>\r\n");
		envelope.append("</soapenv:Body>\r\n</soapenv:Envelope>"); 
	}
	private static StringBuilder addParameters(String nameSpace,
			List<Parameter<String, String>> params) {
		// TODO Auto-generated method stub
		StringBuilder request= new StringBuilder();
		Parameter<String, String> currentParam=null;
		String prefijo=null;
		for (int i = 0; i < params.size(); i++) {
			currentParam= params.get(i);
			if(currentParam.getValue()!=null && currentParam.getValue().length()>0) {
				if (nameSpace != null && !"".equals(nameSpace)) {
					prefijo= nameSpace.substring(7);
					request.append("<");
					request.append(prefijo.substring(0, 3));
					request.append(":");
					request.append(currentParam.getKey());
					request.append(">");
					request.append(currentParam.getValue());
					request.append("</");
					request.append(prefijo.substring(0, 3));
					request.append(":");
					request.append(currentParam.getKey());
					request.append(">\r\n");
				}
				else{
					request.append("<");
					request.append(currentParam.getKey());
					request.append(">");
					request.append(currentParam.getValue());
					request.append("</");
					request.append(currentParam.getKey());
					request.append(">\r\n");
				}
			}
			else{
				if (nameSpace != null && !"".equals(nameSpace)) {
					prefijo= nameSpace.substring(7);
					request.append("<");
					request.append(prefijo.substring(0, 3));
					request.append(":");
					request.append(currentParam.getKey());
					request.append("/>\r\n");
				}
				else{
					request.append("<");
					request.append(currentParam.getKey());
					request.append("/>\r\n");
				}
			}
		}
		return request;
	}
	public static void setSSLConf() throws KeyManagementException, NoSuchAlgorithmException{
		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(null, new TrustManager[] {
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		}, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}
	public static SoapResponse makeSecureSoapConnection(SoapRequest<?> req,float timeout) throws  TimeoutException, ConnectException,InterruptedIOException{
		try {
			setSSLConf();
			return makeSoapConnection(req, timeout);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE,CONNECTIONS, "KeyManagementException: " + e.getMessage());
			return null;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE,CONNECTIONS, "NoSuchAlgorithmException: " + e.getMessage());
			return null;
		}
		catch (Exception e) {
			// TODO: handle exception
			Logger.log(Logger.MESSAGE,CONNECTIONS, "Exception: " + e.getMessage());
			return null;
		}
		
	}

	
	public static SoapResponse makeSoapConnection(SoapRequest<?> req,float timeout) throws  TimeoutException, ConnectException,InterruptedIOException{
		SoapObject request = new SoapObject(req.getNameSpace(),req.getMethod());
		for (Parameter<String, String> property : req.getMethodProperties()) 
			request.addAttribute(property.getKey(), property.getValue());
		
		for (Parameter<String, ?> param : req.getParams()) {
			Logger.log(Logger.MESSAGE,CONNECTIONS, "parametro: "+ param);
			request.addProperty(param.getKey(),param.getValue());
		}
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		SoapResponse response= new SoapResponse();
		try {
			response.connection = new HttpTransportSE(req.getUrl(),(int)(timeout*MIN));
			Logger.log(Logger.MESSAGE, CONNECTIONS, "puerto: "+response.connection.getPort()+" url:"+ req.getUrl() );
			response.connection.debug=true;
			response.connection.call(req.getAction(), envelope);
			Logger.log(Logger.MESSAGE,CONNECTIONS, "request: " + Utility.fixSoapResponse(response.connection.requestDump));
			Logger.log(Logger.MESSAGE,CONNECTIONS, "response: " + Utility.fixSoapResponse(response.connection.responseDump));
			response.response= envelope.getResponse();
			
			envelope=null;
			request=null;
			return response;
		}catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"SOAP XmlPullParserException: " + e.getMessage());
			return null;
		} 
		catch (SecurityException e) {
			// TODO: handle exception
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"SOAP SecurityException: " + e.getMessage());
			return null;
		}
		catch (Exception e) {
			// TODO: handle exception
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"SOAP Exception: " + e);
			if(e!=null && e.getMessage()!=null && (e.getMessage().indexOf("timed out")>=0 || e.getMessage().indexOf("TIMED OUT")>=0)){
				throw new TimeoutException(e.getMessage());
			}
			else
				return null;
		}
	}
	public static void closeSoapConnection(SoapResponse response){
		if(response!=null){
			response.connection.reset();
			response.connection=null;
			response.response=null;
		}
	}

	public static HttpConnection makeHttpConnection(String url, HttpHeaders requestHeaders, byte[] postData,ArrayList<Cookie> cookies,float timeout) throws TimeoutException,ConnectException, IOException
	{
		HttpConnection connection;
		if(postData == null)
			connection=makeGetConnection(url,requestHeaders,cookies,timeout);
		else
			connection=makePostConnection(url,requestHeaders,postData,cookies, timeout);
		return connection;
	}
	public static HttpConnection makeHttpConnection(String url, HttpHeaders requestHeaders, byte[] postData,Cookie cookie,float timeout) throws TimeoutException,ConnectException, IOException
	{
		ArrayList<Cookie> cookies= new ArrayList<Cookie>(1);
		cookies.add(cookie);
		HttpConnection connection;
		if(postData == null)
			connection=makeGetConnection(url,requestHeaders,cookies,timeout);
		else
			connection=makePostConnection(url,requestHeaders,postData,cookies, timeout);
		return connection;
	}
	public static HttpConnection makeHttpConnection(String url, HttpHeaders requestHeaders, byte[] postData,float timeout) throws TimeoutException,ConnectException, IOException
	{
		return Connections.makeHttpConnection(url, requestHeaders, postData, new ArrayList<Cookie>(), timeout);
	}
	public static HttpConnection makeHttpConnection(String url, HttpHeaders requestHeaders, byte[] postData,Cookie cookie) throws TimeoutException, IOException,ConnectException
	{
		return Connections.makeHttpConnection(url, requestHeaders, postData,cookie, 3);
	}
	public static HttpConnection makeHttpConnection(String url, HttpHeaders requestHeaders, byte[] postData) throws TimeoutException, IOException,ConnectException
	{
		return Connections.makeHttpConnection(url, requestHeaders, postData,new ArrayList<Cookie>(), 3);
	}

	private static HttpConnection makePostConnection(String url, HttpHeaders requestHeaders, byte[] postData,ArrayList<Cookie> cookies,float  timeout) throws TimeoutException, IOException{
		// TODO Auto-generated method stub
    	// AndroidManifest.xml must have the following permission:
		//<uses-permission android:name="android.permission.INTERNET"/>
		// Create the Apache HTTP client and post  
		
         HttpConnection conn = new HttpConnection();
         
         final HttpParams httpParams = new BasicHttpParams();
         HttpConnectionParams.setConnectionTimeout(httpParams,(int)(timeout*MIN));
         
         HttpClient httpClient = new DefaultHttpClient(httpParams);
		 
		 if(cookies !=null && cookies.size()>0){
			 Logger.log(Logger.MESSAGE,CONNECTIONS, "hay cookies: ");
			 CookieStore store = new BasicCookieStore();
			 for (int i = 0; i < cookies.size(); i++) {
				store.addCookie(cookies.get(i));
			}
			 ((DefaultHttpClient)httpClient).setCookieStore(store);
		 }
		 
		 HttpPost httppost = new HttpPost(url);  
		 requestHeaders.addProperty("Content-Type", "application/x-www-form-urlencoded");
		 httppost = (HttpPost) setHeaders(httppost, requestHeaders);
		 Logger.log(Logger.MESSAGE,CONNECTIONS, "url: : "+ url);
		 if(postData.length< 1024)
			 Logger.log(Logger.MESSAGE,CONNECTIONS, "request por post: "+ new String(postData));
		 else
			 Logger.log(Logger.MESSAGE,CONNECTIONS, "request por post muuuuuy grande ");
		try {  
			 ByteArrayEntity postEntity= new ByteArrayEntity(postData);
			 postEntity.setContentType("txt/xml");
		     httppost.setEntity(postEntity);  
		     HttpResponse webServerAnswer = httpClient.execute(httppost);  
		     conn.setClient(httpClient);
		     conn.setResponse(webServerAnswer);
		     Logger.log(Logger.MESSAGE,CONNECTIONS, "response: "+conn.getResponseContent());
		 } catch (ClientProtocolException e) {
		     //Deal with it  
			 Logger.log(Logger.EXCEPTION,CONNECTIONS, "Exception: "+ e.getMessage());
			 return null;
		 } catch (SecurityException e) {
			// TODO: handle exception
			 Logger.log(Logger.EXCEPTION,CONNECTIONS, "Exception: "+ e.getMessage());
			 return null;
		}
		catch (Exception e) {
			// TODO: handle exception
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"Exception: " + e.getMessage());
			return null;
		}
		return conn;
	}

	private static HttpConnection makeGetConnection(String url, HttpHeaders requestHeaders,ArrayList<Cookie> cookies, float timeout) throws TimeoutException, IOException,ConnectException{
		// TODO Auto-generated method stub
		// AndroidManifest.xml must have the following permission:
		//<uses-permission android:name="android.permission.INTERNET"/>
		
		HttpResponse webServerResponse = null;
		HttpConnection conn= new HttpConnection();
		final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams,(int)(timeout*MIN));
        
        HttpClient httpClient = new DefaultHttpClient(httpParams);
		HttpGet getMethod = new HttpGet(url);
		getMethod = (HttpGet) setHeaders(getMethod, requestHeaders);
		try {
			webServerResponse = httpClient.execute(getMethod);
			conn.setClient(httpClient);
			conn.setResponse(webServerResponse);
		} catch (ClientProtocolException e) {
			// Deal with it
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"Exception: " + e.getMessage());
			return null;
		}catch (SecurityException e){
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"Exception: " + e.getMessage());
			return null;
		}catch (Exception e) {
			// TODO: handle exception
			Logger.log(Logger.EXCEPTION,CONNECTIONS,"Exception: " + e.getMessage());
			return null;
		}
		
		return conn;
	}
	private static AbstractHttpMessage setHeaders(AbstractHttpMessage connection, HttpHeaders requestHeaders){
   	 
   	 if(requestHeaders==null)
   		 return connection;
   	 
   	 int size = requestHeaders.size();
        for (int i = 0; i < size;) 
        {                    
            String header = requestHeaders.getPropertyKey(i);
            
            String value = requestHeaders.getPropertyValue( i++ );
            if (value != null) 
            {
                connection.setHeader(header, value);
            }
        }
        return connection;
   }
	public static int extractResponseCode(HttpConnection connection) throws SecurityException
	{
		int rCode = -1;
		if(connection != null)
			rCode = connection.getResponse().getStatusLine().getStatusCode();
		else
			return 0;
		return rCode;
	}
	public static byte[] extractResponseData(HttpConnection connection) throws SecurityException
    {
        InputStream in = null;
        ByteArrayOutputStream bytesArray = null;
        
        try{
            in = connection.getResponse().getEntity().getContent();  
            long contentLength = -1;//connection.getLength();
            if(contentLength != -1) { // Buffered fetch.
                bytesArray = new ByteArrayOutputStream();
                byte bytes[] = new byte[(int)contentLength];
                in.read(bytes);
                bytesArray.write(bytes);
            } 
            else { // Buffered fetch of unknown length.
                bytesArray = new ByteArrayOutputStream();
                byte[] bytes = new byte[512];
                int len = 0;
                while ((len = in.read(bytes)) != -1)
                    bytesArray.write(bytes,0,len);
            }
        }
        catch(IOException ioe){
        	Logger.log(Logger.EXCEPTION, CONNECTIONS,  ioe.getMessage());
            return new byte[0];
        }
        catch (Exception e) {
        	Logger.log(Logger.EXCEPTION, CONNECTIONS,  e.getMessage());
            return new byte[0];
        }
        finally {
            if (in != null){
                try{
                    in.close();
                    in = null;
                }
                catch(IOException ioe){
                	Logger.log(Logger.EXCEPTION, CONNECTIONS,  ioe.getMessage());
                    return new byte[0];
                }
            }
        }
        Logger.log(Logger.MESSAGE, CONNECTIONS,  "Response-Data [" + bytesArray.size() + "b]\n" + bytesArray.toString());
        return bytesArray.toByteArray();
    }
    
    public static void closeConnection(HttpConnection connection){
        if (connection != null) {
        		connection.getClient().getConnectionManager().closeExpiredConnections();
                connection = null;
        }
    }

    public static String getLastError(){
        return error;
    }
    public static class SSLFixAndroid21 extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public SSLFixAndroid21(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }
}
