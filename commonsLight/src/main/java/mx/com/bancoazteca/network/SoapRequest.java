package mx.com.bancoazteca.network;

import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.data.Parameter;

public class SoapRequest <P>{
	private String  method;
	private String action;
	private String nameSpace;
	private String url;
	private String request;
	private List<Parameter<String, P>> params = new ArrayList<Parameter<String, P>>();
	private List<Parameter<String, String>> methodProperties = new ArrayList<Parameter<String, String>>();
	
	public void setMethodProperties(List<Parameter<String, String>> methodProperties) {
		this.methodProperties = methodProperties;
	}
	public List<Parameter<String, String>> getMethodProperties() {
		return methodProperties;
	}
	public SoapRequest() {
		// TODO Auto-generated constructor stub
	}
	public SoapRequest(String method,String action) {
		this.method=method;
		this.action=action;
	}
	public SoapRequest(String method,String action, List<Parameter<String,P>> params) {
		this.method=method;
		this.action=action;
		this.params=params;
	}
	public SoapRequest(String method,String action, List<Parameter<String,P>> params,List<Parameter<String,String>> methodProperties) {
		this.method=method;
		this.action=action;
		this.params=params;
		this.methodProperties=methodProperties;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	public List<Parameter<String,P>> getParams() {
		return params;
	}
	public void setParams(List<Parameter<String,P>> params) {
		this.params = params;
	}
	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}
	public String getNameSpace() {
		return nameSpace;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl() {
		return url;
	}
	public void addParameter(Parameter<String,P> param){
		params.add(param);
	}
	public void removeParameter(Parameter<String, P> param){
		params.remove(param);
	}
	public void addMethodProperty(Parameter<String,String> property){
		methodProperties.add(property);
	}
	public void removeMethodeProperty(Parameter<String, String> property){
		methodProperties.remove(property);
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
}
