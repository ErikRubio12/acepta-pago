package mx.com.bancoazteca.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import mx.com.bancoazteca.util.Logger;

import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.protocol.HTTP;

public class HttpConnection {
	private static final String HTTP_CONNECTION = "HttpConnection";
	private HttpClient client;
	private HttpResponse response;
	/**
	 * @return the client
	 */
	public HttpClient getClient() {
		return client;
	}
	/**
	 * @param client the client to set
	 */
	public void setClient(HttpClient client) {
		this.client = client;
	}
	/**
	 * @return the response
	 */
	public HttpResponse getResponse() {
		return response;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(HttpResponse response) {
		this.response = response;
	}
	public  byte[] getResponseByteContent(){
		if(response==null ||  response.getEntity()==null)
			return null;
		
		return null;
	}
	public String getResponseContent(){
		Reader reader=null;
	
		if(response==null ||  response.getEntity()==null)
			return null;
		else{
			try {
				InputStream responseStream= response.getEntity().getContent();
				
				String charset = getContentCharSet(response);
				if (charset==null) {
					charset = HTTP.DEFAULT_CONTENT_CHARSET;
				}
				reader = new InputStreamReader(responseStream, charset);
				StringBuilder buffer = new StringBuilder();

				char[] tmp = new char[1024];
				int l=0;
				while ((l=reader.read(tmp)) != -1) {
					buffer.append(tmp, 0, l);
//					buffer.append(tmp);
				}
				return buffer.toString();

			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				Logger.log(Logger.EXCEPTION, HTTP_CONNECTION,"IllegalStateException: "+ e.getMessage());
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Logger.log(Logger.EXCEPTION, HTTP_CONNECTION,"IllegalStateException: "+ e.getMessage());
				return null;
			}finally{
				try {
					if (reader != null)
						reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Logger.log(Logger.EXCEPTION, HTTP_CONNECTION,"IllegalStateException: "+ e.getMessage());
				}
			}
		}
	}
	

	public String getContentCharSet(final HttpResponse response)  {
		String charset = null;

		if (response.getEntity().getContentType() != null) {
			HeaderElement values[] = response.getEntity().getContentType().getElements();
			if (values.length > 0) {
				NameValuePair param = values[0].getParameterByName("charset");
				if (param != null) {
					charset = param.getValue();
				}
			}
		}
		return charset;
	}
}
