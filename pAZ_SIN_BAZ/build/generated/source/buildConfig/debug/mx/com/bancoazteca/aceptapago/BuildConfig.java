/**
 * Automatically generated file. DO NOT MODIFY
 */
package mx.com.bancoazteca.aceptapago;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "mx.com.bancoazteca.aceptapago";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 91;
  public static final String VERSION_NAME = "13.5";
}
