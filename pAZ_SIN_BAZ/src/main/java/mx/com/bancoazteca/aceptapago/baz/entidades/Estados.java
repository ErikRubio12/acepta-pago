package mx.com.bancoazteca.aceptapago.baz.entidades;

public class Estados {
	private String id;
	private String nombre;
	private String abreviacionCurp;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAbreviacionCurp() {
		return abreviacionCurp;
	}
	public void setAbreviacionCurp(String abreviacionCurp) {
		this.abreviacionCurp = abreviacionCurp;
	}
	
	
}
