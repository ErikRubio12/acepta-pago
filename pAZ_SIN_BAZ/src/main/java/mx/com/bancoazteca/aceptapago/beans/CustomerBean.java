package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.beans.Person;
import mx.com.bancoazteca.util.Utility;
import android.graphics.Bitmap;

public class CustomerBean extends Person implements Serializable, KvmSerializable{
	public static final String BORN_DATE="fechNac";
	public static final String GENDER="sexo";
	public static final String MARITAL_STATUS="edoCivil";
	public static final String CURP="curp";
	public static final String ID="numIdent";
	public static final String RFC="rfc";
	public static final String FOLIO_PREAPERTURA="folioPrea";
	private String customerId;
	public static final String CODIDENT="codIdent";
	public static final String CITIZENSHIP="codNac";
	private static final long serialVersionUID = 5020059787051099555L;
	private Date bornDate;//si aparece
	private int gender=DEFAULT_GENDER;//no aparece
	private int maritalStatus=-1;//no aparece
	private int codIdent=-1;//si aparece
	private int citizenship=-1;//SI aparece
	private String curp="";//si aparece
	private String rfc="";//si aparece
	private String folioPreap;//si aparece 
	private ExtraInfoBean extraInfo;
	private LoginBean login;
	private String device=null;
	private String affiliate;
	private String keyRegister;
	//private final SimpleDateFormat format= new SimpleDateFormat("ddmmyyyy");
	public final static char DEFAULT_GENDER='F';
	public static final String CUSTOMER = "cliente";
	private static final int FIELDS = 23;
	private static Bitmap contractSign;
	private static PropertyInfo PIEconomicActivity= new PropertyInfo();
	private static PropertyInfo PIBusinessType= new PropertyInfo();
	private static PropertyInfo PILastName2= new  PropertyInfo();
	private static PropertyInfo PILastName = new PropertyInfo();
	private static PropertyInfo PIStreet = new PropertyInfo();
	private static PropertyInfo PICodeIdent = new PropertyInfo();
	private static PropertyInfo PICitizenship = new PropertyInfo();
	private static PropertyInfo PIColony = new PropertyInfo();
	private static PropertyInfo PIMail = new PropertyInfo();
	private static PropertyInfo PIZipCode = new PropertyInfo();
	private static PropertyInfo PICurp = new PropertyInfo();
	private static PropertyInfo PIState = new PropertyInfo();
	private static PropertyInfo PIExtension = new PropertyInfo();
	private static PropertyInfo PIBornDate = new PropertyInfo();
	private static PropertyInfo PIFolioPreap = new PropertyInfo();
	private static PropertyInfo PIName = new PropertyInfo();
	private static PropertyInfo PIExternalNumber = new PropertyInfo();
	private static PropertyInfo PIId = new PropertyInfo();
	private static PropertyInfo PIInternalNUmber= new PropertyInfo();
	private static PropertyInfo PILada = new PropertyInfo();
	private static PropertyInfo PITelephone = new PropertyInfo();
	private static PropertyInfo PIPoblacion = new PropertyInfo();
	private static PropertyInfo PIrfc = new PropertyInfo();
	private static PropertyInfo[] listPI= new PropertyInfo[FIELDS];
	static{
		PIEconomicActivity.name=ExtraInfoBean.ECONOMIC_ACTIVITY;
		PIEconomicActivity.type=PropertyInfo.INTEGER_CLASS;
		listPI[0]=PIEconomicActivity;
		
		PIBusinessType.type=PropertyInfo.INTEGER_CLASS;
		PIBusinessType.name=ExtraInfoBean.BUSINESS_TYPE;
		listPI[1]=PIBusinessType;
		
		PILastName2.type=PropertyInfo.STRING_CLASS;
		PILastName2.name=LASTNAME2;
		listPI[2]=PILastName2;
		
		PILastName.type=PropertyInfo.STRING_CLASS;
		PILastName.name=LAST_NAME;
		listPI[3]=PILastName;
		
		PIStreet.type=PropertyInfo.STRING_CLASS;
		PIStreet.name=AddressBean.STREET;
		listPI[4]=PIStreet;
		
		PICodeIdent.type=PropertyInfo.INTEGER_CLASS;
		PICodeIdent.name=CODIDENT;
		listPI[5]=PICodeIdent;
		
		PICitizenship.type=PropertyInfo.INTEGER_CLASS;
		PICitizenship.name=CITIZENSHIP;
		listPI[6]=PICitizenship;
		
		PIColony.type=PropertyInfo.STRING_CLASS;
		PIColony.name=AddressBean.COLONY;
		listPI[7]=PIColony;
		
		PIMail.type=PropertyInfo.STRING_CLASS;
		PIMail.name=MAIL;
		listPI[8]=PIMail;
		
		PIZipCode.type=PropertyInfo.STRING_CLASS;
		PIZipCode.name=AddressBean.ZIP_CODE;
		listPI[9]=PIZipCode;
		
		PICurp.type=PropertyInfo.STRING_CLASS;
		PICurp.name=CURP;
		listPI[10]=PICurp;
		
		PIState.type=PropertyInfo.STRING_CLASS;
		PIState.name=AddressBean.STATE;
		listPI[11]=PIState;
		
		PIExtension.type=PropertyInfo.STRING_CLASS;
		PIExtension.name=AddressBean.EXTENSION;
		listPI[12]=PIExtension;
		
		PIBornDate.type=PropertyInfo.STRING_CLASS;
		PIBornDate.name=BORN_DATE;	
		listPI[13]=PIBornDate;
		
		PIFolioPreap.type=PropertyInfo.STRING_CLASS;
		PIFolioPreap.name=FOLIO_PREAPERTURA;
		listPI[14]=PIFolioPreap;
		
		PIName.type=PropertyInfo.STRING_CLASS;
		PIName.name=NAME;
		listPI[15]=PIName;
		
		PIExternalNumber.type=PropertyInfo.STRING_CLASS;
		PIExternalNumber.name=AddressBean.EXTERNAL_NUMBER;
		listPI[16]=PIExternalNumber;
		
		PIId.type=PropertyInfo.STRING_CLASS;
		PIId.name=ID;
		listPI[17]=PIId;
		
		PIInternalNUmber.type=PropertyInfo.STRING_CLASS;
		PIInternalNUmber.name=AddressBean.INTERNAL_NUMBER;
		listPI[18]=PIInternalNUmber;
		
		PILada.type=PropertyInfo.STRING_CLASS;
		PILada.name=AddressBean.LADA;
		listPI[19]=PILada;
		
		PITelephone.type=PropertyInfo.STRING_CLASS;
		PITelephone.name=AddressBean.TELEPHONE;
		listPI[20]=PITelephone;
		
		PIPoblacion.type=PropertyInfo.STRING_CLASS;
		PIPoblacion.name=AddressBean.POBLACION;
		listPI[21]=PIPoblacion;
		
		PIrfc.type=PropertyInfo.STRING_CLASS;
		PIrfc.name=RFC;
		listPI[22]=PIrfc;
	}
	public CustomerBean() {
	}
	public Date getBornDate() {
		return bornDate;
	}
	public void setBornDate(Date bornDate) {
		this.bornDate = bornDate;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(int maritalStatus) {
//		this.maritalStatus = _maritalStatus[maritalStatus];
		this.maritalStatus = maritalStatus;
	}
	public int getCitizenship() {
		return citizenship;
	}
	public void setCitizenship(int citizenship) {
//		this.citizenship = citizCodes[citizenship];
		this.citizenship = citizenship;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public void setRfc(String rfc) {
		// TODO Auto-generated method stub
		this.rfc=rfc;
	}
	public String getRfc() {
		return rfc;
	}
	public static void setContractSign(Bitmap contractSign) {
		CustomerBean.contractSign = contractSign;
	}
	public static Bitmap getContractSign() {
		return contractSign;
	}
	public void setFolioPreap(String folioPreap) {
		this.folioPreap = folioPreap;
	}
	public String getFolioPreap() {
		return folioPreap;
	}
	public void setExtraInfo(ExtraInfoBean extraInfo) {
		this.extraInfo = extraInfo;
	}
	public ExtraInfoBean getExtraInfo() {
		return extraInfo;
	}
	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0:return extraInfo.getEconomicActivity();
			case 1:return extraInfo.getCategoryEconomicActivity();
			case 2:return lastName2;
			case 3:return lastName;
			case 4:return address.getStreet();
			case 5:return codIdent;
			case 6:return citizenship;
			case 7:return address.getColony();			
			case 8:return mail;
			case 9:return address.getZipCode();
			case 10:return curp;
			case 11:return address.getState();
			case 12:return address.getExtention();
			case 13:return Utility.formatDate(bornDate,"ddMMyyyy");
			case 14:return folioPreap;
			case 15:return name;
			case 16:return address.getExternalNumber();
			case 17:return id;
			case 18:return address.getInternalNumber();
			case 19:return address.getLada();
			case 20:return address.getTelephone();
			case 21:return address.getPoblacion();
			case 22:return rfc;
			default:return null;
		}
		
	}
	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}
	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: extraInfo.setEconomicActivity((Integer) arg1); break;
			case 1: extraInfo.setCategoryEconomicActivity((Integer) arg1); break;
			case 2: lastName2=(String) arg1; break;
			case 3: lastName=(String) arg1; break;
			case 4: address.setStreet((String) arg1); break;
			case 5: codIdent=(Integer) arg1; break;
			case 6: citizenship=(Integer) arg1; break;
			case 7: address.setColony((String) arg1); break;			
			case 8: mail=(String) arg1; break;
			case 9: address.setZipCode((String) arg1); break;
			case 10: curp=(String) arg1; break;
			case 11: address.setState((String) arg1); break;
			case 12: address.setExtention((String) arg1); break;
			case 13: bornDate.setTime(Long.parseLong((String) arg1)); break;
			case 14: folioPreap=(String) arg1; break;
			case 15: name=(String) arg1; break;
			case 16: address.setExternalNumber((String) arg1); break;
			case 17: id=(String) arg1; break;
			case 18: address.setInternalNumber((String) arg1); break;
			case 19: address.setLada((String) arg1); break;
			case 20: address.setTelephone((String) arg1); break;
			case 21: address.setPoblacion((String) arg1); break;
			case 22: rfc=(String) arg1; break;
		}
	}
	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}
	
	public void setCodIdent(int codIdent) {
//		this.codIdent = idCodes[codIdent];
		this.codIdent = codIdent;
	}
	public int getCodIdent() {
		return codIdent;
	}
	public LoginBean getLogin() {
		return login;
	}
	public void setLogin(LoginBean login) {
		this.login = login;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public void setDevice(String device) {
		// TODO Auto-generated method stub
		this.device=device;
	}
	public String getDevice() {
		return device;
	}
	public void setAffiliate(String affiliate) {
		// TODO Auto-generated method stub
		this.affiliate=affiliate;
	}
	public String getAffiliate() {
		// TODO Auto-generated method stub
		return this.affiliate;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer res= new StringBuffer();
		res.append("ID cliente: " + customerId + "\n");
		res.append("Terminal: " + device + "\n");
		res.append("Afiliacion: " + affiliate + "\n");
		res.append("Genero: " + ((char)gender) + "\n");
		res.append("Curp: " + curp + "\n");
		res.append("Rfc: " + rfc + "\n");
		res.append(login);
		res.append(super.toString());
		return res.toString();
	}

	public String getKeyRegister() {
		return keyRegister;
	}

	public void setKeyRegister(String keyRegister) {
		this.keyRegister = keyRegister;
	}
}
