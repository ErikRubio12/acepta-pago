package mx.com.bancoazteca.aceptapago.gui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.messages.EmailBean;
import mx.com.bancoazteca.messages.Mail;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.ClientBean;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.jobs.ClientsJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;

public class EmalConfirmationScreen extends BaseActivity implements MessageListener<Message, String> {
	private static final String EMAIL_CONFIRMATION = "email confirmation";
	private static final int POPUP_EMPTY_FIELDS = 11;
	private static final int POPUP_ERROR = 12;
	private ClientBean clientSelected;
	private EmailBean mail= null;
	private EditText mailEditText;
	private EditText clientEditText;
	private String rfc;
	private String folio;
	private  SaleBean sale;
	private boolean isFirstTime=true;
	private OperationsBean operation;
	private File ticket;
	private Toolbar mtoolbar;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.email_confirmation);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		mailEditText=(EditText) findViewById(R.id.mailClient);
		clientEditText= (EditText) findViewById(R.id.client);
		
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if (params != null) {
			operation= (OperationsBean) params.getSerializable(OperationsBean.OPERATION);
			ticket= (File) params.getSerializable(TicketScreen.TICKET);
			
			clientSelected=(ClientBean) params.getSerializable(Clients.CLIENT_SELECTED);
			mail=(EmailBean) params.getSerializable(EmailBean.EMAIL);
			folio=(String) params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
			rfc=(String) params.getSerializable(CustomerBean.RFC);
			sale=(SaleBean) params.getSerializable(SaleBean.SALE);
			if(ticket!= null && operation!= null && mail==null){
				mail= new EmailBean();
				mail.addAttached(ticket);
			}
			Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "cliente seleccionado: "+ clientSelected + " correo: "+ mail + " folio: "+ folio + " rfc " +rfc  + "  venta: "+ sale + " operacion: " + operation);
			
		}
	}
	public void send(View v){
		try {
			if(validForm()){
				if (isFirstTime) {
					isFirstTime=false;
					if(sale != null && clientSelected != null && clientSelected.getId()!=null && !clientSelected.getId().equalsIgnoreCase("0")){
						Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "se va a hacer el ligue del cliente: "+ clientSelected  + "  venta: "+ sale );
						backgroundThread= new Thread(new ClientsJob<String>(this,folio, rfc,clientSelected.getId(),sale));
						backgroundThread.start();
					}
					else if(sale != null && (clientSelected==null || clientSelected.getId().equalsIgnoreCase("0"))){
//						ClientBean client= new ClientBean();
//						client.setCompleteName(clientEditText.getText().toString().toUpperCase(Locale.getDefault()));
//						client.setMail(mailEditText.getText().toString());
//						backgroundThread= new ClientsJob<String>(this,folio, rfc, client, this, ClientsJob.CREATE_CLIENT);
//						backgroundThread.start();
					}
						
				}
				if(sale != null || mail.getAttached(0).getAbsolutePath().contains("ticketReenvio"))
					mail.setMessage(clientEditText.getText() + " se te ha enviado tu ticket de compra");
				else
					mail.setMessage(clientEditText.getText() + " se te ha enviado tu ticket de cancelación");
				Mail.sendByIntent(mail, this);
			}
		} catch (GenericException e) {
			// TODO Auto-generated catch block
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.mailSendError));
			dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
		}catch (ActivityNotFoundException e) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.noAppForEmail));
			dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
		}
	}

	@Override
	protected boolean validForm() {
		// TODO Auto-generated method stub
		mail.removeDestinies();
		
		if(!"".equalsIgnoreCase(mailEditText.getText().toString()) && !"".equalsIgnoreCase(clientEditText.getText().toString())){
			try {
				mail.addDestiny(mailEditText.getText().toString());
			} catch (GenericException e) {
				// TODO Auto-generated catch block
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.invalidEmail));
				dialog.show(getSupportFragmentManager(),""+POPUP_EMPTY_FIELDS);
				return false;
			}
			return true;
		}
		else{
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.emptyFields));
			dialog.show(getSupportFragmentManager(),""+POPUP_EMPTY_FIELDS);
			return false;
		}
			
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "onActivityResult: "+ requestCode + " , "+ resultCode);
		switch (requestCode) {
		case SaleScreen.SALE_CODE:
			if(resultCode==RESULT_OK){
				if( data != null && data.getExtras()!= null){
					Bundle info=data.getExtras();
					clientSelected= (ClientBean) info.getSerializable(Clients.CLIENT_SELECTED);
					mail=(EmailBean) info.getSerializable(EmailBean.EMAIL);
					folio=(String) info.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
					rfc=(String) info.getSerializable(CustomerBean.RFC);
					sale=(SaleBean) info.getSerializable(SaleBean.SALE);
					operation=(OperationsBean) info.getSerializable(OperationsBean.OPERATION);
					Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "cliente seleccionado: "+ clientSelected + " correo: "+ mail + " folio: "+ folio + " rfc " +rfc  + "  venta: "+ sale + " operacion: " + operation);
					if(clientSelected != null ){
						mailEditText.setText(clientSelected.getMail());
						clientEditText.setText(clientSelected.getCompleteName());
					}
				}
			}
			break;
		case SaleScreen.SETTINGS_CODE:
			if(resultCode==RESULT_OK){
				if(data != null && data.getExtras()!= null){
					Bundle info=data.getExtras();
					clientSelected= (ClientBean) info.getSerializable(Clients.CLIENT_SELECTED);
					mail=(EmailBean) info.getSerializable(EmailBean.EMAIL);
					folio=(String) info.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
					rfc=(String) info.getSerializable(CustomerBean.RFC);
					sale=(SaleBean) info.getSerializable(SaleBean.SALE);
					operation=(OperationsBean) info.getSerializable(OperationsBean.OPERATION);
					Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "cliente seleccionado: "+ clientSelected + " correo: "+ mail + " folio: "+ folio + " rfc " +rfc  + "  venta: "+ sale + " operacion: " + operation);
					
					mailEditText.setText(clientSelected.getMail());
					clientEditText.setText(clientSelected.getCompleteName());
				}
				
			}
			else if(resultCode==SaleScreen.HOME_RESULT){
				setResult(SaleScreen.HOME_RESULT);
				finish();
			}
				
			break;
		}
		
	}
	public void clientsList(View v){
		ArrayList<Serializable> params=new ArrayList<Serializable>();
		params.add(folio);
		params.add(rfc);
		params.add(mail);
		params.add(sale);
		params.add(operation);
		ArrayList<String> paramNames=new ArrayList<String>();
		paramNames.add(CustomerBean.FOLIO_PREAPERTURA);
		paramNames.add(CustomerBean.RFC);
		paramNames.add(EmailBean.EMAIL);
		paramNames.add(SaleBean.SALE);
		paramNames.add(OperationsBean.OPERATION);
		if(operation==null){
			Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "viene de una venta");
			goForwardForResult(Clients.class, this, params, paramNames, SaleScreen.SALE_CODE);
		}
			
		else{
			Logger.log(Logger.MESSAGE, EMAIL_CONFIRMATION, "viene de una cancelación");
			goForwardForResult(Clients.class, this, params, paramNames,SaleScreen.SETTINGS_CODE);
		}
	}
	@Override
	public void sendMessage(Context ctx, Message message, String parameter) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void sendStatus(Context ctx, int status) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void sendMessage(Context ctx, Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
}
