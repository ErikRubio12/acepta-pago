package mx.com.bancoazteca.aceptapago.gui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.gps.SingleShotLocationProvider;
import mx.com.bancoazteca.aceptapago.ws.Constants;
import mx.com.bancoazteca.ui.BaseActivity;

/**
 * Created by ErikRubio on 01/03/18.
 */

public class MapTicketScreen extends BaseActivity{

    private ImageView mapImage;

    private LatLng KYIV;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    private MapView mMapView;
    private int mMapWidth = 600;
    private int mMapHeight = 400;
    private GoogleMapOptions options;
    private Bundle mapViewBundle;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_screen_layout);
        mapImage = (ImageView) findViewById(R.id.mapImage);

        mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }


//        try {
//            Log.i(Constants.GLOBAL_TAG, "initializing map");
//            MapsInitializer.initialize(this);
//        } catch (Exception e) {
//            Log.e(Constants.GLOBAL_TAG, "error while initializing map screen" + e);
//            e.printStackTrace();
//        }


        SingleShotLocationProvider.requestSingleUpdate(MapTicketScreen.this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Log.d("Location", "my location is " + location.latitude + location.longitude);
                        KYIV = new LatLng(location.latitude,location.longitude);

                        options = new GoogleMapOptions()
                                .compassEnabled(false)
                                .mapToolbarEnabled(false)
                                .camera(CameraPosition.fromLatLngZoom(KYIV,15))
                                .liteMode(true);
                        mMapView = new MapView(MapTicketScreen.this, options);
                        mMapView.onCreate(mapViewBundle);

                        mMapView.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
//                                popupTitle=R.string.popUpMessage;
//                                popupMsg= R.string.wait;
//                                showDialog(POPUP_MESSAGES_PROGRESS);
                                googleMap.addMarker(new MarkerOptions()
                                        .position(KYIV)
                                        .title("Kyiv"));
                                mMapView.measure(View.MeasureSpec.makeMeasureSpec(mMapWidth, View.MeasureSpec.EXACTLY),
                                        View.MeasureSpec.makeMeasureSpec(mMapHeight, View.MeasureSpec.EXACTLY));
                                mMapView.layout(0, 0, mMapWidth, mMapHeight);

                                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                    @Override
                                    public void onMapLoaded() {
                                        mMapView.setDrawingCacheEnabled(true);
                                        mMapView.measure(View.MeasureSpec.makeMeasureSpec(mMapWidth, View.MeasureSpec.EXACTLY),
                                                View.MeasureSpec.makeMeasureSpec(mMapHeight, View.MeasureSpec.EXACTLY));
                                        mMapView.layout(0, 0, mMapWidth, mMapHeight);
                                        mMapView.buildDrawingCache(true);
                                        Bitmap b = Bitmap.createBitmap(mMapView.getDrawingCache());
                                        mMapView.setDrawingCacheEnabled(false);
                                        mapImage.setImageBitmap(b);
//                                        removeDialog(POPUP_MESSAGES_PROGRESS);
                                    }






                                });

                            }
                        });
                    }
                });

    }

    protected void loadUiComponents(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }
    }



}
