package mx.com.bancoazteca.aceptapago.gui;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.util.Logger;

public class ContractScreen extends BaseActivity {

    private static final String CONTRACT_SCREEN = "Contract Signature";
    private TextView txtContract;
    private RegistrationBean register;
    private CheckBox checkBoxContract;
    private Button btn_acept;
    private CustomerBean customer;
    private Toolbar mtoolbar;

    @Override
    protected void loadUiComponents() {
        super.loadUiComponents();
        setContentView(R.layout.activity_contract_screen);
        txtContract=(TextView)findViewById(R.id.txt_contract);
        checkBoxContract=(CheckBox)findViewById(R.id.checkBox_contract);
        btn_acept=(Button)findViewById(R.id.btn_next_contract);
        mtoolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }


        AssetManager am = this_.getAssets();



        StringBuilder text = new StringBuilder();

        try {
            InputStream is = am.open("ContratoAdhesionDos.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

        txtContract.setText(text.toString());
    }


    @Override
    protected void processData() {
        super.processData();
        customer= (CustomerBean) params.getSerializable(CustomerBean.CUSTOMER);
        register=(RegistrationBean)params.getSerializable(RegistrationBean.REGISTER);
        Logger.log(Logger.MESSAGE, CONTRACT_SCREEN, "viene del contrato: " + register);

        checkBoxContract.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    btn_acept.setEnabled(true);
                }else {
                    btn_acept.setEnabled(false);
                }
            }
        });
        btn_acept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(RegistrationBean.REGISTER, register);
                intent.putExtra(ConfirmationDataScreen.CONFIRMATION_SCREEN, true);
                goForwardForResult(ContractSignatureScreen.class, getApplicationContext(), RegistrationMenu.CONTRACT_CODE);
                //finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RegistrationMenu.CONTRACT_CODE:
                Log.i("TAG_REgistration_menu", "contract_code");
                if(resultCode==RESULT_OK){
                    setResult(RESULT_OK);
                    finish();
                }
        }
    }
}
