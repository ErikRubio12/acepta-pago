package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.jobs.ChangePasswordJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Utility;

public class ChangePasswordScreen extends BaseActivity implements MessageListener<Message, String>{
	
	private EditText user;
	private EditText pw;
    //	private PaswordSecurityEditText newPassword;
	private EditText newPw;
	private EditText confirmNewPw;
	private static final int MAX_PASSWORD_LENGTH = 50;
	private static final int MIN_PASSWORD_LENGTH = 1;
	private static final int POPUP_CHANGE_PASSWORD = 21;
	private static final int POPUP_VALID_FORM = 22;
	protected static final int POPUP_ERROR = 23;
	protected static final int POPUO_CONNECTION_ERROR = 24;
	protected static final int POPUP_PASSWORD_CHANGED = 25;
	private static final int POPUP_VALID_CHARACTERS = 26;
	public final static String CHARACTERES = "Caracter invalido";
	private Toolbar mtoolbar;

	//	private SeekBar passwordBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		char extraChars[]={'_','-','@','.'};
		setContentView(R.layout.change_password);
		this.user=(EditText) findViewById(R.id.userField);
		this.pw=(EditText) findViewById(R.id.pwField);
		this.newPw=(EditText) findViewById(R.id.newPwField);
		this.confirmNewPw=(EditText) findViewById(R.id.confirmNewPwField);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
		InputFilter filters[]=null;
		filters= new InputFilter[user.getFilters().length +1];
		System.arraycopy(user.getFilters(), 0, filters,0,user.getFilters().length);
		filters[user.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		user.setFilters(filters);
		
		filters=null;
		filters= new InputFilter[pw.getFilters().length +1];
		System.arraycopy(pw.getFilters(), 0, filters,0,pw.getFilters().length);
		filters[pw.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		pw.setFilters(filters);
		
		filters=null;
		filters= new InputFilter[newPw.getFilters().length +1];
		System.arraycopy(newPw.getFilters(), 0, filters,0,newPw.getFilters().length);
		filters[newPw.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC,extraChars);
		newPw.setFilters(filters);
		
		filters=null;
		filters= new InputFilter[confirmNewPw.getFilters().length +1];
		System.arraycopy(confirmNewPw.getFilters(), 0, filters,0,confirmNewPw.getFilters().length);
		filters[confirmNewPw.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC,extraChars);
		confirmNewPw.setFilters(filters);
		
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton accept;
		switch (id) {
		case POPUP_CHANGE_PASSWORD :
			popUp= new PopUp(this, R.string.popUpMessage,R.string.makingpasswordChange);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.setCancelable(false);
			popUp.create();
			return popUp.getDialog();
		default:
			return super.onCreateDialog(id);
		}
		
	}
	public void accept(View v){
		if (validForm()) {
			showDialog(POPUP_CHANGE_PASSWORD);
			int lengthPass = pw.length();
			char[] passw = new char[lengthPass];
			pw.getText().getChars(0,lengthPass,passw,0);
			int lengthNewPass = newPw.length();
			char[] passwNew = new char[lengthNewPass];
			newPw.getText().getChars(0,lengthNewPass,passwNew,0);
			backgroundThread= new Thread(new ChangePasswordJob(this,this, new LoginBean(user.getText().toString(),passw), passwNew));
//			backgroundThread= new Thread(new ChangePasswordJob(this,this, new LoginBean(user.getText().toString(),password.getText().toString()), newPassword.getText().toString()));
			backgroundThread.start();
		}else{
			SimpleDialog dialog=SimpleDialog.newInstance(getString(popupTitle),getString(popupMsg));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
		}
	}
	public boolean isValidCharacters(){
		boolean answer = true;
		if(!Utility.isvalidCharacters(user.getText().toString())){
			answer=false;
			user.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(pw.getText().toString())){
			answer=false;
			pw.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(newPw.getText().toString())){
			answer=false;
			newPw.setError(CHARACTERES);
		}
		return answer;
	}
	@Override
	protected boolean validForm() {
		// TODO Auto-generated method stub
		if (user.getText().toString().length() ==0 ||
			pw.getText().toString().length() ==0 ||
			newPw.getText().toString().length() ==0 ||
			confirmNewPw.getText().toString().length() ==0){
			popupMsg=R.string.emptyFields;
			popupTitle=R.string.popUpError;
			return false;
		}
		else if(!newPw.getText().toString().equals(confirmNewPw.getText().toString())){
			popupMsg=R.string.passwordsShouldBeSame;
			popupTitle=R.string.popUpError;
			return false;
		}
		else if (newPw.getText().toString().length()>MAX_PASSWORD_LENGTH || newPw.getText().toString().length()<MIN_PASSWORD_LENGTH) {
			popupMsg=R.string.badUserPasswordLength;
			popupTitle=R.string.popUpMessage;
			return false;
		}
		return true;
	}
	@Override
	public void sendMessage(Context ctx,Message message,final String parameter) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (isFinishing()) 
					return;
				this_.removeDialog(POPUP_CHANGE_PASSWORD);
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.changePasswordError));
				dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
					@Override
					public void onAccept(DialogInterface dialog) {
						finish();
					}
				});
				dialog.show(getSupportFragmentManager(),""+POPUP_ERROR);
			}
		});
	}
	@Override
	public void sendStatus(Context ctx,final int status) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (isFinishing()) 
					return;
				this_.removeDialog(POPUP_CHANGE_PASSWORD);
				if (status==Message.EXCEPTION){
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUO_CONNECTION_ERROR);
				}else if (status==Message.OK) {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.passwordChanged));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_PASSWORD_CHANGED);
				}
			}
		});
	}
	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
}
