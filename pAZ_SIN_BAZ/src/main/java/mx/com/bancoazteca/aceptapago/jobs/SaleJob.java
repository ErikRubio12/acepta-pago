package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.util.Log;
import android.util.Xml;
import mx.com.bancoazteca.beans.CardBean;
import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.hw.LocationThread;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.Configuration;
import mx.com.bancoazteca.aceptapago.beans.PendingSale;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.db.daos.CardDao;
import mx.com.bancoazteca.aceptapago.db.daos.SaleDao;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class SaleJob extends BaseService<Message, String> {
	
	
	private static final String QUERY_DEVICE_METHOD ="BusinessToBusinessServiceConsultarTerminalIphone";
	private static final String QUERY_DEVICE_ACTION = null;
	private static final String QUERY_DEVICE_NAMESPACE = "http://service.btb.com";
	private static final String QUERY_DEVICE_URL =Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String QUERY_DEVICE_PARAM = "xml";
	private static final String SALE_METHOD = "BusinessToBusinessService";
	private static final String SALE_ACTION =null;
	private static final String SALE_NAMESPACE = "http://service.btb.com";
	private static final String SALE_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String SALE_PARAM = "xml";
	private static final String SALE_JOB = "SALE JOB";
	private static final String SALE_RETURN_PARAM = "xml";
	private static final String SALE_RETURN_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String SALE_RETURN_NAMESPACE = "http://service.btb.com";
	private static final String SALE_RETURN_METHOD = "BusinessToBusinessService";
	private static final String SALE_RETURN_ACTION = null;
	private static final String REVERSE_CAUSE_RESPONSE_RECIVIED_TOO_LATE = "68";
	private static final String REVERSE_CAUSE_SYSTEM_MAL_FUCTION = "22";
	private Location location;
	private String rfc;
	private String folio;
	private CardDao dao;
	private StringBuffer message= new StringBuffer();
	private CardBean creditCard;
	private long amount;
	private String afiliacion="";
	private String idTx= null;
	private String state;
	private String city;
	private String idDevice="";
	private String idDeviceLogin;
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private String reference;
	private String authorizationNumber=null;
	private boolean isSaleReturnTimeOut=false;
	public final static int SALE_SUCCED=10;
	private String validity = null;
	private SaleDao saleDao;	
	private String channel;
	private String imeiField;
	private String longitudStr;
	private String latitudStr;
	private PendingSale pendingdSaleNew;

	public SaleJob(Context ctx,MessageListener<Message, String>listener, String folio, String rfc,CardBean creditCard,long amount,Location location,String idDeviceLogin) {
		this.listener=listener;
		this.location=location;
		this.folio=folio;
		this.rfc=rfc;
		this.creditCard=creditCard;
		this.amount=amount;
		this.idDeviceLogin=idDeviceLogin;
		this.ctx=ctx;
		this.dao= new CardDao(Utility.getInitialContext());
		this.saleDao= new SaleDao(Utility.getInitialContext());
	}
	@Override
	public void run() {
		
		try {			
			location=LocationService.getMostRecentLocation();
			if(location==null){
				location = new Location(LocationThread.GPS);	
			}
			channel=selectChanelSale(creditCard);
			idTx=generateIdTx(idDeviceLogin);//Recibir idDevice
			String pendinSaledataCard=dataCardPending(creditCard);
			validity=validityCard(creditCard);
			if(Configuration.isSwiped()){
				creditCard.setDevice(CardBean.READER_OCOM_SWIPED);
			}else{
				creditCard.setDevice(CardBean.READER_OCOM_BLUE_CHIP);
			}
			imeiField=generateImeiField(creditCard);
			longitudStr=longitudRequest(location);
			latitudStr=latitudRequest(location);
			pendingdSaleNew = new PendingSale();
			pendingdSaleNew.setChannel(channel);
			pendingdSaleNew.setTxDate(Utility.formatDate(new Date(), "yyyy-MM-dd"));
			pendingdSaleNew.setTerminal(Encrypt.encryptStringWS(idDeviceLogin));//idDevice traerlo desde el login
			pendingdSaleNew.setIdTx(idTx);
			pendingdSaleNew.setPan(Encrypt.encryptStringWS(creditCard.getMaskedPan()));
			pendingdSaleNew.setCardData(Encrypt.encryptStringWS(pendinSaledataCard));
			pendingdSaleNew.setValidity(Encrypt.encryptStringWS(validity));
			pendingdSaleNew.setAmount(Encrypt.encryptStringWS(amount+""));
			pendingdSaleNew.setImei(Encrypt.encryptStringWS(imeiField));
			pendingdSaleNew.setLongitud(Encrypt.encryptStringWS(longitudStr));
			pendingdSaleNew.setLatitud(Encrypt.encryptStringWS(latitudStr));
			pendingdSaleNew.setCauseReverse(Encrypt.encryptStringWS(REVERSE_CAUSE_RESPONSE_RECIVIED_TOO_LATE));
			saleDao.insertSale(pendingdSaleNew);
			//Guardar los datos
			if(checkDevice()){			
				if(sale()==0){
					handleTimeOut();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"IOException: "+e.getMessage() );
			listener.sendStatus(ctx,Message.EXCEPTION);
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"TimeoutException: "+e.getMessage() );
			handleTimeOut();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"InterruptedException: "+e.getMessage() );
			handleTimeOut();
		}
	}
	private void handleTimeOut(){
		//if(!"".equalsIgnoreCase(idDevice)){
		Log.i("SALEJOB", "*/*/*/*/*/*/* handledTimeout");
		if(idDeviceLogin!=null){
			if(saleReturn()){
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg,message.toString());
				saleDao.deleteSale(pendingdSaleNew);
			} else{
				if (operationCode != null && !"0".equalsIgnoreCase(operationCode) && !"00".equalsIgnoreCase(operationCode)) {					
					Message msg= new Message();
					msg.setType(Message.ERROR);
					listener.sendMessage(ctx,msg,message.toString());
				}
				else if(isSaleReturnTimeOut)
					listener.sendStatus(ctx,Message.EXCEPTION);
				else
					listener.sendStatus(ctx,Message.EXCEPTION);
			}
		}
		else
			listener.sendStatus(ctx,Message.EXCEPTION);
	}
	private StringBuilder createCheckDeviceRequest(){		
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("CTermIphone"));
		request.append("</idservicio>");
		request.append("<idComercio>");
		request.append(Encrypt.encryptStringWS(folio));
		request.append("</idComercio>");
		request.append("<rfc>");
		request.append(Encrypt.encryptStringWS(rfc));
		request.append("</rfc>");
		request.append("<imei>");
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI()));
		request.append("</imei>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		return request;
	}
	//REVERSO
	private StringBuilder createSaleReturnRequest(){
		
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		if(creditCard.isFullBack() && creditCard.getRawTracks() != null)
			request.append(Encrypt.encryptStringWS("reversoPagoAztecaFullBack"));		
		else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0)
			request.append(Encrypt.encryptStringWS("reversoPagoAztecaEMV"));
		else if(creditCard.getChipData() != null)
			request.append(Encrypt.encryptStringWS("reversoPagoAztecaChip"));
		request.append("</canalEntrada>");
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>");
		request.append("<tipo_operacion>");
		request.append(Encrypt.encryptStringWS("420"));
		request.append("</tipo_operacion>");
		request.append("<idterminal>");
		request.append(Encrypt.encryptStringWS(idDeviceLogin));
		request.append("</idterminal>");
		request.append("<idTransaccion>");
		request.append(Encrypt.encryptStringWS(idTx));
		request.append("</idTransaccion>");
		request.append("<folio>");
		request.append("");
		request.append("</folio>");
		request.append("<autorizacion>");
		request.append("");
		request.append("</autorizacion>");
		request.append("<referencia>");
		request.append("");
		request.append("</referencia>");
		request.append("<track>");
		if(creditCard.isFullBack() && creditCard.getRawTracks() != null  && creditCard.getRawTracks().indexOf("{")>=0)
			request.append(Encrypt.encryptStringWS(creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}"));		
		else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0)
			request.append(Encrypt.encryptStringWS(creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}"));
		else if(creditCard.getChipData()!= null)
			request.append(Encrypt.encryptStringWS(creditCard.getChipData() + "||"+ creditCard.getKSN().toUpperCase(Locale.getDefault())));
		request.append("</track>");
		request.append("<fechaVencimiento>");
		request.append(Encrypt.encryptStringWS(validity));
		request.append("</fechaVencimiento>");
		request.append("<longitud>");
		request.append(Encrypt.encryptStringWS(longitudStr));
		request.append("</longitud>");
		request.append("<latitud>");
		Logger.log(Logger.MESSAGE, SALE_JOB, "latitudStr : "+latitudStr );
		request.append(Encrypt.encryptStringWS(latitudStr));
		request.append("</latitud>");
		request.append("<codReverse>");		
		request.append(Encrypt.encryptStringWS(REVERSE_CAUSE_SYSTEM_MAL_FUCTION));
		request.append("</codReverse>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		return request;
	}
	private StringBuilder createSaleRequest(){
		//Logger.log(Logger.MESSAGE, SALE_JOB,"rawTracks "+ creditCard.getRawTracks());
		//Logger.log(Logger.MESSAGE, SALE_JOB,"Chip Data "+ creditCard.getChipData());
		StringBuilder request= new StringBuilder();
		request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>");
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>");
		request.append("<canalEntrada>");
		request.append(Encrypt.encryptStringWS(channel));
		request.append("</canalEntrada>");
		request.append("<tipo_operacion>");
		request.append(Encrypt.encryptStringWS("200"));
		request.append("</tipo_operacion>");
		request.append("<fechaTransaccion>");
		request.append(Encrypt.encryptStringWS(Utility.formatDate(new Date(), "yyyy-MM-dd")));
		request.append("</fechaTransaccion>");
		request.append("<idterminal>");
		request.append(Encrypt.encryptStringWS(idDevice));
		request.append("</idterminal>");
		request.append("<idTransaccion>");
		//Logger.log(Logger.MESSAGE, SALE_JOB,"Id Tx: "+ idTx);
		request.append(Encrypt.encryptStringWS(idTx));
		request.append("</idTransaccion>");
		request.append("<titular>");
		request.append("");
		request.append("</titular>");
		request.append("<tipoTarjeta>");
		//Logger.log(Logger.MESSAGE, SALE_JOB, " masked pan: "+ creditCard.getMaskedPan() );
		String cardType=dao.queryCardType(creditCard.getMaskedPan().substring(0,6));
		if(cardType==null || "".equalsIgnoreCase(cardType))
			request.append(Encrypt.encryptStringWS("1"));
		else
			request.append(Encrypt.encryptStringWS(CardBean.DEBIT.equalsIgnoreCase(cardType) ? "1" : "2"));
		request.append("</tipoTarjeta>");
		request.append("<tarjeta>");
		String cardData=null;
		
		if(creditCard.isFullBack() && creditCard.getRawTracks() != null  && creditCard.getRawTracks().indexOf("{")>=0){//FALL BACK
			cardData=creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}";
			request.append(Encrypt.encryptStringWS(cardData));			
		}
		else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0){//BANDA
			cardData=creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}";	
			request.append(Encrypt.encryptStringWS(cardData));
		}					
		else if(creditCard.getChipData()!= null){//CHIP
			request.append("");
			
		}
		request.append("</tarjeta>");
		request.append("<cvv>");
		request.append("");
		request.append("</cvv>");
		request.append("<fechaVencimiento>");
		request.append(Encrypt.encryptStringWS(validity));
		request.append("</fechaVencimiento>");
		request.append("<monto>");
		request.append(Encrypt.encryptStringWS(amount+""));
		request.append("</monto>");
		request.append("<imei>");
		request.append(Encrypt.encryptStringWS(imeiField));
		request.append("</imei>");
		if(creditCard.getChipData() != null){
			cardData=creditCard.getChipData() + "||"+ creditCard.getKSN().toUpperCase(Locale.getDefault());
			request.append("<datosChip>");
			request.append(Encrypt.encryptStringWS(cardData));
			request.append("</datosChip>");
		}		
		request.append("<longitud>");
		Logger.log(Logger.MESSAGE, SALE_JOB, "longitud : "+longitudStr );
		request.append(Encrypt.encryptStringWS(longitudStr));
		request.append("</longitud>");
		request.append("<latitud>");
		Logger.log(Logger.MESSAGE, SALE_JOB, "latitudStr : "+latitudStr );
		request.append(Encrypt.encryptStringWS(latitudStr));
		request.append("</latitud>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		return request;
		
	}


//	private StringBuilder createSaleRequestSinEncriptar(){
//		StringBuilder request= new StringBuilder();
//		request= new StringBuilder();
//		request.append("<bancoAzteca>");
//		request.append("<eservices>");
//		request.append("<request>");
//		request.append("<idservicio>");
//		request.append("btob");
//		request.append("</idservicio>");
//		request.append("<origen>");
//		request.append("02");
//		request.append("</origen>");
//		request.append("<canalEntrada>");
//		request.append(channel);
//		request.append("</canalEntrada>");
//		request.append("<tipo_operacion>");
//		request.append("200");
//		request.append("</tipo_operacion>");
//		request.append("<fechaTransaccion>");
//		request.append(Utility.formatDate(new Date(), "yyyy-MM-dd"));
//		request.append("</fechaTransaccion>");
//		request.append("<idterminal>");
//		request.append(idDevice);
//		request.append("</idterminal>");
//		request.append("<idTransaccion>");
//		//Logger.log(Logger.MESSAGE, SALE_JOB,"Id Tx: "+ idTx);
//		request.append(idTx);
//		request.append("</idTransaccion>");
//		request.append("<titular>");
//		request.append("");
//		request.append("</titular>");
//		request.append("<tipoTarjeta>");
//		//Logger.log(Logger.MESSAGE, SALE_JOB, " masked pan: "+ creditCard.getMaskedPan() );
//		String cardType=dao.queryCardType(creditCard.getMaskedPan().substring(0,6));
//		if(cardType==null || "".equalsIgnoreCase(cardType))
//			request.append("1");
//		else
//			request.append(CardBean.DEBIT.equalsIgnoreCase(cardType) ? "1" : "2");
//		request.append("</tipoTarjeta>");
//		request.append("<tarjeta>");
//		String cardData=null;
//
//		if(creditCard.isFullBack() && creditCard.getRawTracks() != null  && creditCard.getRawTracks().indexOf("{")>=0){//FALL BACK
//			cardData=creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}";
//			request.append(cardData);
//		}
//		else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0){//BANDA
//			cardData=creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}";
//			request.append(cardData);
//		}
//		else if(creditCard.getChipData()!= null){//CHIP
//			request.append("");
//
//		}
//		request.append("</tarjeta>");
//		request.append("<cvv>");
//		request.append("");
//		request.append("</cvv>");
//		request.append("<fechaVencimiento>");
//		request.append(validity);
//		request.append("</fechaVencimiento>");
//		request.append("<monto>");
//		request.append(amount+"");
//		request.append("</monto>");
//		request.append("<imei>");
//		request.append(imeiField);
//		request.append("</imei>");
//		if(creditCard.getChipData() != null){
//			cardData=creditCard.getChipData() + "||"+ creditCard.getKSN().toUpperCase(Locale.getDefault());
//			request.append("<datosChip>");
//			request.append(cardData);
//			request.append("</datosChip>");
//		}
//		request.append("<longitud>");
//		Logger.log(Logger.MESSAGE, SALE_JOB, "longitud : "+longitudStr );
//		request.append(longitudStr);
//		request.append("</longitud>");
//		request.append("<latitud>");
//		Logger.log(Logger.MESSAGE, SALE_JOB, "latitudStr : "+latitudStr );
//		request.append(latitudStr);
//		request.append("</latitud>");
//		request.append("</request>");
//		request.append("</eservices>");
//		request.append("</bancoAzteca>");
//
//		return request;
//	}
	private boolean saleReturn(){
		operationCode=null;
		systemError=null;
		message= new StringBuffer();
		SoapRequest<String> req= new SoapRequest<String>(SALE_RETURN_METHOD, SALE_RETURN_ACTION);
		req.setNameSpace(SALE_RETURN_NAMESPACE);
		req.setUrl(SALE_RETURN_URL);
		Parameter<String,String> param= new Parameter<String, String>(SALE_RETURN_PARAM,createSaleReturnRequest().toString());
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if (res==null) {
				return false;
			}
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equals(OPERATION_CODE_TAG)) 
						operationCode= Encrypt.decryptStringWS(parser.getText());
					
					else if (currentTag.equals(SYSTEM_ERROR_TAG)) 
						systemError= Encrypt.decryptStringWS(parser.getText());
					
					
					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			message= new StringBuffer(operationCode==null ? "" : operationCode );
			message.append(" - ");
			message.append(systemError==null ? "" : systemError);
			Logger.log(Logger.MESSAGE, SALE_JOB,"error sistema: "+ systemError + " codigo: " + operationCode );
			if("0".equalsIgnoreCase(operationCode) || "00".equalsIgnoreCase(operationCode) )
				return true;
			else
				return false;
			
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"TimeoutException: "+ e.getMessage());
			isSaleReturnTimeOut=true;
			return false;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"XmlPullParserException: "+ e.getMessage());
			return false;
		}catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			isSaleReturnTimeOut=true;
			Logger.log(Logger.EXCEPTION, SALE_JOB,"InterruptedIOException: "+ e.getMessage());
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"IOException: "+ e.getMessage());
			return false;
		} 
		
	}
	private boolean checkDevice() throws IOException, TimeoutException, InterruptedException{
		idDevice="";
		SoapRequest<String> req= new SoapRequest<String>(QUERY_DEVICE_METHOD, QUERY_DEVICE_ACTION);
		req.setNameSpace(QUERY_DEVICE_NAMESPACE);
		req.setUrl(QUERY_DEVICE_URL);
		Parameter<String,String> param= new Parameter<String, String>(QUERY_DEVICE_PARAM, createCheckDeviceRequest().toString());
		req.addParameter(param);
		SoapResponse res;
		res = Connections.makeSecureSoapConnection(req,TIME_OUT);
		if(res==null || res.getResponse()==null ){
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		}
		Node node=null;
	
		Document dom= Utility.createDom(res.getResponse().toString());
		if(( node=dom.getElementsByTagName("codigo_operacion").item(0))!= null ){
			operationCode=node.getChildNodes().item(0).getNodeValue();
			operationCode=Encrypt.decryptStringWS(operationCode);
		}
		if((node=dom.getElementsByTagName("error_sistema").item(0)) != null){
			systemError=node.getChildNodes().item(0).getNodeValue();
			systemError=Encrypt.decryptStringWS(systemError);
		}
		
		if("0".equals(operationCode)){
			node=dom.getElementsByTagName("idterminal").item(0);
			idDevice=node.getChildNodes().item(0).getNodeValue();
			idDevice=Encrypt.decryptStringWS(idDevice);
			
			node=dom.getElementsByTagName("afiliacion").item(0);
			afiliacion=node.getChildNodes().item(0).getNodeValue();
			afiliacion=Encrypt.decryptStringWS(afiliacion);
			
			node=dom.getElementsByTagName("estado").item(0);
			if(node !=null  && node.getChildNodes()!=null){
				state=node.getChildNodes().item(0).getNodeValue();
				state=Encrypt.decryptStringWS(state);
			} else
				state="";
			
			node=dom.getElementsByTagName("municipio").item(0);
			if(node != null && node.getChildNodes() !=null){
				city=node.getChildNodes().item(0).getNodeValue();
				city=Encrypt.decryptStringWS(city);
			} else
				city="";
			Connections.closeSoapConnection(res);
			return true;
		} else{
			message.append(operationCode);
			message.append(" - ");
			message.append(systemError==null ? "" : systemError);
			Message msg= new Message();
			msg.setType(Message.ERROR);
			listener.sendMessage(ctx,msg,message.toString());
			Connections.closeSoapConnection(res);
			return false;
		}
	}
	
	private int sale() throws IOException, TimeoutException, InterruptedException{
		operationCode=null;
		systemError=null;
		message= new StringBuffer();
		SoapRequest<String> req;
		req= new SoapRequest<String>(SALE_METHOD, SALE_ACTION);
		req.setNameSpace(SALE_NAMESPACE);
		req.setUrl(SALE_URL);
		//QUITAR
//		SoapRequest<String> reqSinEncriptar;
//		reqSinEncriptar= new SoapRequest<String>(SALE_METHOD, SALE_ACTION);
//		reqSinEncriptar.setNameSpace(SALE_NAMESPACE);
//		reqSinEncriptar.setUrl(SALE_URL);


		Parameter<String,String> param;
//		//QUITAR
//		Parameter<String,String> paramSinEcriptar;

		String r=createSaleRequest().toString();
		//QUITAR
//		String rSinEncriptar=createSaleRequestSinEncriptar().toString();

		param= new Parameter<String, String>(SALE_PARAM,r);
		Logger.log(Logger.MESSAGE, "WS", r);
		req.addParameter(param);
		//QUITAR
//		paramSinEcriptar= new Parameter<String, String>(SALE_PARAM,rSinEncriptar);
//		Logger.log(Logger.MESSAGE, "WS", rSinEncriptar);
//		reqSinEncriptar.addParameter(paramSinEcriptar);

		SoapResponse res = Connections.makeSecureSoapConnection(req,TIME_OUT);
		//QUITAR
//		SoapResponse resSinEncriptar = Connections.makeSecureSoapConnection(reqSinEncriptar,TIME_OUT);
		if(res!=null && res.getResponse()!=null){
			Document dom=Utility.createDom(res.getResponse().toString());
			Node node=dom.getElementsByTagName("codigo_operacion").item(0);
			if(node != null){
				operationCode=node.getChildNodes().item(0).getNodeValue();
				Logger.log(Logger.MESSAGE, SALE_JOB,"codigo operacion: "+ operationCode );
				if(operationCode!=null && operationCode.length()>0)
					operationCode=Encrypt.decryptStringWS(operationCode);
			}
			if(dom.getElementsByTagName("errores").item(0)!=null){
				systemError=dom.getElementsByTagName("errores").item(0).getFirstChild().getNodeValue();
				systemError=Encrypt.decryptStringWS(systemError);
			}
				
			else if (dom.getElementsByTagName("error_sistema").item(0)!=null){
				systemError=dom.getElementsByTagName("error_sistema").item(0).getFirstChild().getNodeValue();
				systemError=Encrypt.decryptStringWS(systemError);
			}
			else
				systemError= "Error al realizar la Venta";
			
			
			
			if("0".equals(operationCode) ||"00".equals(operationCode)){
				if(dom.getElementsByTagName("folio").item(0)==null){
					message.append(operationCode);
					message.append(" - ");
					message.append(systemError==null ? "" : systemError);
					Message msg= new Message();
					msg.setType(Message.ERROR);
					listener.sendMessage(ctx,msg,message.toString());
					Connections.closeSoapConnection(res);
					return 0;
				}
				else if(dom.getElementsByTagName("fechaTransaccion").item(0)==null){
					message.append(operationCode);
					message.append(" - ");
					message.append(systemError==null ? "" : systemError);
					Message msg= new Message();
					msg.setType(Message.ERROR);
					listener.sendMessage(ctx,msg,message.toString());
					Connections.closeSoapConnection(res);
					return 0;
				}
				else{
					node=dom.getElementsByTagName("fechaTransaccion").item(0);
					String fecha=node.getChildNodes().item(0).getNodeValue();
					fecha=Encrypt.decryptStringWS(fecha);
					node=dom.getElementsByTagName("folio").item(0);
					folio=node.getChildNodes().item(0).getNodeValue();
					folio=Encrypt.decryptStringWS(folio);
					if(folio == null || folio.length()<=0 || folio.toLowerCase(Locale.getDefault()).equals("null") ){
						message.append(operationCode);
						message.append(" - ");
						message.append(systemError==null ? "" : systemError);
						Message msg= new Message();
						msg.setType(Message.ERROR);
						listener.sendMessage(ctx,msg,message.toString());
						Connections.closeSoapConnection(res);
						return 0;
					}
					else if (folio.length()==1 && folio.equals("0")){
						message.append(operationCode);
						message.append(" - ");
						message.append(systemError==null ? "" : systemError);
						Message msg= new Message();
						msg.setType(Message.ERROR);
						listener.sendMessage(ctx,msg,message.toString());
						Connections.closeSoapConnection(res);
						return 0;
					}
					else{
						node=dom.getElementsByTagName("autorizacion").item(0);
						authorizationNumber=node.getChildNodes().item(0).getNodeValue();
						authorizationNumber=Encrypt.decryptStringWS(authorizationNumber);
						
						
						
						String arqc=null;
						String aid=null;
						String amountReturned=null;
						if(dom.getElementsByTagName("arqc").item(0)!=null){
							node=dom.getElementsByTagName("arqc").item(0);
							arqc=node.getChildNodes().item(0).getNodeValue();
							arqc=Encrypt.decryptStringWS(arqc);
						}
						if(dom.getElementsByTagName("aid").item(0)!=null){
							node=dom.getElementsByTagName("aid").item(0);
							aid=node.getChildNodes().item(0).getNodeValue();
							aid=Encrypt.decryptStringWS(aid);
						}
						if(dom.getElementsByTagName("monto").item(0)!=null){
							node=dom.getElementsByTagName("monto").item(0);
							amountReturned=node.getChildNodes().item(0).getNodeValue();
							amountReturned=Encrypt.decryptStringWS(amountReturned);
						}
						else{
							message.append(operationCode);
							message.append(" - ");
							message.append(systemError==null ? "" : systemError);
							Message msg= new Message();
							msg.setType(Message.ERROR);
							listener.sendMessage(ctx,msg,message.toString());
							Connections.closeSoapConnection(res);
							return 0;
						}
						
						//Logger.log(Logger.MESSAGE, SALE_JOB,"autorizacion = "+ authorizationNumber);
						node=dom.getElementsByTagName("referencia").item(0);
						reference=node.getChildNodes().item(0).getNodeValue();
						reference=Encrypt.decryptStringWS(reference);
						/*Logger.log(Logger.MESSAGE, SALE_JOB,"referencia = "+ reference);
						Logger.log(Logger.MESSAGE, SALE_JOB,"folio = "+ folio);
						Logger.log(Logger.MESSAGE, SALE_JOB,"afiliacion = "+ afiliacion);
						Logger.log(Logger.MESSAGE, SALE_JOB,"fecha = "+ fecha);
						Logger.log(Logger.MESSAGE, SALE_JOB,"arqc = "+ arqc);
						Logger.log(Logger.MESSAGE, SALE_JOB,"aid = "+ aid);
						Logger.log(Logger.MESSAGE, SALE_JOB,"***************** MONTO REGRESADO VENTA = "+ amountReturned);*/
						SaleBean sale= new SaleBean();
						sale.setFolio(folio);
						sale.setAuthorizationNumber(authorizationNumber);
						sale.setReferenceNumber(reference);
						sale.setAfiliacion(afiliacion);
						sale.setEstado(state);
						sale.setPoblacion(city);
						sale.setArqc(arqc);
						sale.setAid(aid);
						sale.setSaleDate(Utility.dateFromFormat(fecha, "yyyy-MM-dd HH:mm:ss"));
						sale.setAmountReturned(amountReturned);
						Message msg= new Message();
						msg.setType(SALE_SUCCED);						
						/**Comentar  delete para forzar reverso con venta*/		
						saleDao.deleteSale(pendingdSaleNew);
						Logger.log(Logger.MESSAGE, SALE_JOB,"venta con exito se elimino: "+ saleDao.countSales());
						listener.sendMessage(ctx,msg,sale);
						Connections.closeSoapConnection(res);
						return 1;
					}
				}
			} else{
				saleDao.deleteSale(pendingdSaleNew);
				Logger.log(Logger.MESSAGE, SALE_JOB,"venta sin exito pero contesto : "+ saleDao.countSales());
				message.append(operationCode);
				message.append(" - ");
				message.append(systemError==null ? "" : systemError);
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg,message.toString());
				Connections.closeSoapConnection(res);
				return -1;
			}
		}
		else{
			listener.sendStatus(ctx,Message.EXCEPTION);
			return -1;
		}
		
	}
	
	
	private String selectChanelSale(CardBean card) {
		String channelLocal=null;
		if(card.isFullBack() && card.getRawTracks() != null)
			channelLocal="PagoAztecaFullBack";					
		else if(card.getRawTracks() != null && card.getRawTracks().indexOf("{")>=0)
			channelLocal="PagoAztecaDeslizadaEMV";
		else if(card.getChipData() != null)
			channelLocal="PagoAztecaChip";
		
		return channelLocal;
	}
	
	private String generateIdTx(String idDev) {
		SimpleDateFormat format2= new SimpleDateFormat("ddMMyyyyHHmmss",Locale.getDefault());
		Random r = new Random();
		long i=Utility.nextLong(r,9999999999L-100)+ 100;
		StringBuilder idTxLocal = new StringBuilder();
		idTxLocal.append(format2.format(new Date()));
		idTxLocal.append("-");
		idTxLocal.append(idDev);
		idTxLocal.append("-");
		idTxLocal.append(i);
		return idTxLocal.toString();

	}
	
	private String dataCardPending(CardBean card) {
		String cardData=null;
		if(card.isFullBack() && card.getRawTracks() != null  && card.getRawTracks().indexOf("{")>=0){//FALL BACK
			cardData=card.getRawTracks() + ",\"ksn\":\""+ card.getKSN().toUpperCase(Locale.getDefault())+ "\"}";			
		}else if(card.getRawTracks() != null && card.getRawTracks().indexOf("{")>=0){//BANDA
			cardData=card.getRawTracks() + ",\"ksn\":\""+ card.getKSN().toUpperCase(Locale.getDefault())+ "\"}";	
		}else if(creditCard.getChipData()!= null){//CHIP
			cardData=card.getChipData() + "||"+ card.getKSN().toUpperCase(Locale.getDefault());
		}
		return cardData;
	}
	
	private String validityCard(CardBean card) {
		String validityLocal= card.getStringValidity().substring(3, 5);
		validityLocal+=card.getStringValidity().subSequence(0, 2);
		return validityLocal;
	}
	
	private String generateImeiField(CardBean card) {
		StringBuffer imeiField= new StringBuffer();
		imeiField.append(HandsetInfo.getIMEI());
		imeiField.append("||");
		imeiField.append(card.getDevice());
		imeiField.append("||");
		String product=Build.PRODUCT;
		if(product==null || product.length()==0)
			product="NA";
		else{
			if(product.indexOf("||")>=0)
				product=product.replace("||", ",");
		}
		TextFilter.filterText(product);
		imeiField.append(product);
		imeiField.append("||");
		int version=Build.VERSION.SDK_INT;
		imeiField.append(version);
		imeiField.append("||");
		imeiField.append(Utility.getAppVersionCode(ctx));
		if(imeiField.length()>50){
			imeiField=new StringBuffer(imeiField.substring(0, 50));
		
		}
		return imeiField.toString();
	}
	
	private String longitudRequest(Location loc) {
		String longitudStr=""+loc.getLongitude();
		int index=longitudStr.indexOf(".");
		Logger.log(Logger.MESSAGE, SALE_JOB, "index : "+index);
		String decimales="";
		if(index>=0){
			decimales=longitudStr.substring(index+1, longitudStr.length());
			Logger.log(Logger.MESSAGE, SALE_JOB, "longitud : "+longitudStr + "   decimales" + decimales);
			if(decimales.length()>7){
				decimales=decimales.substring(0, 6);
				longitudStr=longitudStr.substring(0, index);
				longitudStr=longitudStr + "."+ decimales;
			}
		}
		return longitudStr;
	}
	
	private String latitudRequest(Location loc) {
		String latitudStr=""+loc.getLatitude();
		int index=latitudStr.indexOf(".");
		Logger.log(Logger.MESSAGE, SALE_JOB, "index : "+index);
		String decimales="";
		if(index>=0){
			decimales=latitudStr.substring(index+1, latitudStr.length());
			Logger.log(Logger.MESSAGE, SALE_JOB, "latitudStr : "+latitudStr + "   decimales" + decimales);
			if(decimales.length()>7){
				decimales=decimales.substring(0, 6);
				latitudStr=latitudStr.substring(0, index);
				latitudStr=latitudStr + "."+ decimales;
			}
		}
		return latitudStr;
	}
	
}
