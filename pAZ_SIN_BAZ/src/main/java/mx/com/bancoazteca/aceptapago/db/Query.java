package mx.com.bancoazteca.aceptapago.db;

public final class Query {
	public static String FIND_STATES_BY_COUNTRY="SELECT fiEdo,  fcDescripcion FROM CatEstado WHERE fiPais=?";
	public static String FIND_POBLACION_BY_STATE="SELECT fcDescripcion FROM CatPoblacion WHERE fiEdo =?";
	public static final String FIND_COLONY_BY_POBLACION_AND_STATE = "SELECT fcColonia FROM CatCodigoPostal Where fiPoblacion =? AND fiEdo =? AND fiPais=1 ORDER BY fcColonia";
	public static final String FIND_ZIP_CODE = "SELECT DISTINCT fcCp  FROM CatCodigoPostal Where fiPoblacion =? AND fiEdo =? AND fiPais=1 AND fcColonia like ? ORDER BY fcCp";
	public static final String FIND_MARITAL_STATUS = "SELECT fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiEstatus=1";
	public static final String FIND_MARITAL_STATUS_BY_ID = "SELECT fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiEstatus=1 AND fcValor=?";
	public static final String FIND_CITIZENSHIP = "SELECT fcValor, fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiEstatus=1";
	public static final String FIND_CITIZENSHIP_BY_ID = "SELECT fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiEstatus=1 AND fcValor=?";
	public static final String FIND_IDS = "SELECT fcvalor, fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiestatus=1";
	public static final String FIND_IDS_BY_ID = "SELECT fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiestatus=1 AND fcValor=?";
	public static final String FIND_GENDERS ="SELECT fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiestatus=1";
	public static final String FIND_EXTRA_INFO = "select  fiIdCatGiroPer,fcDescripcion from catGiroPersona order by fcDescripcion";
	public static final String FIND_EXTRA_INFO_BUSINESS = "select fiIdCaActNeg,fcDescripcion from CatActividadNegocio where fiIdCatGiroNeg=?";
	public static final String FIND_KINDSHIP ="SELECT fcValor, fcDescripcion FROM catCatalogo where fiIdCatalogo=? AND fiestatus=1";
	public static final String FIND_EXTRA_INFO_BUSINESS_CATEGORY = "select fiIdCatGiroNeg,fcDescripcion from catGiroNEgocio order by fcDescripcion";
	public static final String TYPE_CARD = "SELECT fcTipo from catBines  where fiBin=?";
	public static final String TYPE_CARD2 = "SELECT fcTipo from catBines2  where fiBin=?";
	public static final String BANK_CARD = "SELECT fcBancoEmisor from catBines  where fiBin=?";
	public static final String BANK_CARD2 = "SELECT fcBancoEmisor from catBines2  where fiBin=?";
	public static final String FIND_EXTRA_INFO_BUSINESS_BY_ID = "SELECT  fcDescripcion FROM catCatalogo where fiIdCatalogo=15 AND fiestatus=1 AND fcValor=?";
	public static final String FIND_FOLIO = "SELECT folio FROM business WHERE folio=?";
	public static final String FIND_FOLIO_STATUS = "SELECT status FROM business WHERE folio=?";
	public static final String FIND_SALE_RETURN_TICKETS = "SELECT ticketPath, maskedPan, reference, authorizationNumber, afiliacion, idDoc, rfc, folio, user FROM SalesReturnTicket WHERE user=?";
	public static final String FIND_SALE_RETURN_TICKET = "SELECT ticketPath, maskedPan, reference, authorizationNumber, afiliacion, idDoc, rfc, folio, user FROM SalesReturnTicket WHERE ticketPath=? AND maskedPan=? AND reference=? AND authorizationNumber=? AND afiliacion=? AND idDoc=? AND rfc=? AND folio=? AND user=?";
	public static final String FIND_PENDING_SALES = "SELECT CANAL, FECHA_TX, TERMINAL, ID_TX, PAN, CARD_DATA, FECHA_VENCIMIENTO, MONTO, IMEI, LONGITUD, LATITUD,MOTIVO_REVERSO FROM Ventas";
	public static final String FIND_LADA = "SELECT fiLada FROM CatLadas WHERE fiPaisId=? AND fiEdoId=? AND fiPobId=?";
	public static final String FIND_SHUTDOWN_STATUS = "SELECT value FROM Status where shutdown=1";
	public static String FIND_ECONOMIC_ACTIVITY="select fiIdCaActPer,fcDescripcion from CatActividadPersona where fiIdCatGiroPer=?";	
}
