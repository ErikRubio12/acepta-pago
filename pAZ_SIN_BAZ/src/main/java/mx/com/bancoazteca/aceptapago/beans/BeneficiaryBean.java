package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;
import mx.com.bancoazteca.beans.AddressBean;



public class BeneficiaryBean implements Serializable{
	private static final long serialVersionUID = -6319991964532259508L;
	public static final String BENEFICIARIES = "BENEFICIARIES";
	private String name="";
	private String lastName="";
	private String lastName2="";
	private String percentage="";
	private String kinship="";
	private String kinshipStr="";
	public String getKinshipStr() {
		return kinshipStr;
	}
	public void setKinshipStr(String kinshipStr) {
		this.kinshipStr = kinshipStr;
	}
	private AddressBean address;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName2() {
		return lastName2;
	}
	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getKinship() {
		return kinship;
	}
	public void setKinship(String kinship) {
		this.kinship = kinship;
	}
	public AddressBean getAddress() {
		return address;
	}
	public void setAddress(AddressBean address) {
		this.address = address;
	}
}
