package mx.com.bancoazteca.aceptapago.db.daos;

import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.aceptapago.db.Query;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class CustomerDao extends Dao{
	
	private static final int MARITAL_STATUS_ID = 2;
	private static final int CITIZENSHIP_ID = 3;
	private static final int IDS_ID = 6;
	private static final int GENDER_ID = 23;
	private static final int KINDSHIP_ID = 4;

	public CustomerDao(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public List<String> queryMaritalStatus() {
		// TODO Auto-generated method stub
		ArrayList<String> elements= new ArrayList<String>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_MARITAL_STATUS, new String[]{String.valueOf(MARITAL_STATUS_ID)});
		if(cursor.moveToFirst()){
			do {
				elements.add(cursor.getString(0).trim());
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}

	public List<Parameter<String, String>> queryCitizenship() {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String, String>> elements= new ArrayList<Parameter<String, String>>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_CITIZENSHIP, new String[]{String.valueOf(CITIZENSHIP_ID)});
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0).trim(), cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}

	public List<Parameter<String, String>> queryIds() {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String,String>> elements= new ArrayList<Parameter<String,String>>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_IDS, new String[]{String.valueOf(IDS_ID)});
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0),cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}

	public List<String> queryGenders() {
		// TODO Auto-generated method stub
		ArrayList<String> elements= new ArrayList<String>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_GENDERS, new String[]{String.valueOf(GENDER_ID)});
		if(cursor.moveToFirst()){
			do {
				elements.add(cursor.getString(0).trim());
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}

	public List<Parameter<String, String>> queryKindship() {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String,String>> elements= new ArrayList<Parameter<String,String>>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_KINDSHIP, new String[]{String.valueOf(KINDSHIP_ID)});
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0),cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}

	public String queryMaritalStatus(int maritalStatus) {
		// TODO Auto-generated method stub
		String res=null;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_MARITAL_STATUS_BY_ID, new String[]{String.valueOf(MARITAL_STATUS_ID),String.valueOf(maritalStatus)});
		if(cursor.moveToFirst()){
			res=cursor.getString(0).trim();
		}
		cursor.close();
		db.close();
		return res;
	}

	public String queryCitizenship(int citizenship) {
		// TODO Auto-generated method stub
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_CITIZENSHIP_BY_ID, new String[]{String.valueOf(CITIZENSHIP_ID),String.valueOf(citizenship)});
		String res = null;
		if(cursor.moveToFirst()){
			res=cursor.getString(0).trim();
		}
		cursor.close();
		db.close();
		return res;	
	}

	public String queryIds(int id) {
		// TODO Auto-generated method stub
		String res = null;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_IDS_BY_ID, new String[]{String.valueOf(IDS_ID),String.valueOf(id)});
		if(cursor.moveToFirst()){
			res=cursor.getString(0);
		}
		cursor.close();
		db.close();
		return res;
	}

	public boolean existFolio(String folioPreap) {
		// TODO Auto-generated method stub
		boolean res=false;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_FOLIO, new String[]{String.valueOf(folioPreap)});
		if(cursor.moveToFirst())
			res=!res;
		cursor.close();
		db.close();
		return res;
	}

//	public double insertFolio(String folioPreap) {
//		// TODO Auto-generated method stub
//		double res=-1;
//		ContentValues newFolio= new ContentValues();
//		newFolio.put("folio", folioPreap);
//		newFolio.put("status",0);
//		res=db.getReadableDatabase().insert("business",null, newFolio);
//		db.close();
//		return res;
//	}

	public int checkFolioStatus(String folioPreap) {
		// TODO Auto-generated method stub
		int res=-1;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_FOLIO_STATUS, new String[]{String.valueOf(folioPreap)});
		if(cursor.moveToFirst()){
			res= Integer.parseInt(cursor.getString(0));
		}
		cursor.close();
		db.close();
		return res;
	}

	public void updatoFolioStatus(String folio,int i) {
		// TODO Auto-generated method stub
		ContentValues newFolio= new ContentValues();
		newFolio.put("status",1);
		db.getReadableDatabase().update("business", newFolio,"folio=?", new String []{folio});
		db.close();
	}
	
}
