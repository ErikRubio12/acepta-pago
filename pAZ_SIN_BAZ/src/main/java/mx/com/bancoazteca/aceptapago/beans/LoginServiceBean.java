package mx.com.bancoazteca.aceptapago.beans;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by B931724 on 15/02/18.
 */

public class LoginServiceBean implements Parcelable{

    @SerializedName("codigoOperacion")
    @Expose
    private int codigoOperacion;

    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    @SerializedName("tokenCode")
    @Expose
    private String tokenCode;

    @SerializedName("folio")
    @Expose
    private String folio;

    public int getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(int codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTokenCode() {
        return tokenCode;
    }

    public void setTokenCode(String tokenCode) {
        this.tokenCode = tokenCode;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    protected LoginServiceBean(Parcel in) {
        codigoOperacion = in.readInt();
        descripcion = in.readString();
        tokenCode = in.readString();
        folio = in.readString();
    }

    public static final Creator<LoginServiceBean> CREATOR = new Creator<LoginServiceBean>() {
        @NonNull
        @Override
        public LoginServiceBean createFromParcel(Parcel source) {
            return new LoginServiceBean(source);
        }

        @NonNull
        @Override
        public LoginServiceBean[] newArray(int size) {
            return new LoginServiceBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
