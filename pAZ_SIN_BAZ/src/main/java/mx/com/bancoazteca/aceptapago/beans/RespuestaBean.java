package mx.com.bancoazteca.aceptapago.beans;

/**
 * Created by B931724 on 20/02/18.
 */
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RespuestaBean implements Parcelable {

    @SerializedName("documentos")
    @Expose
    private List<DocumentoBean> documentos = null;

    protected RespuestaBean(Parcel in) {
        documentos = in.createTypedArrayList(DocumentoBean.CREATOR);
    }

    public static final Creator<RespuestaBean> CREATOR = new Creator<RespuestaBean>() {
        @Override
        public RespuestaBean createFromParcel(Parcel in) {
            return new RespuestaBean(in);
        }

        @Override
        public RespuestaBean[] newArray(int size) {
            return new RespuestaBean[size];
        }
    };

    public List<DocumentoBean> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoBean> documentos) {
        this.documentos = documentos;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(documentos);
    }
}