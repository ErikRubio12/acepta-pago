package mx.com.bancoazteca.aceptapago.baz.ws.parsers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UtilidadesParseo {
	public String xml;
	public Element unElemento;

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public Element getUnElemento() {
		return unElemento;
	}

	public void setUnElemento(Element unElemento) {
		this.unElemento = unElemento;
	}

	public int traeCodigoOperacion() {
		int cOperacion = -1;
		NodeList nlSesion = unElemento.getElementsByTagName("codigo_operacion");
		if (nlSesion != null && nlSesion.getLength() > 0) {
			for (int i = 0; i < nlSesion.getLength(); i++) {
				Element elemento = (Element) nlSesion.item(i);
				cOperacion = obtieneCodigoOperacion(elemento);
			}
		}
		return cOperacion;
	}

	public int obtieneCodigoOperacion(Element elemento) {
		if (elemento.getAttribute("value").trim().equals("")) {
			return -1;
		}
		return Integer.parseInt(elemento.getAttribute("value"));
	}

	public Element parseaTexto() {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream in = new ByteArrayInputStream(
					xml.getBytes("ISO-8859-1"));
			Document dom;

			dom = db.parse(in);
			Element docEle = dom.getDocumentElement();
			return docEle;
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String obtenerTexto(Element elemento, String nombreEtiqueta) {
		String texto = "";
		NodeList nl = elemento.getElementsByTagName(nombreEtiqueta);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			texto = el.getAttribute("value");
			if (texto != null)
			texto = texto.trim();
		}
		
		return texto;
	}
	
	public String obtenerTextoError(Element elemento, String nombreEtiqueta, String etiqueta) {
		String texto = "";
		NodeList nl = elemento.getElementsByTagName(nombreEtiqueta);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			texto = el.getAttribute("value");
			if (texto != null)
			texto = texto.trim();
		}
		
		return texto;
	}
	
	public ArrayList<String> obtenerArreglosTexto(Element elemento, String nombreEtiqueta) {
		ArrayList<String> texto = new ArrayList<String>();
		NodeList nl = elemento.getElementsByTagName(nombreEtiqueta);
		for(int i=0;i<nl.getLength();i++){
			Element el = (Element) nl.item(i);
			String aux = el.getAttribute("value");
			if(aux!=null)
				texto.add(aux);
			else
				texto.add("");
		}
		return texto;
	}

	public Float obtenerCantidadFloat(Element elemento, String nombreEtiqueta) {
		Float cantidad = null;
		String texto = null;
		NodeList nl = elemento.getElementsByTagName(nombreEtiqueta);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			texto = el.getAttribute("value");
		}
		try{
		if (texto != null) {
			texto = texto.replace(",", "");
			texto = texto.replace("$", "");
			cantidad = Float.parseFloat(texto);
		} else {
			cantidad = (float) 0.0;
		}
		}catch(Exception e){
			cantidad = (float) 0.0;
		}
		return cantidad;
	}

	public int obtenerCantidadInt(Element elemento, String nombreEtiqueta) {
		int cantidad = 0;
		String texto = null;
		NodeList nl = elemento.getElementsByTagName(nombreEtiqueta);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			texto = el.getAttribute("value");
		}
		if (texto != null) {
			if(texto.trim().equals("")){
				texto = "0";
			}
			texto = texto.replace(",", "");
			texto = texto.replace("$", "");
			cantidad = Integer.parseInt(texto);
		} else {
			cantidad = 0;
		}
		return cantidad;
	}

	public String obtenerEncriptado(Element elemento, String nombreEtiqueta) {
		String texto = null;
		NodeList nl = elemento.getElementsByTagName(nombreEtiqueta);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			texto = el.getAttribute("cipher");
		}
		return texto;
	}

	private String obtenerTextoEtiqueta(Element elemento) {
		return elemento.getAttribute("value");
	}

	private String obtenerCipherEtiqueta(Element elemento) {
		return elemento.getAttribute("cipher");
	}

	public String traeTextoIndividual(Element docXml, String etiqueta)
			throws ParserConfigurationException, SAXException, IOException {
		NodeList nl = docXml.getElementsByTagName(etiqueta);
		String texto = null;
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element elemento = (Element) nl.item(i);
				texto = obtenerTextoEtiqueta(elemento);
			}
			return texto;
		} else {
			return null;
		}
	}

	public String traeMensajeError(Element docXml) {
		NodeList nl = docXml.getElementsByTagName("mensaje");
		String texto = null;
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element elemento = (Element) nl.item(i);
				try {
					texto = traeTextoIndividual(elemento, "descripcion_codigo");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return texto;
		} else {
			return null;
		}
	}

	public String traeCipherIndividual(Element docXml, String etiqueta)
			throws ParserConfigurationException, SAXException, IOException {
		NodeList nl = docXml.getElementsByTagName(etiqueta);
		String texto = null;
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element elemento = (Element) nl.item(i);
				texto = obtenerCipherEtiqueta(elemento);
			}
			return texto;
		} else {
			return null;
		}
	}

	public String traeTextoServicio(Element docXml)
			throws ParserConfigurationException, SAXException, IOException {
		NodeList nl = docXml.getElementsByTagName("xmlOut");
		String texto = null;
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element elemento = (Element) nl.item(i);
				texto = elemento.getNodeValue();
			}
			return texto;
		} else {
			return null;
		}
	}

}
