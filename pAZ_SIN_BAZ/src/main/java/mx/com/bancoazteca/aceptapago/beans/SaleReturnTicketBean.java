package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;

public class SaleReturnTicketBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ticketPath;
	private String maskedPAN;
	private String reference;
	private String authorizationNumber;
	private String affiliate;
	private String idoc;
	private String rfc;
	private String folio;
	private String user;
	public String getTicketPath() {
		return ticketPath;
	}
	public void setTicketPath(String ticketPath) {
		this.ticketPath = ticketPath;
	}
	public String getMaskedPAN() {
		return maskedPAN;
	}
	public void setMaskedPAN(String maskedPAN) {
		this.maskedPAN = maskedPAN;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}
	public String getAffiliate() {
		return affiliate;
	}
	public void setAffiliate(String affiliate) {
		this.affiliate = affiliate;
	}
	public String getIdoc() {
		return idoc;
	}
	public void setIdoc(String idoc) {
		this.idoc = idoc;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder params= new StringBuilder();
		params.append(" user: ");
		params.append(user);
		params.append(" folio: ");
		params.append(folio);
		params.append(" numer autorizacion: ");
		params.append(authorizationNumber);
		params.append(" rfc: ");
		params.append(rfc);
		params.append(" afiliacion:");
		params.append(affiliate);
		params.append(" referencia: ");
		params.append(reference);
		params.append(" tarjeta: ");
		params.append(maskedPAN);
		params.append(" ruta: "+ ticketPath);
		return params.toString();
	}
}
