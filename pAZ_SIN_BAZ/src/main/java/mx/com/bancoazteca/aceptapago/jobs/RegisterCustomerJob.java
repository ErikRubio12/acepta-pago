package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.os.Build;
import android.util.Xml;

import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class RegisterCustomerJob extends BaseService<Message, String>{
//	private static final String USER_NAMESPACE = "http://endpoint.pagoazteca.ws.com";
//	private static final String USER_URL = Urls.EBANKING + "services/usuario";
//	private static final String USER_ACTION = null;
//	private static final String USER_METHOD = "usuario";
//	private static final String VALID_USER_PARAM = "body";
	private static final String REGISTER_CUSTOMER_ACTION = null;
	private static final String REGISTER_CUSTOMER_METHOD = "BusinessToBusinessLogin";
	private static final String REGISTER_CUSTOMER_PARAM = "xml";
	private static final String REGISTER_CUSTOMER_NAMESPACE = "http://service.btb.com";
	private static final String REGISTER_CUSTOMER_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String USER_ID_TAG = "idUsuario";
	private static final String REGISTER_CUSTOMER = "register customer";
	private String userId=null;
	private CustomerBean customer;
	private SoapRequest<String> req;
	public RegisterCustomerJob(Context ctx,CustomerBean customer,MessageListener<Message, String> listener) {
		// TODO Auto-generated constructor stub
		this.customer=customer;
		this.listener= listener;
		this.ctx=ctx;
		operationCode=null;
		systemError="";
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		/**
		 * 0  - no existe
		 * 1  - existe
		 *-1  - error conexion
		 */
//		int userResponse=verifyUser();
//		
//		if (userResponse==1) {
//			Message msg= new Message();
//			msg.setType(Message.CANCEL);
//			listener.sendMessage(msg, systemError);
//			return;
//		}
//		else if (userResponse==-1) {
//			listener.sendStatus(Message.EXCEPTION);
//			return;
//		}
		
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>\r\n");
		request.append("<eservices>\r\n");
		request.append("<request>\r\n");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>\r\n");
		request.append("<canalEntrada>");
		request.append(Encrypt.encryptStringWS("login.AltaUsuario"));
		request.append("</canalEntrada>\r\n");
		request.append("<login>\r\n");
		request.append("<nombreCliente>");
		request.append(Encrypt.encryptStringWS(customer.getName().trim()));
		request.append("</nombreCliente>\r\n");
		request.append("<apellidoPaterno>");
		request.append(Encrypt.encryptStringWS(customer.getLastName().trim()));
		request.append("</apellidoPaterno>\r\n");
		request.append("<apellidoMaterno>");
		request.append(Encrypt.encryptStringWS(customer.getLasttName2().trim()));
		request.append("</apellidoMaterno>\r\n");
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>\r\n");
		request.append("<version>");
		StringBuilder version=new StringBuilder();
		version.append(Utility.getAppVersionCode(ctx));
		for (int i = 0; i < 3; i++) {
			if (version.length()<3) {
				version.insert(0, "0");
			}
			else
				break;
		}
		request.append(Encrypt.encryptStringWS(version.toString()));
		request.append("</version>\r\n");
		StringBuilder os= new StringBuilder();
		os.append(Build.VERSION.SDK_INT);
		
		for (int i = 0; i < 3; i++) {
			if (os.length()<3) {
				os.insert(0, "0");
			}
			else
				break;
		}
		
		request.append("<detalleorigen>");
		request.append(Encrypt.encryptStringWS(os.toString()));
		request.append("</detalleorigen>\r\n");
		request.append("<complemento>");
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI().trim()));
		request.append("</complemento>\r\n");
		request.append("<usuario>");
		request.append(Encrypt.encryptStringWS(customer.getLogin().getUser().trim()));
		request.append("</usuario>\r\n");
		request.append("<contrasena>");
//		request.append(Encrypt.encryptStringWS(customer.getLogin().getPassword().trim()));
		request.append(Encrypt.encryptStringWS(customer.getLogin().getPassword().trim()));
		request.append("</contrasena>\r\n");
		request.append("<rfcCliente>");
		request.append(Encrypt.encryptStringWS(customer.getRfc().trim()));
		request.append("</rfcCliente>\r\n");
		request.append("<calle>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getStreet().trim()));
		request.append("</calle>\r\n");
		request.append("<numInterior>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getInternalNumber().trim()));
		request.append("</numInterior>\r\n");
		request.append("<numExterior>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getExternalNumber().trim()));
		request.append("</numExterior>\r\n");
		request.append("<colonia>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getColony().trim()));
		request.append("</colonia>\r\n");
		request.append("<municipio>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getPoblacion().trim()));
		request.append("</municipio>\r\n");
		request.append("<estado>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getState().trim()));
		request.append("</estado>\r\n");
		request.append("<cp>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getZipCode().trim()));
		request.append("</cp>\r\n");
		request.append("<email>");
		request.append(Encrypt.encryptStringWS(customer.getMail().trim()));
		request.append("</email>\r\n");
		request.append("<fechaNacimiento>");
		request.append(Encrypt.encryptStringWS(Utility.formatDate(customer.getBornDate(), "yyyy-MM-dd")));
		request.append("</fechaNacimiento>\r\n");
		request.append("<lada>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getLada().trim()));
		request.append("</lada>\r\n");
		request.append("<telefono>");
		request.append(Encrypt.encryptStringWS(customer.getAddress().getTelephone().trim()));
		request.append("</telefono>\r\n");
		request.append("<codigoNacionalidad>");
		request.append(Encrypt.encryptStringWS(String.valueOf(customer.getCitizenship())));
		request.append("</codigoNacionalidad>\r\n");
		request.append("<codigoIdentificacion>");
		request.append(Encrypt.encryptStringWS(String.valueOf(customer.getCodIdent())));
		request.append("</codigoIdentificacion>\r\n");
		request.append("<numeroIdentificacion>");
		request.append(Encrypt.encryptStringWS(customer.getId().trim()));
		request.append("</numeroIdentificacion>\r\n");
		request.append("<sexo>");
		request.append(Encrypt.encryptStringWS(customer.getGender()==1 ? "F" : "M"));
		request.append("</sexo>\r\n");
		request.append("<estadoCivil>");
		request.append(Encrypt.encryptStringWS(String.valueOf(customer.getMaritalStatus())));
		request.append("</estadoCivil>\r\n");
		request.append("<curpCliente>");
		request.append(Encrypt.encryptStringWS(String.valueOf(customer.getCurp().trim())));
		request.append("</curpCliente>\r\n");
		request.append("</login>\r\n");
		request.append("</request>\r\n");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		req= new SoapRequest<String>(REGISTER_CUSTOMER_METHOD, REGISTER_CUSTOMER_ACTION);
		req.setNameSpace(REGISTER_CUSTOMER_NAMESPACE);
		req.setUrl(REGISTER_CUSTOMER_URL);
		Parameter<String,String> param= new Parameter<String, String>(REGISTER_CUSTOMER_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;	
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if(res==null){
				listener.sendStatus(ctx,Message.EXCEPTION);
				return;
			}
				
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			xml=Utility.fixSoapResponse(xml);
			
			
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
						operationCode= Encrypt.decryptStringWS(parser.getText());
					}
					else if (currentTag.equalsIgnoreCase(USER_ID_TAG)){
						userId=Encrypt.decryptStringWS(parser.getText());
					}
					else if (currentTag.equalsIgnoreCase(SYSTEM_ERROR_TAG)){
						systemError=Encrypt.decryptStringWS(parser.getText());
					}
					
					
					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			Message msg= new Message();
			if ("0".equals(operationCode)) {
				msg.setType(Message.OK);
				listener.sendMessage(ctx,msg, userId);
			}
			else{
				StringBuffer message= new StringBuffer(operationCode);
				message.append(" - ");
				message.append(systemError==null ? "" : systemError);
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg, message.toString());
			}
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_CUSTOMER,"InterruptedIOException: "+ e.getMessage());
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_CUSTOMER,"IOException: "+ e.getMessage());
			return;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_CUSTOMER,"XmlPullParserException: "+ e.getMessage());
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_CUSTOMER,"TimeoutException: "+ e.getMessage());
			return;
		}
		
	}
//	private int verifyUser() {
//		// TODO Auto-generated method stub
//		int response=-1;
//		req= new SoapRequest<String>(USER_METHOD, USER_ACTION);
//		req.setNameSpace(USER_NAMESPACE);
//		req.setUrl(USER_URL);
//		
//		StringBuilder _request= new StringBuilder("<?xml version='1.0' encoding='UTF-8'?>");
//		_request.append("<UsuarioRequest xmlns='http://pagoazteca.com/PagoAzteca/ws/schemas'>");
//		_request.append("<usuario>");
//		_request.append(customer.getLogin().getUser());
//		_request.append("</usuario>");
//		_request.append("</UsuarioRequest>");
//		_request = new StringBuilder(Encrypt.encryptStringWS(_request.toString())) ;
//		Parameter<String, String> soapParameter= new Parameter<String, String>(VALID_USER_PARAM,_request.toString());
//		req.addParameter(soapParameter);
//		SoapResponse soapResponse=null;
//		try {
//			soapResponse= Connections.makeSoapConnection(req, 3);
//			if (soapResponse==null) 
//				return -1;
//			
//			long time1=System.currentTimeMillis();
//			Document doc=Utility.createDom(Encrypt.decryptStringWS((String) soapResponse.getResponse()),"UTF-8");
//			NodeList list=doc.getElementsByTagName("status");
//			Node s= list.item(0).getFirstChild();
//			long time2 =System.currentTimeMillis()-time1;
//			System.out.println(time2);
//			if(Integer.parseInt(s.getNodeValue())==1)
//				response=0;
//			else
//				response=1;
//			Connections.closeSoapConnection(soapResponse);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block	
//			response=-1;
//		}catch (NumberFormatException e) {
//			// TODO: handle exception
//			response=-1;
//		}catch (Exception e) {
//			// TODO: handle exception
//			response=-1;
//		}
//		return response;
//	}
}
