package mx.com.bancoazteca.aceptapago.ws;


import android.content.Context;
import android.util.Log;

import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import mx.com.bancoazteca.aceptapago.R;

/**
 * Created by B931724 on 16/02/18.
 */

public class GenPassEncrypt {
    private Context mContext;

    public GenPassEncrypt(Context context){
        this.mContext = context;
    }



//    public static final String CONST_SHA512 = "SHA-512";
    private static final int CONST_ITERATIONCOUNTPASS = 5730;
    public static final String CONST_PBKDF = "PBKDF2WithHmacSHA1";
    public static final String CONST_HMACSHA = "HmacSha256";


//    private static BouncyCastleProvider VAR_BC_PROVIDER = null;

//    public static void createProvider() {
//        Log.d(Constants.GLOBAL_TAG,BouncyCastleProvider.PROVIDER_NAME);
//
//        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
//            VAR_BC_PROVIDER = new BouncyCastleProvider();
//            Security.addProvider(VAR_BC_PROVIDER);
//            Log.d(Constants.GLOBAL_TAG,"Cargado " + VAR_BC_PROVIDER.getInfo());
//
//        } else {
//            VAR_BC_PROVIDER = (BouncyCastleProvider) Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
//            Log.d(Constants.GLOBAL_TAG,"Cargado " + VAR_BC_PROVIDER.getInfo());
//        }
//    }


    public String genPassword(String passPhrase) {
        byte[] salt;

//        if (VAR_BC_PROVIDER == null) {
//            createProvider();
//        }
        try {
            // Forma 2
//            salt = generateSalt(passPhrase, VAR_BC_PROVIDER);

            salt = generateSalt(passPhrase);
            Log.d(Constants.GLOBAL_TAG,"salt " + new String(Base64.encode(salt)));
            PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator();
            gen.init(passPhrase.getBytes("UTF-8"), salt, CONST_ITERATIONCOUNTPASS);
            byte[] dk = ((KeyParameter) gen.generateDerivedParameters(256)).getKey();
            byte[] hashBase64 = Base64.encode(dk);
            return new String(hashBase64);


        } catch (UnsupportedEncodingException e) {
            return "";
        }

    }
//    public static byte[] generateSalt(String thePass, BouncyCastleProvider pBCProvider) {
    public byte[] generateSalt(String thePass) {
//        if (pBCProvider == null) {
//            Log.d(Constants.GLOBAL_TAG,"pBCProvider null");
//            createProvider();
//        }
        try {
//            MessageDigest md = MessageDigest.getInstance(CONST_SHA512, pBCProvider);
            MessageDigest md = MessageDigest.getInstance(getStringSHA());
            byte[] data = thePass.getBytes();
            byte[] digest = md.digest(data);
            int sizeDigest = digest.length / 2;
            byte[] firstDigest = new byte[sizeDigest];
            byte[] secondDigest = new byte[sizeDigest];
            for (int i = 0; i < sizeDigest; i++) {
                firstDigest[i] = digest[i];
            }
            for (int i = sizeDigest, j = 0; i < digest.length; i++, j++) {
                secondDigest[j] = digest[i];
            }
            KeySpec kySpec = new PBEKeySpec(thePass.toCharArray(), firstDigest, 2920, 256);
            SecretKeyFactory skf = SecretKeyFactory.getInstance(CONST_PBKDF);
            SecretKey newKey = skf.generateSecret(kySpec);
            SecretKeySpec skySpec = new SecretKeySpec(newKey.getEncoded(), CONST_HMACSHA);
            Mac mac = Mac.getInstance(CONST_HMACSHA);
            mac.init(skySpec);
            return mac.doFinal(secondDigest);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException e) {
            return new byte[0];
        }
    }

    private String getStringSHA() {
        return mContext.getString(R.string.CONST_SHA512);
    }

}
