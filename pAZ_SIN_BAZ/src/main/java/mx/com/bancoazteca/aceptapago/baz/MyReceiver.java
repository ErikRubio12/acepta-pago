package mx.com.bancoazteca.aceptapago.baz;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.aceptapago.gui.PagoAzteca;

/**
 * Created by jcruzvi on 13/09/2016.
 */
public class MyReceiver extends BroadcastReceiver {

    private LoginBean login = new LoginBean();
    protected Thread backgroundThread;
    private static final int PASSWORD_MIN_LENGTH = 1;
    private static final int USER_MIN_LENGTH = 1;
    protected final static int POPUP_CONNECTION_ERROR = 2111;
    protected final static int POPUP_CONNECTION_ERROR_EXIT = 2112;
    protected final static int POPUP_EMPTY_FORM = 2113;
    protected int popupMsg = 0;
    protected int popupTitle = 0;
    protected Activity this_ = null;
    protected final static int POPUP_MESSAGES = 2114;
    protected final static int POPUP_MESSAGES_EXIT = 2115;
    protected final static int POPUP_MESSAGES_PROGRESS = 2116;
    private final static int POPUP_NETWORK = 28;
    private final static int POPUP_REGISTRATION = 29;
    private final static int POPUP_NO_LOCATION = 30;
    private static final int POPUP_USER_DATA_REGISTERED = 35;
    private static final int GPS_SETTINGS = 11;
    private static final String LOGIN = "AceptaPago";
    protected static final String TAG_MAIN_SCREEN = "Acepta Pago Screen";
    Context myContext;
    public static boolean Receiver=false;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.myContext = context;
        Intent intent1 = new Intent(myContext, PagoAzteca.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent1.putExtras(intent.getExtras());
        Receiver=true;
        myContext.startActivity(intent1);

//        IniciarSesionPAZRemoto(data, context);
    }

//    private boolean validForm(LoginBean login) {
//        // TODO Auto-generated method stub
//        boolean answer = true;
//
//        if (login.getPassword().length() >= PASSWORD_MIN_LENGTH && login.getUser().length() >= USER_MIN_LENGTH) {
//            return answer;
//        } else if (login.getPassword().length() < PASSWORD_MIN_LENGTH || login.getUser().length() < USER_MIN_LENGTH) {
//            answer = false;
//        }
//        return answer;
//    }

//    public void IniciarSesionPAZRemoto(Bundle data, final Context context) {
//
//        login.setUser(data.getString("User"));
//        login.setPassword(data.getString("Password"));
//        if (!validForm(login)) {
//            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
//            alertBuilder.setTitle("Advertencia");
//            alertBuilder.setMessage("La contraseña o usuario son muy cortos");
//            alertBuilder.show();
//        } else if (!HandsetInfo.isOnline(context)) {
//            popupTitle = R.string.popUpMessage;
//            popupMsg = R.string.offLine;
//            AlertDialog.Builder alertBuilderConfirmation = new AlertDialog.Builder(context);
//            alertBuilderConfirmation.setTitle("Mensaje");
//            alertBuilderConfirmation.setMessage("Red no disponible debes configurar una red para continuar");
//            alertBuilderConfirmation.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Utility.launchNetworkSettings(context);
//                }
//            });
//            alertBuilderConfirmation.show();
//
//        } else if (!FileManager.isSdPresent()) {
//            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
//            alertDialog.setTitle("Mensaje");
//            alertDialog.setMessage("Es necesario introducir una tarjeta SD para poder realizar ventas.");
//            alertDialog.show();
//        } else {
//            LocationManager manager = Utility.getLocationManager(context);
//            if (!manager.isProviderEnabled(LocationThread.GPS) && !manager.isProviderEnabled(LocationThread.NETWORK)) {
//
//                AlertDialog.Builder alertBuilderConfirmation = new AlertDialog.Builder(context);
//                alertBuilderConfirmation.setTitle("Mensaje");
//                alertBuilderConfirmation.setMessage("Es necesario habilitar el GPS y la localización por red para facilitar la obtención de tu ubicación.");
//                alertBuilderConfirmation.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Utility.gpsSettings(this_, GPS_SETTINGS);
//                    }
//                });
//                alertBuilderConfirmation.show();
//
//            } else {
//                Intent service = new Intent(context, LocationService.class);
//                context.startService(service);
//                popupTitle = R.string.paz;
//                popupMsg = R.string.checkingLogin;
//
//                //showDialog(POPUP_MESSAGES_PROGRESS);
//                backgroundThread = new Thread(new LoginJob(login, context, MyReceiver.this));
//                backgroundThread.start();
//
//            }
//        }
//    }
//
//    @Override
//    public void sendMessage(Context ctx, Message message, final String parameter) {
//        // TODO Auto-generated method stub
////        runOnUiThread(new Runnable() {
////            @Override
////            public void run() {
////                // TODO Auto-generated method stub
////                if (isFinishing())
////                    return;
////                removeDialog(POPUP_MESSAGES_PROGRESS);
////                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpError), parameter);
////                dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
////            }
////        });
//        Toast.makeText(context, "tres", Toast.LENGTH_SHORT).show();
//
//    }
//
//    @Override
//    public void sendStatus(Context ctx, final int status) {
//        // TODO Auto-generated method stub
////        runOnUiThread(new Runnable() {
////            public void run() {
////                // TODO Auto-generated method stub
////                if(isFinishing())
////                    return;
////                if(status==Message.EXCEPTION)
////                    removeDialog(POPUP_MESSAGES_PROGRESS);
////
////                showDialog(POPUP_CONNECTION_ERROR);
////            }
////        });
//
//        Toast.makeText(context, "hola", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void sendMessage(final Context ctx, final Message message, final Object... parameters) {
//        // TODO Auto-generated method stub
//        if (message.getType() == Message.OK) {
////            runOnUiThread(new Runnable() {
////                public void run() {
////                    if(isFinishing())
////                        return;
////                    final RegistrationBean register= (RegistrationBean) parameters[0];
////                    removeDialog(POPUP_MESSAGES_PROGRESS);
////                    if (parameters.length>1) {
////                        final String status=(String) parameters[1];
////                        Logger.log(Logger.MESSAGE, TAG_MAIN_SCREEN, "status: " + status);
////                        if ("2".equals(status)) {//REGISTRO COMPLETO
////                            register.getCustomer().setLogin(login);
////                            final List<Serializable> params= new ArrayList<Serializable>();
////                            params.add(register.getFiscalData());
////                            params.add(register.getCustomer());
////                            final List<String>paramNames= new ArrayList<String>();
////                            paramNames.add(FiscalDataBean.FISCAL_DATA);
////                            paramNames.add(CustomerBean.CUSTOMER);
////                            goForward(SyncScreen.class, ctx, params, paramNames);
////                        }
////                        else{//REGISTRO SOLO DATOS DEL CLIENTE
////                            registerIncomplete=register;
////                            ConfirmationDialog confirmationDialog=ConfirmationDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.registerIncomplete));
////                            confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
////                                @Override
////                                public void onAccept(DialogInterface dialog) {
////                                    intent.putExtra("customerIncomplete",registerIncomplete.getCustomer());
////                                    goForward(RegistrationMenu.class, PagoAzteca.this);
////                                }
////                            });
////                            confirmationDialog.show(getSupportFragmentManager(),""+POPUP_USER_DATA_REGISTERED);
////                        }
////                    }
////                    else{
////                        register.getCustomer().setLogin(login);
////                        final List<Serializable> params= new ArrayList<Serializable>();
////                        params.add(register.getFiscalData());
////                        params.add(register.getCustomer());
////                        final List<String>paramNames= new ArrayList<String>();
////                        paramNames.add(FiscalDataBean.FISCAL_DATA);
////                        paramNames.add(CustomerBean.CUSTOMER);
////                        goForward(SyncScreen.class, PagoAzteca.this, params, paramNames);
////                    }
////
////                }
////            });
//
//            Toast.makeText(context, "dos", Toast.LENGTH_SHORT).show();
//        }
//    }
}
