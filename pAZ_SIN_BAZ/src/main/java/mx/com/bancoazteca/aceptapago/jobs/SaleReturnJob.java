package mx.com.bancoazteca.aceptapago.jobs;

import android.content.Context;
import android.location.Location;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.net.ConnectException;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import mx.com.bancoazteca.beans.CardBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.hw.LocationThread;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnResponse;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class SaleReturnJob extends BaseService<Message, String> {
	private static final String QUERY_DEVICE_METHOD ="BusinessToBusinessServiceConsultarTerminalIphone";
	private static final String QUERY_DEVICE_ACTION = null;
	private static final String QUERY_DEVICE_NAMESPACE = "http://service.btb.com";
	private static final String QUERY_DEVICE_URL =Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String QUERY_DEVICE_PARAM = "xml";
	private OperationsBean operation;
	private static final String SALE_RETURN_PARAM = "xml";
	private static final String SALE_RETURN_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String SALE_RETURN_NAMESPACE = "http://service.btb.com";
	private static final String SALE_RETURN_METHOD = "BusinessToBusinessService";
	private static final String SALE_RETURN_ACTION = null;
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String SYSTEM_MESSAGE_TAG = "mensaje_sistema";
	private static final String RETURN_SALE = "Return sale";	
	private StringBuffer message= new StringBuffer();
	private String idDevice=null;
	private String folio;
	private Location location;
	private CardBean creditCard;
	private FiscalDataBean fiscalData;
	private String afiliacion;
	private boolean refund=false;
	public SaleReturnJob(CardBean card, OperationsBean operation,String folio,FiscalDataBean fiscal,MessageListener<Message, String>listener,Location location,boolean refund,Context ctx) {
		// TODO Auto-generated constructor stub
		this.operation=operation;
		this.listener=listener;
		this.folio=folio;
		this.fiscalData=fiscal;
		this.ctx=ctx;
		this.location=location;
		this.creditCard=card;
		this.refund=refund;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(checkDevice()){
			location=LocationService.getMostRecentLocation();
			if(location==null)
				location = new Location(LocationThread.GPS);
			returnSale();
		}
		
	}
	private boolean checkDevice() {
		SoapRequest<String> req= new SoapRequest<String>(QUERY_DEVICE_METHOD, QUERY_DEVICE_ACTION);
		req.setNameSpace(QUERY_DEVICE_NAMESPACE);
		
		req.setUrl(QUERY_DEVICE_URL);
		Parameter<String,String> param= new Parameter<String, String>(QUERY_DEVICE_PARAM, createCheckDeviceRequest().toString());
		req.addParameter(param);
		SoapResponse res;
		try {
			res = Connections.makeSecureSoapConnection(req,TIME_OUT);
			if(res==null || res.getResponse()==null ){
				listener.sendStatus(ctx,Message.EXCEPTION);
				return false;
			}
			Node node=null;
		
			Document dom= Utility.createDom(res.getResponse().toString());
			if(( node=dom.getElementsByTagName("codigo_operacion").item(0))!= null ){
				operationCode=node.getChildNodes().item(0).getNodeValue();
				operationCode=Encrypt.decryptStringWS(operationCode);
			}
			if((node=dom.getElementsByTagName("error_sistema").item(0)) != null){
				systemError=node.getChildNodes().item(0).getNodeValue();
				systemError=Encrypt.decryptStringWS(systemError);
			}
			
			if("0".equals(operationCode)){
				node=dom.getElementsByTagName("idterminal").item(0);
				idDevice=node.getChildNodes().item(0).getNodeValue();
				idDevice=Encrypt.decryptStringWS(idDevice);
				
				node=dom.getElementsByTagName("estado").item(0);
				
				if(node !=null  && node.getChildNodes()!=null){
					fiscalData.getAddress().setState(node.getChildNodes().item(0).getNodeValue());
					fiscalData.getAddress().setState(Encrypt.decryptStringWS(fiscalData.getAddress().getState()));
				}
				else
					fiscalData.getAddress().setState("");
				
				node=dom.getElementsByTagName("municipio").item(0);
				if(node != null && node.getChildNodes() !=null){
					fiscalData.getAddress().setPoblacion(node.getChildNodes().item(0).getNodeValue());
					fiscalData.getAddress().setPoblacion(Encrypt.decryptStringWS(fiscalData.getAddress().getPoblacion()));
				}
				else
					fiscalData.getAddress().setPoblacion("");
				node=dom.getElementsByTagName("colonia").item(0);
				if(node != null && node.getChildNodes() !=null){
					fiscalData.getAddress().setColony(node.getChildNodes().item(0).getNodeValue());
					fiscalData.getAddress().setColony(Encrypt.decryptStringWS(fiscalData.getAddress().getColony()));
				}
				else
					fiscalData.getAddress().setColony("");
				node=dom.getElementsByTagName("afiliacion").item(0);
				if(node != null && node.getChildNodes() !=null){
					afiliacion=node.getChildNodes().item(0).getNodeValue();
					afiliacion=Encrypt.decryptStringWS(afiliacion);
				}
				
				
				Connections.closeSoapConnection(res);
				
				return true;
			}
			else{
				message.append(operationCode);
				message.append(" - ");
				message.append(systemError==null ? "" : systemError);
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg,message.toString());
				Connections.closeSoapConnection(res);
				return false;
			}
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"IOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"IOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"IOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		}
		
	}
	private StringBuilder createCheckDeviceRequest(){
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("CTermIphone"));
		request.append("</idservicio>");
		request.append("<idComercio>");
		request.append(Encrypt.encryptStringWS(folio));
		request.append("</idComercio>");
		request.append("<rfc>");
		request.append(Encrypt.encryptStringWS(fiscalData.getRfc()));
		request.append("</rfc>");
		request.append("<imei>");
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI()));
		request.append("</imei>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		return request;
	}
	private boolean returnSale() {
		// TODO Auto-generated method stub
		operationCode=null;
		systemError=null;
		message= new StringBuffer();
		SoapRequest<String> req= new SoapRequest<String>(SALE_RETURN_METHOD, SALE_RETURN_ACTION);
		req.setNameSpace(SALE_RETURN_NAMESPACE);
		req.setUrl(SALE_RETURN_URL);
		Parameter<String,String> param= new Parameter<String, String>(SALE_RETURN_PARAM,createSaleReturnRequest().toString());
		
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if(res==null || res.getResponse()==null ){
				listener.sendStatus(ctx,Message.EXCEPTION);
				return false;
			}
				
			
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			String date= null;
			String arqc=null;
			String aid=null;
			String folio=null;
			String authorization=null;
			String reference=null;
			String codeDescription=null;
			
			
			
			String amount=null;
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equals(OPERATION_CODE_TAG)) 
						operationCode= Encrypt.decryptStringWS(parser.getText());					
					else if (currentTag.equals(SYSTEM_ERROR_TAG)) 
						systemError= Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SYSTEM_MESSAGE_TAG)) 
						systemError= Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.DATE_TAG))
						date=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.ARQC_TAG))
						arqc=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.AID_TAG))
						aid=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.FOLIO_TAG))
						folio=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.REFERENCE_TAG))
						reference=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.AUTHORIZATION_TAG))
						authorization=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.AMOUNT_TAG))
						amount=Encrypt.decryptStringWS(parser.getText());
					else if (currentTag.equals(SaleReturnResponse.CODE_DESCRIPTION))
						codeDescription=Encrypt.decryptStringWS(parser.getText());
					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			
			message= new StringBuffer(operationCode==null ? "" : operationCode );
			message.append(" - ");
			message.append(systemError==null ? "" : systemError);
			if("0".equalsIgnoreCase(operationCode) || "00".equalsIgnoreCase(operationCode) ){
				/*Logger.log(Logger.MESSAGE, RETURN_SALE,"fecha: "+ date);
				Logger.log(Logger.MESSAGE, RETURN_SALE,"arqc: "+ arqc);
				Logger.log(Logger.MESSAGE, RETURN_SALE,"aid: "+ aid);*/
				SaleReturnResponse response= new SaleReturnResponse();
				response.setAid(aid);
				response.setArqc(arqc);
				response.setDate(date);
				response.setFolio(folio);
				response.setAuthorization(authorization);
				response.setReference(reference);
				response.setAmountReturned(amount);
				response.setCodeDescription(codeDescription);
				//Logger.log(Logger.MESSAGE, RETURN_SALE,"***************** MONTO REGRESADO CANCELACION = "+ amount);
				listener.sendMessage(ctx, new Message(Message.OK, 0),fiscalData,afiliacion,response);
				return true;
			}
			else{
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg,message.toString());
				
				return false;
			}
			
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"TimeoutException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"XmlPullParserException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		}catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"InterruptedIOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, RETURN_SALE,"IOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
			return false;
		} 
	}

	private StringBuilder createSaleReturnRequest(){
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>");
		
		Logger.log(Logger.MESSAGE, RETURN_SALE,"Devolucion: "+refund);
		if(refund){
			request.append("<canalEntrada>");
			if(creditCard.isFullBack() && creditCard.getRawTracks() != null)
				request.append(Encrypt.encryptStringWS("PuntoAztecaFallbackDevolucion"));
			else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0)
				request.append(Encrypt.encryptStringWS("PuntoAztecaDeslizadaDevolucion"));
			else if(creditCard.getChipData() != null)
				request.append(Encrypt.encryptStringWS("PuntoAztecaChipDevolucion"));
			request.append("</canalEntrada>");	
			request.append("<tipo_operacion>");
			request.append(Encrypt.encryptStringWS("0200"));
			request.append("</tipo_operacion>");
		}else{
			request.append("<canalEntrada>");
			if(creditCard.isFullBack() && creditCard.getRawTracks() != null)
				request.append(Encrypt.encryptStringWS("PuntoAztecaFallbackCancelacion"));
			else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0)
				request.append(Encrypt.encryptStringWS("PuntoAztecaDeslizadaCancelacion"));
			else if(creditCard.getChipData() != null)
				request.append(Encrypt.encryptStringWS("PuntoAztecaChipCancelacion"));
			request.append("</canalEntrada>");	
			request.append("<tipo_operacion>");
			request.append(Encrypt.encryptStringWS("220"));
			request.append("</tipo_operacion>");
		}												
			
		request.append("<fechaTransaccion>");
		request.append(Encrypt.encryptStringWS(Utility.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss")));
		request.append("</fechaTransaccion>");
		
		request.append("<idterminal>");
		request.append(Encrypt.encryptStringWS(idDevice));
		request.append("</idterminal>");
		request.append("<idTransaccion>");
		request.append(Encrypt.encryptStringWS(operation.getIdTtransaccion()));
		request.append("</idTransaccion>");
		request.append("<folio>");
		request.append(Encrypt.encryptStringWS(operation.getFolio()));
		request.append("</folio>");
		request.append("<autorizacion>");
		request.append(Encrypt.encryptStringWS(operation.getAuthorizedNumber()));
		request.append("</autorizacion>");
		request.append("<referencia>");
		request.append(Encrypt.encryptStringWS(operation.getReference()));
		request.append("</referencia>");		
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>");
		request.append("<track>");
		if(creditCard.isFullBack() && creditCard.getRawTracks() != null  && creditCard.getRawTracks().indexOf("{")>=0)
			request.append(Encrypt.encryptStringWS(creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}"));		
		else if(creditCard.getRawTracks() != null && creditCard.getRawTracks().indexOf("{")>=0)
			request.append(Encrypt.encryptStringWS(creditCard.getRawTracks() + ",\"ksn\":\""+ creditCard.getKSN().toUpperCase(Locale.getDefault())+ "\"}"));
		else if(creditCard.getChipData()!= null)
			//Log.i("PRUEBA SALE_RETURN_JOb", creditCard.getChipData());
			request.append(Encrypt.encryptStringWS(creditCard.getChipData() + "||"+ creditCard.getKSN().toUpperCase(Locale.getDefault())));
		request.append("</track>");
		request.append("<fechaVencimiento>");
		String validity= creditCard.getStringValidity().substring(3, 5);
		validity+=creditCard.getStringValidity().subSequence(0, 2);
		request.append(Encrypt.encryptStringWS(validity));
		request.append("</fechaVencimiento>");
		request.append("<longitud>");
		String longitudStr=""+location.getLongitude();
		int index=longitudStr.indexOf(".");
		Logger.log(Logger.MESSAGE, RETURN_SALE, "index : "+index);
		String decimales="";
		if(index>=0){
			decimales=longitudStr.substring(index+1, longitudStr.length());
			Logger.log(Logger.MESSAGE, RETURN_SALE, "longitud : "+longitudStr + "   decimales" + decimales);
			if(decimales.length()>7){
				decimales=decimales.substring(0, 6);
				longitudStr=longitudStr.substring(0, index);
				longitudStr=longitudStr + "."+ decimales;
			}
		}
		Logger.log(Logger.MESSAGE, RETURN_SALE, "longitud : "+longitudStr );
		request.append(Encrypt.encryptStringWS(longitudStr));
		request.append("</longitud>");
		request.append("<latitud>");
		String latitudStr=""+location.getLatitude();
		index=latitudStr.indexOf(".");
		Logger.log(Logger.MESSAGE, RETURN_SALE, "index : "+index);
		decimales="";
		if(index>=0){
			decimales=latitudStr.substring(index+1, latitudStr.length());
			Logger.log(Logger.MESSAGE, RETURN_SALE, "latitudStr : "+latitudStr + "   decimales" + decimales);
			if(decimales.length()>7){
				decimales=decimales.substring(0, 6);
				latitudStr=latitudStr.substring(0, index);
				latitudStr=latitudStr + "."+ decimales;
			}
		}
		Logger.log(Logger.MESSAGE, RETURN_SALE, "latitudStr : "+latitudStr );
		request.append(Encrypt.encryptStringWS(latitudStr));
		request.append("</latitud>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		return request;
	}
}
