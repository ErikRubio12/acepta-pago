package mx.com.bancoazteca.aceptapago.db.daos;

import mx.com.bancoazteca.aceptapago.db.Query;
import mx.com.bancoazteca.util.Logger;
import android.content.Context;
import android.database.Cursor;

public class CardDao extends Dao {

	public CardDao(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public String queryCardType(String digits) {
		// TODO Auto-generated method stub
		String res= "Desconocido";
		if(digits != null){
			try {
				Cursor cursor=db.getReadableDatabase().rawQuery(Query.TYPE_CARD, new String[]{digits});
				if(cursor.moveToFirst())
					res= cursor.getString(0).trim();
				cursor.close();
				db.close();
				if(res==null){
					cursor=db.getReadableDatabase().rawQuery(Query.TYPE_CARD2, new String[]{String.valueOf(digits)});
					if(cursor.moveToFirst())
						res= cursor.getString(0).trim();
					cursor.close();
					db.close();
					if(res==null)
						return "Internacional";
				}
			} catch (Exception e) {
				Logger.log("Exception: "+ e.getMessage());
				return res;
			}
			
		}
		return res;
	}

	public String queryBankCard(long digits) {
		// TODO Auto-generated method stub
		String res= null;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.BANK_CARD, new String[]{String.valueOf(digits)});
		if(cursor.moveToFirst())
			res= cursor.getString(0).trim();
		cursor.close();
		db.close();
		if(res==null){
			cursor=db.getReadableDatabase().rawQuery(Query.BANK_CARD2, new String[]{String.valueOf(digits)});
			if(cursor.moveToFirst())
				res= cursor.getString(0).trim();
			cursor.close();
			db.close();
			if(res==null)
				return "Internacional";
		}
			
		return res;
	}
}
