package mx.com.bancoazteca.aceptapago.gui;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import mx.com.bancoazteca.hw.Camera;
import mx.com.bancoazteca.hw.CameraCallBackParameters;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class CameraPreviewScreen extends BaseActivity implements MessageListener<Message, CameraCallBackParameters>, SurfaceHolder.Callback{
	
	private static final String CAMERA_PREVIEW_TAG = "CameraPreviewScreen";
	private static final int ERROR_CODE = 123;
//	private static final long HIDE_DELAY = 15000;
	private SurfaceView surface;
	private static ImageButton takeShootButton;
	private boolean isPreviewRunning=false;
	private Camera camera;
	private SurfaceHolder holder;
	public CameraPreviewScreen() {
		// TODO Auto-generated constructor stub
		Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "instance created");
		
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		setContentView(R.layout.camera_preview);
		surface=(SurfaceView) findViewById(R.id.cameraPreview);
		takeShootButton=(ImageButton) findViewById(R.id.takeShoot);
		takeShootButton.setOnClickListener(takeShootListener);
		surface.setOnTouchListener(screenListener);
		Utility.setFullScreen(this);
	}
	@Override
	protected void processData() {
		super.processData();
		holder= surface.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		camera= new Camera(this, holder);
	}
	@Override
	public void release() {
		// TODO Auto-generated method stub
		if(camera != null){
			if(isPreviewRunning)
				camera.stopPreview();
			camera.close();
			isPreviewRunning=false;
			camera=null;
		}
		
	}
	private Object[] sortElements(Object[] measures) {
		// TODO Auto-generated method stub
		Size tempSize;
		Size tempSizeNext;
		Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "numero dimensiones: "+measures.length);
		for (int i = 0; i < measures.length -1; i++) {
			tempSize=(Size) measures[i];
			tempSizeNext=(Size)measures[i+1];
			if(tempSizeNext.height<tempSize.height){
				measures[i]=tempSizeNext;
				measures[i+1]=tempSize;
				i=-1;
			}
		}
		StringBuffer buff= new StringBuffer();
		
		for (int i = 0; i < measures.length; i++) {
			buff.append(((Size)measures[i]).width);
			buff.append("x");
			buff.append(((Size)measures[i]).height);
			buff.append(" , ");
		}
		Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "dimensiones: "+ buff);
		return measures;
	}
	//***************INNER CLASS +*****************************
//	private final static class Delay implements Runnable {
//		private View view;
//		public Delay(View view) {
//			this.view=view;
//		}
//		public void run() {
//			view.setVisibility(View.GONE);
//		}
//	}
	//**************ACTTION LISTENERS *******************************
	
	private OnClickListener takeShootListener= new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			v.setVisibility(View.GONE);
			camera.takeShoot();
		}
	};
	private OnTouchListener screenListener= new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if(takeShootButton.getVisibility()==View.GONE){
				takeShootButton.setVisibility(View.VISIBLE);
//				takeShootButton.postDelayed(new Delay(takeShootButton), HIDE_DELAY);
			}
			
			return true;
		}
	};
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
		if(isPreviewRunning)
			camera.stopPreview();
		
		Parameters cameraParams=camera.getParameters();
		Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "params: "+ cameraParams);
		List<Size> previewSizesList=cameraParams.getSupportedPreviewSizes();
		if(previewSizesList != null){
			Object []measuresArray=previewSizesList.toArray();
			measuresArray= sortElements(measuresArray);
//			Size biggerPreviewSize= (Size) measuresArray[measuresArray.length-1];
			Size smallestPreviewSize= (Size) measuresArray[measuresArray.length-1];
			cameraParams.setPreviewSize(smallestPreviewSize.width, smallestPreviewSize.height);
			Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "preview size: w-" +((Size)measuresArray[measuresArray.length-1]).width + "  h-"+ ((Size)measuresArray[measuresArray.length-1]).height);
		}
		
		camera.setParameters(cameraParams);
		camera.startPreview();
		isPreviewRunning = true;
			
	}
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		try {
			camera.open();
			Parameters cameraParams=camera.getParameters();
			cameraParams.setJpegQuality(20);
			cameraParams.setJpegThumbnailQuality(20);
			List<Size> measures=cameraParams.getSupportedPictureSizes();
			
			if(measures!=null){
				Object []measuresArray=measures.toArray();
				measuresArray= sortElements(measuresArray);
				cameraParams.setPictureSize(((Size)measuresArray[measuresArray.length>1 ? 1: 0]).width,((Size)measuresArray[measuresArray.length>1 ? 1: 0]).height);
				Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "picture size: w-" +((Size)measuresArray[measuresArray.length>1 ? 1: 0]).width + "  h-"+ ((Size)measuresArray[measuresArray.length>1 ? 1: 0]).height);
			}
			//cameraParams.setRotation(90);
			camera.setParameters(cameraParams);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			popUp= new PopUp(this, R.string.popUpError, R.string.cameraOpenError);
			DialogButton button= new DialogButton();
			button.setText(R.string.accept);
			button.setType(DialogButton.POSITIVE);
			button.setListener(new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});
			
			popUp.setCancelable(false);
			popUp.addButton(button);
			popUp.create();
			popUp.show();
		}catch (RuntimeException e) {
			// TODO: handle exception
			Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG,"RuntimeException: Error en settings "+ e.getMessage());
		}
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub}
		release();

	}
	//**********MESSAGE LISTENERS**************************************
	
	@Override
	public void sendMessage(Context ctx,Message message,final CameraCallBackParameters parameter) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		String path=(String) parameters[0];
		switch (message.getType()) {
		case Camera.PICTURE_TAKEN:
			 if(path.length()>0){
				 Logger.log(Logger.MESSAGE, CAMERA_PREVIEW_TAG, "ruta de la foto : "+ path);
				 SaleBean.setImage(path);
				 setResult(RESULT_OK);
				 finish();
			 }
			 else {
				 setResult(ERROR_CODE);
				 finish();
			 }
			break;

		default:
			break;
		}
		System.gc();
	}
	@Override
	public void sendStatus(Context ctx,int message) {
		// TODO Auto-generated method stub
		
	}
}
