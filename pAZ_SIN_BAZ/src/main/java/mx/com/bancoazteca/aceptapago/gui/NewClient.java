package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Locale;

import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.messages.EmailBean;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.ClientBean;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.jobs.ClientsJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;

public class NewClient extends BaseActivity implements MessageListener<Message, String> {
	private static final String LOGIN_VERIFICATION = "NewClient";
	private static final int POPUP_SAVING_CLIENT = 10;
	private static final int POPUP_EDITING_CLIENT = 11;
	protected static final int POPUP_ERROR = 12;
	protected static final int POPUP_EXCEPTION = 13;
	protected String tmpStr;
	protected int tmpInt;
	private EditText name,mail;
	private Button accept;
	private ClientBean clientSelected=null;
	private String folio=null;
	private String rfc=null;
	private ClientBean client=new ClientBean();
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.new_client);
		name=(EditText) findViewById(R.id.nameField);
		mail=(EditText) findViewById(R.id.mailField);
		
		accept=(Button) findViewById(R.id.save);
		
		
		InputFilter filters[]=null;
		filters= new InputFilter[name.getFilters().length +1];
		System.arraycopy(name.getFilters(), 0, filters,0,name.getFilters().length);
		filters[name.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		name.setFilters(filters);
		
		filters= new InputFilter[mail.getFilters().length +1];
		System.arraycopy(mail.getFilters(), 0, filters,0,mail.getFilters().length);
		filters[mail.getFilters().length]=new TextFilter(TextFilter.TYPE_EMAIL);
		mail.setFilters(filters);
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if (params != null) {
			clientSelected=(ClientBean) params.getSerializable(Clients.CLIENT_SELECTED);
			folio=(String) params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
			rfc=(String) params.getSerializable(CustomerBean.RFC);
		}
		
		if (clientSelected!=null) {
			accept.setText(R.string.update);
			name.setText(clientSelected.getCompleteName());
			mail.setText(clientSelected.getMail());
		}
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("tmpStr", tmpStr);
		outState.putInt("tmpInt", tmpInt);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		tmpStr=savedInstanceState.getString("tmpStr");
		tmpInt=savedInstanceState.getInt("tmpInt");
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton btn;
		switch (id) {
		case POPUP_SAVING_CLIENT:
			popUp = new PopUp(this, R.string.popUpMessage,R.string.addClient);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		case POPUP_EDITING_CLIENT:
			popUp = new PopUp(this, R.string.popUpMessage,R.string.changingClient);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();

		default:
			return null;
		}
	}
	public void save(View view){
		if (!isValidForm()) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(popupTitle),getString(popupMsg));
			dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
		}
		else{
			if (clientSelected !=null) 
				editClient();
			else
				saveClient();
		}
	}
	private void saveClient() {
		// TODO Auto-generated method stub
		showDialog(POPUP_SAVING_CLIENT);
		
		
		client.setCompleteName(name.getText().toString().toUpperCase(Locale.getDefault()));
		client.setMail(mail.getText().toString());
		backgroundThread= new ClientsJob<String>(this,folio, rfc, client, this, ClientsJob.CREATE_CLIENT);
		backgroundThread.start();
		
	}
	private void editClient() {
		// TODO Auto-generated method stub
		client.setCompleteName(name.getText().toString().toUpperCase(Locale.getDefault()));
		client.setMail(mail.getText().toString());
		client.setId(clientSelected.getId());
		Logger.log(Logger.MESSAGE, LOGIN_VERIFICATION, "Cliente seleccionado  " + client);
		showDialog(POPUP_EDITING_CLIENT);
		backgroundThread= new ClientsJob<String>(this,folio, rfc, client, this, ClientsJob.EDIT_CLIENT);
		backgroundThread.start();
	}
	private boolean isValidForm(){
		if (name.getText().length() ==0 || mail.getText().length()==0) {
			popupTitle= R.string.popUpError;
			popupMsg= R.string.emptyFields;
			return false;
		}
		else if(!EmailBean.validEmail(mail.getText().toString())){
			popupTitle= R.string.popUpError;
			popupMsg= R.string.invalidEmail;
			return false;
		}
		return true;
	}

	

	@Override
	public void sendMessage(final Context ctx,final Message message,final String parameter) {
		// TODO Auto-generated method stub
		if (isFinishing()) {
			return;
		}
		
		final int mode= ((ClientsJob<String>)backgroundThread).getServiceType();
		runOnUiThread(new Runnable() {
			public void run() {
				removeDialog(POPUP_SAVING_CLIENT);
				removeDialog(POPUP_EDITING_CLIENT);
				Logger.log(Logger.MESSAGE, LOGIN_VERIFICATION, "id regresado =  "+parameter);
				if(mode==ClientsJob.CREATE_CLIENT)
					client.setId(parameter);
				Logger.log(Logger.MESSAGE, LOGIN_VERIFICATION, "id a considerar = "+client.getId());
				if (message.getType()==Message.OK) {
					Intent intent= new Intent();
					intent.putExtra("client", client);
					intent.putExtra("mode", mode);
					
					setResult(RESULT_FIRST_USER, intent);
					finish();
				}
				else if (message.getType()==Message.EXCEPTION){
					tmpInt= message.getTextMessage();
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(tmpInt));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							setResult(101);
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUP_EXCEPTION);
				}
				
			}
		});
	}
	@Override
	public void sendStatus(Context ctx,int status) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void sendMessage(final Context ctx,final Message message, final Object... parameters) {
		// TODO Auto-generated method stub
		if (isFinishing()) {
			return;
		}
		runOnUiThread(new Runnable() {
			public void run() {
				removeDialog(POPUP_SAVING_CLIENT);
				removeDialog(POPUP_EDITING_CLIENT);
				if (message.getType()==Message.ERROR) {
					tmpStr=(String)parameters[0];
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),tmpStr);
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							setResult(101);
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
				}
				
			}
		});
	}
}

