package mx.com.bancoazteca.aceptapago.beans;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class QueryDevice implements KvmSerializable{
	private static final int FIELDS = 4;
	private static final String ID_SERVICE = "idservicio";
	private static final String FOLIO = "idComercio";
	private static final String RFC = "rfc";
	private static final String IMEI = "imei";
	private String idService;
	private String folio;
	private String rfc;
	private String imei;
	private static PropertyInfo listPI[]= new PropertyInfo[FIELDS];
	private static PropertyInfo pIidService;
	private static PropertyInfo pIfolio;
	private static PropertyInfo pIrfc;
	private static PropertyInfo pIimei;
	static{

		pIidService= new PropertyInfo();
		pIidService.name=ID_SERVICE;
		pIidService.type=PropertyInfo.STRING_CLASS;

		pIfolio= new PropertyInfo();
		pIfolio.name=FOLIO;
		pIfolio.type=PropertyInfo.STRING_CLASS;

		pIrfc= new PropertyInfo();
		pIrfc.name=RFC;
		pIrfc.type=PropertyInfo.STRING_CLASS;
		
		pIimei= new PropertyInfo();
		pIimei.name=IMEI;
		pIimei.type=PropertyInfo.STRING_CLASS;
		
		listPI[0]=pIidService;
		listPI[1]=pIfolio;
		listPI[2]=pIrfc;
		listPI[3]=pIimei;
	}
	public String getIdService() {
		return idService;
	}
	public void setIdService(String idService) {
		this.idService = idService;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: return idService;
			case 1: return folio;
			case 2: return rfc;
			case 3: return imei;
			default: return null;
		}
	}
	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}
	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}
	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: pIidService=(PropertyInfo) arg1; break;
			case 1: pIfolio=(PropertyInfo) arg1; break;
			case 2: pIrfc=(PropertyInfo) arg1; break;
			case 3: pIimei=(PropertyInfo) arg1; break;
		}
	}
	
}
