package mx.com.bancoazteca.aceptapago.baz.apertura;

import java.util.ArrayList;

import mx.com.bancoazteca.aceptapago.baz.entidades.Colonia;
import mx.com.bancoazteca.aceptapago.baz.entidades.ZonificacionEkt;
import mx.com.bancoazteca.aceptapago.baz.ws.ServicioWebEktDinero;
import mx.com.bancoazteca.aceptapago.baz.ws.parsers.ParseEktDinero;

public class UtilidadesEktDinero {

	private static final String miUrl = "http://200.38.122.37:80/EktDineroNew/EktDineroPort?wsdl";

	
	

	public ZonificacionEkt traeZonificacion(String cveCliente) {
		ZonificacionEkt retorno = new ZonificacionEkt();
		ServicioWebEktDinero miServicio = new ServicioWebEktDinero();
		miServicio._Metodo = "Zonifica";
		miServicio.wsUrl = miUrl;
		String txtXml = miServicio.traeZonificacion(cveCliente);
		if (txtXml == null)
			return new ZonificacionEkt();
		ParseEktDinero miParser = new ParseEktDinero();
		try {
			retorno = miParser.parseaZonificacion(txtXml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}


	public ArrayList<String> traeCodigosPostales(String idEstado,
			String idPoblacion) {
		ArrayList<String> retorno = new ArrayList<String>();
		ServicioWebEktDinero miServicio = new ServicioWebEktDinero();
		miServicio._Metodo = "EstadoPob_Colonia";
		miServicio.wsUrl = miUrl;
		String txtXml = miServicio.traeCodigosPostales(idEstado, idPoblacion);
		if (txtXml == null)
			return new ArrayList<String>();
		ParseEktDinero miParser = new ParseEktDinero();
		try {
			retorno = miParser.parseaCodigosPostales(txtXml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	public ArrayList<Colonia> traeColonia(String cp) {
		ArrayList<Colonia> retorno = new ArrayList<Colonia>();
		ServicioWebEktDinero miServicio = new ServicioWebEktDinero();
		miServicio._Metodo = "CP_Colonia";
		miServicio.wsUrl = miUrl;
		String txtXml = miServicio.traeColonias(cp);
		if (txtXml == null)
			return new ArrayList<Colonia>();
		ParseEktDinero miParser = new ParseEktDinero();
		try {
			retorno = miParser.parseaColonias(txtXml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	

	

}
