package mx.com.bancoazteca.aceptapago.gui;

import android.content.Intent;
import android.net.Uri;

import com.google.android.gms.plus.PlusShare;

import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.util.Logger;

public class ParseDeepLink extends BaseActivity {
	private static final String TAG_DEEP_LINK = "DEEP LINK";
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		String deepLinkId = PlusShare.getDeepLinkId(this.getIntent());
        parseDeepLinkId(deepLinkId);
        Logger.log(Logger.MESSAGE, TAG_DEEP_LINK, "loaduicomponents");
        if (intent != null) {
          startActivity(intent);
        }

        finish();
	}
    /**
     * Get the intent for an activity corresponding to the deep-link ID.
     *
     * @param deepLinkId The deep-link ID to parse.
     * @return The intent corresponding to the deep-link ID.
     */
    private void parseDeepLinkId(String deepLinkId) {
        intent = new Intent();
        if ("pazid".equals(deepLinkId)) {
        	Logger.log(Logger.MESSAGE, TAG_DEEP_LINK, "deepLinkId: "+ deepLinkId);
        	String url = "http://www.bancoazteca.com.mx";
        	intent = new Intent(Intent.ACTION_VIEW);
        	intent.setData(Uri.parse(url));
        } else {
            // Fallback to the MainActivity in your app.
        	Logger.log(Logger.MESSAGE, TAG_DEEP_LINK, "app deeplink  ");
            intent.setClass(getApplicationContext(), VideoScreen.class);
        }
    }
}
