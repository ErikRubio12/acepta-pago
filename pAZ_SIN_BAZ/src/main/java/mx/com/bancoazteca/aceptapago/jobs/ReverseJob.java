package mx.com.bancoazteca.aceptapago.jobs;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.List;
import java.util.concurrent.TimeoutException;

import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.beans.PendingSale;
import mx.com.bancoazteca.aceptapago.db.daos.SaleDao;
import mx.com.bancoazteca.util.Logger;

public class ReverseJob extends BaseService<Message, String> {		
	private SaleDao saleDao;
	private static final String SALE_RETURN_PARAM = "xml";
	private static final String SALE_RETURN_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String SALE_RETURN_NAMESPACE = "http://service.btb.com";
	private static final String SALE_RETURN_METHOD = "BusinessToBusinessService";
	private static final String SALE_RETURN_ACTION = null;
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String SALE_JOB = "REVERSE_JOB";
	private static final String SUCCESS_CODE = "00";
	private static final String REVERSE_ERROR = "330";
	public ReverseJob (Context ctx){
		this.ctx=ctx;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		int j=0;
		boolean valid=false;
		saleDao= new SaleDao(this.ctx);
		List<PendingSale> list=saleDao.querySales();
		Log.i("REVERSEJOB", "REVERSEJOB-------------");
		for (int i = 0; i < list.size(); i++) {
			do{
				if(saleReturn(list.get(i))){
					saleDao.deleteSale(list.get(i));
					valid=true;
				}	
				else{
					valid=false;
					j++;
				}
			}while(!valid && j<3);
		}
	}
	private boolean saleReturn(PendingSale currentSale){
		//Logger.log(Logger.MESSAGE, SALE_JOB,"Pendiente_sale "+currentSale.toString());
		operationCode=null;
		systemError=null;
		SoapRequest<String> req= new SoapRequest<String>(SALE_RETURN_METHOD, SALE_RETURN_ACTION);
		req.setNameSpace(SALE_RETURN_NAMESPACE);
		req.setUrl(SALE_RETURN_URL);
		Parameter<String,String> param= new Parameter<String, String>(SALE_RETURN_PARAM,createSaleReturnRequest(currentSale).toString());
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if (res==null) {
				return false;
			}
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equals(OPERATION_CODE_TAG)) 
						operationCode= Encrypt.decryptStringWS(parser.getText());
					
					else if (currentTag.equals(SYSTEM_ERROR_TAG)) 
						systemError= Encrypt.decryptStringWS(parser.getText());
					
					
					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			
			Logger.log(Logger.MESSAGE, SALE_JOB,"error sistema: "+ systemError + " codigo: " + operationCode );
			if("0".equalsIgnoreCase(operationCode) || SUCCESS_CODE.equalsIgnoreCase(operationCode) || REVERSE_ERROR.equalsIgnoreCase(operationCode))
				return true;
			else
				return false;
			
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"TimeoutException: "+ e.getMessage());
			return false;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"XmlPullParserException: "+ e.getMessage());
			return false;
		}catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"InterruptedIOException: "+ e.getMessage());
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, SALE_JOB,"IOException: "+ e.getMessage());
			return false;
		} 
	}
	private StringBuilder createSaleReturnRequest(PendingSale currentSale) {
		// TODO Auto-generated method stub
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>");
		request.append("<canalEntrada>");	
		if("PagoAztecaFullBack".equalsIgnoreCase(currentSale.getChannel()))
			currentSale.setChannel("reversoPagoAztecaFullBack");					
		else if("PagoAztecaDeslizadaEMV".equalsIgnoreCase(currentSale.getChannel()))
			currentSale.setChannel("reversoPagoAztecaEMV");
		else if("PagoAztecaChip".equalsIgnoreCase(currentSale.getChannel()))
			currentSale.setChannel("reversoPagoAztecaChip");
		request.append(Encrypt.encryptStringWS(currentSale.getChannel()));		
		request.append("</canalEntrada>");
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>");
		request.append("<tipo_operacion>");
		request.append(Encrypt.encryptStringWS("420"));
		request.append("</tipo_operacion>");
		request.append("<idterminal>");
		request.append(currentSale.getTerminal());
		request.append("</idterminal>");
		request.append("<idTransaccion>");
		request.append(Encrypt.encryptStringWS(currentSale.getIdTx()));
		//Hard Reversosrequest.append("G5IFmdizvGUJbTfih5MNLdQGwm94nLRAEps3znFWgQ2wZbVuv2cS5u6Qw6LEOJO0");
		request.append("</idTransaccion>");
		request.append("<folio>");
		request.append("");
		request.append("</folio>");
		request.append("<autorizacion>");
		request.append("");
		request.append("</autorizacion>");
		request.append("<referencia>");
		request.append("");
		request.append("</referencia>");
		request.append("<track>");
		request.append(currentSale.getCardData());
	    //Hard Reversorequest.append("i0yxkFlL/u9Dkovr9S3WZhsZbfymZ0GRTp1ZAlv5vjfuC7fQtDziV42P58yXN0DTxNYxjzcOsFY8An38shB1hBODsX/fl067fbLDQpIWgkns6/vvdeGQq2HHnXbFa1BN2iy1zaMU27qKdoIrDWPhP/fnGoMZtS8EF7F2giDiu+9Ahjjq0XqZ9YN3cI+gXfvb89YoFJHmrqm0vEv60PNw/lwmXywpoflcdMqXyTXEWHii0O7wxy5I4ONPbA12GTCd9He8ryepxxRgf1L3yRbQeKxxknAJv5Tqy7EtXMaIC3IvJnn42hpksmpY9ymNu2X82x3m3TUxfIH6Nx+myCjbeuGklL2wDHiCGE079X52PKDwQZ4F03txAMzAnAyLQQjHBdSm33GgHcCYLmw4nWHD8DeFP4lFmxExdEGsxx//2LMjWRxwej5Txv1uRFEX/dzjKv3vxcO3bM/Kniz9Kd9fCiBETZEjvOGjVU9P88Tt2DpUcpDECYkpLYc9LUNuolsyAGHRdY6B9uwqX2/B9/eBzDdj7gYAD9v2cZQfYLUY9Ctpm6RyUeUSwH5EGLxLFWhvyU69fcueiY2zI9utcZZ7mo+pPyhCqHMnzsCOlXLx9GMqWM/7Xk4lPEnIcerrvFrgJqFN465e5dv4uVCdTvGEQ8EoChM86MjNmEntFzejTHu2HUtVTvm7nah4l5+fqYTZLQ69lkw48LdWVWk+1A+/kPuR+qHq4jg9SlEhYytDADUZcKG2WFGSEGPpNcd22JnrkTKDXDbgCJnFUh5GCDqFbCJMRNlrYSlGY6wtRTj8JeMvY2uMm39Ez6YFtRRVTFM525au0Kr0P/q+2RfOBperAjWCHNr+4wh6uslAr26HO01sBchx0cr+wlA7NQB3aUBBS4qcdyHMQwzCq7YJZqS/VMpMlQpSOEGriH3D7Tdowq94zOLNnPRRZ5bLj7GA2lFz");
		request.append("</track>");
		request.append("<fechaVencimiento>");
		request.append(currentSale.getValidity());
		request.append("</fechaVencimiento>");
		request.append("<longitud>");		
		request.append(currentSale.getLongitud());
		request.append("</longitud>");
		request.append("<latitud>");		
		request.append(currentSale.getLatitud());
		request.append("</latitud>");
		request.append("<codReverse>");		
		request.append(currentSale.getCauseReverse());
		request.append("</codReverse>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		return request;
	}
	
}
