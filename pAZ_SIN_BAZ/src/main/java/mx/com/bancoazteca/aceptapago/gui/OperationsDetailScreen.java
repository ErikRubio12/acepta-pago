package mx.com.bancoazteca.aceptapago.gui;


import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.aceptapago.jobs.DownloadTicketJob;
import mx.com.bancoazteca.aceptapago.jobs.OperationListJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class OperationsDetailScreen extends BaseActivity implements MessageListener<Message, String>{
	public static final String CANCELLATION = "cancelation";
	public static final String REFUND = "refund";
	private static final int FLAG_CANCELACION=0;
	private static final int FLAG_DEVOLUCION=1;
	private static final int INIT_DAY_TAG = 120;
	private static final int FINAL_DAY_TAG = 121;
	private static final int POPUP_INVALID_DATES = 11;
	private static final int POPUP_INVALID_RANGE = 12;
	private static final int POPUP_LOADING_OPERATIONS = 13;
	private static final int POPUP_LOADING_TICKET = 14;
	private static final int POPOUP_NO_SD = 20;
	protected static final int POPUP_NO_OPERAIIONS = 15;
	protected static final int POPUP_SERVICE_ERROR = 16;
	protected static final int POPUP_CONNECTION_ERROR = 17;
	protected static final int POPUP_NO_TICKET = 18;
	protected static final String TAG_OPERATION_DETAIL = "operation detail screen";
	protected static final String PRUEBA_TAG_OPERATION_DETAIL = " test operation detail screen";
	private TextView todayTextIndicator;
	private TextView intervalTextIndicator;
	private TextView intervalTextFromIndicator;
	private TextView intervalTexToIndicator;
	private CustomerBean customer;
	private FiscalDataBean fiscalData;
	private ArrayList<OperationsBean> cancellations= new ArrayList<OperationsBean>();
	private ArrayList<OperationsBean> approved= new ArrayList<OperationsBean>();
	private ArrayList<OperationsBean> devoluciones= new ArrayList<OperationsBean>();
	private OperationsBean currentOperation=null;
	private String strTagCellToCancel=null;
	private ScrollView cancellationsContent;
	private ScrollView salesContent;
	private ImageView arrowCancellations;
	private ImageView arrowSales;
	private String fechaConsult;
	private Date today;
	private Toolbar mtoolbar;



	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.operation_detail);
		todayTextIndicator= (TextView) findViewById(R.id.todayTextIndicator);
		intervalTextIndicator=(TextView) findViewById(R.id.intervalTextIndicator);
		
		intervalTextFromIndicator=(TextView) findViewById(R.id.intervalTextFromIndicator);
		intervalTexToIndicator=(TextView) findViewById(R.id.intervalTexToIndicator);
		
		cancellationsContent=(ScrollView) findViewById(R.id.cancellationsContent);
		salesContent=(ScrollView) findViewById(R.id.salesContent);		
		
		arrowCancellations=(ImageView) findViewById(R.id.arrowCancellations);
		arrowSales=(ImageView) findViewById(R.id.arrowSales);
		mtoolbar=(Toolbar) findViewById(R.id.toolbar);


		intervalTextFromIndicator.setTag(INIT_DAY_TAG);
		intervalTexToIndicator.setTag(FINAL_DAY_TAG);
		todayTextIndicator.setText(Utility.formatDate(new Date(), "'Hoy'  dd/MM/yyyy"));
		todayTextIndicator.setHint(Utility.formatDate(new Date(), "'Hoy'  dd/MM/yyyy"));
		setSizeContainers();
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
	}


	private void setSizeContainers(){
		LayoutParams params= cancellationsContent.getLayoutParams();
		cancellationsContent.setLayoutParams(params);
		params=salesContent.getLayoutParams();
		params.height=(int) (getHeight()*0.27);
		salesContent.setLayoutParams(params);
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params != null){
			customer=(CustomerBean)params.getSerializable(CustomerBean.CUSTOMER);
			fiscalData=(FiscalDataBean)params.getSerializable(FiscalDataBean.FISCAL_DATA);
		}
		
	}
	@Override
	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		super.startBackgroundProcess();
		showDialog(POPUP_LOADING_OPERATIONS);
		backgroundThread= new Thread(new OperationListJob(this,fiscalData.getRfc(), customer.getFolioPreap(),"","",this));
		backgroundThread.start();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("tagCellCancel", strTagCellToCancel);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		strTagCellToCancel =savedInstanceState.getString("tagCellCancel");
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton acceptBtn;
		DialogButton cancelBtn;
		switch (id) {
		case POPUP_LOADING_OPERATIONS:
			popUp= new PopUp(this, R.string.popUpMessage,R.string.loadingOperations);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog(); 
		case POPUP_LOADING_TICKET:
			popUp= new PopUp(this, R.string.popUpMessage, R.string.checkingTicket);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		default:
			return null;
		}
	}
	@Override
	public void back(View v) {
		// TODO Auto-generated method stub
		setResult(RESULT_OK);
		super.back(v);
	}
	public void textIndicatorSelected(View v){
		if(v.getId()==R.id.todayTextIndicator){
			if(intervalTextIndicator.getVisibility()!=View.VISIBLE){
				todayTextIndicator.setBackgroundResource(R.drawable.background_button);
				todayTextIndicator.setText(todayTextIndicator.getHint());
				intervalTextIndicator.setVisibility(View.VISIBLE);
				intervalTextFromIndicator.setVisibility(View.GONE);
				intervalTexToIndicator.setVisibility(View.GONE);
				intervalTextFromIndicator.setText("");
				intervalTexToIndicator.setText("");
				approved.clear();
				cancellations.clear();
				devoluciones.clear();
				((LinearLayout)salesContent.findViewById(R.id.content1)).removeAllViews();
				((LinearLayout)cancellationsContent.findViewById(R.id.content2)).removeAllViews();
				showDialog(POPUP_LOADING_OPERATIONS);
				
				backgroundThread= new Thread(new OperationListJob(this,fiscalData.getRfc(), customer.getFolioPreap(),"","",this));
				backgroundThread.start();
			}
		}
		else if(v.getId()==R.id.intervalTextIndicator){
			todayTextIndicator.setBackgroundResource(R.drawable.background_button_gray);
			todayTextIndicator.setHint(todayTextIndicator.getText());
			todayTextIndicator.setText("");
			intervalTextIndicator.setVisibility(View.GONE);
			intervalTextFromIndicator.setVisibility(View.VISIBLE);
			intervalTexToIndicator.setVisibility(View.VISIBLE);
			approved.clear();
			cancellations.clear();
			devoluciones.clear();
			((LinearLayout)salesContent.findViewById(R.id.content1)).removeAllViews();
			((LinearLayout)cancellationsContent.findViewById(R.id.content2)).removeAllViews();
		}
	}
	public void arrowSelected(View v){		
		if(v.getId()==R.id.arrowSales || v.getId()==R.id.sales){
			arrowSales.setImageResource(R.drawable.btn_sub_down);
			
			if(((LinearLayout)salesContent.findViewById(R.id.content1)).getChildCount()>0)
				salesContent.setVisibility(View.VISIBLE);
			
			arrowCancellations.setImageResource(R.drawable.btn_sub_right);									
			cancellationsContent.setVisibility(View.GONE);
		}
		else if(v.getId()==R.id.arrowCancellations || v.getId()==R.id.cancellations){
			arrowSales.setImageResource(R.drawable.btn_sub_right);
			salesContent.setVisibility(View.GONE);
			
			arrowCancellations.setImageResource(R.drawable.btn_sub_down);
			if(((LinearLayout)cancellationsContent.findViewById(R.id.content2)).getChildCount()>0)
				cancellationsContent.setVisibility(View.VISIBLE);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case SaleScreen.SETTINGS_CODE:
				if(resultCode==RESULT_OK)
					finish();
			break;

		default:
			break;
		}
	}
	public void download(View v) {
		// TODO Auto-generated method stub
		String tag= v.getTag().toString();
		Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"tag: "+ tag);	
		showDialog(POPUP_LOADING_TICKET);
		StringTokenizer tokens= new StringTokenizer(tag,"-");
		String contentType=tokens.nextToken();//Primero indica el contenedor
		String type=tokens.nextToken();
		Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"tipo oper: " + type );
		String iString=tokens.nextToken();
		StringBuffer key= new StringBuffer(tokens.nextToken());
		key.append("-");
		key.append(tokens.nextToken());
		if(iString!= null && iString.length()>0){
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"operacion del ticket: "+currentOperation);
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"vista1: "+salesContent.findViewById(R.id.content1) + "  vista2: "+cancellationsContent.findViewById(R.id.content2));
			if(contentType.equalsIgnoreCase("1")){ //VENTAS
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"aprovadas: "+approved.size());
				currentOperation=approved.get(Integer.valueOf(iString));
				backgroundThread= new Thread(new DownloadTicketJob(this,this,key.toString(),false,currentOperation));
			}
			else if(contentType.equalsIgnoreCase("2")){ //CANCELACIONES
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"canceladas: "+cancellations.size());
				currentOperation=cancellations.get(Integer.valueOf(iString));
				backgroundThread= new Thread(new DownloadTicketJob(this,this,key.toString(),true,currentOperation));
			}
			backgroundThread.start();
		}
	}
	public void cancel(String tag,boolean refund){
		Logger.log("Se va a cancelar una venta: "+  tag);
		StringTokenizer tokens= new StringTokenizer(tag,"-");
		tokens.nextToken();//Primero indica el contenedor ventas o cancellaciones
		String operType=tokens.nextToken(); // si es una venta o una cancelacion (solo seria para contenedor de cancelacione)
		String iString=tokens.nextToken(); // indice
		OperationsBean operationSelected=null;
		if(iString != null && iString.length()>0){
			if("Venta".equalsIgnoreCase(operType)){
				operationSelected=approved.get(Integer.valueOf(iString));
				ArrayList<Serializable> values= new ArrayList<Serializable>(3);
				values.add(operationSelected);
				values.add(fiscalData);
				values.add(customer.getFolioPreap());
				values.add(customer.getLogin());
				ArrayList<String> names= new ArrayList<String>(3);
				names.add(OperationsBean.OPERATION);
				names.add(FiscalDataBean.FISCAL_DATA);
				names.add(CustomerBean.FOLIO_PREAPERTURA);
				names.add(LoginBean.LOGIN);
				intent.putExtra(CANCELLATION, true);
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "devolucion = "+ refund);
				intent.putExtra(REFUND, refund);
				Log.i(PRUEBA_TAG_OPERATION_DETAIL, "++++++ "+operationSelected.getAmount());
				goForwardForResult(SaleScreen.class,this, values, names,SaleScreen.SETTINGS_CODE);
			}
				
			else{
				Logger.log("Aqui tampoco deberia entrar");
//				operationSelected=cancellations.get(Integer.valueOf(iString));
			}										
		}
	}
	private OnDateSetListener initListener= new OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			StringBuilder date= new StringBuilder();
			if (dayOfMonth<10)
				date.append("0"+cal.get(Calendar.DAY_OF_MONTH));
			else
				date.append(cal.get(Calendar.DAY_OF_MONTH));
			date.append("/");
			if ((cal.get(Calendar.MONTH) + 1)>=10) {
				date.append(cal.get(Calendar.MONTH) + 1);
			}
			else{
				date.append("0");
				date.append(cal.get(Calendar.MONTH) + 1);
			}
			date.append("/");
			date.append(cal.get(Calendar.YEAR));

			intervalTextFromIndicator.setText(date);
		}
	};
	private OnDateSetListener finalListener= new OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			StringBuilder date= new StringBuilder();
			if (dayOfMonth<10)
				date.append("0"+cal.get(Calendar.DAY_OF_MONTH));
			else
				date.append(cal.get(Calendar.DAY_OF_MONTH));
			date.append("/");
			if ((cal.get(Calendar.MONTH) + 1)>=10) {
				date.append(cal.get(Calendar.MONTH) + 1);
			}
			else{
				date.append("0");
				date.append(cal.get(Calendar.MONTH) + 1);
			}
			date.append("/");
			date.append(cal.get(Calendar.YEAR));
			
			intervalTexToIndicator.setText(date);
			searchOperations();
		}
	};
	public void showDates(View v){
		Calendar cal=Calendar.getInstance();
		int tag=(Integer) v.getTag();
		
		DatePickerDialog datePicker= new DatePickerDialog(this,tag== INIT_DAY_TAG ? initListener : finalListener,cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
		datePicker.show();
	}
	public void searchOperations (){
		if(intervalTextFromIndicator.getText()!=null && intervalTextFromIndicator.getText().length()==10 &&
			intervalTexToIndicator.getText()!=null && intervalTexToIndicator.getText().length()==10){
			
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"Buscando ventas");
			String initDate= intervalTextFromIndicator.getText().toString();
			String finalDate= intervalTexToIndicator.getText().toString();
			StringTokenizer tokens= new StringTokenizer(initDate,"/");
			int dayInit=Integer.parseInt(tokens.nextToken());
			int monthInit=Integer.parseInt(tokens.nextToken());
			int yearInit=Integer.parseInt(tokens.nextToken());
			tokens= new StringTokenizer(finalDate,"/");
			int dayFinal=Integer.parseInt(tokens.nextToken());
			int monthFinal=Integer.parseInt(tokens.nextToken());
			int yearFinal=Integer.parseInt(tokens.nextToken());
			
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "dia inicial: " + dayInit + " mes inicial:" + monthInit + " a�o inicial: "+ yearInit);
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "dia final: " + dayFinal + " mes final:" + monthFinal + " a�o final: "+ yearFinal);
			
//			dia inicial: 15 mes inicial:5 a�o inicial: 2013
//			dia final: 6 mes final:6 a�o final: 2013
			String dateFormat=Utility.formatDate(new Date(), "dd/MM/yyyy");
			tokens= new StringTokenizer(dateFormat,"/");
			int currentDay=Integer.parseInt(tokens.nextToken());
			int currentMonth=Integer.parseInt(tokens.nextToken());
			int currentYear=Integer.parseInt(tokens.nextToken());
			if((yearInit>currentYear) ||
				(yearInit==currentYear  && monthInit>currentMonth) ||
				(yearInit==currentYear  && monthInit==currentMonth && dayInit>currentDay)){
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "primer fecha mal");
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.invalidDates));
				dialog.show(getSupportFragmentManager(),""+POPUP_INVALID_DATES);
				return;
			}
			else if((yearFinal>currentYear) ||
					(yearFinal==currentYear  && monthFinal>currentMonth) ||
					(yearFinal==currentYear  && monthFinal==currentMonth && dayFinal>currentDay)){
					Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"segunda fecha mal");
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.invalidDates));
				dialog.show(getSupportFragmentManager(), "" + POPUP_INVALID_DATES);
					return;
				}
			else if ((yearInit > yearFinal) || (yearInit == yearFinal &&  (monthInit>monthFinal)) || (yearInit == yearFinal  && (monthInit==monthFinal) && (dayInit>dayFinal))) {

				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.invalidRangeDates));
				dialog.show(getSupportFragmentManager(), "" + POPUP_INVALID_RANGE);
				return;
			}
			showDialog(POPUP_LOADING_OPERATIONS);
			
			backgroundThread= new Thread(new OperationListJob(this,fiscalData.getRfc(), customer.getFolioPreap(),intervalTextFromIndicator.getText().toString(),intervalTexToIndicator.getText().toString(),this));
			backgroundThread.start();
		}
		else{
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.invalidDates));
			dialog.show(getSupportFragmentManager(),""+POPUP_INVALID_DATES);
		}

	}
	private void drawOperations() {
		// TODO Auto-generated method stub
		RelativeLayout currentOperationLayout;
		ImageButton ticket;
		TextView authorization;
		TextView last4Digits;
		TextView date2;
		TextView amount;
		TextView operationType;
		Button cancellationBtn;
		SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yyyy HH:mm",Locale.getDefault());
		SimpleDateFormat formatterCompare= new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.getDefault());
		((LinearLayout)salesContent.findViewById(R.id.content1)).removeAllViews();
		((LinearLayout)cancellationsContent.findViewById(R.id.content2)).removeAllViews();
		try {
			Log.i("TO DATE", ""+fechaConsult);
			today = formatter.parse(fechaConsult);
			/***Prueba corte prosa SIMULACIONES DE FECHA***/
			//today = formatter.parse("09/07/2015 23:00");
			//today = formatter.parse("08/07/2015 23:00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"operaciones a pintar" + cancellations.size() + " ventas - "+ approved.size());
		for (int i = 0; i < approved.size(); i++) {//VENTAS
			currentOperation=approved.get(i);
			currentOperationLayout=(RelativeLayout) Utility.inflateLayout(this, R.layout.operation, null);
			authorization=(TextView) currentOperationLayout.findViewById(R.id.autorizacion);
			last4Digits=(TextView) currentOperationLayout.findViewById(R.id.digito4Tarjeta);
			date2=(TextView) currentOperationLayout.findViewById(R.id.operationDate);
			amount=(TextView) currentOperationLayout.findViewById(R.id.amount);
			operationType=(TextView) currentOperationLayout.findViewById(R.id.operationType);
			ticket=(ImageButton) currentOperationLayout.findViewById(R.id.ticketButton);
			cancellationBtn=(Button) currentOperationLayout.findViewById(R.id.cancellation);
			authorization.setText(currentOperation.getAuthorizedNumber());
			last4Digits.setText(last4Digits.getText() + currentOperation.getCardNUmber());
			date2.setText(currentOperation.getOperationDate());
			amount.setText(changeMonto(currentOperation.getAmount()));
			operationType.setText(currentOperation.getCodeProcess());
			StringBuffer tag= new StringBuffer("1-");//Contenedor de Ventas
			if(currentOperation.getMessageType().equalsIgnoreCase("0200") || currentOperation.getMessageType().equalsIgnoreCase("200")){
//				operationType.setText(" Venta");
				tag.append("Venta-");
			}else{
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "Aqui no deberia entrar");
//				operationType.setText(" Cancelación");
				tag.append("Cancelacion-");
			}
			tag.append(Integer.valueOf(i));
			tag.append("-");
			tag.append(currentOperation.getFolio());
			tag.append("-");
			tag.append(currentOperation.getAuthorizedNumber());
			cancellationBtn.setTag(tag.toString());
			ticket.setTag(tag.toString());
			currentOperationLayout.setTag(tag.toString());
			
			currentOperationLayout.setOnClickListener(detailListener);
			Date dateToCompare = null;
//			String dateToCompare=currentOperation.getOperationDate().substring(0, 11);
//			dateToCompare=dateToCompare.trim();
			try {
				dateToCompare=formatterCompare.parse(currentOperation.getOperationDate());
				} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"fecha a comparar 1= " +dateToCompare);
			((LinearLayout)salesContent.findViewById(R.id.content1)).addView(currentOperationLayout);
			int resultCorteProsa=corteProsa(dateToCompare, today);
				if (resultCorteProsa==FLAG_DEVOLUCION){
					Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"DEVOLUCION");
					cancellationBtn.setOnClickListener(refund);
					cancellationBtn.setText(R.string.refund);	
				}
				
			}
				
		
		
		for (int i = 0; i < cancellations.size(); i++) {//CANCELACIONES
			currentOperation=cancellations.get(i);
			currentOperationLayout=(RelativeLayout) Utility.inflateLayout(this, R.layout.operation, null);
			authorization=(TextView) currentOperationLayout.findViewById(R.id.autorizacion);
			last4Digits=(TextView) currentOperationLayout.findViewById(R.id.digito4Tarjeta);
			date2=(TextView) currentOperationLayout.findViewById(R.id.operationDate);
			amount=(TextView) currentOperationLayout.findViewById(R.id.amount);
			operationType=(TextView) currentOperationLayout.findViewById(R.id.operationType);
			ticket=(ImageButton) currentOperationLayout.findViewById(R.id.ticketButton);
			cancellationBtn=(Button) currentOperationLayout.findViewById(R.id.cancellation);
			
			authorization.setText(currentOperation.getAuthorizedNumber());
			last4Digits.setText(last4Digits.getText() + currentOperation.getCardNUmber());
			date2.setText(currentOperation.getOperationDate());
			amount.setText(changeMonto(currentOperation.getAmount()));
			operationType.setText(currentOperation.getCodeProcess());
			StringBuffer tag= new StringBuffer("2-");//Contenedor cancelaciones
			if(currentOperation.getMessageType().equalsIgnoreCase("0200")  || currentOperation.getMessageType().equalsIgnoreCase("0220")){
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"agregado 2 a" );
//				operationType.setText(" Venta");
				tag.append("Venta-");
				ticket.setVisibility(View.INVISIBLE);
			}
				
			else{
				Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL,"agregado 2 b" );
//				operationType.setText(" Cancelación");
				tag.append("Cancelacion-");
			}
			tag.append(Integer.valueOf(i));
			tag.append("-");
			tag.append(currentOperation.getFolio());
			tag.append("-");
			tag.append(currentOperation.getAuthorizedNumber());
			cancellationBtn.setTag(tag.toString());
			cancellationBtn.setVisibility(View.GONE);
			ticket.setTag(tag.toString());
			currentOperationLayout.setTag(tag.toString());
			
			currentOperationLayout.setOnClickListener(detailListener);
			
			Date dateToCompare = null;
//			String dateToCompare=currentOperation.getOperationDate().substring(0, 11);
//			dateToCompare=dateToCompare.trim();
			try {
				dateToCompare=formatterCompare.parse(currentOperation.getOperationDate());
				} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				((LinearLayout)cancellationsContent.findViewById(R.id.content2)).addView(currentOperationLayout);		
		}
		
		Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "tamaño: "+ ((LinearLayout)salesContent.findViewById(R.id.content1)).getChildCount() + "  " + ((LinearLayout)cancellationsContent.findViewById(R.id.content2)).getChildCount());
		((LinearLayout)salesContent.findViewById(R.id.content1)).invalidate();
		((LinearLayout)cancellationsContent.findViewById(R.id.content2)).invalidate();
	}	
		
	
	private OnClickListener detailListener= new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			View v=arg0.findViewById(R.id.container2);
			v.setVisibility(View.VISIBLE);
			RelativeLayout currentCell;
			ViewGroup container=((ViewGroup)arg0.getParent());
			Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "registros: "+ container.getChildCount());
			for (int i = 0; i < container.getChildCount(); i++) {
				currentCell=(RelativeLayout) container.getChildAt(i);
				if(currentCell!=arg0){
					currentCell.findViewById(R.id.container2).setVisibility(View.GONE);
					((ImageView)currentCell.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right_grey);
				}
				else{
					currentCell.findViewById(R.id.operationDate).setSelected(true);
					((ImageView)currentCell.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down_grey);
				}
			}
		}
	};
	@Override
	public void sendMessage(Context ctx,final Message message,final String parameter) {
		// TODO Auto-generated method stub
		
		if (isFinishing()) {
			return;
		}
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				if(message.getType()==DownloadTicketJob.DOWNLOADED){
					this_.removeDialog(POPUP_LOADING_TICKET);
					Logger.log("se intento remover el dialogo");
					List<Serializable> params= new ArrayList<Serializable>();
					params.add(parameter);
					params.add(fiscalData.getRfc());
					params.add(customer.getFolioPreap());
					params.add(currentOperation);
					List<String> paramsName= new ArrayList<String>();
					paramsName.add("urlTicket");
					paramsName.add(FiscalDataBean.BUSINESS_RFC);
					paramsName.add(CustomerBean.FOLIO_PREAPERTURA);
					paramsName.add(OperationsBean.OPERATION);
					goForwardForResult(TicketScreen.class, OperationsDetailScreen.this, params, paramsName, SaleScreen.SETTINGS_CODE);
				}
			}
		});
		
	}
		@Override
		public void sendStatus(Context ctx,final int status) {
			// TODO Auto-generated method stub
			if(isFinishing())
				return;

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub

					this_.removeDialog(POPUP_LOADING_OPERATIONS);
					this_.removeDialog(POPUP_LOADING_TICKET);
					if (status==OperationListJob.NO_OPERATIONS) {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.emptyOperations));
						dialog.show(getSupportFragmentManager(),""+POPUP_NO_OPERAIIONS);
						drawOperations();
					}
					else if (status==Message.CANCEL) {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.serviceError));
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								finish();
							}
						});
						dialog.show(getSupportFragmentManager(),""+POPUP_SERVICE_ERROR);
					}else if (status==Message.EXCEPTION) {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError), getString(R.string.connectionFails));
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								finish();
							}
						});
						dialog.show(getSupportFragmentManager(), "" + POPUP_CONNECTION_ERROR);
					}else if(status==DownloadTicketJob.DOWNLOADED_ERROR) {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.noTicket));
						dialog.show(getSupportFragmentManager(),""+POPUP_NO_TICKET);
					}

				}
			});
		}
		@Override
		public void sendMessage(Context ctx,final Message message,final Object... parameters) {
			// TODO Auto-generated method stub
			if(isFinishing())
				return;
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(message.getType()==Message.OK){
						List<List<OperationsBean>>list=(List<List<OperationsBean>>) parameters[0];
						approved= new ArrayList<OperationsBean>(list.get(0));
						cancellations=  new ArrayList<OperationsBean>(list.get(1));
						devoluciones=  new ArrayList<OperationsBean>(list.get(2));
						cancellations.addAll(devoluciones);
						fechaConsult=(String) parameters[1];

						
						Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "cancelaciones: " + list.get(1).size());
						Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "devoluciones: " + devoluciones.size());
						Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "aprovadas: " + approved.size());
						int suma = list.get(1).size()+devoluciones.size()+approved.size();
						Logger.log(Logger.MESSAGE, TAG_OPERATION_DETAIL, "lista total operaciones " + suma);
						this_.removeDialog(POPUP_LOADING_OPERATIONS);
						drawOperations();

					}
				}
			});	
		}
		public OnClickListener refund= new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strTagCellToCancel=v.getTag().toString();
				if(!FileManager.isSdPresent()) {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.sdCardNotPresent));
					dialog.show(getSupportFragmentManager(),""+POPOUP_NO_SD);
				}else{
					//DEVOLUCION
					cancel(strTagCellToCancel,true);// devolucion
					//cancel(strTagCellToCancel,false);// forzar Cancelacion
				}
			}
		};
		public void cancellation(View v){
			strTagCellToCancel=v.getTag().toString();
			if(!FileManager.isSdPresent()) {
				SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.sdCardNotPresent));
				dialog.show(getSupportFragmentManager(), "" + POPOUP_NO_SD);
			}else{
				//CANCELACION
				cancel(strTagCellToCancel, false);// Cancelacion
				//cancel(strTagCellToCancel,true);//forzar Devolucion
			}
		}
		
		public static String changeMonto(String monto) {
			StringBuffer buffer = new StringBuffer();
			String decimalesStr=null;
			String enterosStr=null;
			int index=monto.indexOf(".");
			if(index>=0){
				decimalesStr=monto.substring(index+1);
				enterosStr=monto.substring(0, index);
			}
			else{
				decimalesStr=monto.substring(monto.length()-2);
				enterosStr=monto.substring(0, monto.length()-2);
			}
			int entero=Integer.parseInt(enterosStr);
			String montoAut= entero+"."+decimalesStr;				
			buffer.append(montoAut);
			
			return buffer.toString();
		}
		
		private int corteProsa(Date fechaTransaccion ,Date fechaCurrent) {
			Date fechaProsaCurrent=null;
			Date fechaProsaYesterday=null;
			//Log.i("CORTEPROSA", "FECHA CURRENT PROSA "+fechaCurrent);
			//Log.i("CORTEPROSA", "FECHA TRANSACCION PROSA "+fechaTransaccion);
			Calendar calendarCurrent=new GregorianCalendar();
			calendarCurrent.setTime(fechaCurrent);
			calendarCurrent.set(Calendar.HOUR_OF_DAY, 22);
			calendarCurrent.set(Calendar.MINUTE, 30);
			calendarCurrent.clear(Calendar.SECOND);
			fechaProsaCurrent=calendarCurrent.getTime();
			if (fechaCurrent.compareTo(fechaProsaCurrent)>0) {
				calendarCurrent.add(Calendar.DAY_OF_MONTH, 1);
				fechaProsaCurrent=calendarCurrent.getTime();
			}
			
			Calendar calendarYesterday=new GregorianCalendar();
			calendarYesterday.setTime(fechaProsaCurrent);
			calendarYesterday.add(Calendar.DAY_OF_MONTH, -1);
			fechaProsaYesterday=calendarYesterday.getTime();
			//Log.i("CORTEPROSA", "FECHA CORTE AYER PROSA "+fechaProsaYesterday);
			//Log.i("CORTEPROSA", "FECHA CORTE HOY PROSA "+fechaProsaCurrent);
			
			if ((fechaTransaccion.compareTo(fechaProsaYesterday)>0 && fechaTransaccion.compareTo(fechaProsaCurrent)<0)) {
			//	Log.i("CORTEPROSA", "CANCELACION");
				return FLAG_CANCELACION;
			}else if(fechaTransaccion.compareTo(fechaProsaYesterday)<0 && fechaTransaccion.compareTo(fechaProsaCurrent)<0){
			//	Log.i("CORTEPROSA", "DEVOLUCION");
				return FLAG_DEVOLUCION;
			}
			return 2;
			
			
		}
		
}
