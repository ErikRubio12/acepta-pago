package mx.com.bancoazteca.aceptapago.gui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera.CameraInfo;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.hw.Camera;
import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.hw.LocationThread;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.Configuration;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.aceptapago.db.PagoAztecaDB;
import mx.com.bancoazteca.aceptapago.db.daos.SaleDao;
import mx.com.bancoazteca.aceptapago.jobs.LoginJob;
import mx.com.bancoazteca.aceptapago.jobs.ReverseJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.ConfirmationDialog;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;


public class PagoAzteca extends BaseActivity implements MessageListener<Message, String> {
    private static final int PASSWORD_MIN_LENGTH = 1;
    private static final int USER_MIN_LENGTH = 1;
    private EditText user;
    private EditText pw;
    private TextView registrationLink;
    private Button recoverPwLink;
    private final static int POPUP_NETWORK = 28;
    private final static int POPUP_REGISTRATION = 29;
    private final static int POPUP_NO_LOCATION = 30;
    private static final int POPUP_USER_DATA_REGISTERED = 35;
    private static final int GPS_SETTINGS = 11;
    private static final String LOGIN = "AceptaPago";
    protected static final String TAG_MAIN_SCREEN = "Acepta Pago Screen";
    private SaleDao saleDao;
    private RegistrationBean registerIncomplete = null;
    private Button enterButton;
    private ImageView helpButton;
    private LoginBean login = new LoginBean();
    public static String TEST_READER = "bancoazteca.demoreader";
    private Bundle myBundle;
    private MapView mMapView;
    private ImageView mapImage;

    private GoogleMap googleMap;


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (pw != null && user != null) {

            pw.setText("");
            user.postDelayed(new Runnable() {
                public void run() {
                    HandsetInfo.showKeyboard(PagoAzteca.this, user);
                }
            }, 150);
        }

    }

    private void takeSnapshot() {
        if (googleMap == null) {
            return;
        }

        final ImageView snapshotHolder = (ImageView) findViewById(R.id.mapImage);

        final GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {

                if(snapshot!=null){
                // Callback is called from the main thread, so we can modify the ImageView safely.
                snapshotHolder.setImageBitmap(snapshot);}
            }
        };

        googleMap.snapshot(callback);

    }


    @Override
    protected void loadUiComponents() {
        // TODO Auto-generated method stub

        setContentView(R.layout.login_screen);
//        mMapView = (MapView) findViewById(R.id.map);
//        mMapView.onCreate(null);
//
//        mMapView.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap googleMap) {
//
//                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                googleMap.setMyLocationEnabled(true);
//                googleMap.addMarker(new MarkerOptions().position(new LatLng(18.520897, 73.772396)).title("DSK Ranwara Road"));
//            }
//        });
//        mMapView.getMap().setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        mMapView.getMap().setMyLocationEnabled(true);
//        mMapView.getMap().addMarker(new MarkerOptions().position(new LatLng(18.520897, 73.772396)).title("DSK Ranwara Road"));

//        mapImage = (ImageView) findViewById(R.id.mapImage);
//        if (googleMap == null) {
//            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
//        }
//
//        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//googleMap.setMyLocationEnabled(true);
//
//
//        LatLng coordinate = new LatLng(19.291545,-99.001452);
//        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 11);
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));
//        googleMap.animateCamera(yourLocation);
//
//
//        googleMap.setMyLocationEnabled(true);
//        googleMap.getUiSettings().setZoomControlsEnabled(true);
//
//        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
//            @Override
//            public void onMapLoaded() {
//                takeSnapshot();
//            }
//        });



//        googleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
//            @Override
//            public void onSnapshotReady(Bitmap snapshot) {
//                try {
//                    getWindow().getDecorView().findViewById(android.R.id.content).setDrawingCacheEnabled(true);
//                    Bitmap backBitmap = getWindow().getDecorView().findViewById(android.R.id.content).getDrawingCache();
//                    Bitmap bmOverlay = Bitmap.createBitmap(
//                            backBitmap.getWidth(), backBitmap.getHeight(),
//                            backBitmap.getConfig());
//                    Canvas canvas = new Canvas(bmOverlay);
//                    canvas.drawBitmap(snapshot, new Matrix(), null);
//                    canvas.drawBitmap(backBitmap, 0, 0, null);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        Utility.setInitialContext(this);
        user = (EditText) findViewById(R.id.userEditText);
        pw = (EditText) findViewById(R.id.pwEditText);
        enterButton = (Button) findViewById(R.id.enterButton);
        helpButton = (ImageView) findViewById(R.id.helpButton);
        registrationLink = (TextView) findViewById(R.id.registrationLink);
        recoverPwLink = (Button) findViewById(R.id.recoverPwLink);
        InputFilter filters[] = null;
        filters = new InputFilter[user.getFilters().length + 1];
        System.arraycopy(user.getFilters(), 0, filters, 0, user.getFilters().length);
        filters[user.getFilters().length] = new TextFilter(TextFilter.TYPE_CHARACTERS);
        user.setFilters(filters);

        filters = new InputFilter[pw.getFilters().length + 1];
        System.arraycopy(pw.getFilters(), 0, filters, 0, pw.getFilters().length);
        filters[pw.getFilters().length] = new TextFilter(TextFilter.TYPE_CHARACTERS);
        pw.setFilters(filters);
        setListeners();
    }

    @Override
    protected void processData() {
        // TODO Auto-generated method stub
        Configuration.loadPreferences(this);
        if (!Camera.isCameraAvailable()) {
            Logger.log(Logger.MESSAGE, LOGIN, "Camara no disponible");
            Configuration.setTakeShoot(false);
            Configuration.persistPreferences(this);
        } else
            Logger.log(Logger.MESSAGE, LOGIN, "Camara disponible");
        if (!PagoAztecaDB.loadDB(this)) {
            popupTitle = R.string.popUpError;
            popupMsg = R.string.dbLoadError;
            showDialog(POPUP_MESSAGES_EXIT);
        }

    }

    @Override
    protected void startBackgroundProcess() {
        // TODO Auto-generated method stub
        super.startBackgroundProcess();
        saleDao = new SaleDao(this);
        int count = saleDao.countSales();
        Logger.log(Logger.MESSAGE, LOGIN, "******************numero de reversos: " + count);
        if (count > 0)
            new Thread(new ReverseJob(this)).start();
    }

    private void setListeners() {
        // TODO Auto-generated method stub
        registrationLink.setOnClickListener(registrationLinkListener);
        recoverPwLink.setOnClickListener(pwRecoverListener);
        enterButton.setOnClickListener(enterButtonListener);
        helpButton.setOnClickListener(helpButtonListener);

//        myBundle = getIntent().getExtras();
//
//        if (myBundle != null) {
//            if(MyReceiver.Receiver) {
//                user.setText(myBundle.getString("User"));
//                password.setText(myBundle.getString("Password"));
//                enterButton.performClick();
//            }
//        }
    }

    public void back(View v) {
        goForward(VideoScreen.class, this);
        finish();
    }

    private boolean validForm(LoginBean login) {
        // TODO Auto-generated method stub
        boolean answer = true;

//        if (login.getPassword().length() >= PASSWORD_MIN_LENGTH && login.getUser().length() >= USER_MIN_LENGTH) {
        if (login.getPassword().length() >= PASSWORD_MIN_LENGTH && login.getUser().length() >= USER_MIN_LENGTH) {
            return answer;
//        } else if (login.getPassword().length() < PASSWORD_MIN_LENGTH || login.getUser().length() < USER_MIN_LENGTH) {
        } else if (login.getPassword().length() < PASSWORD_MIN_LENGTH || login.getUser().length() < USER_MIN_LENGTH) {
            answer = false;
        }
        return answer;
    }


    private OnClickListener enterButtonListener = new OnClickListener() {
        public void onClick(View v) {

            login.setUser(user.getText().toString());

//            login.setPassword(password.getText().toString());
            int lengthPass = pw.length();
            char[] passw = new char[lengthPass];
            pw.getText().getChars(0,lengthPass,passw,0);
            login.setPassword(passw);

            if (!validForm(login)) {
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.warningPopUp), getString(R.string.lengthFormLoginPopUpMessage));
                dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
            } else if (!HandsetInfo.isOnline(v.getContext())) {
                popupTitle = R.string.popUpMessage;
                popupMsg = R.string.offLine;
                ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.offLine));
                confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
                    @Override
                    public void onAccept(DialogInterface dialog) {
                        Utility.launchNetworkSettings(PagoAzteca.this);
                    }
                });
                confirmationDialog.show(getSupportFragmentManager(), "" + POPUP_NETWORK);
            } else if (!FileManager.isSdPresent()) {
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.sdCardNotPresent));
                dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
            } else {
                LocationManager manager = Utility.getLocationManager(v.getContext());
                if (!manager.isProviderEnabled(LocationThread.GPS) && !manager.isProviderEnabled(LocationThread.NETWORK)) {
                    SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.gpsAndAsistedDisabled));
                    dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
                        @Override
                        public void onAccept(DialogInterface dialog) {
                            Utility.gpsSettings(this_, GPS_SETTINGS);
                        }
                    });
                    dialog.show(getSupportFragmentManager(), "" + POPUP_NO_LOCATION);
                } else {
                    Intent service = new Intent(v.getContext(), LocationService.class);
                    startService(service);
                    popupTitle = R.string.paz;
                    popupMsg = R.string.checkingLogin;
                    showDialog(POPUP_MESSAGES_PROGRESS);
                    backgroundThread = new Thread(new LoginJob(login, v.getContext(), PagoAzteca.this));
                    backgroundThread.start();

                }
            }
        }
    };

    public void video(View v) {
        goForward(VideoScreen.class, v.getContext());
        finish();
    }

    private OnClickListener helpButtonListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            goForward(HelpScreen.class, v.getContext());
        }
    };
    private OnClickListener registrationLinkListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(getString(R.string.register), getString(R.string.haveUserPassword));
            confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
                @Override
                public void onAccept(DialogInterface dialog) {
                    abreApertura();
                }
            });
            confirmationDialog.show(getSupportFragmentManager(), "" + POPUP_REGISTRATION);
        }
    };

    private OnClickListener pwRecoverListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            goForward(RecoveryOptionsScreen.class, v.getContext());
        }
    };

    protected void abreApertura() {
//        goForward(RegistrationMenu.class, this);
        goForward(AvisoPrivacidad.class, this);
    }


    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    int getFrontCameraId() {
        CameraInfo ci = new CameraInfo();
        for (int i = 0; i < android.hardware.Camera.getNumberOfCameras(); i++) {
            android.hardware.Camera.getCameraInfo(i, ci);
            if (ci.facing == CameraInfo.CAMERA_FACING_FRONT) return i;
        }
        return -1;
    }

    @Override
    public void sendMessage(Context ctx, Message message, final String parameter) {
        // TODO Auto-generated method stub
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (isFinishing())
                    return;
                removeDialog(POPUP_MESSAGES_PROGRESS);
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpError), parameter);
                dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
            }
        });
    }

    @Override
    public void sendStatus(Context ctx, final int status) {
        // TODO Auto-generated method stub
        runOnUiThread(new Runnable() {
            public void run() {
                // TODO Auto-generated method stub
                if (isFinishing())
                    return;
                if (status == Message.EXCEPTION)
                    removeDialog(POPUP_MESSAGES_PROGRESS);

                showDialog(POPUP_CONNECTION_ERROR);
            }
        });
    }

    @Override
    public void sendMessage(final Context ctx, final Message message, final Object... parameters) {
        // TODO Auto-generated method stub
        if (message.getType() == Message.OK) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (isFinishing())
                        return;
                    final RegistrationBean register = (RegistrationBean) parameters[0];
                    removeDialog(POPUP_MESSAGES_PROGRESS);
                    if (parameters.length > 1) {
                        final String status = (String) parameters[1];
                        Logger.log(Logger.MESSAGE, TAG_MAIN_SCREEN, "status: " + status);
                        if ("2".equals(status)) {//REGISTRO COMPLETO
                            register.getCustomer().setLogin(login);
                            final List<Serializable> params = new ArrayList<Serializable>();
                            params.add(register.getFiscalData());
                            params.add(register.getCustomer());
                            final List<String> paramNames = new ArrayList<String>();
                            paramNames.add(FiscalDataBean.FISCAL_DATA);
                            paramNames.add(CustomerBean.CUSTOMER);
                            goForward(SyncScreen.class, ctx, params, paramNames);
                        } else {//REGISTRO SOLO DATOS DEL CLIENTE
                            registerIncomplete = register;
                            ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.registerIncomplete));
                            confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
                                @Override
                                public void onAccept(DialogInterface dialog) {
                                    intent.putExtra("customerIncomplete", registerIncomplete.getCustomer());
                                    goForward(RegistrationMenu.class, PagoAzteca.this);
                                }
                            });
                            confirmationDialog.show(getSupportFragmentManager(), "" + POPUP_USER_DATA_REGISTERED);
                        }
                    } else {
                        register.getCustomer().setLogin(login);
                        final List<Serializable> params = new ArrayList<Serializable>();
                        params.add(register.getFiscalData());
                        params.add(register.getCustomer());
                        final List<String> paramNames = new ArrayList<String>();
                        paramNames.add(FiscalDataBean.FISCAL_DATA);
                        paramNames.add(CustomerBean.CUSTOMER);
                        goForward(SyncScreen.class, PagoAzteca.this, params, paramNames);
                    }

                }
            });
        }
    }
}

