package mx.com.bancoazteca.aceptapago.gui;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.ui.BaseActivity;

public class AvisoPrivacidad extends BaseActivity {

    private static final int ALL_DONE = 16;
    private Toolbar mtoolbar;
    private Button btn_aviso_priv;
    private SwitchCompat switch_aviso_priv;
    private SwitchCompat switch_contratos;
    private SwitchCompat switch_terminos_condiciones;
    private Button btn_terminos_condiciones;
    private Button btn_finalidades_secundarias;

    @Override
    protected void loadUiComponents() {
        // TODO Auto-generated method stub

        super.loadUiComponents();
        setContentView(R.layout.activity_aviso_privacidad);
        mtoolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        btn_aviso_priv = (Button) findViewById(R.id.btn_aviso_priv);
        switch_aviso_priv = (SwitchCompat) findViewById(R.id.switch_aviso_priv);
        btn_terminos_condiciones = (Button) findViewById(R.id.btn_terminos_condiciones);
        switch_terminos_condiciones = (SwitchCompat) findViewById(R.id.switch_terminos_condiciones);
        btn_finalidades_secundarias = (Button) findViewById(R.id.btn_finalidades_secundarias);
        switch_contratos = (SwitchCompat) findViewById(R.id.switch_contratos);

        setListeners();

    }


    private void setListeners() {
        // TODO Auto-generated method stub
        btn_aviso_priv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goForward(ShowPDFScreen.class, AvisoPrivacidad.this,"aviso_privacidad.pdf","nombreArchivo");
            }
        });

        btn_finalidades_secundarias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goForward(ShowPDFScreen.class, AvisoPrivacidad.this,"finalidades_secundarias.pdf","nombreArchivo");
            }
        });



        btn_terminos_condiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goForward(ShowPDFScreen.class, AvisoPrivacidad.this,"terminos.pdf","nombreArchivo");
            }
        });






    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_accept_contract,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        switch(id){
            case R.id.btn_aceptar:
                aceptarContratos();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void aceptarContratos() {
        if(!switch_aviso_priv.isChecked()){
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Aviso")
                    .setMessage(R.string.falta_aviso_priv)
                    .setPositiveButton(getText(R.string.button_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
//        else if(!switch_terminos_condiciones.isChecked()){
//            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Aviso")
//                    .setMessage(R.string.falta_terminos)
//                    .setPositiveButton(getText(R.string.button_ok), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//            AlertDialog alert = builder.create();
//            alert.show();
//        }
        else if(!switch_contratos.isChecked()){
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Aviso")
                    .setMessage(R.string.falta_terminos)
                    .setPositiveButton(getText(R.string.button_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }else{
            goForwardForResult(RegistrationMenu.class, this,ALL_DONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        switch (requestCode) {
            case ALL_DONE:
//                if(resultCode==RESULT_OK){
//                    setResult(RESULT_OK);
                finish();
//                }

                break;
        }
    }
}
