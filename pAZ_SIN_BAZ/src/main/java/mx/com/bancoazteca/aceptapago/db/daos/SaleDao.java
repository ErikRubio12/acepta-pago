package mx.com.bancoazteca.aceptapago.db.daos;

import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.aceptapago.beans.PendingSale;
import mx.com.bancoazteca.aceptapago.db.Query;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class SaleDao extends Dao  {
	private final static String SALES_TABLE="Ventas";
	protected static final String DAO_TICKETS = "DaoTickets";
	public SaleDao(Context context) {
		super(context);
	}

	public long insertSale(PendingSale sale) {
		Log.i("SALEDAO", "++++++++++++++++++++INSERTSALE "+sale.getCauseReverse());
		long res=-1;
		ContentValues values= new ContentValues(12);
		values.put("CANAL",sale.getChannel());
		values.put("FECHA_TX",sale.getTxDate());
		values.put("TERMINAL", sale.getTerminal());
		values.put("ID_TX", sale.getIdTx());
		values.put("PAN",sale.getPan());
		values.put("CARD_DATA", sale.getCardData());
		values.put("FECHA_VENCIMIENTO",sale.getValidity());
		values.put("MONTO", sale.getAmount());
		values.put("IMEI", sale.getImei());
		values.put("LONGITUD",sale.getLongitud());
		values.put("LATITUD",sale.getLatitud());
		values.put("MOTIVO_REVERSO", sale.getCauseReverse());
		res=db.getReadableDatabase().insert(SALES_TABLE, null, values);
		db.close();
		return res;
	}
	public List<PendingSale> querySales() {
		// TODO Auto-generated method stub
		Log.i("SALEDAO", "*************************SELECTSALE");
		ArrayList<PendingSale> pendingTickets= new ArrayList<PendingSale>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_PENDING_SALES, null);
		PendingSale currentSale=null;
		
		if(cursor.moveToFirst()){
			do {
				currentSale= new PendingSale();
				currentSale.setChannel(cursor.getString(0).trim());
				currentSale.setTxDate(cursor.getString(1).trim());
				currentSale.setTerminal(cursor.getString(2).trim());
				currentSale.setIdTx(cursor.getString(3).trim());
				currentSale.setPan(cursor.getString(4).trim());
				currentSale.setCardData(cursor.getString(5).trim());
				currentSale.setValidity(cursor.getString(6).trim());
				currentSale.setAmount(cursor.getString(7).trim());
				currentSale.setImei(cursor.getString(8).trim());
				currentSale.setLongitud(cursor.getString(9).trim());
				currentSale.setLatitud(cursor.getString(10).trim());
				currentSale.setCauseReverse(cursor.getString(11).trim());
				pendingTickets.add(currentSale);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return pendingTickets;
	}
	public int countSales() {
		Cursor c=db.getReadableDatabase().rawQuery("CREATE TABLE IF NOT EXISTS " + SALES_TABLE +
				"(CANAL VARCHAR, FECHA_TX VARCHAR, TERMINAL VARCHAR, ID_TX VARCHAR, PAN VARCHAR, CARD_DATA VARCHAR" +
				", FECHA_VENCIMIENTO VARCHAR, MONTO VARCHAR, IMEI VARCHAR, LONGITUD VARCHAR, LATITUD VARCHAR, MOTIVO_REVERSO VARCHAR)",null);

		Log.i("CREATETABLE", "--------------------------TABLE "+c.getCount());
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_PENDING_SALES, null);
		int count=cursor.getCount();				
		cursor.close();
		c.close();
		db.close();
		return count;
	}
	public long deleteSale(PendingSale sale) {{
        Log.i("SALEDAO", "--------------------------DELETESALE ");
        long res = -1;
        String values[] = new String[1];
        values[0] = sale.getIdTx();
        res = db.getReadableDatabase().delete(SALES_TABLE, "ID_TX=?", values);
        Log.i("SALEDAO", "--------------------------registros borrados " + res);
        db.close();

        return res;
    }}
}
