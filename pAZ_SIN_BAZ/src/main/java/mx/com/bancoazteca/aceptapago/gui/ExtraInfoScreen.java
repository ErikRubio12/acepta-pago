package mx.com.bancoazteca.aceptapago.gui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.aceptapago.beans.AdditionalInfo;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.ExtraInfoBean;
import mx.com.bancoazteca.aceptapago.db.daos.ExtraInfoDao;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;

public class ExtraInfoScreen extends BaseActivity {
	private final static String REQUIRED_FIELD="Campo requerido";
	protected static final int FISCAL_DATA_CODE = 3;
	protected static final int POPUP_VALID_FORM = 31;
	private final static int POPUP_SINGLE_CHOICE=10;
	private static final int SPINNER_EXTRA_INFO_CAT = 1;
	private static final int SPINNER_EXTRA_INFO = 2;
	private static final int SPINNER_BUSINESS_CAT = 3;
	private static final int SPINNER_BUSINESS = 4;
	
	private Button accept;
	private EditText extraInfoEditText;
	private EditText economicActivityEditText;
	private EditText extraInfoBusinessEditText,extraInfoBusinessEditText2;
	private ExtraInfoBean extraInfo= new ExtraInfoBean();
	private ExtraInfoDao daoExtraInfo;
	private ArrayAdapter<String> extraInfoAdapter;
	private List<Parameter<String, String>> extraInfoList;
	private ArrayAdapter<String> economicActivityAdapter;
	private List<Parameter<String,String>> economicActivityList;
	private List<Parameter<String,String>> extraInfoBusinessList;
	private List<Parameter<String,String>> extraInfoBusinessList2;
	private ArrayAdapter<String> extraInfoBusinessAdapter;
	private ArrayAdapter<String> extraInfoBusinessAdapter2;
	private AddressBean address;
	private FiscalDataBean fiscalData;
	private String EXTRA_INFO_SCREEN="ExtraInfoScreen";
	private boolean reset=false,reset2=false;
	private int spinnerType;
	private Toolbar mtoolbar;
	private String rfc;
	private RegistrationBean register;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		setContentView(R.layout.extra_info);
		extraInfoEditText =(EditText) findViewById(R.id.extraInfoSpinner);
		economicActivityEditText=(EditText) findViewById(R.id.businessActivitySpinner);
		accept=(Button) findViewById(R.id.acceptButton);
		accept.setOnClickListener(acceptListener);
		extraInfoBusinessEditText=(EditText) findViewById(R.id.extraInfoBusinessSpinner);
		extraInfoBusinessEditText2=(EditText) findViewById(R.id.extraInfoBusinessSpinner2);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
		
		ArrayList<String>adapterList= new ArrayList<String>();
		adapterList.add(getString(R.string.businessActivity));
		economicActivityAdapter= new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice,adapterList);
		economicActivityEditText.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.businessActivity;
				spinnerType=SPINNER_EXTRA_INFO;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		economicActivityList= new ArrayList<Parameter<String,String>>();
		economicActivityList.add(new Parameter<String, String>("-1", getString(R.string.businessActivity)));
		adapterList= new ArrayList<String>();
		adapterList.add(getString(R.string.extraInfoBusinessCategory));
		extraInfoBusinessAdapter2= new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice, adapterList);
		extraInfoBusinessEditText2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.extraInfoBusinessCategory;
				spinnerType=SPINNER_BUSINESS;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		extraInfoBusinessList2= new ArrayList<Parameter<String,String>>();
		extraInfoBusinessList2.add(new Parameter<String, String>("-1", getString(R.string.extraInfoBusinessCategory)));
		
		daoExtraInfo= new ExtraInfoDao(this);
		
		
		
		extraInfoList= daoExtraInfo.queryExtraInfo(this);
		extraInfoAdapter= new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
		for (Parameter<String, String> value : extraInfoList) 
			extraInfoAdapter.add(value.getValue());
		extraInfoEditText.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.extraInfo;
				spinnerType=SPINNER_EXTRA_INFO_CAT;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		
		
	
		extraInfoBusinessAdapter= new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
		extraInfoBusinessList= daoExtraInfo.queryExtraInfoCategoryBusiness(this);
		for (Parameter<String, String> value : extraInfoBusinessList) 
			extraInfoBusinessAdapter.add(value.getValue());
		extraInfoBusinessEditText.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.extraInfoBusiness;
				spinnerType=SPINNER_BUSINESS_CAT;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		
		
		
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params!= null){
			address=(AddressBean) params.getSerializable(AddressBean.ADDRESS);
			register = (RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
			rfc = params.getString(CustomerBean.RFC);
			fiscalData=(FiscalDataBean)params.getSerializable(FiscalDataBean.FISCAL_DATA);
			if(fiscalData!=null){
				extraInfo=(ExtraInfoBean)params.getSerializable(ExtraInfoBean.EXTRA_INFO);
				reset=true;
				reset2=true;
				int economicActivityCategory=extraInfo.getCategoryEconomicActivity();
				for (int i = 0; i < extraInfoList.size(); i++) {
					if(extraInfoList.get(i).getKey().equals(String.valueOf(economicActivityCategory))){
						extraInfoEditText.setText(extraInfoList.get(i).getValue());
						break;
					}
				}
				int businessTypeCategory=extraInfo.getCategoryBusinessType();
				for (int i = 0; i < extraInfoBusinessList.size(); i++) {
					if(extraInfoBusinessList.get(i).getKey().equals(String.valueOf(businessTypeCategory))){
						extraInfoBusinessEditText.setText(extraInfoBusinessList.get(i).getValue());
						break;
					}
				}
				extraInfo.setBusinessSector("F");
			}
		}
	}
	protected boolean validForm() {
		
		boolean isEmpty=false;
		if(extraInfoEditText.getText()==null || extraInfoEditText.getText().toString().length()==0 || getString(R.string.extraInfo).equalsIgnoreCase(extraInfoEditText.getText().toString())){
			extraInfoEditText.setError(REQUIRED_FIELD);
			isEmpty=true;
		}
		else
			extraInfoEditText.setError(null);
			
		if(economicActivityEditText.getText()==null || economicActivityEditText.getText().toString().length()==0 || getString(R.string.businessActivity).equalsIgnoreCase(economicActivityEditText.getText().toString())){
			economicActivityEditText.setError(REQUIRED_FIELD);
			isEmpty=true;
		}
		else
			economicActivityEditText.setError(null);
		if(extraInfoBusinessEditText.getText()==null || extraInfoBusinessEditText.getText().toString().length()==0 || getString(R.string.extraInfoBusiness).equalsIgnoreCase(extraInfoBusinessEditText.getText().toString())){
			extraInfoBusinessEditText.setError(REQUIRED_FIELD);
			isEmpty=true;
		}
		else
			extraInfoBusinessEditText.setError(null);
		
		if(extraInfoBusinessEditText2.getText()==null || extraInfoBusinessEditText2.getText().toString().length()==0 || getString(R.string.extraInfoBusinessCategory).equalsIgnoreCase(extraInfoBusinessEditText2.getText().toString())){
			extraInfoBusinessEditText2.setError(REQUIRED_FIELD);
			isEmpty=true;
		}
		else
			extraInfoBusinessEditText2.setError(null);
		if(isEmpty){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.emptyFields));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
			return false;
		}
		if(extraInfo.getBusinessType()==-1  ||  extraInfo.getEconomicActivity()==0){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.errorFields));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
			
			return false;
		}
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(data != null)
			params=data.getExtras();
		switch (requestCode) {
		case FISCAL_DATA_CODE:
			  if(resultCode==RESULT_OK){
//				  FiscalDataBean fiscalData=(FiscalDataBean)params.getSerializable(FiscalDataBean.FISCAL_DATA);
//				  ExtraInfoBean extraInfo= (ExtraInfoBean) params.getSerializable(ExtraInfoBean.EXTRA_INFO);
				  RegistrationBean registerBean =(RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
				  intent.putExtra(FiscalDataBean.FISCAL_DATA, fiscalData);
				  intent.putExtra(ExtraInfoBean.EXTRA_INFO, extraInfo);
				  intent.putExtra(RegistrationBean.REGISTER,registerBean);
				  setResult(RESULT_OK, intent);
				  finish();
			  }
			break;

		default:
			break;
		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case POPUP_SINGLE_CHOICE:
			return  showSingleChoicePopup();
		default:
			return super.onCreateDialog(id);
		}
		
	}
	//*************ACTION LISTENERS*******************
	private OnClickListener acceptListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(validForm()){
				extraInfo.setBusinessSector("F");
				intent.putExtra(ExtraInfoBean.EXTRA_INFO, extraInfo);
				intent.putExtra(AddressBean.ADDRESS, address);
				intent.putExtra(FiscalDataBean.FISCAL_DATA,fiscalData);
				intent.putExtra(CustomerBean.RFC,rfc);
				register.getCustomer().setExtraInfo(extraInfo);
				intent.putExtra(RegistrationBean.REGISTER,register);
				goForwardForResult(FiscalDataScreen.class,v.getContext(),FISCAL_DATA_CODE);
			}
		}
	};
	private DialogInterface.OnClickListener spinnerExtraInfoListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			String selected=extraInfoList.get(which).getValue();
			if(selected.length()>0  && !getString(R.string.extraInfo).equalsIgnoreCase(selected)){
				economicActivityAdapter.clear();
				economicActivityEditText.setText("");
				int selection=Integer.parseInt(extraInfoList.get(which).getKey());
				extraInfo.setCategoryEconomicActivity(selection);
				extraInfoEditText.setText(selected);
				economicActivityList= daoExtraInfo.queryEconomicActivity(selection);
				for (Parameter<String, String> value : economicActivityList) 
					economicActivityAdapter.add(value.getValue());
				if(reset){
					reset=false;
					int economicActivity=extraInfo.getEconomicActivity();
					for (int j = 0; j < economicActivityList.size(); j++) {
						if(economicActivityList.get(j).getKey().equals(String.valueOf(economicActivity))){
							economicActivityEditText.setText(economicActivityList.get(j).getValue());
							break;
						}
					}
				}
			}
			else{
				economicActivityAdapter.clear();
				ArrayList<String>adapterList= new ArrayList<String>();
				adapterList.add(getString(R.string.businessActivity));
				economicActivityAdapter= new ArrayAdapter<String>(ExtraInfoScreen.this, android.R.layout.select_dialog_singlechoice,adapterList);
				economicActivityList= new ArrayList<Parameter<String,String>>();
				economicActivityList.add(new Parameter<String, String>("-1", getString(R.string.businessActivity)));

				extraInfo.setCategoryEconomicActivity(-1);
				extraInfoEditText.setText("");
			}

			Logger.log(Logger.MESSAGE, EXTRA_INFO_SCREEN, "ID 1: "+ extraInfo.getCategoryEconomicActivity());
			removeDialog(POPUP_SINGLE_CHOICE);
		}
	};
	private DialogInterface.OnClickListener economicActivityListner= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			int i=Integer.parseInt(economicActivityList.get(which).getKey());
			Logger.log(Logger.MESSAGE, EXTRA_INFO_SCREEN, "ID 4: "+ i);
			if(i!=-1){
				extraInfo.setEconomicActivity(Integer.parseInt(economicActivityList.get(which).getKey()));
				economicActivityEditText.setText(economicActivityList.get(which).getValue()); 
			}
			else{
				economicActivityEditText.setText("");
			}
			removeDialog(POPUP_SINGLE_CHOICE);
		}
		
	};
	private DialogInterface.OnClickListener spinnerExtraInfoBusinessListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			String selected=extraInfoBusinessList.get(which).getValue();
			if(selected.length()>0 && !getString(R.string.extraInfoBusiness).equalsIgnoreCase(selected)){
				extraInfoBusinessAdapter2.clear();
				extraInfoBusinessEditText2.setText("");
				int selection=Integer.parseInt(extraInfoBusinessList.get(which).getKey());
				extraInfo.setCategoryBusinessType(selection);
				extraInfoBusinessEditText.setText(selected);
				extraInfoBusinessList2= daoExtraInfo.queryExtraInfoBusiness(selection);
				for (Parameter<String, String> value : extraInfoBusinessList2) 
					extraInfoBusinessAdapter2.add(value.getValue());		
				if(reset2){
					reset2=false;
					int businessType=extraInfo.getBusinessType();
					for (int j = 0; j < extraInfoBusinessList2.size(); j++) {
						if(extraInfoBusinessList2.get(j).getKey().equals(String.valueOf(businessType))){
							extraInfoBusinessEditText2.setText(extraInfoBusinessList2.get(j).getValue());
							break;
						}
					}
				}
			}
			else{
				extraInfoBusinessAdapter2.clear();
				extraInfoBusinessEditText.setText("");
				ArrayList<String> adapterList= new ArrayList<String>();
				adapterList.add(getString(R.string.extraInfoBusinessCategory));
				extraInfoBusinessAdapter2= new ArrayAdapter<String>(ExtraInfoScreen.this, android.R.layout.select_dialog_singlechoice, adapterList);
				extraInfoBusinessList2= new ArrayList<Parameter<String,String>>();
				extraInfoBusinessList2.add(new Parameter<String, String>("-1", getString(R.string.extraInfoBusinessCategory)));
				extraInfo.setCategoryBusinessType(-1);
			}
			Logger.log(Logger.MESSAGE, EXTRA_INFO_SCREEN, "ID 2: "+ extraInfo.getCategoryBusinessType());
			removeDialog(POPUP_SINGLE_CHOICE);
		}
		
	};
	private DialogInterface.OnClickListener spinnerExtraInfoBusinessListener2= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			int selection=Integer.parseInt(extraInfoBusinessList2.get(which).getKey());
			Logger.log(Logger.MESSAGE, EXTRA_INFO_SCREEN, "ID 3: "+ selection);
			if(selection!= -1){
				extraInfo.setBusinessType(selection);
				extraInfoBusinessEditText2.setText((extraInfoBusinessList2.get(which).getValue()));
			}
			else
				extraInfoBusinessEditText2.setText("");
				
			removeDialog(POPUP_SINGLE_CHOICE);
		}
		
	};
	
	public Dialog showSingleChoicePopup(){
		Builder builder = new AlertDialog.Builder(this);	
		builder.setTitle(popupTitle);		
		builder.setCancelable(false);	
		
		switch (spinnerType) {
		case SPINNER_EXTRA_INFO_CAT:
			if(extraInfoAdapter!=null && extraInfoAdapter.getCount()>0){
				builder.setSingleChoiceItems(extraInfoAdapter, 0, spinnerExtraInfoListener);
				return builder.create();
			}
			else
				return null;
		case SPINNER_EXTRA_INFO:
			if(economicActivityAdapter!=null && economicActivityAdapter.getCount()>0){
				builder.setSingleChoiceItems(economicActivityAdapter, 0, economicActivityListner);
				return builder.create();
			}
			else
				return null;
		case SPINNER_BUSINESS_CAT:
			if(extraInfoBusinessAdapter!=null && extraInfoBusinessAdapter.getCount()>0){
				builder.setSingleChoiceItems(extraInfoBusinessAdapter, 0, spinnerExtraInfoBusinessListener);
				return builder.create();
			}
			else
				return null;
		case SPINNER_BUSINESS:
			if(extraInfoBusinessAdapter2!=null && extraInfoBusinessAdapter2.getCount()>0){
				builder.setSingleChoiceItems(extraInfoBusinessAdapter2, 0, spinnerExtraInfoBusinessListener2);
				return builder.create();
			}
			else
				return null;
		default:
			return null;
		
		}
		
	}


}