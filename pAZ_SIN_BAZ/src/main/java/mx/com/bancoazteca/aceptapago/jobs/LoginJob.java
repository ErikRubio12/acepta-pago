package mx.com.bancoazteca.aceptapago.jobs;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.ExtraInfoBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;

public class LoginJob extends BaseService<Message, String>{
	
	private LoginBean login;
	private SoapRequest<String> req;
	private static final int ERROR_CONSULTA_TERMINAL=1002;
	private static final String LOGIN_ACTION = null;
	private static final String LOGIN_METHOD = "BusinessToBusinessLogin";
	private static final String LOGIN_PARAM = "xml";
	private static final String LOGIN_NAMESPACE = "http://service.btb.com";
	private static final String LOGIN_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String COMMERCE_TAG = "comercio";
	private static final String COMMERCE_NAME_TAG = "nombreComercio";
	private static final String COMMERCE_RFC_TAG = "rfc";
	private static final String STATUS_TAG = "statusComercio";
	private static final String NAME_TAG = "nombreCliente";
	private static final String LAST_NAME_TAG = "apellidoPaterno";
	private static final String SECOND_LAST_NAME_TAG = "apellidoMaterno";
	private static final String STREET_TAG = "calle";
	private static final String ID_ID_TAG = "codigoIdentificacion";
	private static final String ID_CITIZENSHIP_TAG = "codigoNacionalidad";
	private static final String COLONY_TAG = "colonia";
	private static final String ZIP_CODE_TAG = "cp";
	private static final String CURP_TAG = "curpCliente";
	private static final String EMAIL_TAG = "email";
	private static final String STATE_TAG = "estado";
	private static final String MARITAL_STATUS_TAG = "estadoCivil";
    //	private static final String DISCHARGE_DATE_TAG = "fechaAlta";
	private static final String BORN_DATE_TAG = "fechaNacimiento";
	private static final String FOLIO_TAG = "folioPreapertura";
	private static final String CUSTOMER_ID_TAG = "idUsuario";
	private static final String LADA_TAG = "lada";
	private static final String CITY_TAG = "municipio";
	private static final String EXTERNAL_NUMBER_TAG = "numExterior";
	private static final String INTERNAL_NUMBER_TAG = "numInterior";
	private static final String ID_TAG = "numeroIdentificacion";
	private static final String RFC_TAG = "rfcCliente";
	private static final String GENDER_TAG = "sexo";
	private static final String TELEPHONE_TAG = "telefono";
    //	private static final String LATEST_MODIFICATION_TAG = "ultimaModificacion";
    //	private static final String MODIFIED_USER_TAG = "usuarioModifico";
	private static final String QUERY_DEVICE_METHOD ="BusinessToBusinessServiceConsultarTerminalIphone";
	private static final String QUERY_DEVICE_ACTION = null;
	private static final String QUERY_DEVICE_NAMESPACE = "http://service.btb.com";
	private static final String QUERY_DEVICE_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String QUERY_DEVICE_PARAM = "xml";
	private static final String DEVICE_TAG = "idterminal";
	private static final String AFFILIATE_TAG = "afiliacion";
	private static final String COMMERCE_STREET_TAG = "direccion";
	private static final String LOGIN = "LoginJob";
    //	private static final String INVALID_USER = "-5";
	private RegistrationBean register= new RegistrationBean();
	private String status;
    //	private String dischargeDate;
    //	private String latestModification;
	public LoginJob(LoginBean login,Context context,MessageListener<Message,String> listener) {
		// TODO Auto-generated constructor stub
		this.login=login;
		this.listener=listener;
		this.ctx=context;
		operationCode=null;
		systemError="";
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		
		register.setCustomer(new CustomerBean());
		register.getCustomer().setAddress(new AddressBean());
		register.setFiscalData(new FiscalDataBean());
		register.getFiscalData().setAddress(new AddressBean());
		register.getCustomer().setExtraInfo(new ExtraInfoBean());
		int status=login();
		if (status==0) {//new login
			Message msg= new Message();
			if("2".equalsIgnoreCase(this.status)){//solo en caso de que el registro este completo
				if(checkDevice()==0){
					msg.setType(Message.OK);
					listener.sendMessage(ctx,msg, register,this.status);
				}else{
					msg.setType(Message.ERROR);
					listener.sendMessage(ctx, msg, ctx.getResources().getString(R.string.connectionFails));
				}
			}else if ("1".equalsIgnoreCase(this.status)){
				msg.setType(Message.OK);
				listener.sendMessage(ctx,msg, register,this.status);
			}
		}
		else if (status==1) {// hubo error en login (no necesariamente que el usuario no existe)
			//codigo de operacion
			// 0 si paso
			//-5 usuario no existe
			//-1 usuario bloqueado
			//326 contrase�a incorrecta
			//327 no formalizado
			if("-1".equalsIgnoreCase(operationCode)){
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg, ctx.getResources().getString(R.string.userLocked));
			}else if("-5".equalsIgnoreCase(operationCode)){
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg, ctx.getResources().getString(R.string.invalidUserPasword));
			}else if("327".equalsIgnoreCase(operationCode)){
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg, ctx.getResources().getString(R.string.userIncompleteLogin));
			}else{
				Message msg= new Message();
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg, systemError);
			}
		}
		else{//Exception
			listener.sendStatus(ctx,Message.EXCEPTION);
			return;
		}
		
		
	}
	
	
	private int login() {
		// TODO Auto-generated method stub
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>\r\n");
		request.append("<eservices>\r\n");
		request.append("<request>\r\n");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>\r\n");
		request.append("<canalEntrada>");
		request.append(Encrypt.encryptStringWS("login.Login"));
		request.append("</canalEntrada>\r\n");
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>");
		request.append("<login>\r\n");
		request.append("<usuario>");
		request.append(Encrypt.encryptStringWS(login.getUser()));
		request.append("</usuario>\r\n");
		request.append("<contrasena>");
		request.append(Encrypt.encryptStringWS(login.getPassword()));
//		request.append(Encrypt.encryptStringWS(Arrays.toString(login.getPassword())));
//		request.append(login.getPassword());
		request.append("</contrasena>\r\n");
		request.append("</login>\r\n");
		request.append("</request>\r\n");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		req= new SoapRequest<String>(LOGIN_METHOD, LOGIN_ACTION);
		req.setNameSpace(LOGIN_NAMESPACE);
		req.setUrl(LOGIN_URL);
		Parameter<String,String> param= new Parameter<String, String>(LOGIN_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if(res==null){
				return -1;
			}
            
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			parseLoginResponse(xml);
			//Logger.log(Logger.MESSAGE,LOGIN,"Termino parseo de login status: "+ this.status + " codigo operacion: "+ operationCode);
			// 0 si paso
			//-5 usuario no existe
			//-1 usuario bloqueado
			if ("0".equals(operationCode)) {
				return 0;
			}
			else{
				return 1;
			}
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION,LOGIN,"InterruptedIOException"+e.getMessage());
			return -1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, LOGIN,"IOException"+e.getMessage());
			return -1;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, LOGIN,"XmlPullParserException"+e.getMessage());
			return -1;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, LOGIN,"TimeoutException"+e.getMessage());
			return -1;
		} 
	}
	private void parseLoginResponse(String xml) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		xml=Utility.fixSoapResponse(xml);
		
		
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(xml));
		int eventType=parser.getEventType();
		String currentTag=null;
		
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
                case XmlPullParser.START_DOCUMENT:break;
                case XmlPullParser.END_DOCUMENT:break;
                case XmlPullParser.START_TAG:
                    currentTag=parser.getName();
                    break;
                case XmlPullParser.TEXT:
                    
                    if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
                        operationCode= Encrypt.decryptStringWS(parser.getText());
                    }
                    else if (currentTag.equalsIgnoreCase(SYSTEM_ERROR_TAG)){
                        systemError=Encrypt.decryptStringWS(parser.getText());
                    }
                    else if (currentTag.equalsIgnoreCase(COMMERCE_NAME_TAG)){
                        register.getFiscalData().setFiscalName(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(COMMERCE_TAG)){
                        register.getCustomer().setFolioPreap(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(COMMERCE_RFC_TAG)){
                        register.getFiscalData().setRfc(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(STATUS_TAG)){
                        status=Encrypt.decryptStringWS(parser.getText());
                    }
                    else if (currentTag.equalsIgnoreCase(SECOND_LAST_NAME_TAG)){
                        register.getCustomer().setLastName2(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(LAST_NAME_TAG)){
                        register.getCustomer().setLastName(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(STREET_TAG)){
                        register.getCustomer().getAddress().setStreet(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(INTERNAL_NUMBER_TAG)){
                        register.getCustomer().getAddress().setInternalNumber(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(ID_ID_TAG)){
                        register.getCustomer().setCodIdent(Integer.parseInt(Encrypt.decryptStringWS(parser.getText())));
                    }
                    else if (currentTag.equalsIgnoreCase(ID_CITIZENSHIP_TAG)){
                        register.getCustomer().setCitizenship(Integer.parseInt(Encrypt.decryptStringWS(parser.getText())));
                    }
                    else if (currentTag.equalsIgnoreCase(COLONY_TAG)){
                        register.getCustomer().getAddress().setColony(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(ZIP_CODE_TAG)){
                        register.getCustomer().getAddress().setZipCode(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(CURP_TAG)){
                        register.getCustomer().setCurp(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(EMAIL_TAG)){
                        register.getCustomer().setMail(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(STATE_TAG)){
                        register.getCustomer().getAddress().setState(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(MARITAL_STATUS_TAG)){
                        register.getCustomer().setMaritalStatus(Integer.parseInt(Encrypt.decryptStringWS(parser.getText())));
                    }
                    //				else if (currentTag.equalsIgnoreCase(DISCHARGE_DATE_TAG)){
                    //					dischargeDate=Encrypt.decryptStringWS(parser.getText());
                    //				}
                    else if (currentTag.equalsIgnoreCase(BORN_DATE_TAG)){
                        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
                        Date date=null;
                        try {
                            date=formatter.parse(Encrypt.decryptStringWS(parser.getText()));
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                        	Logger.log(Logger.MESSAGE,LOGIN,"Error al parsear la fecha");
                        }
                        register.getCustomer().setBornDate(date);
                    }
                    else if (currentTag.equalsIgnoreCase(FOLIO_TAG)){
                        register.getCustomer().setFolioPreap(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(CUSTOMER_ID_TAG)){
                        register.getCustomer().setCustomerId(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(LADA_TAG)){
                        register.getCustomer().getAddress().setLada(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(CITY_TAG)){
                        register.getCustomer().getAddress().setPoblacion(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(NAME_TAG)){
                        register.getCustomer().setName(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(EXTERNAL_NUMBER_TAG)){
                        register.getCustomer().getAddress().setExternalNumber(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(ID_TAG)){
                        register.getCustomer().setId(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equalsIgnoreCase(GENDER_TAG)){
                        register.getCustomer().setGender(Encrypt.decryptStringWS(parser.getText()).equalsIgnoreCase("F") ? 1 : 0);
                    }
                    else if (currentTag.equalsIgnoreCase(TELEPHONE_TAG)){
                        register.getCustomer().getAddress().setTelephone(Encrypt.decryptStringWS(parser.getText()));
                    }
                    //				else if (currentTag.equalsIgnoreCase(LATEST_MODIFICATION_TAG)){
                    //					latestModification=Encrypt.decryptStringWS(parser.getText());
                    //				}
                    else if (currentTag.equalsIgnoreCase(RFC_TAG)){
                        register.getCustomer().setRfc(Encrypt.decryptStringWS(parser.getText()));
                    }
                    
                    break;
                case XmlPullParser.END_TAG:
                    currentTag=null;
                    break;
                default:
                    break;
			}
			eventType=parser.next();
		}
	}
	private int checkDevice() {
		// TODO Auto-generated method stub
		int operationCodeCheckDevice=ERROR_CONSULTA_TERMINAL;
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("CTermIphone"));
		request.append("</idservicio>");
		request.append("<idComercio>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getFolioPreap()));
		request.append("</idComercio>");
		request.append("<rfc>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getRfc()));
		request.append("</rfc>");
		request.append("<imei>");
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI()));
		request.append("</imei>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		SoapRequest<String> req= new SoapRequest<String>(QUERY_DEVICE_METHOD, QUERY_DEVICE_ACTION);
		req.setNameSpace(QUERY_DEVICE_NAMESPACE);
		req.setUrl(QUERY_DEVICE_URL);
		Parameter<String,String> param= new Parameter<String, String>(QUERY_DEVICE_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;
		
        
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if (res==null) {
				return ERROR_CONSULTA_TERMINAL;
			}
			String xml=Utility.fixSoapResponse(res.getResponse().toString());
			Connections.closeSoapConnection(res);
			Logger.log(Logger.MESSAGE,LOGIN,"Empieza parseo consulta terminal: "+ xml);
			operationCodeCheckDevice=parseCheckDeviceResponse(xml);
			Log.i("operation chech device", ""+operationCodeCheckDevice);
		}catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION,LOGIN,"Error checking device InterruptedIOException: "+ e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION,LOGIN,"Error checking device IOException: "+ e.getMessage());
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION,LOGIN,"Error checking device XmlPullParserException: "+ e.getMessage());
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION,LOGIN,"Error checking device TimeoutException: "+ e.getMessage());
		} 
		 return operationCodeCheckDevice;
	}
	private int parseCheckDeviceResponse(String xml) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(xml));
		int eventType=parser.getEventType();
		String currentTag=null;
		int operationCodeCheckDevice=ERROR_CONSULTA_TERMINAL;
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
                case XmlPullParser.START_DOCUMENT:break;
                case XmlPullParser.END_DOCUMENT:break;
                case XmlPullParser.START_TAG:
                    currentTag=parser.getName();
                    break;
                case XmlPullParser.TEXT:
                	if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
                        operationCodeCheckDevice=Integer.parseInt(Encrypt.decryptStringWS(parser.getText()));
                    }else if (currentTag.equals(AFFILIATE_TAG)) {
                        register.getCustomer().setAffiliate(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equals(COLONY_TAG)) {
                        register.getFiscalData().getAddress().setColony(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equals(ZIP_CODE_TAG)) {
                        register.getFiscalData().getAddress().setZipCode(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equals(COMMERCE_STREET_TAG)) {
                        register.getFiscalData().getAddress().setStreet(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equals(STATE_TAG)) {
                        register.getFiscalData().getAddress().setState(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equals(DEVICE_TAG)) {
                        register.getCustomer().setDevice(Encrypt.decryptStringWS(parser.getText()));
                    }
                    else if (currentTag.equals(CITY_TAG)) {
                    	register.getFiscalData().getAddress().setPoblacion(Encrypt.decryptStringWS(parser.getText()));
                    }
                    
                    break;
                case XmlPullParser.END_TAG:
                    currentTag=null;
                    break;
                default:
                    break;
			}
			eventType=parser.next();
		}
		return operationCodeCheckDevice;
	}
}
