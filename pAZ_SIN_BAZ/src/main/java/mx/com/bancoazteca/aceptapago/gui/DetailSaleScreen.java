package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import mx.com.bancoazteca.aceptapago.gps.SingleShotLocationProvider;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.messages.EmailBean;
import mx.com.bancoazteca.messages.Mail;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.ClientBean;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.db.daos.CardDao;
import mx.com.bancoazteca.aceptapago.jobs.ClientsJob;
import mx.com.bancoazteca.aceptapago.jobs.UploadImageTicketJob;
import mx.com.bancoazteca.social.BaseSocialMapActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.ConfirmationDialog;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.ui.customviews.CustomImageView;
import mx.com.bancoazteca.ui.customviews.LineSignView;
import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class DetailSaleScreen extends BaseSocialMapActivity implements MessageListener<Message, String>, OnMapReadyCallback {

    private static final int ZOOM_LEVEL = 19;
    protected static final String NAME_TICKET = "ticket";
    private static final int EMAIL_CODE = 120;
    private static final String DETAIL_SCREEN = "DetailSaleScreen";
    private static final long TIME_TO_LOAD = 1000 * 7;
    private static final int POPUP_IMAGE_NOT_LOADED = 11;
    private static final int POPUP_SIGN_NOT_LOADED = 12;
    private static final int POPUP_TICKET_NOT_LOADED = 13;
    private static final int POPUP_EMPTY_FIELDS = 16;
    private static final int POPUP_MAP = 15;
    private static final int POPUP_PROCESSING = 0;
    private final static int POPUP_ERROR = 17;
    private final static int POPUP_CONFIRM_EMAIL = 20;
    private static final int POPUP_UPDATING_TWITTER = 19;
    public static final String FILE_PATH = "ticket";
    private SaleBean sale;
    private LineSignView line;
    private LinearLayout container;
    private ImageView productImage;
    private TextView description;
    private TextView amount;
    private TextView folio;
    private TextView validityCard;
    private TextView cardNumber;
    private CustomImageView firmImage;
    private TextView typeCard;
    private TextView ticketDate;
    private TextView ticketRegisterNumber;
    private TextView ticketBusinessText;
    private TextView reference;
    private TextView /*ticketBusinessAddressText,*/ticketCardText, ticketBankText, folioTicket;
    private TextView lugar;
    private TextView ticketFolioPreap;
    private LinearLayout saleDetailLayout, signLayout;
    private CustomerBean customer;
    private FiscalDataBean fiscalData;
    private CardDao dao;
    private boolean isTicketSaved = false;
    private static File ticketFile = null;
    private static boolean isTicketOk = true;
    private Thread backgroundThread;
    private ClientBean clientSelected;
    private EditText businessClientName;
    private EditText businessClientEmail;
    private EmailBean email = new EmailBean();
    private TextView businessName;
    private TextView businessFolio;
    private TextView businessDate;
    private TextView businessTime;
    private TextView businessDescription;
    private TextView businessPrice;
    private TextView businessCard;
    private CustomImageView businessFirm;
    private PopupWindow ticketScreen, fakePopup;
    private PopupWindow twitterScreen;
    private int btnPressed;
    private EditText twitterEdit;
    private LinearLayout aidContent;
    private LinearLayout arqcContent;
    private TextView aid;
    private TextView arqc;
    private Location location;
    private TextView txtTipoVenta;
    private TextView txtSignTitle;
    private Toolbar mtoolbar;

    private ImageView mapImage;

    private LatLng KYIV;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    private MapView mMapView;
    private int mMapWidth = 600;
    private int mMapHeight = 400;
    private GoogleMapOptions options;
    private Bundle mapViewBundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //EGR (new Map Snapshot)
        mapImage = (ImageView) findViewById(R.id.mapImage);
        mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        SingleShotLocationProvider.requestSingleUpdate(this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override
                    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        Log.d("Location", "my location is " + location.latitude + location.longitude);
                        KYIV = new LatLng(location.latitude,location.longitude);

                        options = new GoogleMapOptions()
                                .compassEnabled(false)
                                .mapToolbarEnabled(false)
                                .camera(CameraPosition.fromLatLngZoom(KYIV,15))
                                .liteMode(true);
                        mMapView = new MapView(DetailSaleScreen.this, options);
                        mMapView.onCreate(mapViewBundle);

                        mMapView.getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                googleMap.addMarker(new MarkerOptions()
                                        .position(KYIV)
                                        .title("Kyiv"));
                                mMapView.measure(View.MeasureSpec.makeMeasureSpec(mMapWidth, View.MeasureSpec.EXACTLY),
                                        View.MeasureSpec.makeMeasureSpec(mMapHeight, View.MeasureSpec.EXACTLY));
                                mMapView.layout(0, 0, mMapWidth, mMapHeight);

                                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                    @Override
                                    public void onMapLoaded() {
                                        mMapView.setDrawingCacheEnabled(true);
                                        mMapView.measure(View.MeasureSpec.makeMeasureSpec(mMapWidth, View.MeasureSpec.EXACTLY),
                                                View.MeasureSpec.makeMeasureSpec(mMapHeight, View.MeasureSpec.EXACTLY));
                                        mMapView.layout(0, 0, mMapWidth, mMapHeight);
                                        mMapView.buildDrawingCache(true);
                                        Bitmap b = Bitmap.createBitmap(mMapView.getDrawingCache());
                                        mMapView.setDrawingCacheEnabled(false);
                                        mapImage.setImageBitmap(b);

                                    }


                                });

                            }
                        });
                    }
                });
    }

    protected void loadUiComponents() {
        // TODO Auto-generated method stub
        super.loadUiComponents();
        setContentView(R.layout.detail_sale);
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        loadNewTicketComponents();
        RelativeLayout layout = (RelativeLayout) Utility.inflateLayout(this, R.layout.back_popup, null);
        fakePopup = new PopupWindow(layout, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        fakePopup.setContentView(layout);
        txtSignTitle = (TextView) findViewById(R.id.signTitle);

        container = (LinearLayout) findViewById(R.id.detailLayout);
        LayoutParams params = container.getLayoutParams();
        params.height = getHeight();
        params.width = getWidth();
        container.setLayoutParams(params);
        Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "tamaño contenedor: " + container.getWidth() + "  " + container.getHeight());

        productImage = (ImageView) container.findViewById(R.id.productImage);
        firmImage = (CustomImageView) container.findViewById(R.id.firmImage);
        saleDetailLayout = (LinearLayout) container.findViewById(R.id.saleDetailLayout);
        line = (LineSignView) container.findViewById(R.id.lineFirmView);
        signLayout = (LinearLayout) container.findViewById(R.id.signLayout);
        description = (TextView) container.findViewById(R.id.descriptionTicketTextView);
        amount = (TextView) container.findViewById(R.id.amountTicketTextView);
        folio = (TextView) container.findViewById(R.id.ticketFolio);
        reference = (TextView) container.findViewById(R.id.referenceTextView);
        folioTicket = (TextView) container.findViewById(R.id.folioTicketTextView);
        cardNumber = (TextView) container.findViewById(R.id.cardNumberTextView);
        ticketDate = (TextView) container.findViewById(R.id.ticketDate);
        ticketRegisterNumber = (TextView) container.findViewById(R.id.ticketRegisterNumberId);
        lugar = (TextView) container.findViewById(R.id.location);
        ticketBusinessText = (TextView) container.findViewById(R.id.ticketBusinessText);
        ticketBankText = (TextView) container.findViewById(R.id.ticketBankTextId);
        ticketCardText = (TextView) container.findViewById(R.id.ticketCardTextId);
        validityCard = (TextView) container.findViewById(R.id.validityCard);
        typeCard = (TextView) container.findViewById(R.id.typeCard);
        ticketFolioPreap = (TextView) container.findViewById(R.id.ticketFolioPreap);
        txtTipoVenta = (TextView) container.findViewById(R.id.txt_tipo_venta);
        aidContent = (LinearLayout) container.findViewById(R.id.aidContent);
        arqcContent = (LinearLayout) container.findViewById(R.id.arqcContent);

        aid = (TextView) container.findViewById(R.id.aid);
        arqc = (TextView) container.findViewById(R.id.arqc);




        params = productImage.getLayoutParams();
        params.height = (int) (getHeight() * 0.23);
        params.width = getWidth() / 2;
        productImage.setLayoutParams(params);


        params = saleDetailLayout.getLayoutParams();
        params.width = getWidth() / 2;
        params.height = (int) (getHeight() * 0.30);
        saleDetailLayout.setLayoutParams(params);

        params = signLayout.getLayoutParams();
        params.width = getWidth() / 2;
        params.height = (int) (getHeight() * 0.30);
        signLayout.setLayoutParams(params);



        Bitmap b = null;
        File f;
        f = FileManager.openFile(DetailSaleScreen.this, SaleBean.getImage(), FileManager.EXTERNAL, FileManager.MODE_APPEND ^ FileManager.MODE_RW);
        if (f != null && f.exists()) {

            b = Utility.decodeImage(DetailSaleScreen.this, SaleBean.getImage(), FileManager.EXTERNAL, (int) getWidth() / 2, (int) (getHeight() * 0.25));
            if (b != null) {
                try {
                    Matrix m = new Matrix();
                    RectF drawableRect = new RectF(0, 0, b.getWidth(), b.getHeight());
                    RectF viewRect = new RectF(0, 0, getWidth() / 2, (int) (getHeight() * 0.25));
                    m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
                    b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                    m = new Matrix();
                    m.setRotate(90f);
                    b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                    productImage.setImageBitmap(b);
                    productImage.setBackgroundColor(Color.BLACK);
                    Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "w3: " + productImage.getWidth() + " h3:" + productImage.getHeight());

                    b = null;
                    System.gc();
                } catch (Exception e) {
                    SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.imageNotEnoughMemory));
                    dialog.show(getSupportFragmentManager(), "" + POPUP_IMAGE_NOT_LOADED);
                    Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "no se pudo cargar la imagen del producto");
                }
            } else {
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.imageNotEnoughMemory));
                dialog.show(getSupportFragmentManager(), "" + POPUP_IMAGE_NOT_LOADED);
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "no se pudo cargar la imagen del producto");
            }
        }

        f = FileManager.openFile(DetailSaleScreen.this, "sign.png", FileManager.EXTERNAL, FileManager.MODE_APPEND);
        if (f != null && f.exists()) {
            try {
                b = BitmapFactory.decodeFile(f.getAbsolutePath());
                if (b != null) {
                    Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "ancho: " + b.getWidth() + " alto: " + b.getHeight());
                    b = Bitmap.createScaledBitmap(b, (int) (b.getWidth() * 0.3), (int) (b.getHeight() * 0.3), false);
                    System.gc();
                    Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "ancho: " + b.getWidth() + " alto: " + b.getHeight());
                    firmImage.setImageBitmap(b);
                    b = Bitmap.createScaledBitmap(b, (int) (b.getWidth() * 0.7), (int) (b.getHeight() * 0.7), false);
                    Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "ancho: " + b.getWidth() + " alto: " + b.getHeight());
                    businessFirm.setImageBitmap(b);
                    System.gc();
                } else {
                    //if (popUp!= null)
                    //	removeDialog(POPUP_IMAGE_NOT_LOADED);
                    SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.signNotEnoughMemory));
                    dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
                    Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "no se pudo cargar la firma del ticket");
                }
            } catch (OutOfMemoryError e) {
                // TODO: handle exception
                //if (popUp!= null)
                //	removeDialog(POPUP_IMAGE_NOT_LOADED);
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.signNotEnoughMemory));
                dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "OutOfMemoryError no se pudo cargar la firma del ticket: " + e);
            } catch (Exception e) {
                // TODO: handle exception
                //if (popUp!= null)
                //	removeDialog(POPUP_IMAGE_NOT_LOADED);
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.signNotEnoughMemory));
                dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "Exception no se pudo cargar la firma del ticket: " + e);
            }

        } else {
            //if (popUp!= null)
            //	removeDialog(POPUP_IMAGE_NOT_LOADED);
            SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.signNotEnoughMemory));
            dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
            Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "Exception no se pudo cargar la firma del ticket");
        }
        b = null;
        System.gc();




    }

    @Override
    protected void processData() {
        // TODO Auto-generated method stub
        Bundle params = getIntent().getExtras();

        if (params != null) {
            dao = new CardDao(this);
            sale = (SaleBean) params.getSerializable(SaleBean.SALE);
            fiscalData = (FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
            customer = (CustomerBean) params.getSerializable(CustomerBean.CUSTOMER);




            //GoogleMap mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();


            //mapFragment.getMapAsync(this);
            if (location != null) {
                if (location.getLatitude() != 0) {

                } else {
                    //mapFragment.setUserVisibleHint(false);
                }
            }
            if (sale.getCard().getChipData() != null) {
                Logger.log(Message.MESSAGE, DETAIL_SCREEN, "fue chip: " + sale.getAid() + "  -  " + sale.getArqc());
                aidContent.setVisibility(View.VISIBLE);
                arqcContent.setVisibility(View.GONE);
                aid.setText(sale.getAid());
                arqc.setText(sale.getArqc());
            }

            String decimalesStr = null;
            String enterosStr = null;
            Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "***************** MONTO REGRESADO VENTA = " + sale.getAmountReturned());
            int index = sale.getAmountReturned().indexOf(".");
            if (index >= 0) {
                decimalesStr = sale.getAmountReturned().substring(index + 1);
                enterosStr = sale.getAmountReturned().substring(0, index);
            } else {
                decimalesStr = sale.getAmountReturned().substring(sale.getAmountReturned().length() - 2);
                enterosStr = sale.getAmountReturned().substring(0, sale.getAmountReturned().length() - 2);
            }
            int entero = Integer.parseInt(enterosStr);
            String montoAut = entero + "." + decimalesStr;


            amount.setText(montoAut);
            description.setText(sale.getDescription());
            cardNumber.setText("*** *** *** " + sale.getCard().getMaskedPan().substring(sale.getCard().getMaskedPan().length() - 4));
            validityCard.setText(validityCard.getText() + sale.getCard().getStringValidity().replace("/", " - "));

            if (sale.getCard().getMaskedPan().startsWith("4"))
                typeCard.setText(typeCard.getText() + getResources().getString(R.string.visa));
            else if (sale.getCard().getMaskedPan().startsWith("5"))
                typeCard.setText(typeCard.getText() + getResources().getString(R.string.masterCard));

            if (sale.getCard().getCardOwner() != null)
                line.setText(sale.getCard().getCardOwner());
            ticketBusinessText.setText(fiscalData.getFiscalName());
            folio.setText(folio.getText() + sale.getFolio());
            folioTicket.setText(sale.getAuthorizationNumber());
            reference.setText(sale.getReferenceNumber());
            ticketRegisterNumber.setText(ticketRegisterNumber.getText().toString() + " " + sale.getAfiliacion());
            ticketFolioPreap.setText(ticketFolioPreap.getText() + " " + customer.getFolioPreap());
            lugar.setText(sale.getEstado() + " " + sale.getPoblacion());
            try {
                Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Mascara: " + sale.getCard().getMaskedPan());
                ticketCardText.setText(ticketCardText.getText() + dao.queryCardType(sale.getCard().getMaskedPan().substring(0, 6)));
                ticketBankText.setText(ticketBankText.getText() + dao.queryBankCard(Long.parseLong(sale.getCard().getMaskedPan().substring(0, 6))));
            } catch (NumberFormatException e) {
                // TODO: handle exception
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "NumberFormatException no se pudo parsear la mascara: " + e);
                ticketCardText.setText("Internacional");
                ticketBankText.setText("Internacional");
            } catch (Exception e) {
                // TODO: handle exception
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "Exception no se pudo parsear la mascara: " + e);
                ticketCardText.setText("Internacional");
                ticketBankText.setText("Internacional");
            }

            StringBuilder address = new StringBuilder(fiscalData.getAddress().getStreet());
            address.append(" ");
            address.append(fiscalData.getAddress().getExternalNumber());
            address.append(" ");
            address.append(fiscalData.getAddress().getColony());

            firmImage.setAuthorizationNumber(sale.getAuthorizationNumber());
            if (sale.getSaleDate() == null)
                sale.setSaleDate(new Date());
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            SimpleDateFormat hourFormatter = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            String tmpDate = dateFormatter.format(sale.getSaleDate()) + " Hora: " + hourFormatter.format(sale.getSaleDate());
            ticketDate.setText(ticketDate.getText().toString() + tmpDate);
            txtTipoVenta.setText(getString(R.string.ticketOperationType) + " " + getString(R.string.operationTypeSale));
            txtSignTitle.setText(getString(R.string.operationTypeSale));

            tmpDate = Utility.formatDate(sale.getSaleDate(), "yyyy/MM/dd");

            businessName.setText(fiscalData.getFiscalName());
            businessFolio.setText(businessFolio.getText() + ": " + sale.getFolio());
            businessDate.setText(businessDate.getText() + ": " + tmpDate);
            businessTime.setText(businessTime.getText() + ": " + Utility.formatDate(sale.getSaleDate(), "HH:mm:ss"));
            businessDescription.setText(businessDescription.getText() + ": " + description.getText());
            businessPrice.setText("$" + montoAut);
            businessCard.setText(cardNumber.getText());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_sale, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_menu_finish_sale:
                showDialog(POPUP_PROCESSING);
                new Wait().execute(Integer.valueOf(item.getItemId()));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void startBackgroundProcess() {
        // TODO Auto-generated method stub
        showDialog(POPUP_MAP);

        new CountDownTimer(TIME_TO_LOAD, TIME_TO_LOAD) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (isFinishing())
                    return;
                if (popUp != null)
                    this_.removeDialog(POPUP_MAP);
                sendTicket();
            }
        }.start();
    }

    public void home(View v) {
        showDialog(POPUP_PROCESSING);
        new Wait().execute(Integer.valueOf(v.getId()));
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        outState.putInt("btnPressed", btnPressed);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState);
        btnPressed = savedInstanceState.getInt("btnPressed");
    }

    private void sendTicket() {
        Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Inicia envio de correo");
        if (!isTicketSaved) {
            try {
                ticketScreen = new PopupWindow(container, (int) (getWidth() * 0.9), (int) (getHeight() * 0.9));
                ticketScreen.setContentView(container);
                container.invalidate();
                Logger.log("tamaños 3: " + getWidth() + "  " + getHeight());
                Bitmap bitmapTicket = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmapTicket);
                canvas.drawColor(Color.WHITE);
                ticketScreen.getContentView().draw(canvas);
                StringBuffer dirTicket = new StringBuffer(Utility.formatDate(new Date(), "dd-MM-yy"));
                FileManager.createDirectory(dirTicket.toString(), FileManager.EXTERNAL);
                dirTicket.append(FileManager.separator);
                dirTicket.append(NAME_TICKET);
                dirTicket.append(Utility.formatDate(new Date(), "-yyMMddHHmmss"));
                ticketFile = FileManager.saveBitmap(bitmapTicket, this, dirTicket.toString(), 60, FileManager.JPEG, FileManager.EXTERNAL);
                if (ticketFile == null) {
                    SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.dontSendByMemoryOrMounted));
                    dialog.show(getSupportFragmentManager(), "" + POPUP_TICKET_NOT_LOADED);
                    return;
                }

                Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "El ticket se ha almacenado: " + ticketFile.getAbsolutePath());
                bitmapTicket = null;
                canvas = null;
                System.gc();
            } catch (OutOfMemoryError e) {
                // TODO: handle exception
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "OutOfMemoryError No se pudo enviar el ticket" + e);
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.signNotEnoughMemory));
                dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
                    @Override
                    public void onAccept(DialogInterface dialog) {
                        setResult(RESULT_OK);
                        finish();
                    }
                });
                dialog.show(getSupportFragmentManager(), "" + POPUP_TICKET_NOT_LOADED);
                return;
            } catch (Exception e) {
                // TODO: handle exception
                Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "Exception enviando No se pudo enviar el ticket" + e);
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.signNotEnoughMemory));
                dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
                    @Override
                    public void onAccept(DialogInterface dialog) {
                        setResult(RESULT_OK);
                        finish();
                    }
                });
                dialog.show(getSupportFragmentManager(), "" + POPUP_TICKET_NOT_LOADED);
                return;
            }

        } else {
            ticketFile = FileManager.openFile(this, NAME_TICKET, FileManager.EXTERNAL, FileManager.MODE_RW);
            Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "El ticket ya estaba almacenado: " + (ticketFile != null ? ticketFile.getAbsolutePath() : "no hay ticket"));
        }
        isTicketSaved = true;
        backgroundThread = new Thread(new UploadImageTicketJob(this, this, sale.getFolio(), sale.getAuthorizationNumber(), customer.getLogin().getUser(), fiscalData.getRfc(), sale.getReferenceNumber(), sale.getAfiliacion(), ticketFile, sale.getCard().getMaskedPan()));
        backgroundThread.start();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

    }


    public void showTicket(View v) {

        goForward(ImageTicketScreen.class, ticketFile, FILE_PATH);
    }


    private void loadNewTicketComponents() {
        // TODO Auto-generated method stub
        businessClientEmail = (EditText) findViewById(R.id.businessClientEmail);
        businessClientName = (EditText) findViewById(R.id.businessClientName);
        businessName = (TextView) findViewById(R.id.businessName);
        businessFolio = (TextView) findViewById(R.id.businessFolio);
        businessDate = (TextView) findViewById(R.id.businessDate);
        businessTime = (TextView) findViewById(R.id.businessTime);
        businessDescription = (TextView) findViewById(R.id.businessDescription);
        businessPrice = (TextView) findViewById(R.id.businessPrice);
        businessCard = (TextView) findViewById(R.id.businessCard);
        businessFirm = (CustomImageView) findViewById(R.id.businessFirm);
        businessFirm.setWatermark(false);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        DialogButton a;
        DialogButton b;
        switch (id) {
            case POPUP_UPDATING_TWITTER:

                popUp = new PopUp(this, popupTitle, popupMsg);
                popUp.setType(PopUp.PROGRESS_INFINIT);
                popUp.create();
                popUp.setCancelable(false);
                return popUp.getDialog();
            case POPUP_MAP:
                popUp = new PopUp(this, R.string.popUpMessage, R.string.downloadingMap);
                popUp.setType(PopUp.PROGRESS_INFINIT);
                popUp.setCancelable(false);
                popUp.create();
                return popUp.getDialog();

            case POPUP_PROCESSING:
                popUp = new PopUp(this, R.string.popUpMessage, R.string.processingTicket);
                popUp.setType(PopUp.PROGRESS_INFINIT);
                popUp.setCancelable(false);
                popUp.create();
                return popUp.getDialog();
            default:
                return super.onCreateDialog(id);
        }

    }

    private boolean sendMail(boolean isPDF) {
        try {
            File pdfFile = null;
            if ("".equalsIgnoreCase(businessClientEmail.getText().toString().trim()) || isPDF || "".equalsIgnoreCase(businessClientName.getText().toString())) {
                SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.emptyFields));
                dialog.show(getSupportFragmentManager(), "" + POPUP_EMPTY_FIELDS);
                return false;
            }
            if (clientSelected == null) {
                clientSelected = new ClientBean();
                clientSelected.setCompleteName(businessClientName.getText().toString());
                clientSelected.setMail(businessClientEmail.getText().toString().trim());
            }
            email.setSubject("Banco azteca, ticket de compra");
            email.setMessage(clientSelected.getCompleteName() + " se te ha enviado tu ticket de compra");
            email.addAttached(ticketFile);
            email.addDestiny(clientSelected.getMail());
            if (!isTicketOk) {
                showDialog(POPUP_PROCESSING);
                new Wait().execute(0);
            } else
                sendEmail();
            return true;
        } catch (GenericException e) {
            // TODO Auto-generated catch block
            Logger.log(Logger.EXCEPTION, DETAIL_SCREEN, "Correo: " + e.getMessage());
            SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpError), getString(R.string.mailSendError));
            dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case TWITTER_CODE:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        checkTwitterRedirect();
                        if (isTwitterLoggedInAlready()) {
                            container.post(new Runnable() {
                                public void run() {
                                    showTwitterScreen();
                                }
                            });
                        }
                    }
                }).start();

                break;
            case SaleScreen.SALE_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null && data.getExtras() != null) {
                        Bundle info = data.getExtras();
                        clientSelected = (ClientBean) info.getSerializable(Clients.CLIENT_SELECTED);
                        Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "cliente seleccionado: " + clientSelected);
                        if (clientSelected != null) {
                            businessClientEmail.setText(clientSelected.getMail());
                            businessClientName.setText(clientSelected.getCompleteName());
                        }
                    }
                } else if (resultCode == SaleScreen.HOME_RESULT) {
                    //gplusDisconnect();
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case EMAIL_CODE:
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK);
                    //gplusDisconnect();
                    finish();
                }
                break;
            case GOOGLE_COONNECTION_FAILED_CODE:
                Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Regreso de connection failed");
                if (resultCode == RESULT_OK) {// La conexion pudo haber fallado porque no ha hecho login SIGN_IN_REQUIRED
                    Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Se va a conectar 2");
                    //gplusConnect();
                }
                break;
            case GOOGLE_CONNECTED_CODE:
                Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Regreso de lanzar la aplicacion puede que sea version vieja: " + resultCode);
                break;
            case GOOGLE_SERVICES_CODE:
                Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Regreso de GOOGLE SERVICES: " + resultCode);
                break;
        }

    }

    private void deleteTicket(String path) {
        File f = new File(path);
        if (f != null && f.exists())
            f.delete();
        int index = path.indexOf("pdf");
        if (index >= 0) {
            path = path.replace("pdf", "jpg");
            f = new File(path);
            if (f != null && f.exists())
                f.delete();
        } else if ((index = path.indexOf("jpg")) >= 0) {
            path = path.replace("jpg", "pdf");
            f = new File(path);
            if (f != null && f.exists())
                f.delete();
        }

    }

    //********ACTION LISTENERS*****************

    public void send(View v) {
        if (findViewById(R.id.socialContent).getVisibility() == View.VISIBLE) {
            findViewById(R.id.contentShare).setVisibility(View.VISIBLE);
            findViewById(R.id.socialContent).setVisibility(View.GONE);
        } else if ("".equalsIgnoreCase(businessClientEmail.getText().toString()) || "".equalsIgnoreCase(businessClientName.getText().toString())) {
            SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.emptyFields));
            dialog.show(getSupportFragmentManager(), "" + POPUP_EMPTY_FIELDS);
        } else {
            btnPressed = v.getId();
            String message = getString(R.string.confirmEmail) + "\n" + businessClientEmail.getText().toString() + "\n" + getString(R.string.getVoucher);
            ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(getString(R.string.popUpMessage), message);
            confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
                @Override
                public void onAccept(DialogInterface dialog) {
                    if (sendMail(false) && btnPressed == R.id.addSend) {
                        saveClient();
                    }

                }
            });
            confirmationDialog.show(getSupportFragmentManager(), "" + POPUP_CONFIRM_EMAIL);
        }
    }

    private void saveClient() {
        // TODO Auto-generated method stub
        ClientBean client = new ClientBean();
        client.setCompleteName(businessClientName.getText().toString().toUpperCase(Locale.getDefault()));
        client.setMail(businessClientEmail.getText().toString().trim());
        new ClientsJob<String>(this, customer.getFolioPreap(), fiscalData.getRfc(), client, this, ClientsJob.CREATE_CLIENT).start();

    }

    public void search(View v) {
        ArrayList<Serializable> params = new ArrayList<Serializable>();
        params.add(customer.getFolioPreap());
        params.add(fiscalData.getRfc());
        params.add(sale);
        params.add(email);
        ArrayList<String> paramNames = new ArrayList<String>();
        paramNames.add(CustomerBean.FOLIO_PREAPERTURA);
        paramNames.add(CustomerBean.RFC);
        paramNames.add(SaleBean.SALE);
        paramNames.add(EmailBean.EMAIL);
        Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "viene de una venta");
        goForwardForResult(Clients.class, this, params, paramNames, SaleScreen.SALE_CODE);
    }

    private void sendEmail() {
        try {
            removeDialog(POPUP_PROCESSING);
            Mail.sendByIntent(email, DetailSaleScreen.this);
        } catch (GenericException e) {
            // TODO Auto-generated catch block
            SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpError), getString(R.string.mailSendError));
            dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
        } catch (ActivityNotFoundException e) {
            SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpError), getString(R.string.noAppForEmail));
            dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
        }
    }

    @Override
    public void sendMessage(Context ctx, Message message, String parameter) {
        // TODO Auto-generated method stub

    }

    @Override
    public void sendStatus(final Context ctx, final int status) {
        // TODO Auto-generated method stub,
        Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Contesto el servicio de digitalizacion");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (isFinishing())
                    return;
                if (status == Message.OK)
                    isTicketOk = true;
                else
                    isTicketOk = false;
            }
        });
    }

    @Override
    public void sendMessage(Context ctx, Message message, Object... parameters) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Logger.log(Logger.MESSAGE, DETAIL_SCREEN, "Mapa Ready");
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.overlay);
        LatLng locationSale;

        if (location != null) {
            locationSale = new LatLng(location.getLatitude(), location.getLongitude());
        } else {
            locationSale = new LatLng(19.296836, -99.1900087);
        }

        googleMap.addMarker(new MarkerOptions()
                .position(locationSale))
                .setIcon(icon);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationSale, ZOOM_LEVEL));


    }

    private class Wait extends AsyncTask<Integer, Integer, Integer> {


        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected Integer doInBackground(Integer... arg0) {
            // TODO Auto-generated method stub
            while (backgroundThread != null && backgroundThread.isAlive() && !backgroundThread.isInterrupted()) {
            }
            return arg0[0];
        }

        @Override
        protected void onPostExecute(Integer result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (result == 0)
                sendEmail();
            else {
                removeDialog(POPUP_PROCESSING);
                //gplusDisconnect();
                setResult(RESULT_OK);
                finish();
            }
        }

    }

    public void share(View v) {
        if (v.getId() == R.id.shareButton) {
            findViewById(R.id.contentShare).setVisibility(View.GONE);
            findViewById(R.id.socialContent).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.contentShare).setVisibility(View.VISIBLE);
            findViewById(R.id.socialContent).setVisibility(View.GONE);
        }

    }

    public void social(final View v) {
        if (v.getId() == R.id.fb) {
            loginFb(v);
        }
        /*else if (v.getId()==R.id.gplus){
            loginGPlus(v);
		}*/
        else {
            if (!isTwitterLoggedInAlready()) {
                popupMsg = R.string.connectTwitter;
                popupTitle = R.string.popUpMessage;
                showDialog(POPUP_MESSAGES_PROGRESS);
                new Thread(new Runnable() {
                    public void run() {
                        loginTwitter(v, TwitterTemp.class);
                    }
                }).start();
            } else
                showTwitterScreen();
        }
    }

    public void postTwitter(View v) {
        postTwitter(twitterEdit.getText().toString());
    }

    public void cancelTweet(View v) {
        hideTwitterScreen();
    }

    @Override
    protected void hideTwitterScreen() {
        // TODO Auto-generated method stub
        fakePopup.dismiss();
        twitterScreen.dismiss();
    }

    private void showTwitterScreen() {
        LinearLayout layout = (LinearLayout) Utility.inflateLayout(this, R.layout.twitter_screen, null);
        twitterEdit = (EditText) layout.findViewById(R.id.textTwitter);
        twitterScreen = new PopupWindow(layout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        twitterScreen.setContentView(layout);
        fakePopup.showAtLocation(businessFirm, Gravity.CENTER, 0, 0);
        twitterScreen.showAtLocation(businessFirm, Gravity.CENTER, 0, 0);
        twitterScreen.setFocusable(true);
        twitterScreen.update();

    }
}