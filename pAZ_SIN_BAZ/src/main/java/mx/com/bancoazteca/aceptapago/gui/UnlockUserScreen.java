package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.jobs.UnlockUserJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Utility;

public class UnlockUserScreen extends BaseActivity implements MessageListener<Message, String>{
	private static final int POPUP_VALID_FORM = 11;
	private static final int POPUP_UNLOCK_USER = 12;
	protected static final int POPUP_ERROR = 13;
	protected static final int POPUP_EXCEPTION = 14;
	protected static final int POPUP_UNLOCKED_USER = 15;
	private String tmpParameter;
	public final static String CHARACTERES = "Caracter invalido";
	private EditText user;
	private Toolbar mtoolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.unlock_user);
		this.user=(EditText) findViewById(R.id.userField);
		
		InputFilter filters[]=null;
		filters= new InputFilter[user.getFilters().length +1];
		System.arraycopy(user.getFilters(), 0, filters,0,user.getFilters().length);
		filters[user.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		user.setFilters(filters);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("parameter", tmpParameter);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		tmpParameter=savedInstanceState.getString("parameter");
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton accept;
		switch (id) {
		case POPUP_UNLOCK_USER:
			popUp= new PopUp(this, R.string.popUpMessage,R.string.unlockingUser);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.setCancelable(false);
			popUp.create();
			return popUp.getDialog();
		default:
			return super.onCreateDialog(id);
		}
	}
	public void accept(View v){
	if (validForm()) {
			this_.showDialog(POPUP_UNLOCK_USER);
			backgroundThread= new Thread(new UnlockUserJob(this,this, this.user.getText().toString()));
			backgroundThread.start();
		}
	}
	public boolean isValidCharacters(){
		boolean answer = true;
		if(!Utility.isvalidCharacters(user.getText().toString())){
			answer=false;
			user.setError(CHARACTERES);
		}
		
		
		return answer;
	}
	@Override
	protected boolean validForm() {
		// TODO Auto-generated method stub
		if (user.getText().toString().length() ==0 ){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.emptyFields));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
			return false;
		}
		return true;
	}
	@Override
	public void sendMessage(Context ctx,Message message,final String parameter) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (isFinishing()) {
					return;
				}
				this_.removeDialog(POPUP_UNLOCK_USER);
				tmpParameter=parameter;
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),tmpParameter);
				dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
					@Override
					public void onAccept(DialogInterface dialog) {
						finish();
					}
				});
				dialog.show(getSupportFragmentManager(),""+POPUP_ERROR);
			}
		});
	}
	@Override
	public void sendStatus(Context ctx,final int status) {
		// TODO Auto-generated method stub
		
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (isFinishing()) 
					return;
				this_.removeDialog(POPUP_UNLOCK_USER);
				if (status==Message.EXCEPTION) {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_EXCEPTION);
				}else if (status==Message.OK) {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.unlockSucced));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_UNLOCKED_USER);
				}
			}
		});
	}
	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
}
