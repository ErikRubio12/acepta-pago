package mx.com.bancoazteca.aceptapago.jobs;

import android.content.Context;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.concurrent.TimeoutException;

import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

/**
 * Created by B931724 on 21/02/18.
 */

public class SendPrivacityKeyJob extends BaseService<Message, String> {

    private static final String REGISTER_BUSINESS_ACTION = null;
    private static final String REGISTER_BUSINESS_METHOD = "setKeyPrivacidad";
    private static final String REGISTER_BUSINESS_PARAM = "xml";
    private static final String REGISTER_BUSINESS_NAMESPACE = "http://service.btb.com";
    private static final String REGISTER_BUSINESS_URL = Urls.PAZ3 + "services/PB2B";
    private static final String OPERATION_CODE_TAG = "codigo_operacion";
    private static final String SYSTEM_ERROR_TAG = "error_sistema";
    public static final int ERROR_SENDING_DATA = 132;
    public static final int EXCEPTION = 189;
    private static final String MENSAJE_SISTEMA = "mensajeSistema";
    private static final String REGISTER_BUSINESS = "register business";


    private final RegistrationBean register;
    private SoapRequest<String> req;
    private String mensajeSistema;

    public SendPrivacityKeyJob(Context ctx, RegistrationBean register, MessageListener<Message, String> listener){
        this.register=register;
        this.listener=listener;
        this.ctx=ctx;
        operationCode=null;
        systemError="";
    }


    @Override
    public void run() {
        // TODO Auto-generated method stub
        StringBuilder request= new StringBuilder();
        request.append("<bancoAzteca>\r\n");
        request.append("<eservices>\r\n");
        request.append("<request>\r\n");
        request.append("<idservicio>");
        request.append(Encrypt.encryptStringWS("btob"));
        request.append("</idservicio>\r\n");
        request.append("<canalEntrada>");
        request.append(Encrypt.encryptStringWS(REGISTER_BUSINESS_METHOD));
        request.append("</canalEntrada>\r\n");
        request.append("<keyPrivacidad>");
        request.append(Encrypt.encryptStringWS(register.getCustomer().getKeyRegister()));
//        request.append(Encrypt.encryptStringWS("dhoufoewgd-dsadiasdaf-asfdasfasf-sfefefe"));
        request.append("</keyPrivacidad>\r\n");
        request.append("<folioAzteca>");
        request.append(Encrypt.encryptStringWS(register.getCustomer().getFolioPreap()));
//        request.append(Encrypt.encryptStringWS("124364561436457427457"));
        request.append("</folioAzteca>\r\n");
        request.append("</request>");
        request.append("</eservices>");
        request.append("</bancoAzteca>");

        req= new SoapRequest<String>(REGISTER_BUSINESS_METHOD, REGISTER_BUSINESS_ACTION);
        req.setNameSpace(REGISTER_BUSINESS_NAMESPACE);
        req.setUrl(REGISTER_BUSINESS_URL);
        Parameter<String,String> param= new Parameter<String, String>(REGISTER_BUSINESS_PARAM, request.toString());
        req.addParameter(param);
        SoapResponse res = null;
        try {
            res = Connections.makeSecureSoapConnection(req, TIME_OUT);
            if(res==null){
                listener.sendStatus(ctx,EXCEPTION);
                return;
            }

            String xml=res.getResponse().toString();
            Connections.closeSoapConnection(res);
            xml= Utility.fixSoapResponse(xml);


            XmlPullParser parser= Xml.newPullParser();
            parser.setInput(new StringReader(xml));
            int eventType=parser.getEventType();
            String currentTag=null;

            while(eventType!=XmlPullParser.END_DOCUMENT){
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:break;
                    case XmlPullParser.END_DOCUMENT:break;
                    case XmlPullParser.START_TAG:
                        currentTag=parser.getName();

                        break;
                    case XmlPullParser.TEXT:
                        if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
                            operationCode= Encrypt.decryptStringWS(parser.getText());
                        }
                        else if (currentTag.equalsIgnoreCase(MENSAJE_SISTEMA)){
                            mensajeSistema=Encrypt.decryptStringWS(parser.getText());
                        }
                        else if (currentTag.equalsIgnoreCase(SYSTEM_ERROR_TAG)){
                            systemError=Encrypt.decryptStringWS(parser.getText());
                        }


                        break;
                    case XmlPullParser.END_TAG:
                        currentTag=null;
                        break;
                    default:
                        break;
                }
                eventType=parser.next();
            }
            Message msg= new Message();
//            if ("00".equals(operationCode)) {
                msg.setType(Message.OKKey);
                listener.sendMessage(ctx,msg, mensajeSistema);
//            }
//            else{
//                msg.setType(ERROR_SENDING_DATA);
//                StringBuffer message= new StringBuffer(operationCode);
//                message.append(" - ");
//                message.append(systemError);
//                listener.sendMessage(ctx,msg, message.toString());
//            }
        } catch (InterruptedIOException e) {
            // TODO Auto-generated catch block
            listener.sendStatus(ctx,EXCEPTION);
            Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"InterruptedException: " +e.getMessage());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            listener.sendStatus(ctx,EXCEPTION);
            Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"IOException: "+ e.getMessage());
            return;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            listener.sendStatus(ctx,EXCEPTION);
            Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"XmlPullParserException: "+ e.getMessage());
            return;
        } catch (TimeoutException e) {
            // TODO Auto-generated catch block
            listener.sendStatus(ctx,EXCEPTION);
            Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"TimeoutException: "+ e.getMessage());
        }
    }


}
