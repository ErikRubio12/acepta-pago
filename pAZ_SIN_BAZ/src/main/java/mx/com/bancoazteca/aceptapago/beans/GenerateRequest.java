package mx.com.bancoazteca.aceptapago.beans;

/**
 * Created by B931724 on 20/02/18.
 */

public class GenerateRequest {

    private int NumContrato;
    private int Pais;
    private int Canal;
    private int Sucursal;
    private int Folio;
    private int CanalDig;
    private int SucDig;
    private int IdProducto;
    private int NumEmpleado;
    private String descProd;
    private String origen;
    private String Workstation;
    private String NombreCte;
    private String APaternoCte;
    private String AMaternoCte;
    private String Consiento;
    private String FechaAviso;
    private String FirmaAviso;
    private String Documentos;
    private String tokenCode;

    public GenerateRequest(String nombreCte, String aPaternoCte, String aMaternoCte,
                           String fechaAviso,String tokenCode) {
        this.NumContrato = 62;
        this.Pais = 0;
        this.Canal = 0;
        this.Sucursal = 0;
        this.Folio = 0;
        this.CanalDig = 33;
        this.SucDig = 9546;
        this.IdProducto = 6;
        this.NumEmpleado = 111111;
        this.descProd = "ACP";
        this.origen = "AppMovil";
        this.Workstation = "AppMovil";
        this.NombreCte = nombreCte;
        this.APaternoCte = aPaternoCte;
        this.AMaternoCte = aMaternoCte;
        this.Consiento = "X";
        this.FechaAviso = fechaAviso;
        this.FirmaAviso = "--";
        this.Documentos = "709,822";
        this.tokenCode = tokenCode;
    }


    public int getNumContrato() {
        return NumContrato;
    }

    public void setNumContrato(int numContrato) {
        NumContrato = numContrato;
    }

    public int getPais() {
        return Pais;
    }

    public void setPais(int pais) {
        Pais = pais;
    }

    public int getCanal() {
        return Canal;
    }

    public void setCanal(int canal) {
        Canal = canal;
    }

    public int getSucursal() {
        return Sucursal;
    }

    public void setSucursal(int sucursal) {
        Sucursal = sucursal;
    }

    public int getFolio() {
        return Folio;
    }

    public void setFolio(int folio) {
        Folio = folio;
    }

    public int getCanalDig() {
        return CanalDig;
    }

    public void setCanalDig(int canalDig) {
        CanalDig = canalDig;
    }

    public int getSucDig() {
        return SucDig;
    }

    public void setSucDig(int sucDig) {
        SucDig = sucDig;
    }

    public int getIdProducto() {
        return IdProducto;
    }

    public void setIdProducto(int idProducto) {
        IdProducto = idProducto;
    }

    public int getNumEmpleado() {
        return NumEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        NumEmpleado = numEmpleado;
    }

    public String getDescProd() {
        return descProd;
    }

    public void setDescProd(String descProd) {
        this.descProd = descProd;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getWorkStation() {
        return Workstation;
    }

    public void setWorkStation(String workStation) {
        Workstation = workStation;
    }

    public String getNombreCte() {
        return NombreCte;
    }

    public void setNombreCte(String nombreCte) {
        NombreCte = nombreCte;
    }

    public String getAPaternoCte() {
        return APaternoCte;
    }

    public void setAPaternoCte(String APaternoCte) {
        this.APaternoCte = APaternoCte;
    }

    public String getAMaternoCte() {
        return AMaternoCte;
    }

    public void setAMaternoCte(String AMaternoCte) {
        this.AMaternoCte = AMaternoCte;
    }

    public String getConsiento() {
        return Consiento;
    }

    public void setConsiento(String consiento) {
        Consiento = consiento;
    }

    public String getFechaAviso() {
        return FechaAviso;
    }

    public void setFechaAviso(String fechaAviso) {
        FechaAviso = fechaAviso;
    }

    public String getFirmaAviso() {
        return FirmaAviso;
    }

    public void setFirmaAviso(String firmaAviso) {
        FirmaAviso = firmaAviso;
    }

    public String getDocumentos() {
        return Documentos;
    }

    public void setDocumentos(String documentos) {
        Documentos = documentos;
    }

    public String getTokenCode() {
        return tokenCode;
    }

    public void setTokenCode(String tokenCode) {
        this.tokenCode = tokenCode;
    }
}
