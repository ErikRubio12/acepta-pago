package mx.com.bancoazteca.aceptapago.gui;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;

import java.util.List;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.ui.BaseActivity;

public class ShowPDFScreen extends BaseActivity implements OnPageChangeListener, OnLoadCompleteListener, OnPageErrorListener {

    private Toolbar mtoolbar;
    private PDFView contratos;
    private String pdfFileName;
    private String SAMPLE_FILE="terminos.pdf";
    private int pageNumber = 0;
    private String TAG = "AP";

    @Override
    protected void loadUiComponents() {
        // TODO Auto-generated method stub

        super.loadUiComponents();
        setContentView(R.layout.show_pdf_screen);
        mtoolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Bundle miBundle = new Bundle();
        miBundle = getIntent().getExtras();
        if(miBundle != null){
            SAMPLE_FILE = miBundle.getString("nombreArchivo");
        }
//        contratos = (WebView) findViewById(R.id.contratos);
        contratos = (PDFView) findViewById(R.id.contratos);
        contratos.setBackgroundColor(Color.LTGRAY);
        displayFromAsset(SAMPLE_FILE);



    }

    private void displayFromAsset(String assetFileName) {
        pdfFileName = assetFileName;

        contratos
                .fromAsset(SAMPLE_FILE)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .spacing(10) // in dp
                .onPageError(this)
//                .pageFitPolicy(FitPolicy.BOTH)
                .load();
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = contratos.getDocumentMeta();
        Log.e(TAG, "title = " + meta.getTitle());
        Log.e(TAG, "author = " + meta.getAuthor());
        Log.e(TAG, "subject = " + meta.getSubject());
        Log.e(TAG, "keywords = " + meta.getKeywords());
        Log.e(TAG, "creator = " + meta.getCreator());
        Log.e(TAG, "producer = " + meta.getProducer());
        Log.e(TAG, "creationDate = " + meta.getCreationDate());
        Log.e(TAG, "modDate = " + meta.getModDate());

        printBookmarksTree(contratos.getTableOfContents(), "-");
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void onPageError(int page, Throwable t) {
        Log.e(TAG, "Cannot load page " + page);
    }
}
