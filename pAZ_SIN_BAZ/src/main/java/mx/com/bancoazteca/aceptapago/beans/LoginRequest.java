package mx.com.bancoazteca.aceptapago.beans;

/**
 * Created by B931724 on 19/02/18.
 */

public class LoginRequest {

    private String username;
    private String thePass;
    private String keyApp;
    private String dispositivo;
    private String infoHashActivation;
    private String aplicacion;


    public LoginRequest(String username, String thePass, String keyApp, String dispositivo, String infoHashActivation, String aplicacion) {
        this.username = username;
        this.thePass = thePass;
        this.keyApp = keyApp;
        this.dispositivo = dispositivo;
        this.infoHashActivation = infoHashActivation;
        this.aplicacion = aplicacion;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getThePass() {
        return thePass;
    }

    public void setThePass(String thePass) {
        this.thePass = thePass;
    }

    public String getKeyApp() {
        return keyApp;
    }

    public void setKeyApp(String keyApp) {
        this.keyApp = keyApp;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public String getInfoHashActivation() {
        return infoHashActivation;
    }

    public void setInfoHashActivation(String infoHashActivation) {
        this.infoHashActivation = infoHashActivation;
    }

    public String getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(String aplicacion) {
        this.aplicacion = aplicacion;
    }
}
