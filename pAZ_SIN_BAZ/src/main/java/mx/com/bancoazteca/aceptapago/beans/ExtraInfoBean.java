package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;


public class ExtraInfoBean implements Serializable{
	
	public static final String BUSINESS_TYPE="actGiroProf";
	public static final String ECONOMIC_ACTIVITY="actEconom";
	public static final String BUSSINES_SECTOR="secLab";
	public static String EXTRA_INFO="extra info";
	private static final long serialVersionUID = -192505058075400808L;
	public static final String WEB_PAGE = "webNeg";
	private int businessType;//si esta
	private int categoryBusinessType;//si esta
	private int categoryEconomicActivity;
	private int economicActivity;//SI ESTA
	private String businessSector;//si esta
	private String webPage="";
	
	public int getBusinessType() {
		return businessType;
	}
	public void setBusinessType(int businessType) {
		this.businessType= businessType;
	}
	public int getEconomicActivity() {
		return economicActivity;
	}
	public void setEconomicActivity(int economicActivity) {
		this.economicActivity = economicActivity;
	}
	public String getBusinessSector() {
		return businessSector;
	}
	public void setBusinessSector(String businessSctor) {
		this.businessSector = businessSctor;
	}
	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}
	public String getWebPage() {
		return webPage;
	}
	public void setCategoryEconomicActivity(int categoryEconomicActivity) {
		this.categoryEconomicActivity = categoryEconomicActivity;
	}
	public int getCategoryEconomicActivity() {
		return categoryEconomicActivity;
	}
	public void setCategoryBusinessType(int categoryBusinessType) {
		this.categoryBusinessType = categoryBusinessType;
	}
	public int getCategoryBusinessType() {
		return categoryBusinessType;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer res= new StringBuffer("economicActivity: ");
		res.append(economicActivity);
		res.append("\ncategoryEconomicActivity: ");
		res.append(categoryEconomicActivity);
		res.append("\nbusinessType: ");
		res.append(businessType);
		res.append("\ncategoryBusinessType: ");
		res.append(categoryBusinessType);
		res.append("\nweb: ");
		res.append(webPage);
		res.append("\nsector");
		res.append(businessSector);
		res.append("\n");
		return res.toString();
	}
}
