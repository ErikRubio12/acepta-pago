package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Xml;

import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.util.Logger;

public class ChangePasswordJob extends BaseService<Message, String> {

	private static final String RECOVER_PASSWORD_METHOD = "BusinessToBusinessLogin";
	private static final String RECOVER_PASSWORD_ACTION = null;
	private static final String RECOVER_PASSWORD_NAMESPACE = "http://service.btb.com";
	private static final String RECOVER_PASSWORD_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String RECOVER_PASSWORD_PARAM = "xml";
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String CHANGE_PASSWORD = "Change password";
//	private String newPassword;
	private char[] newPassword;
	private LoginBean login;
	
	public  ChangePasswordJob(Context ctx,MessageListener<Message, String> listener,LoginBean login,char[] newPassword){
		this.listener=listener;
		this.login=login;
		this.newPassword=newPassword;
		this.ctx=ctx;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append(Encrypt.encryptStringWS("login.Reset"));
		request.append("</canalEntrada>");
		request.append("<login>");
		request.append("<usuario>");
		request.append(Encrypt.encryptStringWS(login.getUser()));
		request.append("</usuario>");
		request.append("<contrasena>");
//		request.append(Encrypt.encryptStringWS(login.getPassword()));
		request.append(Encrypt.encryptStringWS(login.getPassword()));
		request.append("</contrasena>");
		request.append("<nuevaContrasena>");
		request.append(Encrypt.encryptStringWS(Arrays.toString(newPassword).replace(",","").replace("[","").replace("]","").replace(" ","").trim()));
		request.append("</nuevaContrasena>");
		request.append("</login>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		SoapRequest<String> req= new SoapRequest<String>(RECOVER_PASSWORD_METHOD, RECOVER_PASSWORD_ACTION);
		req.setNameSpace(RECOVER_PASSWORD_NAMESPACE);
		req.setUrl(RECOVER_PASSWORD_URL);
		Parameter<String,String> param= new Parameter<String, String>(RECOVER_PASSWORD_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req,TIME_OUT);
			if (res==null) {
				listener.sendStatus(ctx,Message.EXCEPTION);
				return;
			}
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equals(OPERATION_CODE_TAG)) {
						operationCode= Encrypt.decryptStringWS(parser.getText());
					}
					else if (currentTag.equals(SYSTEM_ERROR_TAG)) {
						systemError= Encrypt.decryptStringWS(parser.getText());
					}
					
					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			if ("0".equals(operationCode)) {
				listener.sendStatus(ctx,Message.OK);
			}
			else{
				listener.sendMessage(ctx,new Message(), systemError);
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, CHANGE_PASSWORD,"XmlPullParserException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		}  catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, CHANGE_PASSWORD,"TimeoutException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, CHANGE_PASSWORD,"InterruptedIOExceptionn: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		}catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, CHANGE_PASSWORD,"IOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		}
		
		
	}

}
