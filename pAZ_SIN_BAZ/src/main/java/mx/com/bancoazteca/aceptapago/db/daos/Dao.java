package mx.com.bancoazteca.aceptapago.db.daos;

import mx.com.bancoazteca.aceptapago.db.PagoAztecaDB;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class Dao {
	protected PagoAztecaDB db;
	protected SQLiteDatabase connection;
	protected Context context;
	public Dao(Context context) {
		// TODO Auto-generated constructor stub
		this.context=context;
		db= new PagoAztecaDB(context,false);
	}
}
