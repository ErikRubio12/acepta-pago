package mx.com.bancoazteca.aceptapago.gui;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.ui.BaseActivity;

public class RecoveryOptionsScreen extends BaseActivity {

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.recovery_options);
		Button recoveryPw = (Button) findViewById(R.id.recoverPw);
		Button changePw = (Button) findViewById(R.id.changePw);
		Button unlockUser = (Button) findViewById(R.id.lockUser);
		
		unlockUser.setOnClickListener(unlockUserListener);
		changePw.setOnClickListener(changePwListener);
		recoveryPw.setOnClickListener(recoveryPwListener);
		Toolbar mtoolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
		
	}


	private OnClickListener recoveryPwListener= new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			goForward(PasswordRecoverScreen.class,v.getContext());
		}
	};

	private OnClickListener unlockUserListener= new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			goForward(UnlockUserScreen.class,arg0.getContext());
		}
	};
	private OnClickListener changePwListener= new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			goForward(ChangePasswordScreen.class, v.getContext());
		}
	}; 
}
