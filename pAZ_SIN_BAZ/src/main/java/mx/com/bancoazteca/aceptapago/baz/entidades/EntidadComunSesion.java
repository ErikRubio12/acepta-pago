package mx.com.bancoazteca.aceptapago.baz.entidades;


public class EntidadComunSesion extends EntidadComun {
	private String idSession;
	private String idAplicacion;

	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}

	public String getIdSession() {
		return idSession;
	}

	public String getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
}
