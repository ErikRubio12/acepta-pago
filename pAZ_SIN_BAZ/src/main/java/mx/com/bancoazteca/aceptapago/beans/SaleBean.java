package mx.com.bancoazteca.aceptapago.beans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import mx.com.bancoazteca.beans.CardBean;
import mx.com.bancoazteca.data.files.FileManager;

import android.content.Context;
import android.graphics.Bitmap;

public class SaleBean implements Serializable,KvmSerializable{
	
	private static final long serialVersionUID = -4629268418227953773L;
	private String  aid;
	private String arqc;
	private String Description="";
	private  static String image=null;
	private String authorizationNumber;
	private Bitmap bitmapTicket;
	private CardBean creditCard;
	private String folio="";
	public final static String FOLIO="folio";
	public static final String SALE ="sale";
	private static final int FIELDS = 12;
	private String idServicio;
	private String inputChannel;
	private String transactionType;
	private String transactionDate;
	private String idDevice;
	private String idTransaction;
	private String afiliacion;
	private String estado;
	private Date saleDate;
	private String poblacion;
	private Long amount;
	private String amountReturned;
	private String signPath;
	private String referenceNumber;
	private static PropertyInfo listPI[]= new PropertyInfo[FIELDS];
	private static PropertyInfo pIidServicio= new PropertyInfo();
	private static PropertyInfo pIinputChannel= new PropertyInfo();
	private static PropertyInfo pItransactionType= new PropertyInfo();
	private static PropertyInfo pItransactionDate= new PropertyInfo();
	private static PropertyInfo pIidDevice= new PropertyInfo();
	private static PropertyInfo pIidTransaction= new PropertyInfo();
	private static PropertyInfo pIcardOwner= new PropertyInfo();
	private static PropertyInfo pIcardType= new PropertyInfo();
	private static PropertyInfo pIcard= new PropertyInfo();
	private static PropertyInfo pIcvv= new PropertyInfo();
	private static PropertyInfo pIcardValidity= new PropertyInfo();
	private static PropertyInfo pIamount= new PropertyInfo();
	private static final String ID_SERVICIO = "idservicio";
	private static final String  INPUT_CHANNEL= "canalEntrada";
	private static final String TRANSACTION_TYPE = "tipo_operacion";
	private static final String TRANSACTION_DATE = "fechaTransaccion";
	private static final String ID_DEVICE = "idterminal";
	private static final String ID_TRANSACTION = "idTransaccion";
	private static final String CARD_OWNER = "titular";
	private static final String CARD_TYPE = "tipoTarjeta";
	private static final String CARD = "tarjeta";
	private static final String CVV = "cvv";
	private static final String CARD_VALIDITY = "fechaVencimiento";
	private static final String AMOUNT = "monto";
	public static final String AFILIACION = "afiliacion";
	public static final String SIGN = "sign";
	
	static{

		pIidServicio= new PropertyInfo();
		pIidServicio.name=ID_SERVICIO;
		pIidServicio.type=PropertyInfo.STRING_CLASS;

		pIinputChannel= new PropertyInfo();
		pIinputChannel.name=INPUT_CHANNEL;
		pIinputChannel.type=PropertyInfo.STRING_CLASS;

		pItransactionType= new PropertyInfo();
		pItransactionType.name=TRANSACTION_TYPE;
		pItransactionType.type=PropertyInfo.STRING_CLASS;
		
		pItransactionDate= new PropertyInfo();
		pItransactionDate.name=TRANSACTION_DATE;
		pItransactionDate.type=PropertyInfo.STRING_CLASS;
		
		pIidDevice=new PropertyInfo();
		pIidDevice.name=ID_DEVICE;
		pIidDevice.type=PropertyInfo.STRING_CLASS;
		
		pIidTransaction= new PropertyInfo();
		pIidTransaction.name=ID_TRANSACTION;
		pIidTransaction.type=PropertyInfo.STRING_CLASS;
		
		pIcardOwner= new PropertyInfo();
		pIcardOwner.name= CARD_OWNER;
		pIcardOwner.type=PropertyInfo.STRING_CLASS;
		
		pIcardType=  new PropertyInfo();
		pIcardType.name= CARD_TYPE;
		pIcardType.type=PropertyInfo.INTEGER_CLASS;
		
		pIcard= new PropertyInfo();
		pIcard.name=CARD;
		pIcard.type=PropertyInfo.STRING_CLASS;
		
		pIcvv= new PropertyInfo();
		pIcvv.name= CVV;
		pIcvv.type=PropertyInfo.INTEGER_CLASS;
		
		pIcardValidity= new PropertyInfo();
		pIcardValidity.name=CARD_VALIDITY;
		pIcardValidity.type=PropertyInfo.INTEGER_CLASS;
		
		pIamount= new PropertyInfo();
		pIamount.name=AMOUNT;
		pIamount.type=PropertyInfo.STRING_CLASS;
		
		listPI[0]= pIidServicio;
		listPI[1]=pIinputChannel;
		listPI[2]=pItransactionType;
		listPI[3]=pItransactionDate;
		listPI[4]=pIidDevice;
		listPI[5]=pIidTransaction;
		listPI[6]=pIcardOwner;
		listPI[7]=pIcardType;
		listPI[8]=pIcard;
		listPI[9]=pIcvv;
		listPI[10]=pIcardValidity;
		listPI[11]=pIamount;
	}
	public SaleBean() {
		// TODO Auto-generated constructor stub
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getInputChannel() {
		return inputChannel;
	}

	public void setInputChannel(String inputChannel) {
		this.inputChannel = inputChannel;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDateTransaction() {
		return transactionDate;
	}

	public void setDateTransaction(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getIdDevice() {
		return idDevice;
	}

	public void setIdDevice(String idDevice) {
		this.idDevice = idDevice;
	}

	public String getIdTransaction() {
		return idTransaction;
	}

	public void setIdTransaction(String idTransaction) {
		this.idTransaction = idTransaction;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}


	public void setCard(CardBean creditCard) {
		this.creditCard = creditCard;
	}

	public CardBean getCard() {
		return creditCard;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolio() {
		return folio;
	}

	public static void setImage(String image) {
		SaleBean.image = image;
	}

	public static String getImage() {
		return image;
	}

//	public static void setSignImage(Bitmap signImage) {
//		SaleBean.signImage = signImage;
//	}
//
//	public static Bitmap getSignImage() {
//		return signImage;
//	}
//
//	public static void setImageDecoded(WeakReference<Bitmap> imageDecoded) {
//		SaleBean.imageDecoded = imageDecoded.get();
//	}
//
//	public static Bitmap getImageDecoded() {
//		return imageDecoded;
//	}

	public void setBitmapTicket(WeakReference<Bitmap> bitmapTicket) {
		this.bitmapTicket = bitmapTicket.get();
	}

	public Bitmap getBitmapTicket() {
		return bitmapTicket;
	}

//	public static void setImageTicket(Bitmap imageTicket) {
//		SaleBean.imageTicket = imageTicket;
//	}
//
//	public static Bitmap getImageTicket() {
//		return imageTicket;
//	}
	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0: return idServicio;
		case 1: return inputChannel; 
		case 2: return transactionType; 
		case 3: return transactionDate; 
		case 4: return idDevice; 
		case 5: return idTransaction; 
		case 6: return creditCard.getCardOwner(); 
		case 7: return creditCard.getCardType(); 
		case 8: return creditCard.getNumber(); 
		case 9: return creditCard.getSecurity(); 
		case 10: return creditCard.getValidity(); 
		case 11: return amount.toString(); 
		default: return null;
	}
	}

	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}

	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}
	
	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: pIidServicio=(PropertyInfo) arg1; break;
			case 1: pIinputChannel=(PropertyInfo) arg1; break;
			case 2: pItransactionType=(PropertyInfo) arg1; break;
			case 3: pItransactionDate=(PropertyInfo) arg1; break;
			case 4: pIidDevice=(PropertyInfo) arg1; break;
			case 5: pIidTransaction=(PropertyInfo) arg1; break;
			case 6: pIcardOwner=(PropertyInfo) arg1; break;
			case 7: pIcardType=(PropertyInfo) arg1; break;
			case 8: pIcard=(PropertyInfo) arg1; break;
			case 9: pIcvv=(PropertyInfo) arg1; break;
			case 10: pIcardValidity=(PropertyInfo) arg1; break;
			case 11: pIamount=(PropertyInfo) arg1; break;
		}
	}

	public boolean saveSign(byte[] bytes,Context context){
		File f= FileManager.openFile(context, "sign.png", FileManager.MODE_RW);
		FileOutputStream stream = null;
		try {
			stream = new FileOutputStream(f);
			stream.write(bytes);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}finally{
			try {
				stream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;
		
	}

	public void setSignPath(String path) {
		// TODO Auto-generated method stub
		this.signPath=path;
	}

	public String getSignPath() {
		// TODO Auto-generated method stub
		return this.signPath;
	}

	public String getAuthorizationNumber() {
		return authorizationNumber;
	}

	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public void setReferenceNumber(String reference) {
		// TODO Auto-generated method stub
		this.referenceNumber=reference;
	}
	public String getReferenceNumber(){
		return this.referenceNumber;
	}
	
	public void setEstado(String st){
		this.estado = st;
	}
	
	public String getEstado(){
		return estado;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer res= new StringBuffer();
		res.append("afiliacion: ");
		res.append(afiliacion);
		res.append("Numero de autorizacion: ");
		res.append(authorizationNumber);
		res.append("folio:");
		res.append(folio);
		res.append("referencia: ");
		res.append(referenceNumber);
		res.append("dispositivo: ");
		res.append(idDevice);
		res.append("monto: ");
		res.append(amount);
		res.append("arqc: ");
		res.append(arqc);
		res.append("aid: ");
		res.append(aid);
		res.append("ruta: ");
		res.append(image);
		res.append("tarjeta: ");
		res.append(creditCard);
		return res.toString();
	}

	public String getArqc() {
		return arqc;
	}

	public void setArqc(String arqc) {
		this.arqc = arqc;
	}

	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getAmountReturned() {
		return amountReturned;
	}

	public void setAmountReturned(String amountReturned) {
		this.amountReturned = amountReturned;
	}
}
