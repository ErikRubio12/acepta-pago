package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.os.Build;
import android.util.Xml;

public class RegisterBusinessJob extends BaseService<Message, String> {
	
	private static final String REGISTER_BUSINESS_ACTION = null;
	private static final String REGISTER_BUSINESS_METHOD = "BusinessToBusinessPagoAzteca";
	private static final String REGISTER_BUSINESS_PARAM = "xml";
	private static final String REGISTER_BUSINESS_NAMESPACE = "http://service.btb.com";
	private static final String REGISTER_BUSINESS_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	public static final int ERROR_SENDING_DATA = 132;
	public static final int EXCEPTION = 189;
	private static final String FOLIO_TAG = "folio";
	private static final String REGISTER_BUSINESS = "register business";
	private RegistrationBean register;
	private SoapRequest<String> req;
	private String folio="";
	public RegisterBusinessJob(Context ctx,RegistrationBean register,MessageListener<Message, String> listener) {
		// TODO Auto-generated constructor stub
		this.register=register;
		this.listener=listener;
		this.ctx=ctx;
		operationCode=null;
		systemError="";
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>\r\n");
		request.append("<eservices>\r\n");
		request.append("<request>\r\n");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>\r\n");
		request.append("<canalEntrada>");
		request.append(Encrypt.encryptStringWS("Preapertura"));
		request.append("</canalEntrada>\r\n");
		request.append("<tipo_operacion>");
		request.append(Encrypt.encryptStringWS("preapertura"));
		request.append("</tipo_operacion>\r\n");
		request.append("<datos_Preapertura>\r\n");
		request.append("<origen>");
		request.append(Encrypt.encryptStringWS("02"));
		request.append("</origen>\r\n");
		request.append("<version>");
		StringBuilder version=new StringBuilder();
		version.append(Utility.getAppVersionCode(ctx));
		for (int i = 0; i < 3; i++) {
			if (version.length()<3) {
				version.insert(0, "0");
			}
			else
				break;
		}
		request.append(Encrypt.encryptStringWS(version.toString()));
		request.append("</version>\r\n");
		StringBuilder os= new StringBuilder();
		os.append(Build.VERSION.SDK_INT);

		for (int i = 0; i < 5; i++) {
			if (os.length()<5) {
				os.insert(0, "0");
			}
			else
				break;
		}

		request.append("<detalleorigen>");
		request.append(Encrypt.encryptStringWS(os.toString()));
		request.append("</detalleorigen>\r\n");
		request.append("<complemento>");
		SimpleDateFormat format= new SimpleDateFormat("ddMMyyyyhhmmss",Locale.getDefault());
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI().trim() + format.format(new Date())));
		request.append("</complemento>\r\n");
		request.append("<codigoIdentificacion>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getCodIdent())));
		request.append("</codigoIdentificacion>\r\n");
		request.append("<numeroIdentificacion>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getId().trim()));
		request.append("</numeroIdentificacion>\r\n");
		request.append("<nombreCliente>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getName().trim()));
		request.append("</nombreCliente>\r\n");
		request.append("<apellidoPaterno>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getLastName().trim()));
		request.append("</apellidoPaterno>\r\n");
		request.append("<apellidoMaterno>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getLasttName2().trim()));
		request.append("</apellidoMaterno>\r\n");
		request.append("<calle>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getStreet().trim()));
		request.append("</calle>\r\n");
		request.append("<numExterior>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getExternalNumber().trim()));
		request.append("</numExterior>\r\n");
		request.append("<numeroInterior>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getInternalNumber().trim()));
		request.append("</numeroInterior>\r\n");
		request.append("<cpCliente>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getZipCode().trim()));
		request.append("</cpCliente>\r\n");
		request.append("<coloniaCliente>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getColony().trim()));
		request.append("</coloniaCliente>\r\n");
		request.append("<estadoCliente>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getState().trim()));
		request.append("</estadoCliente>\r\n");
		request.append("<poblacionCliente>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getPoblacion().trim()));
		request.append("</poblacionCliente>\r\n");
		request.append("<codigoNacionalidad>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getCitizenship())));
		request.append("</codigoNacionalidad>\r\n");
		request.append("<fechaNacimiento>");
		request.append(Encrypt.encryptStringWS(Utility.formatDate(register.getCustomer().getBornDate(), "yyyy-MM-dd")));
		request.append("</fechaNacimiento>\r\n");
		request.append("<actividadGiroProfesion>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getExtraInfo().getEconomicActivity())));
		request.append("</actividadGiroProfesion>\r\n");
		request.append("<actividadEconomica>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getExtraInfo().getCategoryEconomicActivity())));
		request.append("</actividadEconomica>\r\n");
		request.append("<numeroLada>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getLada().trim()));
		request.append("</numeroLada>\r\n");
		request.append("<numeroTelefono>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getTelephone().trim()));
		request.append("</numeroTelefono>\r\n");
		request.append("<numeroExtension>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getAddress().getExtention().trim()));
		request.append("</numeroExtension>\r\n");
		request.append("<curpCliente>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getCurp().trim())));
		request.append("</curpCliente>\r\n");
		request.append("<rfcCliente>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getRfc().trim()));
		request.append("</rfcCliente>\r\n");
		request.append("<email>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getMail().trim()));
		request.append("</email>\r\n");
		request.append("<sexo>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getGender()==1 ? "F" : "M"));
		request.append("</sexo>\r\n");
		request.append("<estadoCivil>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getMaritalStatus())));
		request.append("</estadoCivil>\r\n");
		request.append("<sectorLaboral>");
		request.append(Encrypt.encryptStringWS("1"));
		request.append("</sectorLaboral>\r\n");
		request.append("<nombreNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getFiscalName().trim()));
		request.append("</nombreNegocio>\r\n");
		request.append("<paginaWeb>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getExtraInfo().getWebPage().trim()));
		request.append("</paginaWeb>\r\n");
		request.append("<rfcNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getRfc().trim()));
		request.append("</rfcNegocio>\r\n");
		request.append("<calleNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getStreet().trim()));
		request.append("</calleNegocio>\r\n");
		request.append("<numeroInteriorNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getInternalNumber().trim()));
		request.append("</numeroInteriorNegocio>\r\n");
		request.append("<numeroExteriorNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getExternalNumber().trim()));
		request.append("</numeroExteriorNegocio>\r\n");
		request.append("<municipioNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getPoblacion().trim()));
		request.append("</municipioNegocio>\r\n");
		request.append("<coloniaNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getColony().trim()));
		request.append("</coloniaNegocio>\r\n");
		request.append("<estadoNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getState().trim()));
		request.append("</estadoNegocio>\r\n");
		request.append("<cpNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getZipCode().trim()));
		request.append("</cpNegocio>\r\n");
		request.append("<ladaNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getLada().trim()));
		request.append("</ladaNegocio>\r\n");
		request.append("<telefonoNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getLada().trim()));
		request.append("</telefonoNegocio>\r\n");
		request.append("<numeroExtensionNegocio>");
		request.append(Encrypt.encryptStringWS(register.getFiscalData().getAddress().getExtention().length()==0 ? "0" : register.getFiscalData().getAddress().getExtention()));
		request.append("</numeroExtensionNegocio>\r\n");
		request.append("<giroNegocio>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getExtraInfo().getBusinessType())));
		request.append("</giroNegocio>\r\n");
		request.append("<actividadNegocio>");
		request.append(Encrypt.encryptStringWS(String.valueOf(register.getCustomer().getExtraInfo().getCategoryBusinessType())));
		request.append("</actividadNegocio>\r\n");
		request.append("<id_Usuario>");
		request.append(Encrypt.encryptStringWS(register.getCustomer().getCustomerId().trim()));
		request.append("</id_Usuario>\r\n");
		request.append("<modelo>");
		Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"model: "+ Build.MODEL);
		request.append(Encrypt.encryptStringWS(Build.MODEL));
		request.append("</modelo>\r\n");
		request.append("</datos_Preapertura>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");

		req= new SoapRequest<String>(REGISTER_BUSINESS_METHOD, REGISTER_BUSINESS_ACTION);
		req.setNameSpace(REGISTER_BUSINESS_NAMESPACE);
		req.setUrl(REGISTER_BUSINESS_URL);
		Parameter<String,String> param= new Parameter<String, String>(REGISTER_BUSINESS_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;	
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if(res==null){
				listener.sendStatus(ctx,EXCEPTION);
				return;
			}

			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			xml=Utility.fixSoapResponse(xml);


			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;

			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();

					break;
				case XmlPullParser.TEXT:
					if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
						operationCode= Encrypt.decryptStringWS(parser.getText());
					}
					else if (currentTag.equalsIgnoreCase(FOLIO_TAG)){
						folio=Encrypt.decryptStringWS(parser.getText());
					}
					else if (currentTag.equalsIgnoreCase(SYSTEM_ERROR_TAG)){
						systemError=Encrypt.decryptStringWS(parser.getText());
					}


					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			Message msg= new Message();
			if ("0".equals(operationCode)) {
				msg.setType(Message.OK);
				listener.sendMessage(ctx,msg, folio);
			}
			else{
				msg.setType(ERROR_SENDING_DATA);
				StringBuffer message= new StringBuffer(operationCode);
				message.append(" - ");
				message.append(systemError);
				listener.sendMessage(ctx,msg, message.toString());
			}
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"InterruptedException: " +e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"IOException: "+ e.getMessage());
			return;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"XmlPullParserException: "+ e.getMessage());
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,EXCEPTION);
			Logger.log(Logger.EXCEPTION, REGISTER_BUSINESS,"TimeoutException: "+ e.getMessage());
		} 
	}

}
