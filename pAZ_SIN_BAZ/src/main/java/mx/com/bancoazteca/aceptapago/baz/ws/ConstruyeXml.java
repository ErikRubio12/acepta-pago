package mx.com.bancoazteca.aceptapago.baz.ws;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


public class ConstruyeXml {
	//private String xml;
	private NumberFormat nmfLocal = NumberFormat.getInstance(Locale.ENGLISH);
	private DecimalFormat dec = (DecimalFormat) nmfLocal;
	private String formato = "###0.00;-###0.00";
	private boolean esFacial = false;
	private String fotoBase64;
	
	public ConstruyeXml(){
		dec.applyPattern(formato);
		
	}
	
	public String getFotoBase64() {
		return fotoBase64;
	}

	public void setFotoBase64(String fotoBase64) {
		this.fotoBase64 = fotoBase64;
	}

	public boolean isEsFacial() {
		return esFacial;
	}
	
	public void setEsFacial(boolean esFacial) {
		this.esFacial = esFacial;
	}
	
	/**
	 * Este metodo obtiene el ID Diario de la aplicación.
	 * 
	 * @return
	 */
	public String xmlFirmaAplicacion() {
		String xml=traeEncabezado()
				+ "<idservicio value=\"LOGIN\"/>"
				+ "<tipo_operacion value=\"APLICACION\"/>"
				+ "<aplicacion value=\"app_rma_iphone\" />"
				+ "<certificado cipher=\"DGl/ljFKaA7AvZDj/SZaILECRnYdvyHar9GUUKP9oZw=\"/>"
				//+ "<aplicacion value=\"app_android\" />"
				//+ "<certificado cipher=\"1Di2OrCvsCN6YOqnXCeCqA==\"/>"
				+ traePie();
		return  xml;
	}

	public String xmlFirmaAplicacionDesarrollo() {
		return traeEncabezado()
				+ "<idservicio value=\"LOGIN\"/>"
				+ "<tipo_operacion value=\"APLICACION\"/>"
				+ "<aplicacion value=\"app_rma_iphone\" />"
				+ "<certificado cipher=\"DGl/ljFKaA7AvZDj/SZaILECRnYdvyHar9GUUKP9oZw=\"/>"
				//+ "<aplicacion value=\"app_android\" />"
				//+ "<certificado cipher=\"1Di2OrCvsCN6YOqnXCeCqA==\"/>"
				+traePie();
	}
	
	

	public String xmlListadoCuentas(String sesion) {
		return traeEncabezado()
				+ "<idservicio value=\"SALDOS\"/>" + "<idsesion cipher=\""
				+ sesion + "\"/>" + "<comando value=\"EJECUCION\" />"
				+ traePie();
	}

	public String xmlRecuperaPasswordPaso1(String id, String tarjeta)
			throws InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idaplicacion cipher=\"" + id + "\"/>"
				+ "<idservicio value=\"RECUPERAR_PASSWORD\" /> "
				+ "<tarjeta_cuenta value=\"" + tarjeta + "\" /> "
				+ "<comando value=\"solicitud\" />" + "</request>"
				+ "</eservices>" + "</bancoazteca>";
	}

	

	public String xmlRecuperaPasswordPaso3(String sesion,
			String correoElectronico, String numero, String compania,
			String aplicacion) throws InvalidKeyException,
			NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idsesion cipher=\"" + sesion + "\"/>"
				+ "<idaplicacion cipher=\"" + aplicacion + "\"/>"
				+ "<idservicio value=\"RECUPERAR_PASSWORD\" /> "
				+ "<comando value=\"actualizacion\" />"
				+ "<correo_electronico value=\"" + correoElectronico + "\"/>"
				+ "<numero_celular value=\"" + numero + "\"/>"
				+ "<compania_celular value=\"" + compania + "\"/>"
				+ "</request>" + "</eservices>" + "</bancoazteca>";
	}

	public String xmlRecuperaPasswordActualizacion(String sesion,
			String correoElectronico, String numero, String compania,
			String aplicacion) throws InvalidKeyException,
			NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idsesion cipher=\"" + sesion + "\"/>"
				+ "<idaplicacion cipher=\"" + aplicacion + "\"/>"
				+ "<idservicio value=\"recuperar_password_opciones\" /> "
				+ "<comando value=\"ACTUALIZACION\" />"
				+ "<correo_electronico value=\"" + correoElectronico + "\"/>"
				+ "<numero_celular value=\"" + numero + "\"/>"
				+ "<compania_celular value=\"" + compania + "\"/>"
				+ "</request>" + "</eservices>" + "</bancoazteca>";
	}
	public String xmlRegistraCuentaPaso1(String id, String tarjeta)
			throws InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idaplicacion cipher=\"" + id + "\"/>"
				+ "<idservicio value=\"ALTAS_USUARIOS\" /> "
				+ "<cuenta_cargo value=\"" + tarjeta + "\" /> "
				+ "<comando value=\"solicitud\" />" + "</request>"
				+ "</eservices>" + "</bancoazteca>";
	}

	

	public String xmlRegistraCuentaContrato(String id, String sesion)
			throws InvalidKeyException, NoSuchAlgorithmException,
			InvalidKeySpecException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idaplicacion cipher=\"" + id + "\"/>"
				+ "<idservicio value=\"ALTAS_USUARIOS\" /> "
				+ "<idsesion cipher=\"" + sesion + "\"/>"
				+ "<comando value=\"contrato\" />" + "</request>"
				+ "</eservices>" + "</bancoazteca>";
	}
	
	public String xmlRegistraCuentaPaso3(String sesion,
			String correoElectronico, String alias, String pregunta,
			String respuesta) throws InvalidKeyException,
			NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idsesion cipher=\"" + sesion + "\"/>"
				+ "<idservicio value=\"ALTAS_USUARIOS\" /> "
				+ "<comando value=\"REGISTRAR\" />" + "<alias value=\"" + alias
				+ "\"/>" + "<correo_electronico value=\"" + correoElectronico
				+ "\"/>" + "<pregunta value=\"" + pregunta + "\"/>"
				+ "<respuesta value=\"" + respuesta + "\"/>" + "</request>"
				+ "</eservices>" + "</bancoazteca>";
	}

	
	public String traeEncabezado() {
		return "<?xml version='1.0' encoding='iso-8859-1'?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>";
	}

	public String traePie() {
		return "</request>" + "</eservices>" + "</bancoazteca>";
	}

	public String xmlSolicitudColonias(String idAplicacion, String cp){
		return traeEncabezado()
				+"<postal value=\"" + cp + "\"/>"
				+"<comando value=\"colonias\" />"
				+"<idservicio value=\"preapertura_debito\" />"
				+"<idaplicacion cipher=\"" + idAplicacion + "\"/>"
		+traePie();
	}		
	
	

	

	public String xmlDesbloqueaPasswordActualizacion(String sesion,
			String correoElectronico, String numero, String compania,
			String aplicacion) throws InvalidKeyException,
			NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		return "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"
				+ "<bancoazteca>" + "<eservices>" + "<request>"
				+ "<idsesion cipher=\"" + sesion + "\"/>"
				+ "<idaplicacion cipher=\"" + aplicacion + "\"/>"
				+ "<idservicio value=\"DESBLOQUEAR_USUARIO\" /> "
				+ "<comando value=\"ACTUALIZACION\" />"
				+ "<correo_electronico value=\"" + correoElectronico + "\"/>"
				+ "<numero_celular value=\"" + numero + "\"/>"
				+ "<compania_celular value=\"" + compania + "\"/>"
				+ "</request>" + "</eservices>" + "</bancoazteca>";
	}

	
	
	

	
	
	public String xmlTraeInformacionGeneral(String sesion) {
		String xml =  traeEncabezado()
				+ "<idsesion cipher=\"" + sesion + "\"/>"
				+ "<idservicio value=\"info_general_usuario\" /> "
				+ "<comando value=\"EJECUCION\" />"
				+traePie();
		return xml;
	}

	
	public String seleccionaSeguridad(){
		if(esFacial){
			return "<opcion_seguridad value=\"FACIAL\" />" +
					"<facial_seguridad value=\"" + fotoBase64 + "\"/>";
			
		} else {
			return "<opcion_seguridad value=\"TOKEN\" />";
		}
	}

	

}
