package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeoutException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import android.content.Context;

import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class CheckDevice extends BaseService<Message,String> {
	
	private static final String QUERY_DEVICE_METHOD ="BusinessToBusinessServiceConsultarTerminalIphone";
	private static final String QUERY_DEVICE_ACTION = null;
	private static final String QUERY_DEVICE_NAMESPACE = "http://service.btb.com";
	private static final String QUERY_DEVICE_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String QUERY_DEVICE_PARAM = "xml";
	private static final String CHECK_DEVICE = "Check device";
	private String folio;
	private String rfc;
	public CheckDevice(Context ctx,MessageListener<Message, String> listener,String folio,String rfc) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		this.rfc=rfc;
		this.folio=folio;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		String idDevice="";
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("CTermIphone"));
		request.append("</idservicio>");
		request.append("<idComercio>");
		request.append(Encrypt.encryptStringWS(folio));
		request.append("</idComercio>");
		request.append("<rfc>");
		request.append(Encrypt.encryptStringWS(rfc));
		request.append("</rfc>");
		request.append("<imei>");
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI()));
		request.append("</imei>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		SoapRequest<String> req= new SoapRequest<String>(QUERY_DEVICE_METHOD, QUERY_DEVICE_ACTION);
		req.setNameSpace(QUERY_DEVICE_NAMESPACE);
		req.setUrl(QUERY_DEVICE_URL);
		Parameter<String,String> param= new Parameter<String, String>(QUERY_DEVICE_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;
		
		
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if(res==null || res.getResponse()==null){
				Logger.log(Logger.EXCEPTION, CHECK_DEVICE,"Error de conexion");
				listener.sendStatus(ctx,Message.EXCEPTION);
				return;
			}
			Document dom= Utility.createDom(res.getResponse().toString());
			Node node=dom.getElementsByTagName("codigo_operacion").item(0);
			String result=node.getChildNodes().item(0).getNodeValue();
			result=Encrypt.decryptStringWS(result);
			if("0".equals(result)){
				node=dom.getElementsByTagName("idterminal").item(0);
				idDevice=node.getChildNodes().item(0).getNodeValue();
				idDevice=Encrypt.decryptStringWS(idDevice);
				Message msg= new Message();
				msg.setType(Message.OK);
				node=dom.getElementsByTagName("afiliacion").item(0);
				String afiliacion=node.getChildNodes().item(0).getNodeValue();
				afiliacion=Encrypt.decryptStringWS(afiliacion);
				listener.sendMessage(ctx,msg, idDevice,afiliacion);
			}
			else
				listener.sendStatus(ctx,Message.ERROR);
			Connections.closeSoapConnection(res);
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, CHECK_DEVICE,"InterruptedIOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		}catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, CHECK_DEVICE,"IOException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, CHECK_DEVICE,"TimeoutException: "+ e.getMessage());
			listener.sendStatus(ctx,Message.EXCEPTION);
		} 
		
	}

}
