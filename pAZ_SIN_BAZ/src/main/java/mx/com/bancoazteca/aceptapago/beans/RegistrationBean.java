package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;
import mx.com.bancoazteca.beans.FiscalDataBean;


public class RegistrationBean implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public static final String REGISTER = "register";
	private FiscalDataBean fiscalData;
	private CustomerBean customer;
	private Beneficiaries beneficiaries;
	private AdditionalInfo additionalInfo;

	private String keyRegister;

	public FiscalDataBean getFiscalData() {
		return fiscalData;
	}
	public void setFiscalData(FiscalDataBean fiscalData) {
		this.fiscalData = fiscalData;
	}

	public void setCustomer(CustomerBean customer) {
		this.customer = customer;
	}
	public CustomerBean getCustomer() {
		return customer;
	}
	public void setBeneficiaries(Beneficiaries beneficiaries) {
		this.beneficiaries = beneficiaries;
	}
	public Beneficiaries getBeneficiaries() {
		return beneficiaries;
	}
	
	public void setAdditionalInfo(AdditionalInfo additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public AdditionalInfo getAdditionalInfo() {
		return additionalInfo;
	}

	public String getKeyRegister() {
		return keyRegister;
	}

	public void setKeyRegister(String keyRegister) {
		this.keyRegister = keyRegister;
	}
}
