package mx.com.bancoazteca.aceptapago.baz.entidades;

public class EntidadComun {

	private int codigoOperacion;
	private String mensaje;
	private String descripcion;
	private String folioOperacionServicio;

	public int getCodigoOperacion() {
		return codigoOperacion;
	}

	public void setCodigoOperacion(int codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFolioOperacionServicio() {
		return folioOperacionServicio;
	}

	public void setFolioOperacionServicio(String folioOperacionServicio) {
		this.folioOperacionServicio = folioOperacionServicio;
	}

}
