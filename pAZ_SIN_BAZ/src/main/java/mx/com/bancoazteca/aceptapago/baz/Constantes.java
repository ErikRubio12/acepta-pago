package mx.com.bancoazteca.aceptapago.baz;

import android.os.Environment;

//import com.baz.rma.R;

//public class Constantes {
//	public static String URL_SERVICIO = "http://10.63.85.151:9583/eServiceWeb/services/EService";
//	public static String NAMESPACE_SERVICIO = "http://10.63.85.151:9583/eServiceWeb/services/EService";
//	public static String METOD_NAME = "eServices";
//	public static String ACTION = ""; 
//}

public class Constantes {
	public static String idSesion;
	public static long ultimaConsulta;
	//Produccion
	public static String URL_SERVICIO = "https://www.bazm.mx/services/proxy.de";
	//Desarrollo
	//public static String URL_SERVICIO = "http://10.50.70.62:10080/eServiceWeb/proxy.de";
	public static String NAMESPACE_SERVICIO = "http://ws.baz.com/";
	public static String METOD_NAME = "serviceAndroid";
	public static String ACTION = "";
	
	public static String URL_FOTO_CLIENTE = "http://200.38.122.100/WSFotoRMA/rma";
	
//	
//	public static String URL_SERVICIO = "http://10.51.209.71/Ebanking/RedMovilAztecaPort";
//	public static String NAMESPACE_SERVICIO = "http://ws.baz.com/";
//	public static String METOD_NAME = "serviceAndroid";
//	public static String ACTION = "";
	//Desarrollo
	public static String URL_SERVICIO_DESARROLLO = "http://200.38.122.100/Ebanking/RedMovilAztecaPort";
//	public static String URL_SERVICIO_DESARROLLO = "http://10.51.209.71/Ebanking/RedMovilAztecaPort";
	public static String NAMESPACE_SERVICIO_DESARROLLO = "http://ws.baz.com/";
	public static String METOD_NAME_DESARROLLO = "validaNIPaNdroid";
	public static String ACTION_DESARROLLO = "";
	

	
	public static int FOTO_IFE = 1001;
	public static int FOTO_COMPROBANTE = 1002;
	public static int FOTO_ROSTRO = 1003;
	public static int LISTA_CLIENTES = 1004;
	public static int ABRE_APERTURA = 1005;
	public static int FOTO_ROSTRO_ANTERIORES = 1006;
	
	public static String DATA_PATH = Environment
			.getExternalStorageDirectory().toString() + "/BazMovilApertura/";
}
