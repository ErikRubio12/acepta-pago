package mx.com.bancoazteca.aceptapago.gui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.datecs.audioreader.AudioReader;
import com.datecs.audioreader.AudioReader.Battery;
import com.datecs.audioreader.AudioReader.CardInfo;
import com.datecs.audioreader.AudioReader.CardStatusResponse;
import com.datecs.audioreader.AudioReader.FinancialCard;
import com.datecs.audioreader.AudioReader.MessageProcessingResults;
import com.datecs.audioreader.AudioReaderException;
import com.datecs.audioreader.AudioReaderManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

import mx.com.bancoazteca.IntentFilters;
import mx.com.bancoazteca.beans.CardBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.hw.Camera;
import mx.com.bancoazteca.hw.HeadsetDetector;
import mx.com.bancoazteca.hw.HeadsetListener;
import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.hw.LocationThread;
import mx.com.bancoazteca.hw.emvreader.EMVProcessorHelper;
import mx.com.bancoazteca.hw.emvreader.EMVTags;
import mx.com.bancoazteca.hw.emvreader.EmvProcessor;
import mx.com.bancoazteca.hw.emvreader.HexUtil;
import mx.com.bancoazteca.hw.emvreader.backend.EMVProcessor;
import mx.com.bancoazteca.hw.emvreader.backend.EMVProcessor.TransactionResponse;
import mx.com.bancoazteca.hw.emvreader.backend.ProcessingResult;
import mx.com.bancoazteca.hw.emvreader.emv.EMVResponse;
import mx.com.bancoazteca.hw.emvreader.emv.PrivateTags;
import mx.com.bancoazteca.hw.emvreader.tlv.BerTlv;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.Configuration;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnResponse;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnTicketBean;
import mx.com.bancoazteca.aceptapago.db.daos.TicketsDao;
import mx.com.bancoazteca.aceptapago.gui.AlertDialog.ErrorDeviceDialog;
import mx.com.bancoazteca.aceptapago.jobs.SaleJob;
import mx.com.bancoazteca.aceptapago.jobs.SaleReturnJob;
import mx.com.bancoazteca.aceptapago.jobs.UploadImageTicketJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.ConfirmationDialog;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class SaleScreen extends BaseActivity implements HeadsetListener{
	public static final int HOME_RESULT=323;
	private static final String TAG_SALE_SCREEN = "Sale screen";
	private static final String OPERATION_TYPE_SALE_9C="00";
	private static final String OPERATION_TYPE_REFUND_9C="20";
	private FiscalDataBean fiscalData;
	private CustomerBean customer;
	private TicketsDao dao;
	private GridLayout keyboard;
	private ImageButton cameraBtn;
	private EditText description;
	private final static String MX ="MX$ ";
	private static final int GPS_SETTINGS = 11;
	protected static final int POPUP_ERROR = 913;
	protected static final int POPUP_NO_LOCATION = 911;
	private static final int POPUP_PENDING_TICKETS = 100;
	protected static final int POPUP_ASK_SWIPED = 101;
	private static final int POPUP_AMEX_NOT_ALLOWED = 21;

	private static final int POPUP_CANCEL_OK = 24;
	protected static final int SALE_CODE = 16;
	private SaleBean sale;
	public final static int TAKE_SHOT_CODE=10;
	private List<SaleReturnTicketBean> pendingTickets;
	private boolean isCancellation=false;
	//private ImageButton settings;
	private static final long MAX_AMOUNT = 99999999999L;
	private TextView statusText;
	private EditText amount;
	private final static int OP_CLEAN=R.id.calc16;
	private OperationsBean operationSelected;
	private TextView fieldSelected;
	private HeadsetDetector detector;
	private AudioReader reader = null;
	private EmvProcessor callback;
	private CardBean card;
	private static final int WAIT_CARD_TIME = 20000;	
	private static int FALL_BACK_COUNTER=0;
	private final static int MAX_FALL_BACK_COUNTER=2;
	private final static int RETURN_FALL_BACK=111;
	private final static int RETURN_READ_CARD_OK=100;
	private final static int RETURN_READ_CARD_WRONG=103;
	private final static int ABORTED_OPERATION=32769;
	public static final int SETTINGS_SALE_CODE = 12;
	public static final int SETTINGS_CODE = 121;
	private String tmpMsg;
	private String folioPreap;
	private LoginBean login;
//	private TextView startReadingBtn;
	private boolean isCameraPressed=false;
	private ImageView swipeCardImage;
	private TextView textSaleCancel;
	private int imgBattery;
	private Location location;
	private String afiliacion;
	private SaleReturnResponse saleReturnResponse=null;
	public final static  String RETURN_RESPONSE="response";
	private Toolbar mtoolbar;
	private boolean isEnableMenu =false;
	private DrawerLayout drawerLayout;
	private NavigationView navigationView;
	private LinearLayout txtTitleVenta;
	private ActionBarDrawerToggle drawerToggle;
	private boolean isRefund;
	private String operationTypeTag9C;
	private String typeException;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_read_card, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id=item.getItemId();
		Log.i(TAG_SALE_SCREEN, "Menu presed" + id);
		switch (id){
			case R.id.btn_menu_read_card:
				startReadCard();
				return true;

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (isEnableMenu) {
			menu.getItem(0).setEnabled(true);
		}else{
			menu.getItem(0).setEnabled(false);



		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.sale_screen);
		mtoolbar=(Toolbar) findViewById(R.id.toolbar);
		drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
		navigationView=(NavigationView)findViewById(R.id.navigation_view);
		cameraBtn=(ImageButton) findViewById(R.id.cameraBtn);
		keyboard=(GridLayout) findViewById(R.id.keyboardView);
		statusText=(TextView) findViewById(R.id.statusText);
		amount=(EditText) findViewById(R.id.amountEdit);
		description=(EditText) findViewById(R.id.description);
		swipeCardImage=(ImageView) findViewById(R.id.swipeCardImage);
		swipeCardImage.setVisibility(View.GONE);
		textSaleCancel=(TextView) findViewById(R.id.textSaleCancel);
		description.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				description.setCursorVisible(true);
				keyboard.setVisibility(View.GONE);
			}
		});

		amount.setText("");
		fieldSelected=amount;
		amount.requestFocus();
		description.setCursorVisible(false);
		txtTitleVenta=(LinearLayout) findViewById(R.id.txt_title_venta);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar=getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDefaultDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);

		}
		navigationView.setItemIconTintList(null);
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem) {
				if (menuItem.isChecked()) {
					menuItem.setChecked(false);
				} else {
					menuItem.setChecked(true);
				}
				drawerLayout.closeDrawers();
				ArrayList<Serializable> params = new ArrayList<Serializable>();
				ArrayList<String> paramNames = new ArrayList<String>();
				params.add(fiscalData);
				params.add(customer);
				paramNames.add(FiscalDataBean.FISCAL_DATA);
				paramNames.add(CustomerBean.CUSTOMER);
				Log.i(TAG_SALE_SCREEN, "Customer " + customer);
				Log.i(TAG_SALE_SCREEN, "Fiscal data " + fiscalData);
				switch (menuItem.getItemId()) {
					case R.id.dw_ajustes:
						goForward(ChangesScreen.class, getApplicationContext(), params, paramNames);
						return true;
					case R.id.dw_operaciones:
						goForward(OperationsDetailScreen.class, getApplicationContext(), params, paramNames);
						return true;
					case R.id.dw_clientes:
						intent.putExtra(CustomerBean.FOLIO_PREAPERTURA, customer.getFolioPreap());
						intent.putExtra(CustomerBean.RFC, fiscalData.getRfc());
						goForward(Clients.class, getApplicationContext());
						return true;
					case R.id.dw_ayuda:
						intent.putExtra("logged", true);
						intent.putExtra(CustomerBean.FOLIO_PREAPERTURA, customer.getFolioPreap());
						intent.putExtra(FiscalDataBean.BUSINESS_RFC, fiscalData.getRfc());
						intent.putExtra(FiscalDataBean.FISCAL_NAME, fiscalData.getFiscalName());
						goForward(HelpScreen.class, getApplicationContext());
						return true;

				}
				return true;
			}
		});

		drawerToggle=new ActionBarDrawerToggle(this,drawerLayout,mtoolbar,R.string.open,R.string.close){
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}
		};
		drawerLayout.setDrawerListener(drawerToggle);
		drawerToggle.syncState();



	}
	


	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params != null){			
			fiscalData=(FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
			customer= (CustomerBean) params.getSerializable(CustomerBean.CUSTOMER);
			isCancellation=params.getBoolean(OperationsDetailScreen.CANCELLATION,false);
			isRefund=params.getBoolean(OperationsDetailScreen.REFUND,false);
			login=(LoginBean) params.getSerializable(LoginBean.LOGIN);
			folioPreap=(String) params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
			operationSelected=(OperationsBean) params.getSerializable(OperationsBean.OPERATION);
			statusText.setText(R.string.device_unplugged);
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," operacion selected: "+ operationSelected);
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, (isCancellation ? "Va a ser cancelacion" : "Va a ser venta   ") + "datos fiscales = "+ fiscalData+ " folioPreap = "+ folioPreap + " operacion = "+ operationSelected+ /*" tarjeta : " + card + */" login: " + login + " ruta imagen: " + SaleBean.getImage() );
			if (isCancellation){
				drawerToggle.setDrawerIndicatorEnabled(false);
				getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
				drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						finish();
					}
				});

				if(isRefund){
					textSaleCancel.setText(R.string.refund);
					description.setHint(R.string.refund);
				}
				else{
					textSaleCancel.setText(R.string.cancellation);
					description.setHint(R.string.cancellation);
				}
				
				description.setEnabled(false);
				
//				settings.setVisibility(View.GONE);
				amount.setText(MX + OperationsDetailScreen.changeMonto(operationSelected.getAmount()));
				//Log.i(TAG_SALE_SCREEN_P, "++++++" +operationSelected.getAmount());
				amount.setEnabled(false);			
				amount.setFocusable(false);
				amount.setFocusableInTouchMode(false);
				keyboard.setVisibility(View.GONE);
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.warningAmount));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);

			}
			else{
				drawerToggle.setDrawerIndicatorEnabled(true);
				getSupportActionBar().setDisplayHomeAsUpEnabled(false);
				description.setHint(R.string.description);
				description.setEnabled(true);
				textSaleCancel.setText(R.string.sale);

				dao= new TicketsDao(this);
				pendingTickets= dao.querySalesReturnTickets(customer.getLogin().getUser());
				if(pendingTickets.size()>0){
					Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "tickets pendientes: " + pendingTickets.size());
					ConfirmationDialog confirmationDialog=ConfirmationDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.pendingTockets));
					confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							for(int i=0; i<pendingTickets.size(); i++)
								Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, pendingTickets.get(i).toString());
							backgroundThread= new Thread(new UploadImageTicketJob(SaleScreen.this,null,pendingTickets));
							backgroundThread.start();
						}
					});
					confirmationDialog.show(getSupportFragmentManager(),""+POPUP_PENDING_TICKETS);
				}
				else{
					Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"No hay tickets pendientes");
				}
			}
		}
		amount.requestFocus();
		registerDetector();
	}

	public void errorChipPopup(){
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "reiniciar lector");
		if(detector != null && detector.isDevicePlugged())
			statusText.setText(R.string.device_plugged);
		else
			statusText.setText(R.string.device_unplugged);
		isEnableMenu =true;
		invalidateOptionsMenu();
	}

	private boolean isEmptyFields(){
		String amountStr=amount.getText().toString().replace(MX, "");
		float amountInt=0;
		if(amountStr.trim().length()==1 && amountStr.trim().equalsIgnoreCase(".")){
			popupMsg=R.string.invalidAmountPopUp;
			popupTitle=R.string.popUpError;
			return false;
		}
			
		if(amountStr.trim().length()>0)
			amountInt=Float.parseFloat(amountStr.trim());
		
		if(amount.getText().toString().replace(MX, "").length()==0 ){ 	 		
			popupTitle=R.string.popUpMessage;
			popupMsg=R.string.amountEmpty;
			return false;		
		}
		else if(amountInt<5.00){
			popupMsg=R.string.invalidAmountPopUp;
			popupTitle=R.string.popUpError;
			return false;
		}			
		else {
			if(! isCancellation){
				sale= new SaleBean();
				String amountString=amount.getText().toString().replace(MX, "");
				if(amountString.indexOf("$")>=0)
					amountString=amountString.replace("$", "");
				int pointIndex=amountString.indexOf(".");
				if(pointIndex>=0){
					if(amountString.substring(pointIndex +1).length()> 1)
						amountString=amountString.substring(0, pointIndex) + amountString.substring(pointIndex +1);
					else if(amountString.substring(pointIndex +1).length()==1)
						amountString=amountString.substring(0, pointIndex) + amountString.substring(pointIndex +1) + "0";
					else
						amountString=amountString.substring(0, pointIndex)+ "00";
				}
					
				else
					amountString=amountString + "00";
				sale.setDescription(description.getText().toString());
				sale.setAmount(amountString.length()==0 ? 0:Long.parseLong(amountString));
				
				if(sale.getAmount()>0 && sale.getAmount() <=MAX_AMOUNT)
					return true;
				else if(sale.getAmount()>MAX_AMOUNT){
					popupMsg=R.string.invalidMaxAmount;
					popupTitle=R.string.popUpError;
					return false;
				}
				
				return true;
			}
			else 
				return true;
		}
	}
	private OnClickListener saleListener= new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(!validForm()) {
				SimpleDialog dialog = SimpleDialog.newInstance(getString(popupTitle), getString(popupMsg));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
			}else{
				location=LocationService.getMostRecentLocation();
				startSaleThread();
			}
											
		}
	};
	private void startSaleThread(){		
		if(location!=null){
			Intent service = new Intent(SaleScreen.this, LocationService.class);
			stopService(service);
		}
		if(!isCancellation){
			popupTitle=R.string.popUpMessage;
			popupMsg=R.string.makingSale;
			showDialog(POPUP_MESSAGES_PROGRESS);
			/*Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"folio preap : "+ customer.getFolioPreap());
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," rfc : "+ fiscalData.getRfc());
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," tarjeta : "+ sale.getCard());
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," monto : " + sale.getAmount());
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," ubicacion: "+ location);*/
						
			backgroundThread= new Thread(new SaleJob(this,new HandlerSaleCancellation(),customer.getFolioPreap(),fiscalData.getRfc(),sale.getCard(),sale.getAmount(),location,customer.getDevice()));
			backgroundThread.start();
		} else{
			String strCardOperation=operationSelected.getCardNUmber().substring(operationSelected.getCardNUmber().length() - 4);
			String strCard=card.getMaskedPan().substring(card.getMaskedPan().length()-4);
			double operationAmount= Double.valueOf(OperationsDetailScreen.changeMonto(operationSelected.getAmount()));
			double amountSelected= Double.valueOf(amount.getText().toString().replace(MX, ""));
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "tarjeta de operacion: " + strCardOperation + " tarjeta obtenida: " + strCard);
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN_P, "monto operacion: " + operationAmount + " monto obtenido: " + amountSelected);
			
			if(!strCard.equalsIgnoreCase(strCardOperation) || amountSelected!=operationAmount){
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.differentCards));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
//				startReadingBtn.setVisibility(View.VISIBLE);
				isEnableMenu=true;
				invalidateOptionsMenu();
				FALL_BACK_COUNTER=0;
				return;
			}
			popupTitle=R.string.popUpMessage;
			if(isRefund)
				popupMsg=R.string.makingRefund;
			else
				popupMsg=R.string.makingreturn;
			
			showDialog(POPUP_MESSAGES_PROGRESS);
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "datos fiscales = "+ fiscalData+ "\nfolioPreap = "+ folioPreap );
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "operacion = "+ operationSelected+ " tarjeta : " + card);
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "devolucion = "+ isRefund);
			backgroundThread= new Thread(new SaleReturnJob(card,operationSelected,folioPreap,fiscalData,new HandlerSaleCancellation(),location,isRefund,this));
			backgroundThread.start();
		}
	}
	
	protected boolean validForm() {
		// TODO Auto-generated method stub
		boolean answer=true;
		
		if(!isCancellation){
			if ( sale != null && sale.getCard()==null) {
				popupMsg=R.string.readCard;
				popupTitle=R.string.popUpError;
				return false;
			}
			if (sale.getCard().getMaskedPan()==null  || sale.getCard().getMaskedPan().length()==0 ){
				popupMsg=R.string.emptyFields;
				popupTitle=R.string.popUpError;
			}
			
			else if(sale.getCard().getMaskedPan().length()  < CardBean.CARD_NUMBER_LENGTH){
				popupMsg=R.string.invalidCardNumberPopUpMesage;
				popupTitle=R.string.popUpError;
			}			
			else
				return answer;
			return !answer;
		}
		else{
			if ( operationSelected != null && card==null) {
				popupMsg=R.string.readCard;
				popupTitle=R.string.popUpError;
				return false;
			}
			
			
			if (card.getMaskedPan()==null  || card.getMaskedPan().length()==0){
				popupMsg=R.string.emptyFields;
				popupTitle=R.string.popUpError;
			}
			
			else if(card.getMaskedPan().length()  < CardBean.CARD_NUMBER_LENGTH){
				popupMsg=R.string.invalidCardNumberPopUpMesage;
				popupTitle=R.string.popUpError;
			}
			else
				return answer;
			return !answer;
			
		}
		
	}
	@Override
	public void release() {
		// TODO Auto-generated method stub
		super.release();
		if(SaleBean.getImage()!=null){
			File f=FileManager.openFile(this, SaleBean.getImage(), FileManager.MODE_APPEND);
			if(f!= null && f.exists()){
				f.delete();
				Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "se elimino la imagen del producto en onBackPressed");
				SaleBean.setImage(null);
			}
		}
		if(backgroundThread != null && backgroundThread.isAlive())
			backgroundThread.interrupt();
		FALL_BACK_COUNTER=0;

		if(reader != null){
			reader.close();	
			reader=null;
		}
		unregisterDetector();
	}
	
	private void loadChipCard() {
		// TODO Auto-generated method stub
		try {
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Chip card: ");	
			reader = AudioReaderManager.getReader(this);

			if(reader==null){
				Logger.log(Logger.EXCEPTION, TAG_SALE_SCREEN,"Error al obtener el lector ");
				popupMsg=R.string.chipReaderError;
				popupTitle=R.string.popUpError;
				if(!isFinishing()){
					ErrorDeviceDialog errorDeviceDialog=ErrorDeviceDialog.newInstance();
					errorDeviceDialog.setmAcceptListener(new ErrorDeviceDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					errorDeviceDialog.show(getSupportFragmentManager(), getString(R.string.exceptionChipDevice));
				}
			}
			if(backgroundThread != null && backgroundThread.isAlive())
				backgroundThread.interrupt();
			backgroundThread = new Thread(new Runnable() {
				public void run() {
					Looper.prepare();
					long startTime = System.currentTimeMillis();   
					try {
						callback= new EmvProcessor(SaleScreen.this);
						reader.powerOn();
						if(checkDeviceInfo())
							runTransaccion();
						
					} catch (IOException e) {
						e.printStackTrace();
						Log.i("SALE SCREEN !", "Message " + e.getMessage());
						typeException=e.getMessage();
						StringWriter sw = new StringWriter();
						e.printStackTrace(new PrintWriter(sw));
						String stacktrace = sw.toString();       
						Logger.log(Logger.EXCEPTION, TAG_SALE_SCREEN, "CRITICAL ERROR: " + stacktrace + "\n" + e.getMessage());
						if(!isFinishing()){
							runOnUiThread(new Runnable() {
								public void run() {
									if (typeException.equalsIgnoreCase(getString(R.string.exceptionChipCard))){
										SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.card_not_present_error));
									dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
										@Override
										public void onAccept(DialogInterface dialog) {
											errorChipPopup();
										}
									});
									dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
									}else if (typeException.equalsIgnoreCase(getString(R.string.exceptionChipDevice))){
										ErrorDeviceDialog errorDeviceDialog=ErrorDeviceDialog.newInstance();
										errorDeviceDialog.setmAcceptListener(new ErrorDeviceDialog.AcceptListener() {
											@Override
											public void onAccept(DialogInterface dialog) {
												errorChipPopup();
											}
										});
										errorDeviceDialog.show(getSupportFragmentManager(),getString(R.string.exceptionChipDevice));
									}else{
										SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.chipReaderError));
										dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
											@Override
											public void onAccept(DialogInterface dialog) {
												errorChipPopup();
											}
										});
										dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
									}




								}
							});
						}
					}catch (Exception e) {
						// TODO: handle exception
						Logger.log(Logger.EXCEPTION, TAG_SALE_SCREEN, "Otra excepcion: " + e.getMessage());
						e.printStackTrace();
						popupMsg=R.string.chipReaderError;
						if(!isFinishing()){
							runOnUiThread(new Runnable() {
								public void run() {
									ErrorDeviceDialog errorDeviceDialog=ErrorDeviceDialog.newInstance();
									errorDeviceDialog.setmAcceptListener(new ErrorDeviceDialog.AcceptListener() {
										@Override
										public void onAccept(DialogInterface dialog) {
											errorChipPopup();
										}
									});
									errorDeviceDialog.show(getSupportFragmentManager(), getString(R.string.exceptionChipDevice));
								}
							});
						}

					}
					finally{
						if(reader != null){
							reader.close();
						}
					}

					long endTime = System.currentTimeMillis() - startTime;
					Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"# Finish for " + (endTime / 1000) + "." + ((endTime % 1000) / 100) + "s\n");
					Looper.loop();
				}
			});
			backgroundThread.start();
		} catch (IOException e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String stacktrace = sw.toString();
			Logger.log(Logger.EXCEPTION, TAG_SALE_SCREEN,"CRITICAL ERROR 2: " + stacktrace + "\n" + e.getMessage());
			if(reader!= null)
				reader.close();
			popupMsg=R.string.chipReaderError;
			popupTitle=R.string.popUpError;
			ErrorDeviceDialog errorDeviceDialog=ErrorDeviceDialog.newInstance();
			errorDeviceDialog.setmAcceptListener(new ErrorDeviceDialog.AcceptListener() {
				@Override
				public void onAccept(DialogInterface dialog) {
					errorChipPopup();
				}
			});
			errorDeviceDialog.show(getSupportFragmentManager(), getString(R.string.exceptionChipDevice));

		}
	}
	private boolean checkDeviceInfo() throws AudioReaderException, IOException{
		if(reader !=null){

			//------------------------------------------------------------------------
			Battery battery= reader.getBattery();
			if(battery != null){
				Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "Bateria: "+battery.level);
				imgBattery=R.drawable.bateria1_0;
				if(battery.level >=0 && battery.level<=25)
					imgBattery=R.drawable.bateria1_0;
				else if(battery.level > 25 && battery.level<=50)
					imgBattery=R.drawable.bateria1_50;
				else if(battery.level > 50 && battery.level<=80)
					imgBattery=R.drawable.bateria1_75;
				else if(battery.level > 80 )
					imgBattery=R.drawable.bateria1_100;

				statusText.post(new Runnable() {
					@Override
					public void run() {
						statusText.setText(R.string.chipDetected);
						statusText.setCompoundDrawablesWithIntrinsicBounds(0, 0,imgBattery , 0);
					}
				});
			}
			return true;
			//-------------------------------------------------------------------------
		}else{
			return false;
		}
	}
	private void unregisterDetector(){
		if (detector != null){
			unregisterReceiver(this.detector);
			detector=null;
		}
	}
	private void registerDetector(){
		if(detector == null){
			this.detector = new HeadsetDetector(this);
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(IntentFilters.Actions.HEADSET_PLUG);
			registerReceiver(this.detector, intentFilter);
		}
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("Msg", tmpMsg);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		tmpMsg=savedInstanceState.getString("Msg");
	}


	private void startReadCard(){
		LocationManager manager=Utility.getLocationManager(this);
		if(!isEmptyFields()) {
			SimpleDialog dialog = SimpleDialog.newInstance(getString(popupTitle), getString(popupMsg));
			dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
		}else if(!FileManager.isSdPresent()){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.sdCardNotPresent));
			dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
		}

		else if(!manager.isProviderEnabled(LocationThread.GPS) && !manager.isProviderEnabled(LocationThread.NETWORK)) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.gpsAndAsistedDisabled));
			dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
				@Override
				public void onAccept(DialogInterface dialog) {
					Utility.gpsSettings(SaleScreen.this,GPS_SETTINGS);
				}
			});
			dialog.show(getSupportFragmentManager(), "" + POPUP_NO_LOCATION);
		}else{
			Intent service = new Intent(this, LocationService.class);
			startService(service);
			disableButtons();
			isEnableMenu=false;
			invalidateOptionsMenu();
			statusText.setClickable(false);
			//statusText.setBackgroundResource(R.drawable.bg_dispositivo);
			//statusText.setTextColor(Color.WHITE);
			statusText.setVisibility(View.VISIBLE);
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "detector registrado");
			if(detector.isDevicePlugged()){
				Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "se va a cargar SDK");
				loadChipCard();
			}
			else
				statusText.setText(R.string.device_unplugged);
		}

	}


	private void disableButtons() {
		// TODO Auto-generated method stub
		description.setEnabled(false);
		amount.setEnabled(false);
		keyboard.setVisibility(View.GONE);
		cameraBtn.setEnabled(false);
		HandsetInfo.dismissKeyboard(this, description);
	}
	private void enableButtons() {
		cameraBtn.setEnabled(true);
		description.setEnabled(true);
		amount.setEnabled(true);
		keyboard.setVisibility(View.VISIBLE);
		HandsetInfo.dismissKeyboard(this, description);
	}

	public void camera(View v){
		if(!isCameraPressed){
			isCameraPressed=true;
			if(Camera.isCameraAvailable())
				goForwardForResult(CameraPreviewScreen.class, v.getContext(), TAKE_SHOT_CODE);
		}

	}

	public void clickDigit(View v){

		if(fieldSelected==null)
			return;
		String digitStr=v.getTag().toString();
		String currentAmount=fieldSelected.getText().toString().replace(MX, "");
		currentAmount=currentAmount.replace(",", ".");
		int index=-1;
		if(digitStr!= null){
			if((index=currentAmount.indexOf("."))>=0){
				if(currentAmount.substring(index).length()>2)
					return;
				else if(".".equalsIgnoreCase(digitStr))
					return;
			}
			StringBuffer buff= new StringBuffer(MX);
			buff.append(fieldSelected.getText().toString().replace(MX, ""));
			buff.append(digitStr);
			fieldSelected.setText(buff.toString());
		}
	}
	public void clickOperation(View v)
	{
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "campo seleccionado: " + fieldSelected);
		if(v.getId()==OP_CLEAN){
			fieldSelected.setText("");

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case TAKE_SHOT_CODE:
			isCameraPressed=false;
			if(resultCode==RESULT_OK){
				File f=FileManager.openFile(this,SaleBean.getImage(),FileManager.EXTERNAL,FileManager.MODE_APPEND ^ FileManager.MODE_RW);
				Bitmap b=BitmapFactory.decodeFile(f.getAbsolutePath());
				if(b!= null){
					Log.i("imageBit","cambia imagen+++++++");
					b=Bitmap.createScaledBitmap(b, cameraBtn.getWidth(), cameraBtn.getHeight(), false);
					Matrix m= new Matrix();
					m.setRotate(90f);
					b= Bitmap.createBitmap(b, 0, 0, b.getWidth(),b.getHeight(),m, true);

					cameraBtn.setImageBitmap(b);
					cameraBtn.setBackgroundColor(Color.BLACK);
					b=null;
					System.gc();
				}

			}
			break;
		case GPS_SETTINGS:
			break;
		case SaleScreen.SETTINGS_CODE:
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "termina cancelacion");
			setResult(RESULT_OK);
			finish();
			break;
		case SALE_CODE:
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "regresa de venta");
			enableButtons();
			swipeCardImage.setVisibility(View.GONE);
			statusText.setVisibility(View.VISIBLE);
			statusText.setText("");
			amount.setText("");
			description.setText("");
			isEnableMenu=true;
			invalidateOptionsMenu();
			cameraBtn.setImageResource(R.drawable.btn_foto);
			cameraBtn.setBackgroundColor(Color.TRANSPARENT);
			registerDetector();
			break;
		case SETTINGS_SALE_CODE:
			registerDetector();
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "regresa de settings");
			enableButtons();
			description.setCursorVisible(false);
			statusText.setVisibility(View.VISIBLE);
			statusText.setText("");
			cameraBtn.setImageResource(R.drawable.btn_foto);
			cameraBtn.setBackgroundColor(Color.TRANSPARENT);
			swipeCardImage.setVisibility(View.GONE);
			if(SaleBean.getImage()!=null){
				File f=FileManager.openFile(this, SaleBean.getImage(), FileManager.MODE_APPEND);
				if(f!= null && f.exists()){
					f.delete();
					Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "se elimino la imagen del producto en onActivityResult");
					SaleBean.setImage(null);
				}
			}
			break;
		default:
			break;
		}
	}

	public void selected(View v){
		keyboard.setVisibility(View.VISIBLE);
		HandsetInfo.dismissKeyboard(this, description);
		if(v.getId()==R.id.amountEdit)
			fieldSelected=amount;
		else
			fieldSelected=null;
		description.setCursorVisible(false);
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "field selected: " + fieldSelected);
	}


	private void runTransaccion() throws AudioReaderException, IOException {
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"# Process transaction\n");
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"� Power on reader\n");
		EMVProcessor emvProcessor;
		emvProcessor = new EMVProcessor(reader);
		Log.i("SALE SCREEN !", "emv processor "+emvProcessor);

		//-------------------------------------------------------------------------
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," Wait for card and reset\n");
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Timeout: " + (WAIT_CARD_TIME / 1000) + "s\n");

		runOnUiThread(new Runnable() {
			public void run() {
				swipeCardImage.setVisibility(View.VISIBLE);
				statusText.setText(R.string.msg_please_insert_card);}
		});




		reader.setMagneticCardMode(AudioReader.ENCRYPTION_TYPE_IDTECH,
				AudioReader.TRACK_READ_MODE_ALLOWED,
				AudioReader.TRACK_READ_MODE_ALLOWED,
				AudioReader.TRACK_READ_MODE_ALLOWED);
		reader.setMagneticCardMaskMode(true, 6, 4);

		CardInfo cardInfo;
		cardInfo = reader.waitForCard(WAIT_CARD_TIME);
		Log.i("SALE SCREEN !", "cardInfo "+cardInfo.toString());
		runOnUiThread(new Runnable() {
			public void run() {
				statusText.setText(R.string.msg_please_wait);
				}
		});

		if (cardInfo.cardType == AudioReader.CARD_TYPE_MAGNETIC) {

			processSwipedResult();
		} else if (cardInfo.cardType == AudioReader.CARD_TYPE_RFID) {
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Init Contactless EMV processing\n");
		} else if (cardInfo.cardType == AudioReader.CARD_TYPE_SMART_CARD) {
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," Init EMV processing\n");

				runOnUiThread(new Runnable() {
					public void run() {statusText.setText(R.string.msg_please_wait_chip);}
				});
			int	resultReadCard=processChipResult(emvProcessor);
			if(resultReadCard==RETURN_READ_CARD_OK || resultReadCard==RETURN_READ_CARD_WRONG){
				FALL_BACK_COUNTER=0;
			} else if(resultReadCard==RETURN_FALL_BACK){
				if(FALL_BACK_COUNTER==MAX_FALL_BACK_COUNTER){
				Logger.log("Se va a pedir que sea fallback");
				runOnUiThread(new Runnable() {
					public void run() {
						SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.requiredFallback));
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								if (detector.isDevicePlugged()) {
									statusText.setText(R.string.chipDetected);
									loadChipCard();
								} else {
									statusText.setText(R.string.device_unplugged);
								}
							}
						});
						dialog.show(getSupportFragmentManager(), "" + POPUP_ASK_SWIPED);
					}
				});
				return;
				}
			}
		}

		//-----------------------------------------------------------------------------
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"� Power off reader\n");
		reader.close();

	}
	private void processSwipedResult() throws AudioReaderException, IOException{
		// TODO Auto-generated method stub
		 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," Init Magstripe processing\n");
         final FinancialCard fc = reader.getFinancialCardData();
    	/* Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Holder: " + fc.holder + "\n");
    	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  PAN: " + fc.number + "\n");
    	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Expiry: " + fc.month + "/" + fc.year + "\n");
    	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Encrypted block: "+"    "+ fc.data.length+"   " + HexUtil.byteArrayToHexString(fc.data) + "\n");*/
    	 if (fc.number==null ||fc.number.length()==0 ||
    		 fc.data.length==0 ) {
    		 runOnUiThread(new Runnable() {
				public void run() {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.cardBadDecoded));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUP_ERROR);
				}
			});
    		return;
    	 }
    	 if(fc.number.length()>16){
    		 runOnUiThread(new Runnable() {
 				public void run() {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.weirdCard));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
 		    		 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "Puede ser tarjeta Internacional");				
 				}
 			});
    		 return ;
    	 }
		card= new CardBean();
		try {
			if(!validateTrack(fc.data))
				return;
		} catch (Exception e) {
			runOnUiThread(new Runnable() {
 				public void run() {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.cardBadDecoded));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);

 				}
 			});
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Excepcion " + e.getMessage());
			return;
		}
		StringBuffer rawTracks= new StringBuffer("{\"tracks\":\"");
		rawTracks.append(HexUtil.byteArrayToHexString(fc.data));
		rawTracks.append("\"");
		
		card.setRawTracks(rawTracks.toString());
		card.setKSN(reader.getSerialNumber());
		card.setMaskedPan(fc.number);
		String month=String.valueOf(fc.month);
		if(month.length()==1)
			month="0"+ month;
		final String expiryDate=month+ "/" +String.valueOf(fc.year);
		//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "fecha de expiracion: "+expiryDate);
		card.setStringValidity(expiryDate); 
		card.setValidity(Integer.parseInt( expiryDate.replace("/", "")));
		if(!isCancellation)
			sale.setCard(card);
		runOnUiThread(new Runnable() {
			public void run() {
				statusText.setText(R.string.cardDecoded);
				if(card.getMaskedPan().toString().startsWith("3")){
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.americanExpressNotAllowed));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUP_AMEX_NOT_ALLOWED);
					return;
				}
				Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "lectura completada");
				saleListener.onClick(null);																
			}
		});
	}
	private int processChipResult(EMVProcessor emvProcessor)  {
		// TODO Auto-generated method stub
		long fixSaleAmount=0;
		long fixResSaleAmount=0;
		String fixSaleAmountStr;
		double res=0;
		if(isCancellation){
			 //Log.i("PREUBA ProccessChipResult Cance/devo",operationSelected.getAmount().toString());
			long amountLong = Long.parseLong(operationSelected.getAmount());
				 fixSaleAmount=amountLong/100;
				 fixResSaleAmount=amountLong%100;
				 fixSaleAmountStr= fixSaleAmount + "." +fixResSaleAmount;
				 res=Double.parseDouble(fixSaleAmountStr);
			if (isRefund){
				operationTypeTag9C=OPERATION_TYPE_REFUND_9C;
			}else{
				operationTypeTag9C=OPERATION_TYPE_SALE_9C;
			}
		}else{
			//Log.i("PREUBA ProccessChipResult sale", sale.getAmount().toString());
			 fixSaleAmount=sale.getAmount()/100;
			 fixResSaleAmount=sale.getAmount()%100;
			 fixSaleAmountStr= fixSaleAmount + "." +fixResSaleAmount;
			 res=Double.parseDouble(fixSaleAmountStr);
			operationTypeTag9C=OPERATION_TYPE_SALE_9C;
		}
		
		 //String amount=String.valueOf(isCancellation ? operationSelected.getAmount() : res);
		String amount=String.valueOf(res);
		 Log.i("PRUEBA ChipResult 2", amount);
		 List<BerTlv> initData = new ArrayList<BerTlv>();
		initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9F03_AMOUNT_OTHER_NUM, "000000000000"));
		initData.add(EMVProcessorHelper.createTlv(PrivateTags.TAG_C4_INITIATE_PROCESSING_FLAGS, "40000000"));
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9C_TRANSACTION_TYPE, operationTypeTag9C));
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9F1C_TERMINAL_ID, "00000001"));
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9F53_TRANSACTION_CATEGORY_CODE, "52"));
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9F41_TRANSACTION_SEQ_COUNTER, EMVProcessorHelper.encodeTransactionSequence(callback.getTransactionSequence())));
		Log.i("variable verch----", " " + callback.getTransactionSequence());
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_81_AMOUNT_AUTHORISED_BINARY, EMVProcessorHelper.encodeAmount(amount)));
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9A_TRANSACTION_DATE, EMVProcessorHelper.encodeTransactionDate(Calendar.getInstance())));
         initData.add(EMVProcessorHelper.createTlv(EMVTags.TAG_9F21_TRANSACTION_TIME, EMVProcessorHelper.encodeTransactionTime(Calendar.getInstance())));
         //Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,callback.logData(initData).toString());
         //Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Monto EMV: " + EMVProcessorHelper.encodeAmount(amount));
         //Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Monto EMV: " + amount);
                                         
         TransactionResponse transactionResponse = null;
         try {
			// Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," *****---------*******"+initData.toString());
			transactionResponse = emvProcessor.initEMVProcessing(BerTlv.listToByteArray(initData), callback);
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," *****---------*******"+transactionResponse.tags);
	         
	         if(transactionResponse==null  || transactionResponse.tags== null){
	        	 validateFallBack(R.string.popUpError, R.string.txError);
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"------------Cadena es nula --------------");

	        	 return RETURN_FALL_BACK;
	         }
//<<<<<<< HEAD
//
//			 StringBuffer cadena=callback.logData(BerTlv.createList(BerTlv.mapToByteArray(transactionResponse.tags)));
//
//			 ArrayList<BerTlv> mArrayListBerTlv = EMVProcessor.mArrayBerTlv;
//			 StringBuffer cadenaAux = callback.logData(BerTlv.createList(BerTlv.arrayListToByteArray(mArrayListBerTlv)));
//=======
	         StringBuffer cadena=callback.logData(BerTlv.createList(BerTlv.mapToByteArray(transactionResponse.tags)));
			 ArrayList<BerTlv> mArrayListBerTlv = EMVProcessor.mArrayBerTlv;
			 StringBuffer cadenaAux = callback.logData(BerTlv.createList(BerTlv.arrayListToByteArray(mArrayListBerTlv)));

	                 
	         Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Cadena obtenida con exito " +cadena);
	         if(cadena==null){
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"------------Cadena es nula --------------");
	        	 validateFallBack(R.string.popUpError, R.string.txError);
	        	 return RETURN_FALL_BACK;
	         }
//<<<<<<< HEAD
//
//			 cadena= new StringBuffer(cadena.toString().replace(" ", ""));
//			 cadenaAux = new StringBuffer(cadenaAux.toString().replace(" ",""));
//			 cadena = cadenaAux;
//=======
	                 
	         cadena= new StringBuffer(cadena.toString().replace(" ", ""));
			 cadenaAux = new StringBuffer(cadenaAux.toString().replace(" ",""));
			 cadena = cadenaAux;
	         Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"� Power off card\n");
	         CardStatusResponse cardStatusResponse;
	         cardStatusResponse = reader.performCardPowerOff();
	         
	         String result=EMVProcessorHelper.getMessageResultDescription(cardStatusResponse.result);
	         if (cardStatusResponse.result != MessageProcessingResults.OK) {
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Result: " + result + "\n");
	         }else
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Result: " + result + "\n");        

	         int processingResult = transactionResponse.getProcessingResult();
	         Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN," Decode EMV processing result");
	         result=EMVProcessorHelper.getMessageResultDescription(processingResult);
	         if (processingResult == ProcessingResult.OK) {
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Transaction: Successful\n");
	        	 int resultBuil=buildReceipt(transactionResponse, cadena);
	        	 return resultBuil;
	         } 
	         else if (processingResult == ProcessingResult.CARD_BLOCKED) {
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Transaction: Successful\n");
	        	 runOnUiThread(new Runnable() {
					public void run() {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.cardBlocked));
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								errorChipPopup();
							}
						});
						dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
					}
				});
	        	 return RETURN_READ_CARD_WRONG;
	        	 
	         }else if(processingResult==ABORTED_OPERATION) {
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Transaction: Aborted*-*-*-*");
	        	 runOnUiThread(new Runnable() {
					public void run() {
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.abortedOperation));
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								errorChipPopup();
							}
						});
						dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
					}
				});
	        	 return RETURN_READ_CARD_WRONG;
	         }else{
	        	 validateFallBack(R.string.popUpError,R.string.txAborted);
	        	 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"  Transaction: Aborted (" + result+ ")\n");
	  			return RETURN_FALL_BACK;
	         }
		} catch (AudioReaderException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Error initEMVProcessing 1");
			 validateFallBack(R.string.popUpError, R.string.txError);
        	 return RETURN_FALL_BACK;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			validateFallBack(R.string.popUpError, R.string.txError);
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"Error initEMVProcessing 2");
			return RETURN_FALL_BACK;
		}
         
         
                 
	}


	private void validateFallBack(final int popupTitle,final int popupMessage){
		Log.i("TES FALLBACK", "************ +VALIDFALLBACk " +FALL_BACK_COUNTER);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(FALL_BACK_COUNTER!=MAX_FALL_BACK_COUNTER){
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(popupMessage));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
					FALL_BACK_COUNTER++;
					Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"AUMENTO CONTADOR FALLBACK : "+ FALL_BACK_COUNTER);
				}	
			}
		});
	}
	private int buildReceipt(final TransactionResponse response,final StringBuffer cadena) throws IOException  {     
		
		
		byte[] value = null;
		String pan = null;
		value = response.getValue(EMVTags.TAG_5A_PAN);
		if (value != null) {
			pan = EMVProcessorHelper.getMaskedString(EMVProcessorHelper.decodeNib(value), 6, 4);
			if(pan.length()>16){
	    		 Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "Puede ser tarjeta Internacional");
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.weirdCard));
				dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
					@Override
					public void onAccept(DialogInterface dialog) {
						errorChipPopup();
					}
				});
				dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
	    		 return RETURN_READ_CARD_WRONG;
	    	 }
		}else{
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "No trae numero de tarjeta ");
			validateFallBack(R.string.popUpError, R.string.cardBadDecoded);
			return RETURN_FALL_BACK;
		}
				
		Log.i("SALESCREEN", "LEctura tarjeta---------------+++++ "+response.getTransactonResult() +" "+response.getTransactonResultDescription()+" "+response.getTransactonResult());
		String payment = null;         
		switch (response.getTransactonResult()) {
		case TransactionResponse.DECLINED: 
			payment = "  Declined  "; 
			break;
		case TransactionResponse.ABORTED: 
			payment = "  Aborted   ";
			break;
		case TransactionResponse.NOT_ACCEPTED: 
			payment = "Not Accepted";
			break;
		case TransactionResponse.AUTHORIZED:
		case TransactionResponse.AUTHORIZED_SIGNATURE:
			payment = " Authorized ";   
			break; 
		default:
			throw new RuntimeException("Unknown transaction result " + response.getTransactonResult());
		}
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "PAYMENT = "+payment);
		if (response.getTransactonResult()==TransactionResponse.ABORTED) {
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "TRANSACCTION ABORTED++++++++++++");
			runOnUiThread(new Runnable() {
				public void run() {
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.abortedOperation));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
				}
			});
			return RETURN_READ_CARD_WRONG;
		}else if(response.getTransactonResult()!=TransactionResponse.AUTHORIZED  && response.getTransactonResult()!=TransactionResponse.AUTHORIZED_SIGNATURE){
			validateFallBack(R.string.popUpError,R.string.cardError);
			return RETURN_FALL_BACK;
		}
		
		if(response.getValue(EMVTags.TAG_5F24_EXPIRY_DATE)==null){
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "No trae fecha expiracion");
			validateFallBack(R.string.popUpError, R.string.cardBadDecoded);
			return RETURN_FALL_BACK;
		}
		String expirationDate=EMVProcessorHelper.decodeDate(response.getValue(EMVTags.TAG_5F24_EXPIRY_DATE));
		
	
		StringTokenizer tokens= new StringTokenizer(expirationDate,"-");
		
		if(tokens.countTokens()>=2){
			String year = tokens.nextToken();
			year=year.substring(2);
			expirationDate= tokens.nextToken()+ "/" + year;
		}
		
		card= new CardBean();
		card.setKSN(reader.getSerialNumber());
		//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "ksn = "+card.getKSN());
		card.setStringValidity(expirationDate); 
		card.setValidity(Integer.parseInt( expirationDate.replace("/", "")));
		card.setMaskedPan(pan);
		card.setChipData(cadena.toString());
		if(!isCancellation)
			sale.setCard(card);
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "maskedPan = "+ pan  + " chipData: " + cadena + " vigencia: "+ expirationDate);
		runOnUiThread(new Runnable() {
			public void run() {
				statusText.setText(R.string.cardDecoded);
				if(card.getMaskedPan().toString().startsWith("3")){
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.americanExpressNotAllowed));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							errorChipPopup();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_AMEX_NOT_ALLOWED);
					return;
				}
				Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "lectura completada");
				saleListener.onClick(null);
					
			}
		});
		return RETURN_READ_CARD_OK;
	}
	private boolean validateTrack(byte [] data) {
		// TODO Auto-generated method stub
		EMVResponse res= Encrypt.parseTracks(data);
		if(res.getTrack1Len() ==0 || res.getTrack2Len()==0){
			statusText.setText(R.string.cardBadDecoded);
			Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "Algun track nulo");
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.cardBadDecoded));
			dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
				@Override
				public void onAccept(DialogInterface dialog) {
					errorChipPopup();
				}
			});
			dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
			return false;
		}
		if(FALL_BACK_COUNTER >= MAX_FALL_BACK_COUNTER){
			card.setFullBack(true);
		}

		return true;

	}
	@Override
	public void headsetPlugged(int microphoneState) {
		Logger.log( Logger.MESSAGE,TAG_SALE_SCREEN,"headsetPlugged");
		Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN,"microphoneState = " +microphoneState );
		if(microphoneState==HeadsetDetector.MICROPHONE_PRESENT  ){
			isEnableMenu=true;
			invalidateOptionsMenu();
			statusText.setText(R.string.swiperDetected);	
		}
	}
	@Override
	public void headsetUnplugged(int microphoneState) {
		Logger.log( Logger.MESSAGE,TAG_SALE_SCREEN,"headsetUnplugged");
		
		if(microphoneState==HeadsetDetector.MICROPHONE_PRESENT ){
			statusText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			statusText.setText(R.string.swiperRemoved);
			isEnableMenu=false;
			invalidateOptionsMenu();
			if(reader != null){
				reader.close();
			}
		}
	}

	private class HandlerSaleCancellation implements MessageListener<Message, String>{
		@Override
		public void sendMessage(Context ctx, final Message message, final String parameter) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(isFinishing())
						return;
					this_.removeDialog(POPUP_MESSAGES_PROGRESS);
					
					if(message.getType()==Message.ERROR){
						if(detector!=null && detector.isDevicePlugged())							
							statusText.setText(R.string.device_plugged);
						
						else
							statusText.setText(R.string.device_unplugged);
						if(card.isFullBack()){
							FALL_BACK_COUNTER=0;
							card.setFullBack(false);
						}
						isEnableMenu=true;
						invalidateOptionsMenu();
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),parameter);
						dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
					}
					
				}
			});
			
		}
		@Override
		public void sendStatus(Context ctx, final int status) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(isFinishing())
						return;
					this_.removeDialog(POPUP_MESSAGES_PROGRESS);
					if(status==Message.EXCEPTION){
						if(detector!=null && detector.isDevicePlugged())							
							statusText.setText(R.string.device_plugged);
						
						else
							statusText.setText(R.string.device_unplugged);
						if(card.isFullBack()){
							FALL_BACK_COUNTER=0;
							card.setFullBack(false);
						}
						isEnableMenu=true;
						invalidateOptionsMenu();
						showDialog(POPUP_CONNECTION_ERROR);
					}
						
				}
			});
		}
		@Override
		public void sendMessage(Context ctx,final Message message,
				final Object... parameters) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(isFinishing())
						return;
					this_.removeDialog(POPUP_MESSAGES_PROGRESS);
					if(message.getType()==Message.OK){// Caso exito cancelacion o devolucion
						fiscalData=(FiscalDataBean) parameters[0];
						afiliacion= (String) parameters[1];
						saleReturnResponse=(SaleReturnResponse) parameters[2];
						String titleDialog="";
						if(isRefund) {
							titleDialog=getString(R.string.refundSucced);
						}else {
							titleDialog=getString(R.string.cancellationSucced);
						}
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage), titleDialog);
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "Devolucion: "+ isRefund);
								FALL_BACK_COUNTER=0;
								if(!Configuration.isSwiped())
									unregisterDetector();
								intent.putExtra(OperationsDetailScreen.CANCELLATION, isCancellation);
								intent.putExtra(OperationsDetailScreen.REFUND, isRefund);
								ArrayList<Serializable> params= new ArrayList<Serializable>();
								params.add(operationSelected);
								params.add(fiscalData);
								params.add(card);
								params.add(login);
								params.add(folioPreap);
								params.add(afiliacion);
								params.add(saleReturnResponse);
								ArrayList<String> names= new ArrayList<String>();
								names.add(OperationsBean.OPERATION);
								names.add(FiscalDataBean.FISCAL_DATA);
								names.add(CardBean.CREDIT_CARD);
								names.add(LoginBean.LOGIN);
								names.add(CustomerBean.FOLIO_PREAPERTURA);
								names.add(SaleBean.AFILIACION);
								names.add(RETURN_RESPONSE);
								intent.putExtra(LocationThread.LOCATION, location);
								goForwardForResult(SignScreen.class,SaleScreen.this,params,names, SaleScreen.SETTINGS_CODE);
							}
						});
						dialog.show(getSupportFragmentManager(),""+POPUP_CANCEL_OK);


					}
					else{// Caso exito solo venta
						forwardSignScreen((SaleBean) parameters[0]);
					}
				}
			});
		}
		private void forwardSignScreen(SaleBean resultSale){
			
			sale.setFolio(resultSale.getFolio());
			sale.setAuthorizationNumber(resultSale.getAuthorizationNumber());
			sale.setAfiliacion(resultSale.getAfiliacion());
			sale.setReferenceNumber(resultSale.getReferenceNumber());
			sale.setEstado(resultSale.getEstado());
			sale.setPoblacion(resultSale.getPoblacion());
			sale.setAid(resultSale.getAid());
			sale.setArqc(resultSale.getArqc());
			sale.setAmountReturned(resultSale.getAmountReturned());
			List<Serializable> params= new ArrayList<Serializable>();
			List<String>paramNames= new ArrayList<String>();
			intent.putExtra(OperationsDetailScreen.CANCELLATION, isCancellation);
			intent.putExtra(LocationThread.LOCATION, location);
			params.add(fiscalData);
			params.add(customer);
			params.add(sale);					
			
			paramNames.add(FiscalDataBean.FISCAL_DATA);
			paramNames.add(CustomerBean.CUSTOMER);
			paramNames.add(SaleBean.SALE);
			//Logger.log(Logger.MESSAGE, TAG_SALE_SCREEN, "tarjeta 2: " + sale.getCard().getMaskedPan());
			
			FALL_BACK_COUNTER=0;
			if(!Configuration.isSwiped())
				unregisterDetector();
			goForwardForResult(SignScreen.class,SaleScreen.this,params,paramNames,SALE_CODE);
		}
	}
	
}
