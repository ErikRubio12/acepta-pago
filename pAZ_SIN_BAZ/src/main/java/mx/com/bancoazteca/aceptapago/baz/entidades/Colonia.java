package mx.com.bancoazteca.aceptapago.baz.entidades;

import java.io.Serializable;

public class Colonia implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8583551915455689884L;
	private String colonia;
	private String consec;
	private String estado;
	private String idEdo;
	private String idPob;
	private String poblacion;
	
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getConsec() {
		return consec;
	}
	public void setConsec(String consec) {
		this.consec = consec;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getIdEdo() {
		return idEdo;
	}
	public void setIdEdo(String idEdo) {
		this.idEdo = idEdo;
	}
	public String getIdPob() {
		return idPob;
	}
	public void setIdPob(String idPob) {
		this.idPob = idPob;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

}
