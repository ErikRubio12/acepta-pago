package mx.com.bancoazteca.aceptapago.baz.entidades;

import java.io.Serializable;

public class ZonificacionEkt implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8950083532519608313L;
	private String calle;
	private String calleBaz;
	private String codigoPostal;
	private String codigoPostalBaz;
	private String colonia;
	private String coloniaBaz;
	private String diaDesc;
	private String estado;
	private String estadoBaz;
	private String idCalleBaz;
	private String idColoniaBaz;
	private String idCuadrante;
	private String idEstadoBaz;
	private String idMunicipoBaz;
	private String idPais;
	private String idZG;
    private String latitud;
    private String latitudBaz;
    private String longitud;
    private String longitudBaz;
    private String mapa;
    private String municipio;
    private String municipoBaz;
    private String nomEmp;
    private String numEmp;
    private String numero;
    private String numeroBaz;
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getCalleBaz() {
		return calleBaz;
	}
	public void setCalleBaz(String calleBaz) {
		this.calleBaz = calleBaz;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getCodigoPostalBaz() {
		return codigoPostalBaz;
	}
	public void setCodigoPostalBaz(String codigoPostalBaz) {
		this.codigoPostalBaz = codigoPostalBaz;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getColoniaBaz() {
		return coloniaBaz;
	}
	public void setColoniaBaz(String coloniaBaz) {
		this.coloniaBaz = coloniaBaz;
	}
	public String getDiaDesc() {
		return diaDesc;
	}
	public void setDiaDesc(String diaDesc) {
		this.diaDesc = diaDesc;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getEstadoBaz() {
		return estadoBaz;
	}
	public void setEstadoBaz(String estadoBaz) {
		this.estadoBaz = estadoBaz;
	}
	public String getIdCalleBaz() {
		return idCalleBaz;
	}
	public void setIdCalleBaz(String idCalleBaz) {
		this.idCalleBaz = idCalleBaz;
	}
	public String getIdColoniaBaz() {
		return idColoniaBaz;
	}
	public void setIdColoniaBaz(String idColoniaBaz) {
		this.idColoniaBaz = idColoniaBaz;
	}
	public String getIdCuadrante() {
		return idCuadrante;
	}
	public void setIdCuadrante(String idCuadrante) {
		this.idCuadrante = idCuadrante;
	}
	public String getIdEstadoBaz() {
		return idEstadoBaz;
	}
	public void setIdEstadoBaz(String idEstadoBaz) {
		this.idEstadoBaz = idEstadoBaz;
	}
	public String getIdMunicipoBaz() {
		return idMunicipoBaz;
	}
	public void setIdMunicipoBaz(String idMunicipoBaz) {
		this.idMunicipoBaz = idMunicipoBaz;
	}
	public String getIdPais() {
		return idPais;
	}
	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}
	public String getIdZG() {
		return idZG;
	}
	public void setIdZG(String idZG) {
		this.idZG = idZG;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLatitudBaz() {
		return latitudBaz;
	}
	public void setLatitudBaz(String latitudBaz) {
		this.latitudBaz = latitudBaz;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getLongitudBaz() {
		return longitudBaz;
	}
	public void setLongitudBaz(String longitudBaz) {
		this.longitudBaz = longitudBaz;
	}
	public String getMapa() {
		return mapa;
	}
	public void setMapa(String mapa) {
		this.mapa = mapa;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getMunicipoBaz() {
		return municipoBaz;
	}
	public void setMunicipoBaz(String municipoBaz) {
		this.municipoBaz = municipoBaz;
	}
	public String getNomEmp() {
		return nomEmp;
	}
	public void setNomEmp(String nomEmp) {
		this.nomEmp = nomEmp;
	}
	public String getNumEmp() {
		return numEmp;
	}
	public void setNumEmp(String numEmp) {
		this.numEmp = numEmp;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNumeroBaz() {
		return numeroBaz;
	}
	public void setNumeroBaz(String numeroBaz) {
		this.numeroBaz = numeroBaz;
	}

}
