package mx.com.bancoazteca.aceptapago.jobs;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpStatus;

import android.content.Context;
import mx.com.bancoazteca.ciphers.Base64;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.HttpConnection;
import mx.com.bancoazteca.network.HttpHeaders;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnTicketBean;
import mx.com.bancoazteca.aceptapago.db.daos.TicketsDao;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class UploadImageTicketJob extends BaseService<Message, String> {
	private static final String DIGITIZING_URL = Urls.DIGITALIZACION
			+ "DigWebServices/services/SubirImagen";
	private static final String DIGITIZING_METHOD = "setImagen";
	private static final String DIGITIZING_ACTION = "";
	private static final String UPLOAD_IMAGE = "UploadImageTicketJob";
	// private static final String
	// DIGITIZING_NAMESPACE="http://webservice.digitalizacion.bancoazteca.com.mx";

	private TicketsDao dao;
	private List<SaleReturnTicketBean> ticketList;

	public UploadImageTicketJob(Context context,MessageListener<Message, String>listener, String folio,
			String authorizationNumber, String user, String rfc,
			String reference, String afiliacion, File ticket, String maskedPan) {
		// TODO Auto-generated constructor stub
		this(context,listener, folio, authorizationNumber, user, rfc, reference,
				afiliacion, ticket, maskedPan, "516");
	}

	public UploadImageTicketJob(Context context,MessageListener<Message, String>listener, String folio,
			String authorizationNumber, String user, String rfc,
			String reference, String afiliacion, File ticket, String maskedPan,
			String idDoc) {
		// TODO Auto-generated constructor stub
		this.ctx = context;
		this.listener=listener;
		StringBuilder params = new StringBuilder();
		params.append(" folio: ");
		params.append(folio);
		params.append(" numer autorizacion: ");
		params.append(authorizationNumber);
		params.append(" rfc: ");
		params.append(rfc);
		params.append(" afiliacion:");
		params.append(afiliacion);
		params.append(" referencia: ");
		params.append(reference);
		params.append(" tarjeta: ");
		params.append(maskedPan);
		params.append("id Doc");
		params.append(idDoc);
		Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, params.toString());
		dao = new TicketsDao(ctx);
		ticketList = new ArrayList<SaleReturnTicketBean>(1);
		SaleReturnTicketBean currentTicket = new SaleReturnTicketBean();
		currentTicket.setTicketPath(ticket.getAbsolutePath());
		currentTicket.setMaskedPAN(maskedPan);
		currentTicket.setReference(reference);
		currentTicket.setAuthorizationNumber(authorizationNumber);
		currentTicket.setAffiliate(afiliacion);
		currentTicket.setIdoc(idDoc);
		currentTicket.setRfc(rfc);
		currentTicket.setFolio(folio);
		currentTicket.setUser(user);
		ticketList.add(currentTicket);
	}

	public UploadImageTicketJob(Context context,MessageListener<Message, String>listener,
			List<SaleReturnTicketBean> tickets) {
		this.ctx = context;
		this.ticketList = tickets;
		this.listener=listener;
		dao = new TicketsDao(ctx);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (SaleReturnTicketBean ticket : this.ticketList)
			sendTicket(ticket);
	}

	private void sendTicket(SaleReturnTicketBean bean) {
		HttpHeaders soapHeader = new HttpHeaders();
		soapHeader.addProperty("Content-Type",
				"application/soap+xml;charset=UTF-8");
		soapHeader.addProperty("SOAPAction", DIGITIZING_ACTION);
		HttpConnection conn = null;
		try {
			conn = Connections.makeHttpConnection(DIGITIZING_URL, soapHeader,
					createRequest(bean).toString().getBytes());

			if (conn == null) {
				saveTicket(bean);
				if(listener!= null)
					listener.sendStatus(ctx, Message.ERROR);
				return;
			}
			if (conn.getResponse().getStatusLine() != null
					&& conn.getResponse().getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, "envio exitoso");
				
				Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, "busqueda de ticket: "+ bean);
				SaleReturnTicketBean oldBean=dao.querySaleReturnTicket(bean);
				if(oldBean != null){
					Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, "era un reenvio de ticket de BD");
					dao.deleteSaleReturnTicket(oldBean);
					deleteTicket(oldBean.getTicketPath());
				}
				else{
					Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, "era un ticket de de venta o cancelacion");
				}
					
				
				if(listener!= null)
					listener.sendStatus(ctx, Message.OK);
			}

			else{
				saveTicket(bean);
				if(listener!= null)
					listener.sendStatus(ctx, Message.ERROR);
			}
				
			Connections.closeConnection(conn);
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, UPLOAD_IMAGE, e.getMessage());
			saveTicket(bean);
			if(listener!= null)
				listener.sendStatus(ctx, Message.ERROR);
			return;
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, UPLOAD_IMAGE, e.getMessage());
			saveTicket(bean);
			if(listener!= null)
				listener.sendStatus(ctx, Message.ERROR);
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, UPLOAD_IMAGE, e.getMessage());
			saveTicket(bean);
			if(listener!= null)
				listener.sendStatus(ctx, Message.ERROR);
			return;
		}
	}

	private StringBuilder createRequest(SaleReturnTicketBean bean) {
		StringBuilder requestStr = new StringBuilder();
		requestStr
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservice.digitalizacion.bancoazteca.com.mx\">");
		requestStr.append("<soapenv:Header/>");
		requestStr.append("<soapenv:Body>");
		requestStr.append("<web:");
		requestStr.append(DIGITIZING_METHOD);
		requestStr.append(">");
		requestStr.append("<web:strAppimx>");
		requestStr.append("PAGOAZTECA");
		requestStr.append("</web:strAppimx>");
		requestStr.append("<web:intIdDocto>");
		requestStr.append(bean.getIdoc());
		requestStr.append("</web:intIdDocto>");
		requestStr.append("<web:strimx02>");
		requestStr.append(bean.getFolio());
		requestStr.append("</web:strimx02>");
		requestStr.append("<web:strimx03>");
		requestStr.append(bean.getAuthorizationNumber());
		requestStr.append("</web:strimx03>");
		requestStr.append("<web:strimx04>");
		requestStr.append(bean.getUser());
		requestStr.append("</web:strimx04>");
		requestStr.append("<web:strimx05>");
		requestStr.append(Utility.formatDate(new Date(), "yyyyMMdd"));
		requestStr.append("</web:strimx05>");
		requestStr.append("<web:strimx06>");
		requestStr.append(bean.getRfc());
		requestStr.append("</web:strimx06>");
		requestStr.append("<web:strimx07>");
		requestStr.append(bean.getReference());
		requestStr.append("</web:strimx07>");
		requestStr.append("<web:strimx08>");
		requestStr.append(bean.getAffiliate());
		requestStr.append("</web:strimx08>");
		requestStr.append("<web:strimx09>");
		requestStr.append(bean.getMaskedPAN());
		requestStr.append("</web:strimx09>");
		requestStr.append("<web:strimx10>");
		requestStr.append("1");
		requestStr.append("</web:strimx10>");
		requestStr.append("<web:strimx11>");
		requestStr.append("");
		requestStr.append("</web:strimx11>");
		requestStr.append("<web:strimx12>");
		requestStr.append("");
		requestStr.append("</web:strimx12>");
		requestStr.append("<web:strimx13>");
		requestStr.append("");
		requestStr.append("</web:strimx13>");
		requestStr.append("<web:strimx14>");
		requestStr.append("");
		requestStr.append("</web:strimx14>");
		requestStr.append("<web:strimx15>");
		requestStr.append("");
		requestStr.append("</web:strimx15>");
		requestStr.append("<web:strimx16>");
		requestStr.append("");
		requestStr.append("</web:strimx16>");
		requestStr.append("<web:strimx17>");
		requestStr.append("");
		requestStr.append("</web:strimx17>");
		requestStr.append("<web:strimx18>");
		requestStr.append("");
		requestStr.append("</web:strimx18>");
		requestStr.append("<web:strimx19>");
		requestStr.append("");
		requestStr.append("</web:strimx19>");
		requestStr.append("<web:strimx20>");
		requestStr.append("");
		requestStr.append("</web:strimx20>");
		requestStr.append("<web:strClienteUnico>");
		requestStr.append("");
		requestStr.append("</web:strClienteUnico>");
		requestStr.append("<web:strExtensionArchivo>");
		requestStr.append("jpeg");
		requestStr.append("</web:strExtensionArchivo>");
		requestStr.append("<web:strUsuario>");
		requestStr.append(bean.getUser());
		requestStr.append("</web:strUsuario>");
		requestStr.append("<web:strWorkStation>");
		requestStr.append("0");
		requestStr.append("</web:strWorkStation>");
		requestStr.append("<web:intDomiciliado>");
		requestStr.append("0");
		requestStr.append("</web:intDomiciliado>");
		requestStr.append("<web:intIdSucursalDom>");
		requestStr.append("0");
		requestStr.append("</web:intIdSucursalDom>");
		requestStr.append("<web:intSubproducto>");
		requestStr.append("1");
		requestStr.append("</web:intSubproducto>");
		requestStr.append("<web:intEtapa>");
		requestStr.append("1");
		requestStr.append("</web:intEtapa>");
		requestStr.append("<web:intEnviaimagen>");
		requestStr.append("0");
		requestStr.append("</web:intEnviaimagen>");
		requestStr.append("<web:strEdoImx>");
		requestStr.append("w");
		requestStr.append("</web:strEdoImx>");
		requestStr.append("<web:intTipo>");
		requestStr.append("0");
		requestStr.append("</web:intTipo>");
		requestStr.append("<web:strImagen>");
		String currentDate = Utility.formatDate(new Date(), "yyyyMMdd");
		int oddDigitsSum = 0;
		int digitPairs = 0;
		for (int i = 0; i < currentDate.length(); i++) {
			if (i % 2 == 0) {
				oddDigitsSum += Integer.parseInt("" + currentDate.charAt(i));
			} else {
				digitPairs += Integer.parseInt("" + currentDate.charAt(i));
			}
		}
		oddDigitsSum = oddDigitsSum * 3;

		int res = oddDigitsSum + digitPairs;
		int digit = -1;
		do {
			digit++;
		} while ((res + digit) % 10 != 0);
		StringBuilder verifyDigit = new StringBuilder("");
		verifyDigit.append(oddDigitsSum);
		verifyDigit.append(res);
		verifyDigit.append(digit);
		InputStream ticketIS = null;
		ByteArrayOutputStream buffer = null;
		try {
			ticketIS = new BufferedInputStream(new FileInputStream(
					bean.getTicketPath()));
			buffer = new ByteArrayOutputStream();
			byte[] data = new byte[1024]; // 1K
			int bytesRead;
			while ((bytesRead = ticketIS.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, bytesRead);
			}
			requestStr.append(Base64.encodeToString(buffer.toByteArray(),
					Base64.DEFAULT));
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			Logger.log(Logger.EXCEPTION, UPLOAD_IMAGE,
					"No se encontro el ticket: " + e.getMessage());
			requestStr.append("");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.log(Logger.EXCEPTION, UPLOAD_IMAGE,
					"No se encontro el ticket: " + e.getMessage());
			requestStr.append("");
		} finally {
			try {
				buffer.close();
				ticketIS.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		requestStr.append("</web:strImagen>");
		requestStr.append("<web:strVerifica>");
		requestStr.append(verifyDigit.toString());
		requestStr.append("</web:strVerifica>");
		requestStr.append("<web:intTrace>");
		requestStr.append("0");
		requestStr.append("</web:intTrace>");
		requestStr.append("</web:");
		requestStr.append(DIGITIZING_METHOD);
		requestStr.append(">");
		requestStr.append("</soapenv:Body>");
		requestStr.append("</soapenv:Envelope>");
		Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, requestStr.toString());
		return requestStr;
	}
	private void  deleteTicket(String path){
		File f= new File(path);
		if(f != null && f.exists())
			f.delete();
		int index=path.indexOf("pdf");
		if(index>=0){
			path=path.replace("pdf", "jpg");
			f= new File(path);
			if(f != null && f.exists())
				f.delete();
		}
		else if((index=path.indexOf("jpg"))>=0){
			path=path.replace("jpg", "pdf");
			f= new File(path);
			if(f != null && f.exists())
				f.delete();
		}
	}
	private void saveTicket(SaleReturnTicketBean bean) {
		// TODO Auto-generated method stub
		StringBuilder params = new StringBuilder();
		params.append("Se va a guardar el ticket");
		params.append(bean.toString());
		Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, params.toString());
		long id = dao.insertSaleReturnTicket(bean);
		Logger.log(Logger.MESSAGE, UPLOAD_IMAGE, "id: " + id);
	}
}
