package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Xml;

import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.util.Logger;

public class RecoverPasswordJob extends BaseService<Message, String> {
	
	private static final String RECOVER_PASSWORD_METHOD = "BusinessToBusinessLogin";
	private static final String RECOVER_PASSWORD_ACTION = null;
	private static final String RECOVER_PASSWORD_NAMESPACE = "http://service.btb.com";
	private static final String RECOVER_PASSWORD_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String RECOVER_PASSWORD_PARAM = "xml";
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String EMAIL_TAG = "email";
	private static final String RECOVER_PASSWORD = "recover password";
	private String user;
	private String email;
	private Message msg= new Message();
	public RecoverPasswordJob(Context ctx,String user,MessageListener<Message, String> listener) {
		// TODO Auto-generated constructor stub
		this.user=user;
		this.listener= listener;
		this.ctx=ctx;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("btob"));
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append(Encrypt.encryptStringWS("login.Reenvio"));
		request.append("</canalEntrada>");
		request.append("<login>");
		request.append("<usuario>");
		request.append(Encrypt.encryptStringWS(user));
		request.append("</usuario>");
		request.append("</login>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		SoapRequest<String> req= new SoapRequest<String>(RECOVER_PASSWORD_METHOD, RECOVER_PASSWORD_ACTION);
		req.setNameSpace(RECOVER_PASSWORD_NAMESPACE);
		req.setUrl(RECOVER_PASSWORD_URL);
		Parameter<String,String> param= new Parameter<String, String>(RECOVER_PASSWORD_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if (res==null) {
				msg= new Message();
				msg.setType(Message.EXCEPTION);
				listener.sendMessage(ctx,msg, "");
				return;
			}
			String xml=res.getResponse().toString();
			Connections.closeSoapConnection(res);
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equals(OPERATION_CODE_TAG)) {
						operationCode= Encrypt.decryptStringWS(parser.getText());
					}
					else if (currentTag.equals(SYSTEM_ERROR_TAG)) {
						systemError= Encrypt.decryptStringWS(parser.getText());
					}
					else if(currentTag.equals(EMAIL_TAG))
						email=Encrypt.decryptStringWS(parser.getText());;
					
					break;
				case XmlPullParser.END_TAG:
					currentTag=null;
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			if ("0".equals(operationCode)) {
				msg= new Message();
				msg.setType(Message.OK);
				listener.sendMessage(ctx,msg, email);
			}
			else{
				listener.sendMessage(ctx,new Message(), systemError);
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			msg= new Message();
			msg.setType(Message.EXCEPTION);
			listener.sendMessage(ctx,msg, e.getMessage());
			Logger.log(Logger.EXCEPTION, RECOVER_PASSWORD,"XmlPullParserException"+ e.getMessage());
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			msg= new Message();
			msg.setType(Message.EXCEPTION);
			listener.sendMessage(ctx,msg, e.getMessage());
			Logger.log(Logger.EXCEPTION, RECOVER_PASSWORD,"InterruptedIOException"+ e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			msg= new Message();
			msg.setType(Message.EXCEPTION);
			listener.sendMessage(ctx,msg, e.getMessage());
			Logger.log(Logger.EXCEPTION, RECOVER_PASSWORD,"IOException"+ e.getMessage());
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			msg= new Message();
			msg.setType(Message.EXCEPTION);
			listener.sendMessage(ctx,msg, e.getMessage());
			Logger.log(Logger.EXCEPTION, RECOVER_PASSWORD,"TimeoutException"+ e.getMessage());
		}
	}
}
