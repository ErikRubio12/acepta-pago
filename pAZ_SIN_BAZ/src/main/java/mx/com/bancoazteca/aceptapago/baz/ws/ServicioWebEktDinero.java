package mx.com.bancoazteca.aceptapago.baz.ws;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


public class ServicioWebEktDinero {
	public String wsUrl ="http://200.38.122.100/AperDepRet/AperturasWSPort?wsdl";
	public String NAMESPACE = "http://ws.baz.com/";
	public String _Metodo = "validaNIP";
	public String _SoapAction = "";
	SoapObject Request;
	
	
	public String traeProductos(){
		  
	Request = new SoapObject(NAMESPACE, _Metodo);
	PropertyInfo pi = new PropertyInfo();
    pi.setName("arg0");
    pi.setValue(new SoapObject(NAMESPACE, _Metodo)
                .addProperty("capDisponible", 1000)
                .addProperty("tiempoCliente", 1));
	Request.addProperty(pi);
	String result = null;
	HttpTransportSE httpt = new HttpTransportSE(wsUrl);
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	envelope.setAddAdornments(false);
	envelope.dotNet = false;
	envelope.bodyOut = Request;
	try{  
		httpt.debug = true;
		httpt.call(_SoapAction, envelope);
		result=httpt.responseDump;
	}catch(Exception e){
		result="";
		e.printStackTrace();
		return null;
	}
	Request = null;
	return result;
}
	public String traePlazos(String canal, String monto, String sku){
		Request = new SoapObject(NAMESPACE, _Metodo);
		PropertyInfo pi = new PropertyInfo();
	    pi.setName("arg0");
	    pi.setValue(new SoapObject(NAMESPACE, _Metodo)
	                .addProperty("canal", canal)
	                .addProperty("monto", monto)
	                .addProperty("sku", sku));
		Request.addProperty(pi);
		String result = null;
		HttpTransportSE httpt = new HttpTransportSE(wsUrl);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setAddAdornments(false);
		envelope.dotNet = false;
		envelope.bodyOut = Request;
		try{  
			httpt.debug = true;
			httpt.call(_SoapAction, envelope);
			result=httpt.responseDump;
		}catch(Exception e){
			result="";
			e.printStackTrace();
			return null;
		}
		Request = null;
		return result;
	}

	public String traeClientes(String fechaNacimiento, String nombre, String paterno, String materno){
		Request = new SoapObject(NAMESPACE, _Metodo);
		PropertyInfo pi = new PropertyInfo();
	    pi.setName("arg0");
	    pi.setValue(new SoapObject(NAMESPACE, _Metodo)
	                .addProperty("fechaNac", fechaNacimiento)
	                .addProperty("paterno", paterno)
	                .addProperty("materno", materno)
	                .addProperty("nombre", nombre));
		Request.addProperty(pi);
		String result = null;
		HttpTransportSE httpt = new HttpTransportSE(wsUrl);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setAddAdornments(false);
		envelope.dotNet = false;
		envelope.bodyOut = Request;
		try{  
			httpt.debug = true;
			httpt.call(_SoapAction, envelope);
			result=httpt.responseDump;
		}catch(Exception e){
			result="";
			e.printStackTrace();
			return null;
		}
		Request = null;
		return result;
	}
	
	public String traeZonificacion(String claveCliente){
		Request = new SoapObject(NAMESPACE, _Metodo);
		PropertyInfo pi = new PropertyInfo();
	    pi.setName("arg0");
	    pi.setValue(new SoapObject(NAMESPACE, _Metodo)
	                .addProperty("cveCliente", claveCliente));
		Request.addProperty(pi);
		String result = null;
		HttpTransportSE httpt = new HttpTransportSE(wsUrl);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setAddAdornments(false);
		envelope.dotNet = false;
		envelope.bodyOut = Request;
		try{  
			httpt.debug = true;
			httpt.call(_SoapAction, envelope);
			result=httpt.responseDump;
		}catch(Exception e){
			result="";
			e.printStackTrace();
			return null;
		}
		Request = null;
		return result;
	}
	
		
	
	public String traeCodigosPostales(String idEstado, String idPoblacion){
		Request = new SoapObject(NAMESPACE, _Metodo);
		PropertyInfo pi = new PropertyInfo();
	    pi.setName("arg0");
	    pi.setValue(new SoapObject(NAMESPACE, _Metodo)
	                .addProperty("idEstado", idEstado)
	                .addProperty("idPoblacion", idPoblacion));
		Request.addProperty(pi);
		String result = null;
		HttpTransportSE httpt = new HttpTransportSE(wsUrl);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setAddAdornments(false);
		envelope.dotNet = false;
		envelope.bodyOut = Request;
		try{  
			httpt.debug = true;
			httpt.call(_SoapAction, envelope);
			result=httpt.responseDump;
		}catch(Exception e){
			result="";
			e.printStackTrace();
			return null;
		}
		Request = null;
		return result;
	}
	
	public String traeColonias(String codPos){
		Request = new SoapObject(NAMESPACE, _Metodo);
		PropertyInfo pi = new PropertyInfo();
	    pi.setName("arg0");
	    pi.setValue(new SoapObject(NAMESPACE, _Metodo)
	                .addProperty("codPos", codPos));
		Request.addProperty(pi);
		String result = null;
		HttpTransportSE httpt = new HttpTransportSE(wsUrl);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setAddAdornments(false);
		envelope.dotNet = false;
		envelope.bodyOut = Request;
		try{  
			httpt.debug = true;
			httpt.call(_SoapAction, envelope);
			result=httpt.responseDump;
		}catch(Exception e){
			result="";
			e.printStackTrace();
			return null;
		}
		Request = null;
		return result;
	}
	
}
