package mx.com.bancoazteca.aceptapago.db.daos;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.db.PagoAztecaDB;
import mx.com.bancoazteca.aceptapago.db.Query;

public class ExtraInfoDao  {
//	private static final int EXTRAINFO_ID = 15;
//	private static final int EMPLOYEE = 1;
//	private static final int ECONOMIC_ACTIVITY_ID_FOR_EMPLOYEE = 17;
//	private static final int WORKER = 5;
//	private static final int ECONOMIC_ACTIVITY_ID_FOR_WORKER = 20;
//	private static final int NOT_WORKER = 6;
//	private static final int ECONOMIC_ACTIVITY_ID_FOR_NOT_WORKER = 18;
//	private static final int OWN_BUSINESS = 7;
//	private static final int ECONOMIC_ACTIVITY_ID_FOR_OWN_BUSINESS = 19;
//	private static final int ECONOMIC_ACTIVITY_ID_FOR_PROFESIONAL_BUSINESS = 21;
//	private static final int EXTRAINFO_BUSINESS_CATEGORY_ID = 0;
	private PagoAztecaDB db;
	
	public ExtraInfoDao(Context context) {
		// TODO Auto-generated constructor stub
		db= new PagoAztecaDB(context);
	}
	public List<Parameter<String, String>> queryExtraInfo(Context ctx) {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String, String>> elements= new ArrayList<Parameter<String, String>>();
		elements.add(new Parameter<String, String>("0", ctx.getString(R.string.extraInfo)));
//		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_EXTRA_INFO, new String[]{String.valueOf(EXTRAINFO_ID)});
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_EXTRA_INFO, null);
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0).trim(), cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}
	public List<Parameter<String,String>> queryEconomicActivity(int selection) {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String, String>> elements= new ArrayList<Parameter<String, String>>();
//		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_ECONOMIC_ACTIVITY, 
//						new String[]{String.valueOf(selection==EMPLOYEE ? ECONOMIC_ACTIVITY_ID_FOR_EMPLOYEE :
//													(selection==WORKER ? ECONOMIC_ACTIVITY_ID_FOR_WORKER :
//													(selection==NOT_WORKER ? ECONOMIC_ACTIVITY_ID_FOR_NOT_WORKER :
//													(selection==OWN_BUSINESS ?	ECONOMIC_ACTIVITY_ID_FOR_OWN_BUSINESS :
//													 ECONOMIC_ACTIVITY_ID_FOR_PROFESIONAL_BUSINESS)))
//												   )
//									});
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_ECONOMIC_ACTIVITY, new String[]{String.valueOf(selection)});
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0).trim(), cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}
	public List<Parameter<String,String>> queryExtraInfoCategoryBusiness(Context ctx) {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String, String>> elements= new ArrayList<Parameter<String, String>>();
		elements.add(new Parameter<String, String>("0", ctx.getResources().getString(R.string.extraInfoBusiness)));
//		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_EXTRA_INFO_BUSINESS_CATEGORY, new String[]{String.valueOf(EXTRAINFO_BUSINESS_CATEGORY_ID)});
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_EXTRA_INFO_BUSINESS_CATEGORY, null);
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0).trim(), cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}
	
	public List<Parameter<String, String>> queryExtraInfoBusiness(int selection) {
		// TODO Auto-generated method stub
		ArrayList<Parameter<String, String>> elements= new ArrayList<Parameter<String, String>>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_EXTRA_INFO_BUSINESS, new String[]{String.valueOf(selection)});
		if(cursor.moveToFirst()){
			do {
				elements.add(new Parameter<String, String>(cursor.getString(0).trim(), cursor.getString(1).trim()));
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return elements;
	}
	public String queryExtraInfoBusinessById(int businessType) {
		// TODO Auto-generated method stub
		String res=null;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_EXTRA_INFO_BUSINESS_BY_ID, new String[]{String.valueOf(businessType)});
		if(cursor.moveToFirst())
			res=cursor.getString(0);
		else
			res="";
		cursor.close();
		db.close();
		return res;
	}
	
}
