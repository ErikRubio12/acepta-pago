package mx.com.bancoazteca.aceptapago.gui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html.ImageGetter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class HelpScreen  extends BaseActivity implements ImageGetter{
	private static final String HELP_SCREEN = "AztecaHelp";
	private static final int POPUP_CONTACTUS = 10;
	private TextView version;
	private LinearLayout whatPAZIs;
	private LinearLayout howSwipeCard;
	private LinearLayout aztecaPhone;
	private LinearLayout taxesContent;
	private LinearLayout howRecoverPw;
	private LinearLayout howGetMoney;
	private FrameLayout whatPAZIsOption;
	private FrameLayout howGetMoneyOption;
	private FrameLayout howSwipeCardOption;
	private FrameLayout aztecaPhoneOption;
	private FrameLayout taxOption;
	private FrameLayout howRecoverPwOption;
	private ScrollView scrollContent;
	private boolean isLogged=false;
	private String rfc;
	private String fiscalName;
	private Toolbar mtoolbar;
	private TextView stepsHelp;


	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub

		super.loadUiComponents();
	}

	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params!= null){
			isLogged=params.getBoolean("logged",false);
			rfc=params.getString(FiscalDataBean.BUSINESS_RFC);
			fiscalName=params.getString(FiscalDataBean.FISCAL_NAME);
		}
		setContentView(R.layout.help2);

		
		version=(TextView) findViewById(R.id.version);
		stepsHelp=(TextView)findViewById(R.id.steps_help);
		whatPAZIs= (LinearLayout) findViewById(R.id.whatPAZIs);
		howRecoverPw=(LinearLayout) findViewById(R.id.howRecoverPw);
		howSwipeCard=(LinearLayout) findViewById(R.id.howSwipeCard);
		taxesContent=(LinearLayout) findViewById(R.id.taxesContent);
		aztecaPhone= (LinearLayout) findViewById(R.id.aztecaPhone);
		howGetMoney= (LinearLayout) findViewById(R.id.howGetMoney);
		whatPAZIsOption= (FrameLayout) findViewById(R.id.whatPAZIsOption);
		howRecoverPwOption=(FrameLayout) findViewById(R.id.howRecoverPwOption);
		howSwipeCardOption=(FrameLayout) findViewById(R.id.howSwipeCardOption);
		howGetMoneyOption=(FrameLayout) findViewById(R.id.howGetMoneyOption);
		taxOption=(FrameLayout) findViewById(R.id.taxOption);
		aztecaPhoneOption= (FrameLayout) findViewById(R.id.aztecaPhoneOption);

		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
		scrollContent=(ScrollView) findViewById(R.id.scrollContent);
//		if(isLogged) {
//			String folio= (String) params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
//			stepsHelp.setText(getString(R.string.helpStep1Login)+" "+ folio +" "+getString(R.string.helpStep2));
//
//		}else {
			stepsHelp.setText(getString(R.string.ayuda1) +"" + getString(R.string.ayuda2)+ "" + getString(R.string.ayuda3)+ "" + getString(R.string.ayuda4)+ "" + getString(R.string.ayuda5)+ "" + getString(R.string.ayuda6)+ "" + getString(R.string.ayuda7) );
//		}
		version.setText(version.getText()+ " "+ String.valueOf(Utility.getAppVersionName(this)) );
		version.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				version.setText("14/febrero/2018 " );
				
			}
		});
	}


	public void makeCall(View v){
		if(HandsetInfo.supportPhoneCall(this)){
			Logger.log(Logger.MESSAGE, HELP_SCREEN, "en make call");
			String uri;
			if(v.getId()==R.id.aztecaPhoneDF)
				uri = "tel:54478810";
			else
				uri = "tel:018000407777";
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse(uri));
			startActivity(intent);
		}
		
	}
	public void menuClick(View v){
		
		switch (v.getId()) {
		case R.id.whatPAZIsOption:
			if(whatPAZIs.getVisibility() == View.GONE){
				whatPAZIs.setVisibility(View.VISIBLE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}		
			else{
				whatPAZIs.setVisibility(View.GONE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}
			howSwipeCard.setVisibility(View.GONE);
			aztecaPhone.setVisibility(View.GONE);
			taxesContent.setVisibility(View.GONE);

			break;
		case R.id.howSwipeCardOption:
			if(howSwipeCard.getVisibility() == View.GONE){
				howSwipeCard.setVisibility(View.VISIBLE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}
			else{
				howSwipeCard.setVisibility(View.GONE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}
			aztecaPhone.setVisibility(View.GONE);
			whatPAZIs.setVisibility(View.GONE);
			taxesContent.setVisibility(View.GONE);

			break;
		case R.id.aztecaPhoneOption:
			if(aztecaPhone.getVisibility() == View.GONE){
				aztecaPhone.setVisibility(View.VISIBLE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}
			else{
				aztecaPhone.setVisibility(View.GONE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}
			whatPAZIs.setVisibility(View.GONE);
			howSwipeCard.setVisibility(View.GONE);
			taxesContent.setVisibility(View.GONE);

			break;
			case R.id.howGetMoneyOption:
				if(howGetMoney.getVisibility() == View.GONE){
					howGetMoney.setVisibility(View.VISIBLE);
					((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down);
					((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

				}
				else{
					howGetMoney.setVisibility(View.GONE);
					((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
					((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				}
				whatPAZIs.setVisibility(View.GONE);
				howSwipeCard.setVisibility(View.GONE);
				aztecaPhone.setVisibility(View.GONE);
				taxesContent.setVisibility(View.GONE);
				break;
		case R.id.taxOption:
			if(taxesContent.getVisibility() ==View.GONE){
				taxesContent.setVisibility(View.VISIBLE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down);

			}
			else{
				taxesContent.setVisibility(View.GONE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

			}
			whatPAZIs.setVisibility(View.GONE);
			howSwipeCard.setVisibility(View.GONE);
			aztecaPhone.setVisibility(View.GONE);

			break;
		case R.id.howRecoverPwOption:
			if(howRecoverPw.getVisibility() ==View.GONE){
				howRecoverPw.setVisibility(View.VISIBLE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

				((ImageView)howRecoverPwOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_down);
			}
			else{
				howRecoverPw.setVisibility(View.GONE);
				((ImageView)whatPAZIsOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howGetMoneyOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)howSwipeCardOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)aztecaPhoneOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
				((ImageView)taxOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);

				((ImageView)howRecoverPwOption.findViewById(R.id.arrow)).setImageResource(R.drawable.btn_sub_right);
			}
			whatPAZIs.setVisibility(View.GONE);
			howSwipeCard.setVisibility(View.GONE);
			aztecaPhone.setVisibility(View.GONE);
			taxesContent.setVisibility(View.GONE);
			break;
		default:
			break;
		}
		scrollContent.getRootView().invalidate();
	}

	@Override
	public Drawable getDrawable(String arg0) {
		// TODO Auto-generated method stub
		Drawable img=getResources().getDrawable(R.drawable.logobaz);
		img.setBounds(0, 0, img.getIntrinsicWidth(), img.getIntrinsicHeight());
		Logger.log(Logger.MESSAGE, HELP_SCREEN,"entro a getDrawable: "+ img.getIntrinsicHeight() + " " +img.getIntrinsicWidth() );
		return img;
	}
}
