package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;
import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.FiscalDataBean;


public class AdditionalInfo implements KvmSerializable,Serializable{
	private static final int FIELDS = 17;
	private static final long serialVersionUID = -188714629446682371L;
	private static PropertyInfo PIFolio= new PropertyInfo();
	private static PropertyInfo PIGender= new PropertyInfo();
	private static PropertyInfo PIMaritalStatus= new PropertyInfo();
	private static PropertyInfo PIBusinessSector= new PropertyInfo();
	private static PropertyInfo PIFiscalName= new PropertyInfo();
	private static PropertyInfo PIRfc= new PropertyInfo();
	private static PropertyInfo PIStreet= new PropertyInfo();
	private static PropertyInfo PIInternalNumber= new PropertyInfo();
	private static PropertyInfo PIExternalNumber= new PropertyInfo();
	private static PropertyInfo PITown= new PropertyInfo();
	private static PropertyInfo PIColony= new PropertyInfo();
	private static PropertyInfo PIState= new PropertyInfo();
	private static PropertyInfo PIZipCode= new PropertyInfo();
	private static PropertyInfo PILada= new PropertyInfo();
	private static PropertyInfo PITelephone= new PropertyInfo();
	private static PropertyInfo PIExtention= new PropertyInfo();
	private static PropertyInfo PIWebPage= new PropertyInfo();
	private static PropertyInfo[] listPI= new PropertyInfo[FIELDS];
	private String rfc;
	private String folio;
	private String fiscalName;
	private int maritalStatus;
	private int gender;
	private ExtraInfoBean extraInfo;
	private AddressBean address;
	static{
		PIStreet.name=FiscalDataBean.BUSINESS_STREET;
		PIStreet.type=PropertyInfo.STRING_CLASS;
		listPI[0]=PIStreet;
		
		PIColony.name=FiscalDataBean.BUSINESS_COLONY;
		PIColony.type=PropertyInfo.STRING_CLASS;
		listPI[1]=PIColony;
		
		PIZipCode.name=FiscalDataBean.BUSINESS_ZIPCODE;
		PIZipCode.type=PropertyInfo.STRING_CLASS;
		listPI[2]=PIZipCode;
		
		PIMaritalStatus.name=CustomerBean.MARITAL_STATUS;
		PIMaritalStatus.type=PropertyInfo.INTEGER_CLASS;
		listPI[3]=PIMaritalStatus;
		
		PIState.name=FiscalDataBean.BUSINESS_STATE;
		PIState.type=PropertyInfo.STRING_CLASS;
		listPI[4]=PIState;
		
		PIExtention.name=FiscalDataBean.BUSINESS_EXTENSION;
		PIExtention.type=PropertyInfo.STRING_CLASS;
		listPI[5]=PIExtention;
		
		PIFolio.name=CustomerBean.FOLIO_PREAPERTURA;
		PIFolio.type=PropertyInfo.STRING_CLASS;
		listPI[6]=PIFolio;
		
		PILada.name=FiscalDataBean.BUSINESS_LADA;
		PILada.type=PropertyInfo.STRING_CLASS;
		listPI[7]=PILada;
		
		PITown.name=FiscalDataBean.BUSINESS_TOWN;
		PITown.type=PropertyInfo.STRING_CLASS;
		listPI[8]=PITown;
		
		PIFiscalName.name=FiscalDataBean.FISCAL_NAME;
		PIFiscalName.type=PropertyInfo.STRING_CLASS;
		listPI[9]=PIFiscalName;
		
		PIExternalNumber.name=AddressBean.EXTERNAL_NUMBER;
		PIExternalNumber.type=PropertyInfo.STRING_CLASS;
		listPI[10]=PIExternalNumber;
		
		PIInternalNumber.name=AddressBean.INTERNAL_NUMBER;
		PIInternalNumber.type=PropertyInfo.STRING_CLASS;
		listPI[11]=PIInternalNumber;
		
		PIRfc.name=FiscalDataBean.BUSINESS_RFC;
		PIRfc.type=PropertyInfo.STRING_CLASS;
		listPI[12]=PIRfc;
		
		PIBusinessSector.name=ExtraInfoBean.BUSSINES_SECTOR;
		PIBusinessSector.type=PropertyInfo.STRING_CLASS;
		listPI[13]=PIBusinessSector;
		
		PIGender.name=CustomerBean.GENDER;
		PIGender.type=PropertyInfo.INTEGER_CLASS;
		listPI[14]=PIGender;

		PITelephone.name=FiscalDataBean.BUSSINES_TELEPHONE;
		PITelephone.type=PropertyInfo.STRING_CLASS;
		listPI[15]=PITelephone;
		
		PIWebPage.name=ExtraInfoBean.WEB_PAGE;
		PIWebPage.type=PropertyInfo.STRING_CLASS;
		listPI[16]=PIWebPage;
	}
	public AdditionalInfo(FiscalDataBean fiscalData,CustomerBean customer) {
		// TODO Auto-generated constructor stub
		rfc= fiscalData.getRfc();
		folio=customer.getFolioPreap();
		fiscalName= fiscalData.getFiscalName();
		maritalStatus= customer.getMaritalStatus();
		extraInfo=customer.getExtraInfo();
		address=fiscalData.getAddress();
		gender=customer.getGender();
	}
	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: return address.getStreet();
			case 1: return address.getColony();
			case 2: return address.getZipCode();
			case 3: return maritalStatus;
			case 4: return address.getState();
			case 5: return address.getExtention();
			case 6: return folio;
			case 7: return address.getLada();
			case 8: return address.getPoblacion();
			case 9: return fiscalName;
			case 10: return address.getExternalNumber();
			case 11: return address.getInternalNumber();
			case 12: return rfc;
			case 13: return extraInfo.getBusinessSector();
			case 14: return gender;
			case 15: return address.getTelephone();
			case 16: return extraInfo.getWebPage();
			default:return null;
		}
	}
	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}
	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}
	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
			case 0: address.setStreet((String) arg1);
			case 1: address.setColony((String) arg1);
			case 2: address.setZipCode((String) arg1);
			case 3: maritalStatus=(Integer) arg1;
			case 4: address.setState((String) arg1);
			case 5: address.setExtention((String) arg1);
			case 6: folio=(String) arg1;
			case 7: address.setLada((String) arg1);
			case 8: address.setPoblacion((String) arg1);
			case 9: fiscalName=(String) arg1;
			case 10: address.setExternalNumber((String) arg1);
			case 11: address.setInternalNumber((String) arg1);
			case 12: rfc=(String) arg1;
			case 13: extraInfo.setBusinessSector((String) arg1);
			case 14: gender=(Integer) arg1;
			case 15: address.setTelephone((String) arg1);
			case 16: extraInfo.setWebPage((String) arg1);
		}
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}

}
