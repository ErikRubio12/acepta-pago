package mx.com.bancoazteca.aceptapago.beans;

import java.util.Hashtable;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class RequesUpdateImeiBean implements KvmSerializable{

	private static final int FIELDS = 2;
	private String newFolioPreap;
	private String folioPreap;
	private static PropertyInfo pIFoliopreap= new PropertyInfo();
	private static PropertyInfo pINewFoliopreap= new PropertyInfo();
	private static PropertyInfo listPI[]= new PropertyInfo[FIELDS];
	private static final String FOLIO = "folioPreap";
	private static final String NEW_FOLIO = "newFolioPreap";
	static{


		pIFoliopreap= new PropertyInfo();
		pIFoliopreap.name=FOLIO;
		pIFoliopreap.type=PropertyInfo.STRING_CLASS;

		pINewFoliopreap= new PropertyInfo();
		pINewFoliopreap.name=NEW_FOLIO;
		pINewFoliopreap.type=PropertyInfo.STRING_CLASS;

		listPI[0]= pIFoliopreap;
		listPI[1]=pINewFoliopreap;
	}
	public RequesUpdateImeiBean(String folioPreap, String newFolioPreap) {
		// TODO Auto-generated constructor stub
		this.folioPreap=folioPreap;
		this.newFolioPreap=newFolioPreap;
	}

	public String getNewFolioPreap() {
		return newFolioPreap;
	}

	public void setNewFolioPreap(String newFolioPreap) {
		this.newFolioPreap = newFolioPreap;
	}

	public String getFolioPreap() {
		return folioPreap;
	}

	public void setFolioPreap(String folioPreap) {
		this.folioPreap = folioPreap;
	}

	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0: return folioPreap;
		case 1: return newFolioPreap; 
		default: return null;
		}
	}

	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}

	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}

	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0: pIFoliopreap=(PropertyInfo) arg1; break;
		case 1: pINewFoliopreap=(PropertyInfo) arg1; break;
		}
	}

}
