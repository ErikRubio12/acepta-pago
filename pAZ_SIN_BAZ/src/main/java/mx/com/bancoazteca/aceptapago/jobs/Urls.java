 package mx.com.bancoazteca.aceptapago.jobs;

public class Urls {
	/**
	 * DESARROLLO
	 * ip Ebanking  : http://10.50.70.80:9080
	 * ip Alnova	: http://10.51.209.71
	 * ip PAZ3		: http://10.51.193.119:9086
	 * PRODUCCION
	 * ip Ebanking	: http://200.38.122.178 con salida a internet
	 * ip Ebanking	: http://10.63.85.151:9080
	 * ip Alnova	: http://200.38.122.37
	 * ip PAZ3		: http://www.pagoazteca.com.mx
	 */	
//********************** DESARROLLO ************************************ 9096
//
//	public final static String EBANKING = "http://10.50.70.80:9080/WSPagoAzteca/";
//	public final static String ALNOVA = "http://10.51.209.71/PagoAzcteca/";
//	public final static String PAZ3 = "http://10.50.109.44:9086/BusinessToBusinessWS/";
//	public final static String DIGITALIZACION="http://10.51.82.189:8080/";
//
//********************** PRODUCCION ************************************

	public final static String DIGITALIZACION = "http://200.38.122.86:8080/";
	public final static String EBANKING = "http://200.38.122.178/WSPagoAzteca/";
	public final static String ALNOVA = "http://200.38.122.37/PagoAzteca/";
//	public final static String PAZ3="http://www.pagoazteca.com.mx/BusinessToBusinessWS/";
	public final static String PAZ3="https://www.puntoazteca.com.mx/BusinessToBusinessWS/";
}
//10.57.161.25