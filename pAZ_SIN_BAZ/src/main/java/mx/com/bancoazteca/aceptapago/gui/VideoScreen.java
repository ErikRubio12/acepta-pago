package mx.com.bancoazteca.aceptapago.gui;

import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.hw.LocationThread;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.util.Utility;

public class VideoScreen extends BaseActivity{	
	private static final String VIDEO_NAME = "video";
	public final static String TAG_VIDEO ="VIDEO";
	private Toolbar mtoolbar;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();

		setContentView(R.layout.video_screen);
		mtoolbar=(Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		ActionBar actionBar=getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDefaultDisplayHomeAsUpEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
		}

		LocationManager manager=Utility.getLocationManager(this);
		if(manager.isProviderEnabled(LocationThread.GPS) && manager.isProviderEnabled(LocationThread.NETWORK)){
			Intent service = new Intent(this, LocationService.class);
			startService(service);
		}

	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
			
	}
	@Override
	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		super.startBackgroundProcess();
		
	}
	@Override
	protected void loadUILandscape() {
		// TODO Auto-generated method stub
		super.loadUILandscape();

	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	@Override
	public void release() {
		// TODO Auto-generated method stub
		super.release();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_ingresar,menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id=item.getItemId();
		switch (id){
			case R.id.btn_menu_sesion:
				login();
				return true;

		}
		return super.onOptionsItemSelected(item);
	}

	public void login(){
		goForward(PagoAzteca.class,this);
		finish();
	}
}

