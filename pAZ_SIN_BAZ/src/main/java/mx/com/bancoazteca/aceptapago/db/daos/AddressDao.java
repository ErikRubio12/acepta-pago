package mx.com.bancoazteca.aceptapago.db.daos;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.database.Cursor;
import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.db.Queryable;
import mx.com.bancoazteca.aceptapago.db.Query;

public class AddressDao extends Dao implements Queryable<AddressBean>{

	public AddressDao(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public String queryLada(int idCountry,int idState,int idCity){
		String res="";
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_LADA, new String[]{String.valueOf(idCountry),String.valueOf(idState),String.valueOf(idCity)});
		if(cursor.moveToFirst()){
			res=cursor.getString(0).trim();
		}
		cursor.close();
		db.close();
		return res;
	}
	public List<String>queryPoblacion(int idState){
		ArrayList<String> states= new ArrayList<String>();
		states.add("POBLACION");
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_POBLACION_BY_STATE, new String[]{String.valueOf(idState)});
		if(cursor.moveToFirst()){
			do {
				states.add(cursor.getString(0).trim());
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return states;
	}
	public List<String> queryColony(int idState,int idPoblacion) {
		// TODO Auto-generated method stub
		ArrayList<String> colonies= new ArrayList<String>();
		colonies.add("COLONIA");
		String currentColony=null;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_COLONY_BY_POBLACION_AND_STATE, new String[]{String.valueOf(idPoblacion),String.valueOf(idState)});
		if(cursor.moveToFirst()){
			do {
				currentColony=cursor.getString(0).trim();
				if(currentColony.length()>50)
					currentColony=currentColony.substring(0, 50);
				colonies.add(currentColony);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return colonies;
	}
	public List<String> queryZipcode(int idPoblacion, int idState,String colony) {
		// TODO Auto-generated method stub
		ArrayList<String> zipcodes= new ArrayList<String>();
		colony="%"+colony+"%";
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_ZIP_CODE, new String[]{String.valueOf(idPoblacion),String.valueOf(idState),colony});
		if(cursor.moveToFirst()){
			do {
				zipcodes.add(cursor.getString(0).trim());
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return zipcodes;
	}

	
	@Override
	public List<AddressBean> queryAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AddressBean queryById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(AddressBean entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(AddressBean entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean insert(AddressBean entity) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public AddressBean query(String table,String columns[],String selection,String selectionArgs[],String groupBy,String having,String orderBy) {
		// TODO Auto-generated method stub
		return null;
	}
}
