package mx.com.bancoazteca.aceptapago.baz.ws.parsers;

import java.io.IOException;
import java.util.ArrayList;
import mx.com.bancoazteca.aceptapago.baz.entidades.Colonia;
import mx.com.bancoazteca.aceptapago.baz.entidades.ZonificacionEkt;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseEktDinero {
	UtilidadesParseo util = new UtilidadesParseo();


	
	
	
	public ZonificacionEkt parseaZonificacion(String xml)
			throws SAXException, IOException {
		ZonificacionEkt unaZonificacion = new ZonificacionEkt();
		util.setXml(xml);
		try {
			Element docEle = util.parseaTexto();
			NodeList nl = docEle.getElementsByTagName("return");
			if (nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {
					Node nodo = nl.item(i);
					if (nodo.getNodeType() == Node.ELEMENT_NODE) { 
						
						Element elemento = (Element) nodo;
						unaZonificacion.setCalle(traeTexto(elemento,"calle"));
						unaZonificacion.setCalleBaz(traeTexto(elemento,"calleBaz"));
						//unaZonificacion.setCodigoPostal(traeTexto(elemento,"codigoPostal"));
						unaZonificacion.setCodigoPostalBaz(traeTexto(elemento,"codigoPostalBaz"));
						unaZonificacion.setColonia(traeTexto(elemento,"colonia"));
						unaZonificacion.setColoniaBaz(traeTexto(elemento,"coloniaBaz"));
						unaZonificacion.setDiaDesc(traeTexto(elemento,"diaDesc"));
						unaZonificacion.setEstado(traeTexto(elemento,"estado"));
						unaZonificacion.setEstadoBaz(traeTexto(elemento,"estadoBaz"));
						unaZonificacion.setIdCalleBaz(traeTexto(elemento,"idCalleBaz"));
						unaZonificacion.setIdColoniaBaz(traeTexto(elemento,"idColoniaBaz"));
						unaZonificacion.setIdCuadrante(traeTexto(elemento,"idCuadrante"));
						unaZonificacion.setIdEstadoBaz(traeTexto(elemento,"idEstadoBaz"));
						unaZonificacion.setIdMunicipoBaz(traeTexto(elemento,"idMunicipoBaz"));
						unaZonificacion.setIdPais(traeTexto(elemento,"idPais"));
						unaZonificacion.setIdZG(traeTexto(elemento,"idZG"));
						unaZonificacion.setLatitud(traeTexto(elemento,"latitud"));
						unaZonificacion.setLatitudBaz(traeTexto(elemento,"latitudBaz"));
						unaZonificacion.setLongitud(traeTexto(elemento,"longitud"));
						unaZonificacion.setLongitudBaz(traeTexto(elemento,"longitudBaz"));
						unaZonificacion.setMapa(traeTexto(elemento,"mapa"));
						unaZonificacion.setMunicipio(traeTexto(elemento,"municipio"));
						unaZonificacion.setMunicipoBaz(traeTexto(elemento,"municipoBaz"));
						unaZonificacion.setNomEmp(traeTexto(elemento,"nomEmp"));
						unaZonificacion.setNumEmp(traeTexto(elemento,"numEmp"));
						unaZonificacion.setNumero(traeTexto(elemento,"numero"));
						unaZonificacion.setNumeroBaz(traeTexto(elemento,"numeroBaz"));

					}
				}
			}
			if(unaZonificacion.getCalle().trim().length()==0)
				return null;
			else
				return unaZonificacion;
		} catch (Exception pce) {
			pce.printStackTrace();
			unaZonificacion = new ZonificacionEkt();
			return unaZonificacion;
		}
	}
	
	public String traeTexto(Element elemento,String campo){
		String retorno = "";
		try{
			retorno = elemento.getElementsByTagName(campo).item(0).getFirstChild().getNodeValue();
		}catch(Exception e){
			 return "";
		}
		
		if(retorno == null){
			 return "";
		} else {
			retorno = retorno.trim();
			return retorno;
		}
		
	}
	
	
	
	public ArrayList<String> parseaCodigosPostales(String xml)
			throws SAXException, IOException {
		ArrayList<String> misCodigosPostales = new ArrayList<String>();
		util.setXml(xml);
		try {
			Element docEle = util.parseaTexto();
			NodeList nl = docEle.getElementsByTagName("return");
			if (nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {
					Node nodo = nl.item(i);
					if (nodo.getNodeType() == Node.ELEMENT_NODE) { 
						Element elemento = (Element) nodo;
						String miCP = (elemento.getElementsByTagName("codPos").item(0).getFirstChild().getNodeValue().trim());
						misCodigosPostales.add(miCP);
					}
				}
			}
			return misCodigosPostales;
		} catch (Exception pce) {
			pce.printStackTrace();
			misCodigosPostales = new ArrayList<String>();
			return misCodigosPostales;
		}
	}
	
	public ArrayList<Colonia> parseaColonias(String xml)
			throws SAXException, IOException {
		ArrayList<Colonia> misColonias = new ArrayList<Colonia>();
		util.setXml(xml);
		try {
			Element docEle = util.parseaTexto();
			NodeList nl = docEle.getElementsByTagName("return");
			if (nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {
					Node nodo = nl.item(i);
					if (nodo.getNodeType() == Node.ELEMENT_NODE) { 
						Element elemento = (Element) nodo;
						Colonia miColonia = new Colonia();
						miColonia.setColonia(elemento.getElementsByTagName("colonia").item(0).getFirstChild().getNodeValue().trim());
					    miColonia.setConsec(elemento.getElementsByTagName("consec").item(0).getFirstChild().getNodeValue().trim());
						miColonia.setEstado(elemento.getElementsByTagName("estado").item(0).getFirstChild().getNodeValue().trim());
						miColonia.setIdEdo(elemento.getElementsByTagName("idEdo").item(0).getFirstChild().getNodeValue().trim());
						miColonia.setIdPob(elemento.getElementsByTagName("idPob").item(0).getFirstChild().getNodeValue().trim());
						miColonia.setPoblacion(elemento.getElementsByTagName("poblacion").item(0).getFirstChild().getNodeValue().trim());
						misColonias.add(miColonia);
					}
				}
			}
			return misColonias;
		} catch (Exception pce) {
			pce.printStackTrace();
			misColonias = new ArrayList<Colonia>();
			return misColonias;
		}
	}
}