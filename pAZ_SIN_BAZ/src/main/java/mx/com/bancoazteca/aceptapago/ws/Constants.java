package mx.com.bancoazteca.aceptapago.ws;

/**
 * Created by B931724 on 15/02/18.
 */

public class Constants {


//  *****************************          DESARROLLO   ***************************************************
//    public static final String BASE_URL_DIGI = "http://10.51.53.90:8443/WS_BDMAPPS/external/";
//    public static final String BASE_URL_DIGI = "http://10.51.53.121:8079/WS_BDMAPPS/external/";
//
//
//
// *****************************          PRODUCCIÓN   ***************************************************
    public static final String BASE_URL_DIGI = "https://bazdigital.com:443/bazdigapp/";


    public static final String WS_HAZ_LOGIN = "security/login/0.1/dologinapp";
    public static final String WS_GENERA_LLAVE = "digitalizacion/generar/documento/hash";
    public static final String GLOBAL_TAG = "WS_AP";






}
