package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mx.com.bancoazteca.adapters.PersonAdapter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.messages.EmailBean;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.ClientBean;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnResponse;
import mx.com.bancoazteca.aceptapago.jobs.ClientsJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.ConfirmationDialog;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.ui.customviews.EditTextSearch;
import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;

public class Clients extends BaseActivity implements MessageListener<Message, String>{
	public static final int CLIENTS_CODE = 100;
	public static final String CLIENT_SELECTED="clientSelected";
	private static final int DELETE_ALL =-100;
	protected static final String CLIENTS = "CLIENTES";
	private static final int POPUP_LOADING_CLIENTS = 11;
	private static final int POPUP_WANT_DELETE_ALL = 12;
	protected static final int POPUP_WANT_DELETE = 13;
	private static final int POPUP_DELETE_ALL = 14;
	private static final int POPUP_DELETE = 15;
	protected static final int POPUP_EXPTION = 16;
	protected static final int POPUP_ERROR = 17;
	protected static final int POPUP_ERROR2 = 19;
	protected static final int POPUP_ERROR_ADDING=20;
	private static final int POPUP_ADDING_CUSTOMER = 21;	
	private static final String CLIENTS_SCREEN = "Clients";	
	private ReloaderClients reloader;
	private List<ClientBean>list;
	public ListView clientsList;
	private EditTextSearch search;
	private String rfc;
	private String folio;
	private EmailBean email=null;
	private boolean deletingMode=false;
	private Integer rowClientToDelete=-1;
	private SaleBean sale;
	private SaleReturnResponse operationResult;
	private String tmpTag;
	private int tmpMsg;
	private String tmpParameter;
	private LinearLayout container;
	private EditText nameEdit,emailEdit;
	private ClientsAdd handler= new ClientsAdd();
	private boolean afterCreateDelete=false;
	private Toolbar mtoolbar;

	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(email==null)
			super.onBackPressed();
	}
	
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.clients);
		clientsList=(ListView) findViewById(R.id.clientsTable);
		search=(EditTextSearch) findViewById(R.id.editSearch);
		container=(LinearLayout) findViewById(R.id.container);
		nameEdit=(EditText) findViewById(R.id.name);
		emailEdit=(EditText) findViewById(R.id.email);
		search.setTableToUpdate(clientsList);
		mtoolbar=(Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}

	}



	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params != null){
			folio=(String) params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
			rfc=(String) params.getSerializable(CustomerBean.RFC);
			
			email=(EmailBean) params.getSerializable(EmailBean.EMAIL);
			sale=(SaleBean) params.getSerializable(SaleBean.SALE);
			operationResult= (SaleReturnResponse) params.getSerializable(SaleScreen.RETURN_RESPONSE);
		}
	}
	@Override
	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		super.startBackgroundProcess();
		showDialog(POPUP_LOADING_CLIENTS);
		reloader=new ReloaderClients();
		backgroundThread= new ClientsJob<ArrayList<ClientBean>>(this,folio, rfc,reloader);
		backgroundThread.start();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("tag", tmpTag);
		outState.putInt("msg",tmpMsg );
		outState.putString("parameter", tmpParameter);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		tmpTag=savedInstanceState.getString("tag");
		tmpMsg=savedInstanceState.getInt("tag");
		tmpParameter=savedInstanceState.getString("parameter");
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Log.i("clients","id dialog "+id);
		DialogButton cancel;
		DialogButton accept;
		DialogButton button;
		switch (id) {
		case POPUP_ADDING_CUSTOMER:
			popUp = new PopUp(this, R.string.popUpMessage,R.string.addClient);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		case POPUP_LOADING_CLIENTS:
			popUp = new PopUp(this, R.string.popUpMessage,R.string.loadingClients);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		case POPUP_DELETE_ALL:
			popUp = new PopUp(this, R.string.popUpMessage,R.string.deletingClients);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		case POPUP_DELETE:
			popUp = new PopUp(this, R.string.popUpMessage,R.string.deletingClient);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		default:
			return null;
		}
	}
	private void fillData(){
		PersonAdapter adapter=new PersonAdapter(this,list);		
		if(email != null){
			Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "email no nulo: " + email);
			adapter.setLsitener(clientSelectedToSendMail);
		}
			
		clientsList.setAdapter(adapter);
	}
	public void editingMode(View a){
		if(!deletingMode){
			deletingMode=!deletingMode;
			RelativeLayout curreLayout=null;
			((PersonAdapter)clientsList.getAdapter()).setEditingMode(deletingMode);
			View currentDeleteEditView=null;
			container.setVisibility(View.VISIBLE);
			for (int i = 0; i < clientsList.getChildCount(); i++) {
				curreLayout=(RelativeLayout) clientsList.getChildAt(i);
				Logger.log("Log " + curreLayout.getTag());
				currentDeleteEditView= curreLayout.findViewById(R.id.content);
				currentDeleteEditView.setTag(curreLayout.getTag());
				currentDeleteEditView.findViewById(R.id.edit).setTag(curreLayout.getTag());
				currentDeleteEditView.findViewById(R.id.delete).setTag(curreLayout.getTag());
			}
		}
		else{
			deletingMode=!deletingMode;
			((PersonAdapter)clientsList.getAdapter()).setEditingMode(deletingMode);
			RelativeLayout curreLayout=null;
			View currentDeleteEditView=null;
			container.setVisibility(View.GONE);
			for (int i = 0; i < clientsList.getChildCount(); i++) {
				curreLayout=(RelativeLayout) clientsList.getChildAt(i);
				currentDeleteEditView= curreLayout.findViewById(R.id.content);

				
			}
		}
		search.invalidate();
		clientsList.invalidate();
	}
	public void home(View v){
		setResult(SaleScreen.HOME_RESULT);
		finish();
	}
	@Override
	public void back(View v) {
		// TODO Auto-generated method stub
		super.back(v);
		finish();
	}
	public void add(View v){
		if (nameEdit.getText().length() ==0 || emailEdit.getText().length()==0) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.emptyFields));
			dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
		}
		else if(!EmailBean.validEmail(emailEdit.getText().toString())){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.invalidEmail));
			dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
		}
		else{
			newClient(null);
		}
	}
	public void newClient(View view){
		if(container.getVisibility()==View.VISIBLE){
			showDialog(POPUP_ADDING_CUSTOMER);
			ClientBean client=new ClientBean();
			client.setCompleteName(nameEdit.getText().toString().toUpperCase(Locale.getDefault()));
			client.setMail(emailEdit.getText().toString());
			backgroundThread= new ClientsJob<String>(this,folio, rfc, client, handler, ClientsJob.CREATE_CLIENT);
			backgroundThread.start();
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE,CLIENTS_SCREEN,"resultcode: "+ resultCode + "  requestCode: " + requestCode);
		switch (requestCode) {
		case CLIENTS_CODE:
			if (resultCode==SaleScreen.HOME_RESULT) {
				setResult(SaleScreen.HOME_RESULT);
				finish();
			}
			else if (resultCode==RESULT_FIRST_USER) {

				if (data.getExtras()!=null) {
					int mode=data.getExtras().getInt("mode");
					Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "modo "+mode);
				}
				showDialog(POPUP_LOADING_CLIENTS);
				search.setText("");
				afterCreateDelete=true;
				reloadClients();
				
				
			}
			else if (resultCode==RESULT_CANCELED){
				Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "BackPresed");
			}
		break;
		case  SaleScreen.SETTINGS_CODE:
			if (resultCode==RESULT_OK) {
				setResult(RESULT_OK);
				finish();
			}
			else if (resultCode==101){
				Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "Error en el servicio");
			}
			else if (resultCode==RESULT_CANCELED){
				Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "BackPresed");
			}
			else if (resultCode==RESULT_FIRST_USER) {

				showDialog(POPUP_LOADING_CLIENTS);
				reloadClients();
				
			}
			break;
		}
		
	}

	
	private void reloadClients(){
		
		clientsList.setAdapter(new PersonAdapter(this, new ArrayList<ClientBean>()));		
		backgroundThread= new Thread(new ClientsJob<ArrayList<ClientBean>>(this,folio, rfc,reloader));
		backgroundThread.start();
	}
	
	private OnClickListener clientSelectedToSendMail= new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			ClientBean clientSelected=list.get((Integer)v.getTag());
			Logger.log(Logger.MESSAGE, CLIENTS_SCREEN,"CLIENTE : " + clientSelected);
			email.removeDestinies();
			try {
				email.addDestiny(clientSelected.getMail());
			} catch (GenericException e) {
				// TODO Auto-generated catch block
				Logger.log(Logger.EXCEPTION, CLIENTS_SCREEN,"email invalido");
			}
			ArrayList<Serializable> params=new ArrayList<Serializable>();
			params.add(clientSelected);
			params.add(email);
			params.add(folio);
			params.add(rfc);
			params.add(sale);
			params.add(operationResult);
			ArrayList<String> paramsName=new ArrayList<String>();
			paramsName.add(CLIENT_SELECTED);
			paramsName.add(EmailBean.EMAIL);
			paramsName.add(CustomerBean.FOLIO_PREAPERTURA);
			paramsName.add(CustomerBean.RFC);
			paramsName.add(SaleBean.SALE);
			paramsName.add(SaleScreen.RETURN_RESPONSE);
			intent= new Intent();
			for (int i = 0; i < paramsName.size(); i++) 
				intent.putExtra(paramsName.get(i), params.get(i));
			
			setResult(RESULT_OK, intent);
			finish();
			
		}
	};
	
	
	
	private void deleteAll() {
		// TODO Auto-generated method stub
		if (list.size()>0) {
			showDialog(POPUP_DELETE_ALL);
			rowClientToDelete=DELETE_ALL;
			backgroundThread = new Thread(new ClientsJob<String>(this,folio, rfc, list,this, ClientsJob.DELETE_CLIENT));
			backgroundThread.start();
		}
		
	}
	
	@Override
	public void sendMessage(Context ctx,final Message message,final String parameter) {
		// TODO Auto-generated method stub
		if(isFinishing())
			return;
		runOnUiThread(new Runnable() {
			public void run() {
				removeDialogs();
				if (message.getType()==Message.OK) {
					if (rowClientToDelete!=DELETE_ALL) {
						((PersonAdapter) clientsList.getAdapter()).deleteComplete(rowClientToDelete.intValue());
						((BaseAdapter) clientsList.getAdapter()).notifyDataSetChanged();
						showDialog(POPUP_LOADING_CLIENTS);
						afterCreateDelete=true;
						reloadClients();
					}
					else{						
						((PersonAdapter) clientsList.getAdapter()).deleteAllComplete();
						((BaseAdapter) clientsList.getAdapter()).notifyDataSetChanged();
						showDialog(POPUP_LOADING_CLIENTS);
						reloadClients();
					}
					
				}
				else if (message.getType()==Message.EXCEPTION){
					tmpMsg=message.getTextMessage();
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(tmpMsg));
					dialog.show(getSupportFragmentManager(),""+POPUP_EXPTION);
				}
				
			}
		});
		
		
	}
	@Override
	public void sendStatus(Context ctx,int status) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void sendMessage(Context ctx,final Message message, final Object... parameters) {
		// TODO Auto-generated method stub
		if(isFinishing())
			return;
		runOnUiThread(new Runnable() {
			public void run() {
				if (message.getType()==Message.ERROR) {
					removeDialogs();
					tmpParameter=(String)parameters[0];
					ConfirmationDialog confirmationDialog=ConfirmationDialog.newInstance(getString(R.string.popUpError),tmpParameter);
					confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							dialog.dismiss();
							showDialog(POPUP_LOADING_CLIENTS);
							reloadClients();
						}
					});
					confirmationDialog.setmCancelListener(new ConfirmationDialog.CancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							finish();
						}
					});
					confirmationDialog.show(getSupportFragmentManager(),""+POPUP_ERROR);
				}
				
			}
		});
	}
	
	//No afecta el hecho de que se mande remover un dialogo que no es
	private void removeDialogs(){
		removeDialog(POPUP_DELETE);
		removeDialog(POPUP_DELETE_ALL);
		removeDialog(POPUP_LOADING_CLIENTS);
		removeDialog(POPUP_ADDING_CUSTOMER);
	}
	
	public void delete(View v){
		Logger.log(Logger.MESSAGE, CLIENTS_SCREEN,"ELIMINAR");		
		tmpTag =v.getTag().toString();
		Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "tag del cliente a eliminar: " + tmpTag);
		rowClientToDelete=Integer.parseInt(tmpTag);
		final ClientBean clientToDelete= list.get(rowClientToDelete);
		Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "cliente a eliminar: " + clientToDelete);
		StringBuffer text= new StringBuffer(getResources().getString(R.string.wantToDeleteClient));
		text.append("\n");
		text.append(clientToDelete.getCompleteName());
		text.append(" - ");
		text.append(clientToDelete.getMail());
		ConfirmationDialog confirmationDialog=ConfirmationDialog.newInstance(getString(R.string.popUpMessage),text.toString());
		confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
			@Override
			public void onAccept(DialogInterface dialog) {
				dialog.dismiss();
				showDialog(POPUP_DELETE);
				clientToDelete.setId(clientToDelete.getId());
				backgroundThread = new Thread(new ClientsJob<String>(Clients.this,folio, rfc, clientToDelete,Clients.this, ClientsJob.DELETE_CLIENT));
				backgroundThread.start();
			}
		});
		confirmationDialog.show(getSupportFragmentManager(),""+POPUP_WANT_DELETE);
	}
	public void edit(View v){
		Logger.log(Logger.EXCEPTION, CLIENTS_SCREEN,"EDITAR: "+ v.getTag().toString());
		ArrayList<Serializable> params=new ArrayList<Serializable>();
		params.add( list.get((Integer)v.getTag()));
		params.add(folio);
		params.add(rfc);
		ArrayList<String> paramsName=new ArrayList<String>();
		paramsName.add(CLIENT_SELECTED);
		paramsName.add(CustomerBean.FOLIO_PREAPERTURA);
		paramsName.add(CustomerBean.RFC);
		goForwardForResult(NewClient.class, v.getContext(), params, paramsName, CLIENTS_CODE);
		
	}
	private final class ReloaderClients implements MessageListener<Message, ArrayList<ClientBean>>{
		
		@Override
		public void sendMessage(Context ctx,final Message message,final ArrayList<ClientBean> parameter) {
			// TODO Auto-generated method stub
			Logger.log(Logger.MESSAGE, CLIENTS_SCREEN,"entro en reloader");
			runOnUiThread(new Runnable() {
				public void run() {

					if (message.getType()==Message.OK) {
						list=parameter;
						fillData();
						clientsList.invalidate();
						removeDialogs();
						if(afterCreateDelete){
							afterCreateDelete=false;
							deletingMode=true;
							editingMode(null);
						}
						
					}
					else if (message.getType()==Message.EXCEPTION){
						removeDialogs();
						tmpMsg= message.getTextMessage();
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(tmpMsg));
						dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								finish();
							}
						});
						dialog.show(getSupportFragmentManager(), "" + POPUP_EXPTION);
					}

				}
			});


		}

		@Override
		public void sendStatus(Context ctx,int status) {
			// TODO Auto-generated method stub

		}
		@Override
		public void sendMessage(Context ctx,final Message message, final Object... parameters) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {
				public void run() {
					removeDialogs();
					if (message.getType()==Message.ERROR) {
						tmpParameter=(String)parameters[0];
						ConfirmationDialog confirmationDialog=ConfirmationDialog.newInstance(getString(R.string.popUpError),tmpParameter);
						confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								dialog.dismiss();
								showDialog(POPUP_LOADING_CLIENTS);
								reloadClients();
							}
						});
						confirmationDialog.setmCancelListener(new ConfirmationDialog.CancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								finish();
							}
						});
						confirmationDialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
					}
				}
			});
		}
	}
	private class ClientsAdd implements MessageListener<Message, String>{

		@Override
		public void sendMessage(Context ctx,final  Message message, String parameter) {
			// TODO Auto-generated method stub
			if (isFinishing()) {
				return;
			}
			runOnUiThread(new Runnable() {				
				public void run() {
					removeDialog(POPUP_ADDING_CUSTOMER);
					if(message.getType()==Message.OK){
						showDialog(POPUP_LOADING_CLIENTS);
						nameEdit.setText("");
						emailEdit.setText("");
						afterCreateDelete=true;
						reloadClients();
					}
					else if(message.getType()==Message.EXCEPTION){
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
						dialog.show(getSupportFragmentManager(), "" + POPUP_CONNECTION_ERROR);
					}
				}
			});
		}

		@Override
		public void sendStatus(Context ctx, int status) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void sendMessage(final Context ctx,final Message message,
				final Object... parameters) {
			if (isFinishing()) {
				return;
			}
			runOnUiThread(new Runnable() {	
				public void run() {
					removeDialog(POPUP_ADDING_CUSTOMER);
					if (message.getType()==Message.ERROR) {
						tmpParameter=(String)parameters[0];
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),tmpParameter);
						dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR_ADDING);
						Logger.log(Logger.MESSAGE, CLIENTS_SCREEN, "Error en el servicio");

					}
					
				}
			});
		}
		
	}
}
