package mx.com.bancoazteca.aceptapago.ws;

/**
 * Created by B931724 on 15/02/18.
 */

import android.util.Log;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import mx.com.bancoazteca.aceptapago.beans.GenerateKeyBean;
import mx.com.bancoazteca.aceptapago.beans.GenerateRequest;
import mx.com.bancoazteca.aceptapago.beans.LoginRequest;
import mx.com.bancoazteca.aceptapago.beans.LoginServiceBean;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class APIClient {

    private static ApiInterface ApiInterface;


    public static ApiInterface getClient() {


        if (ApiInterface == null) {
            CookieHandler cookieHandler = new CookieManager();

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient clienthttp = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .cookieJar(new JavaNetCookieJar(cookieHandler))
                    .build();

            Retrofit client = new Retrofit.Builder()
                    .client(clienthttp)
                    .baseUrl(Constants.BASE_URL_DIGI)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiInterface = client.create(ApiInterface.class);
        }
        return ApiInterface;

    }

    public interface ApiInterface {
        @POST(Constants.WS_HAZ_LOGIN)
        @Headers("Content-Type:text/plain")
        Call<LoginServiceBean> methodName(@Body LoginRequest body);


        @POST(Constants.WS_GENERA_LLAVE)
        @Headers("Content-Type:text/plain; charset=utf-8")
        Call<GenerateKeyBean> generateKey(@Header("Cookie") String cookieValue,@Body GenerateRequest body);
    }

}
