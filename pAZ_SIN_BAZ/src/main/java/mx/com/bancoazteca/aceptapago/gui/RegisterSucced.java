package mx.com.bancoazteca.aceptapago.gui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.ui.BaseActivity;

public class RegisterSucced extends BaseActivity {
	private Button accept;
	private TextView text;
	private RegistrationBean register;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.registration_succed);
		accept=(Button) findViewById(R.id.acceptButton);
		text=(TextView) findViewById(R.id.textFolio);
		accept.setOnClickListener(acceptListener);

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		accept.callOnClick();
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params !=null) {
			register = (RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
			text.setText(getString(R.string.textFolioRegisterSucced) + " " + params.getString(CustomerBean.FOLIO_PREAPERTURA));
		}

	}
	private OnClickListener acceptListener= new OnClickListener() {
		public void onClick(View v) {
			RegistrationBean registerBean=(RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
			intent.putExtra(RegistrationBean.REGISTER,registerBean);
			setResult(RESULT_OK,intent);
			finish();

		}
	};
}
