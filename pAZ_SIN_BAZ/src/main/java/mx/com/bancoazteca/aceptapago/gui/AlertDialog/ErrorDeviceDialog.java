package mx.com.bancoazteca.aceptapago.gui.AlertDialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import mx.com.bancoazteca.aceptapago.R;


/**
 * Created by marcoantonio on 08/02/16.
 */
public class ErrorDeviceDialog extends DialogFragment{
    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_MESSAGE = "ARG_MESSAGE";
    private String mTitle;
    private String mMessage;
    private AcceptListener mAcceptListener;
    private DismissListener mDismissListener;


    public ErrorDeviceDialog() {
    }


    public static ErrorDeviceDialog newInstance() {
        ErrorDeviceDialog fragment = new ErrorDeviceDialog();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.popup_error, null, false);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setPositiveButton(mx.commons.R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mAcceptListener != null) {
                            mAcceptListener.onAccept(dialog);
                        } else {
                            dialog.dismiss();
                        }
                    }
                });
        AlertDialog dialog=builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    public void setmAcceptListener(AcceptListener mAcceptListener) {
        this.mAcceptListener = mAcceptListener;
    }

    public void setmDismissListener(DismissListener mDismissListener) {
        this.mDismissListener = mDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mDismissListener!=null){
            mDismissListener.onDismiss();
        }
    }

    public interface  AcceptListener{
        public void onAccept(DialogInterface dialog);
    }

    public interface DismissListener{
        public void onDismiss();

    }
}
