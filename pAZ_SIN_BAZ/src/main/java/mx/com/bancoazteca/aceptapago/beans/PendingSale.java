package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;

public class PendingSale implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private	String channel;
	private	String txDate;
	private	String terminal;
	private	String idTx;
	private	String pan;
	private	String cardData; 
	private	String validity;
	private	String imei;
	private	String longitud; 
	private	String latitud;
	private String amount;
	private String causeReverse;
	
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getTxDate() {
		return txDate;
	}

	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getIdTx() {
		return idTx;
	}

	public void setIdTx(String idTx) {
		this.idTx = idTx;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getCardData() {
		return cardData;
	}

	public void setCardData(String cardData) {
		this.cardData = cardData;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	
	public String getCauseReverse() {
		return causeReverse;
	}

	public void setCauseReverse(String causeReverse) {
		this.causeReverse = causeReverse;
	}

	@Override
	public String toString() {
		return "PendingSale [channel=" + channel + ", txDate=" + txDate
				+ ", terminal=" + terminal + ", idTx=" + idTx + ", pan=" + pan
				+ ", cardData=" + cardData + ", validity=" + validity
				+ ", imei=" + imei + ", longitud=" + longitud + ", latitud="
				+ latitud + ", amount=" + amount + ", causeReverse="
				+ causeReverse + "]";
	}
	
	

	
}
