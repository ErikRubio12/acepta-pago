package mx.com.bancoazteca.aceptapago.gui;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.social.BaseSocialMapActivity;
import mx.com.bancoazteca.util.Logger;

public class TwitterTemp extends Activity {
	private final static String TAG_TWITTER_TMP="TWITTER TMP";
	private WebView web;
	private String url;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.twitter_confirm);
		Bundle params= getIntent().getExtras();
		if (params != null) {
			web=(WebView) findViewById(R.id.web);
			web.setWebViewClient(new WebClient());	
			url=params.getString("url");
			web.loadUrl(url);
		}
		
	}
	
	private class WebClient extends WebViewClient{
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			Logger.log(Logger.MESSAGE, TAG_TWITTER_TMP,"onPageStarted "+ url);
			if (url!= null) {
				Uri param=Uri.parse(url);
				BaseSocialMapActivity.setVerifier(param.getQueryParameter(DetailSaleScreen.URL_TWITTER_OAUTH_VERIFIER));

				if (BaseSocialMapActivity.getVerifier() != null) {
					Logger.log(Logger.MESSAGE, TAG_TWITTER_TMP,"verifier "+ BaseSocialMapActivity.getVerifier());
					setResult(RESULT_OK);
					finish();
				}
			}
			
		}
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			Logger.log(Logger.MESSAGE, TAG_TWITTER_TMP,"shouldOverrideUrlLoading "+ url);
			if (url!= null) {
				Uri param=Uri.parse(url);
				BaseSocialMapActivity.setVerifier(param.getQueryParameter(DetailSaleScreen.URL_TWITTER_OAUTH_VERIFIER));

				if (BaseSocialMapActivity.getVerifier() != null) {
					Logger.log(Logger.MESSAGE, TAG_TWITTER_TMP,"verifier "+ BaseSocialMapActivity.getVerifier());
					setResult(RESULT_OK);
					finish();
					return true;
				}
			}
			return false;
		}
		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			Logger.log(Logger.MESSAGE, TAG_TWITTER_TMP,"onReceivedError "+ failingUrl);
			super.onReceivedError(view, errorCode, description, failingUrl);
		}
	}
}
