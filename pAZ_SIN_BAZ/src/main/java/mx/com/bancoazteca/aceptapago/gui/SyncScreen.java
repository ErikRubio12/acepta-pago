package mx.com.bancoazteca.aceptapago.gui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.media.AudioManager;
import android.view.View;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.aceptapago.Configuration;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.ui.BaseActivity;

public class SyncScreen extends BaseActivity {

	private FiscalDataBean fiscalData;
	private CustomerBean customer;
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.popup_reader);
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params != null){			
			fiscalData=(FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
			customer= (CustomerBean) params.getSerializable(CustomerBean.CUSTOMER);									
		}
	}
	public void confirmReader(View v){
		Configuration.setSwiped(false);
		Configuration.persistPreferences(this);
		AudioManager manager=(AudioManager) getSystemService(AUDIO_SERVICE);
		manager.setStreamVolume(3, manager.getStreamMaxVolume(3), AudioManager.FLAG_SHOW_UI);
		final List<Serializable> params= new ArrayList<Serializable>();
		params.add(fiscalData);
		params.add(customer);
		final List<String>paramNames= new ArrayList<String>();
		paramNames.add(FiscalDataBean.FISCAL_DATA);
		paramNames.add(CustomerBean.CUSTOMER);
		goForward(SaleScreen.class, this, params, paramNames);
		finish();
	}		
}
