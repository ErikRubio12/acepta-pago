package mx.com.bancoazteca.aceptapago.gui;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import java.io.File;

import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.customviews.ZoomImage;
import mx.com.bancoazteca.util.Logger;

public class ImageTicketScreen extends BaseActivity {

	private static final String TAG_IMAGE_TICKET = "TICKET IMAGE";
	private File ticketFile;
	private ZoomImage image;
	private int imageWidth,imageHeight;
	
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix(); 
	PointF startPoint = new PointF(); 
	PointF midPoint = new PointF();
	float oldDist = 1f; 
	static final int NONE = 0; 
	static final int DRAG = 1; 
	static final int ZOOM = 2; 
	int mode = NONE;
	private Toolbar mtoolbar;


	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.image_ticket);
		image=(ZoomImage) findViewById(R.id.image);
		image.setMaxZoom(2.0f);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params!= null){
			ticketFile=(File) params.getSerializable(DetailSaleScreen.FILE_PATH);
			image.setImageURI(Uri.fromFile(ticketFile));
			imageWidth=this.params.getInt("ancho");
			imageHeight=this.params.getInt("alto");
			Logger.log(Logger.MESSAGE, TAG_IMAGE_TICKET, "ancho: "+ imageWidth +  " alto: "+ imageHeight);
			Logger.log(Logger.MESSAGE, TAG_IMAGE_TICKET, "ancho: "+ getWidth() +  " alto: "+ getHeight());
			image.setOnTouchListener(listener);
		}
	}
	private OnTouchListener listener=new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			ImageView view = (ImageView) v; 
			System.out.println("matrix=" + savedMatrix.toString()); 
			switch (event.getAction() & MotionEvent.ACTION_MASK) { 
			case MotionEvent.ACTION_DOWN: 
				savedMatrix.set(matrix); 
				startPoint.set(event.getX(), event.getY()); 
				mode = DRAG; 
				break; 
			case MotionEvent.ACTION_POINTER_DOWN:
			oldDist = spacing(event); 
			if (oldDist > 10f) { 
				savedMatrix.set(matrix); 
				midPoint(midPoint, event); 
				mode = ZOOM; 

			}
			break; 
			case MotionEvent.ACTION_UP: 
			case MotionEvent.ACTION_POINTER_UP: 
				mode = NONE; 
				break; 
			case MotionEvent.ACTION_MOVE: 
				if (mode == DRAG) { 
					matrix.set(savedMatrix);
					matrix.postTranslate(event.getX() - startPoint.x, event.getY() - startPoint.y); 
				} 	
				else if (mode == ZOOM) { 
					float newDist = spacing(event); 
					if (newDist > 10f) { 
						matrix.set(savedMatrix); 
						float scale = newDist / oldDist; 
						System.out.println("escalado");
						matrix.postScale(scale, scale, midPoint.x, midPoint.y); 
					} 
				} 

				break; 
			} 
			view.setImageMatrix(matrix); 
			view.invalidate();
			return true;
		}
	};
	private float spacing(MotionEvent event) { 
		float x = event.getX(0) - event.getX(1); 
		float y = event.getY(0) - event.getY(1); 
		return FloatMath.sqrt(x * x + y * y); 
	} 
	private void midPoint(PointF point, MotionEvent event) { 
		float x = event.getX(0) + event.getX(1); 
		float y = event.getY(0) + event.getY(1); 
		point.set(x / 2, y / 2); 
	}

	
}
