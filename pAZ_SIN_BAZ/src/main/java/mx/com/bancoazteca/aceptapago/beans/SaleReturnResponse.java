package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;

public class SaleReturnResponse  implements Serializable{
	/**
	 * 
	 */
	public final static String DATE_TAG = "fechaTransaccion";
	public final static String ARQC_TAG = "arqc";
	public final static String AID_TAG = "aid";
	public final static String FOLIO_TAG="folio";
	public final static String REFERENCE_TAG="referencia";
	public final static String AUTHORIZATION_TAG="autorizacion";
	public final static String AMOUNT_TAG="monto";
	public final static String CODE_DESCRIPTION="descripcion_codigo";

	private static final long serialVersionUID = 1L;
	private String date;
	private String aid;
	private String arqc;
	private String folio;
	private String reference;
	private String authorization;
	private String amountReturned;
	private String codeDescription;

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getArqc() {
		return arqc;
	}
	public void setArqc(String arqc) {
		this.arqc = arqc;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String autorizacion) {
		this.authorization = autorizacion;
	}
	public String getAmountReturned() {
		return amountReturned;
	}
	public void setAmountReturned(String amountReturned) {
		this.amountReturned = amountReturned;
	}

	public String getCodeDescription() {
		return codeDescription;
	}
	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}
}

