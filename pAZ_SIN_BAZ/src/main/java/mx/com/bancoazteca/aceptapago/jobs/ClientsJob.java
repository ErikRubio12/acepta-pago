package mx.com.bancoazteca.aceptapago.jobs;

import android.content.Context;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import mx.com.bancoazteca.ciphers.Encrypt;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.ClientBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.util.HandsetInfo;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class ClientsJob <T> extends Thread{
	private static final String QUERY_DEVICE_METHOD ="BusinessToBusinessServiceConsultarTerminalIphone";
	private static final String QUERY_DEVICE_ACTION = null;
	private static final String QUERY_DEVICE_NAMESPACE = "http://service.btb.com";
	private static final String QUERY_DEVICE_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String QUERY_DEVICE_PARAM = "xml";
	protected static final String _URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String _NAMESPACE = "http://service.btb.com";
	private static final String CLIENTS_ACTION = null;
	private static final String CLIENTS_METHOD = "BusinessToBusinessCliente";
	private static final String CLIENTS_PARAM = "xml";
	private static final String CLIENTS_TAG = "Clientes";
	private static final String MAIL_TAG = "correo";
	private static final String ID_CLIENT_TAG = "idCliente";
	private static final String NAME_TAG = "nombre";
	private MessageListener<Message, T> listener;
	private T response=null;
	private String folio;
	private String rfc;
	private String device;
	public final static int EDIT_CLIENT=1;
	public final static int DELETE_CLIENT=2;
	public final static int CREATE_CLIENT=3;
	public final static int SEARCH_CLIENT=4;
	public final static int LINK_CLIENT=5;
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String ERROR_TAG = "error_sistema";
	private static final String CLIENT_ID_TAG = "idCliente";
	private static final String CLIENTS = "clients";
	private int serviceType=SEARCH_CLIENT;
	private StringBuilder request= new StringBuilder();
	private Parameter<String,String> param=null;
	private SoapRequest<String> req;
	private SoapResponse res = null;
	private String operationCode="-1";
	private String errorMessage="";
	private String idClient;
	private SaleBean sale;
	private List<ClientBean> clients;
	protected final static float TIME_OUT=2.0f;
	private static final String CLIENTS_JOB = "ClientsJob";
	private Context ctx;
	public ClientsJob() {
		// TODO Auto-generated constructor stub
	}
	public ClientsJob(Context ctx,String folio,String rfc,ClientBean clientToEdit,MessageListener<Message, T> listener,int serviceType) {
		// TODO Auto-generated constructor stub
		this(ctx,folio, rfc, listener, serviceType);
		clients= new ArrayList<ClientBean>();
		clients.add(clientToEdit);
	}
	public ClientsJob(Context ctx,String folio,String rfc,List<ClientBean> clients,MessageListener<Message, T> listener,int serviceType) {
		// TODO Auto-generated constructor stub
		this(ctx,folio, rfc, listener, serviceType);
		this.clients=clients;
	}
	public ClientsJob(Context ctx,String folio,String rfc,MessageListener<Message, T> listener) {
		// TODO Auto-generated constructor stub
		this(ctx,folio, rfc, listener, SEARCH_CLIENT);
	}
	public ClientsJob(Context ctx,String folio,String rfc,String idClient,SaleBean sale) {
		// TODO Auto-generated constructor stub
		this(ctx,folio, rfc, null, LINK_CLIENT);
		this.idClient=idClient;
		this.sale=sale;
	}
	public ClientsJob(Context ctx,String folio,String rfc,MessageListener<Message, T> listener,int serviceType) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		this.folio=folio;
		this.rfc=rfc;
		this.serviceType=serviceType;
		this.ctx=ctx;
		
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Message msg=new Message();
		
		request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append(Encrypt.encryptStringWS("CTermIphone"));
		request.append("</idservicio>");
		request.append("<idComercio>");
		request.append(Encrypt.encryptStringWS(folio));
		request.append("</idComercio>");
		request.append("<rfc>");
		request.append(Encrypt.encryptStringWS(rfc));
		request.append("</rfc>");
		request.append("<imei>");
		request.append(Encrypt.encryptStringWS(HandsetInfo.getIMEI()));
		request.append("</imei>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		req= new SoapRequest<String>(QUERY_DEVICE_METHOD, QUERY_DEVICE_ACTION);
		req.setNameSpace(QUERY_DEVICE_NAMESPACE);
		req.setUrl(QUERY_DEVICE_URL);
		param= new Parameter<String, String>(QUERY_DEVICE_PARAM, request.toString());
		req.addParameter(param);
		
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if (res==null) {
				msg.setType(Message.EXCEPTION);
				msg.setTextMessage( R.string.connectionFails);
				if(listener!=null)
					listener.sendMessage(ctx,msg,response);
				return;
			}
			Document dom= Utility.createDom(res.getResponse().toString());
			Node node=dom.getElementsByTagName("codigo_operacion").item(0);
			String result=node.getChildNodes().item(0).getNodeValue();
			result=Encrypt.decryptStringWS(result);
			Connections.closeSoapConnection(res);
			if("0".equals(result)){
				node=dom.getElementsByTagName("idterminal").item(0);
				device=node.getChildNodes().item(0).getNodeValue();
				device=Encrypt.decryptStringWS(device);
				req= new SoapRequest<String>(CLIENTS_METHOD,CLIENTS_ACTION);
				req.setNameSpace(_NAMESPACE);
				req.setUrl(_URL);
				switch (serviceType) {
				case SEARCH_CLIENT:search();break;
				case EDIT_CLIENT:edit();break;
				case CREATE_CLIENT:create();break;
				case DELETE_CLIENT:delete();break;
				case LINK_CLIENT:link();break;
				default:break;
				}
			}else{
				msg.setType(Message.ERROR);
				msg.setTextMessage( R.string.connectionError);
				if(listener!=null)
					listener.sendMessage(ctx,msg,"Error consultar la terminal");
			}
				
			
		} catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			if(listener!=null)
				listener.sendMessage(ctx,msg,response);
			Logger.log(Logger.EXCEPTION,CLIENTS,"InterruptedIOException: "+ e);
		}catch (IOException e) {
			// TODO Auto-generated catch block
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			if(listener!=null)
				listener.sendMessage(ctx,msg,response);
			Logger.log(Logger.EXCEPTION,CLIENTS,"IOException: "+ e);
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage(R.string.serviceError);
			if(listener!=null)
				listener.sendMessage(ctx,msg,response);
			Logger.log(Logger.EXCEPTION, CLIENTS,"XmlPullParserException: "+ e);
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			if(listener!=null)
				listener.sendMessage(ctx,msg,response);
			Logger.log(Logger.EXCEPTION,CLIENTS,"TimeoutException: "+ e);
		} catch (Exception e) {
			// TODO: handle exception
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			if(listener!=null)
				listener.sendMessage(ctx,msg,response);
			Logger.log(Logger.EXCEPTION,CLIENTS,"Exception: "+ e);
		}
		
	}
	private void link() throws IOException, XmlPullParserException, TimeoutException, InterruptedIOException {
		// TODO Auto-generated method stub
		Message msg=new Message();
		request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append("btob");
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append("LigarCliente");
		request.append("</canalEntrada>");
		request.append("<idComercio>");
		request.append(folio);
		request.append("</idComercio>");
		request.append("<terminal>");
		request.append((device));
		request.append("</terminal>");
		request.append("<clienteRequestTo>");
		request.append("<idCliente>");
		request.append(idClient);
		request.append("</idCliente>");
		request.append("</clienteRequestTo>");
		request.append("<operacionRequestTo>");
		request.append("<autorizacion>");
		request.append(sale.getAuthorizationNumber());
		request.append("</autorizacion>");
		request.append("<folio>");
		request.append(sale.getFolio());
		request.append("</folio>");
		request.append("<referencia>");
		request.append(sale.getReferenceNumber());
		request.append("</referencia>");
		request.append("</operacionRequestTo>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		res=null;
		param= new Parameter<String, String>(CLIENTS_PARAM,request.toString());
		req.addParameter(param);
		res= Connections.makeSecureSoapConnection(req,TIME_OUT);
		if (res==null) {
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			if(listener!=null)
				listener.sendMessage(ctx,msg,response);
			return;
		}
		parseLinkResponse(Utility.fixSoapResponse(res.getResponse().toString()));
		if (operationCode.equalsIgnoreCase("0") || operationCode.equalsIgnoreCase("00")) {
			msg.setType(Message.OK);
			if(listener!=null)
				listener.sendMessage(ctx,msg, response);
			Connections.closeSoapConnection(res);
		}
		else{
			msg.setType(Message.ERROR);
			if(listener!=null)
				listener.sendMessage(ctx,msg,errorMessage);
			Connections.closeSoapConnection(res);
		}
	}
	private void parseLinkResponse(String fixSoapResponse) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(fixSoapResponse));
		int eventType=parser.getEventType();
		String currentTag=null;
		
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:break;
			case XmlPullParser.END_DOCUMENT:break;
			case XmlPullParser.START_TAG:
				currentTag=parser.getName();
				
				break;
			case XmlPullParser.TEXT:
				
				if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
					operationCode=parser.getText();
				}
				else if (currentTag.equalsIgnoreCase(ERROR_TAG)){
					errorMessage=parser.getText();
				}
				break;
			case XmlPullParser.END_TAG:
				currentTag=null;
				break; 
			default:
				break;
			}
			eventType=parser.next();
		}
	}
	private void delete() throws IOException, XmlPullParserException, TimeoutException, InterruptedIOException {
		// TODO Auto-generated method stub
		String idClient;
		ClientBean client=null;
		Message msg=new Message();
		for (int i = 0; i <clients.size(); i++) {
			client=clients.get(i);
			idClient=client.getId();
			request= new StringBuilder();
			request.append("<bancoAzteca>");
			request.append("<eservices>");
			request.append("<request>");
			request.append("<idservicio>");
			request.append("btob");
			request.append("</idservicio>");
			request.append("<canalEntrada>");
			request.append("EliminarCliente");
			request.append("</canalEntrada>");
			request.append("<idComercio>");
			request.append(folio);
			request.append("</idComercio>");
			request.append("<terminal>");
			request.append(device);
			request.append("</terminal>");
			request.append("<clienteRequestTo>");
			request.append("<idCliente>");
			request.append(idClient);
			request.append("</idCliente>");
			request.append("</clienteRequestTo>");
			request.append("</request>");
			request.append("</eservices>");
			request.append("</bancoAzteca>");
			
			res=null;
			param= new Parameter<String, String>(CLIENTS_PARAM,request.toString());
			req.addParameter(param);
			Logger.log(Logger.MESSAGE,CLIENTS,"va a eliminar");
			res= Connections.makeSecureSoapConnection(req,TIME_OUT);
			if (res==null) {
				msg.setType(Message.EXCEPTION);
				msg.setTextMessage( R.string.connectionFails);
				listener.sendMessage(ctx,msg,response);
				return;
			}
			parseDeleteResponse(Utility.fixSoapResponse(res.getResponse().toString()));
			if (operationCode.equalsIgnoreCase("0") || operationCode.equalsIgnoreCase("00")) {
				Logger.log(Logger.MESSAGE,CLIENTS_JOB, "Cliente eliminado: "+ idClient);
				Connections.closeSoapConnection(res);
			}
			else{
				msg.setType(Message.ERROR);
				listener.sendMessage(ctx,msg,errorMessage);
				Connections.closeSoapConnection(res);
				return;
			}
			req.removeParameter(param);
		}
		msg.setType(Message.OK);
		listener.sendMessage(ctx,msg, response);
	}
	private void parseDeleteResponse(String fixSoapResponse) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(fixSoapResponse));
		int eventType=parser.getEventType();
		String currentTag=null;
		
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:break;
			case XmlPullParser.END_DOCUMENT:break;
			case XmlPullParser.START_TAG:
				currentTag=parser.getName();
				
				break;
			case XmlPullParser.TEXT:
				
				if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
					operationCode=parser.getText();
				}
				else if (currentTag.equalsIgnoreCase(ERROR_TAG)){
					errorMessage=parser.getText();
				}
				break;
			case XmlPullParser.END_TAG:
				currentTag=null;
				break; 
			default:
				break;
			}
			eventType=parser.next();
		}
	}
	private void create() throws IOException, XmlPullParserException, TimeoutException, InterruptedIOException {
		// TODO Auto-generated method stub
		ClientBean client =clients.get(0);
		Message msg=new Message();
		request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append("btob");
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append("AltaCliente");
		request.append("</canalEntrada>");
		request.append("<idComercio>");
		request.append(folio);
		request.append("</idComercio>");
		request.append("<terminal>");
		request.append(device);
		request.append("</terminal>");
		request.append("<clienteRequestTo>");
		request.append("<nombreCompleto>");
		request.append(client.getCompleteName());
		request.append("</nombreCompleto>");
		request.append("<correo>");
		request.append(client.getMail());
		request.append("</correo>");
		request.append("</clienteRequestTo>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		res=null;
		param= new Parameter<String, String>(CLIENTS_PARAM,request.toString());
		req.addParameter(param);
		res= Connections.makeSecureSoapConnection(req,TIME_OUT);
		if (res==null) {
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			listener.sendMessage(ctx,msg,response);
			return;
		}
		parseCreateResponse(Utility.fixSoapResponse(res.getResponse().toString()));
		if (operationCode.equalsIgnoreCase("0") || operationCode.equalsIgnoreCase("00")) {
			msg.setType(Message.OK);
			listener.sendMessage(ctx,msg, response);
			Connections.closeSoapConnection(res);
		}
		else{
			msg.setType(Message.ERROR);
			listener.sendMessage(ctx,msg,errorMessage);
			Connections.closeSoapConnection(res);
		}
	}
	private void parseCreateResponse(String fixSoapResponse) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(fixSoapResponse));
		int eventType=parser.getEventType();
		String currentTag=null;
		
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:break;
			case XmlPullParser.END_DOCUMENT:break;
			case XmlPullParser.START_TAG:
				currentTag=parser.getName();
				
				break;
			case XmlPullParser.TEXT:
				if (currentTag.equalsIgnoreCase(CLIENT_ID_TAG)){
					response= (T) new String(parser.getText());
				}
				else if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
					operationCode=parser.getText();
				}
				else if (currentTag.equalsIgnoreCase(ERROR_TAG)){
					errorMessage=parser.getText();
				}
				break;
			case XmlPullParser.END_TAG:
				currentTag=null;
				break; 
			default:
				break;
			}
			eventType=parser.next();
		}
	}
	private void edit() throws IOException, XmlPullParserException, TimeoutException, InterruptedIOException {
		// TODO Auto-generated method stub
		ClientBean client =clients.get(0);
		Message msg=new Message();
		request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append("btob");
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append("EditarCliente");
		request.append("</canalEntrada>");
		request.append("<idComercio>");
		request.append(folio);
		request.append("</idComercio>");
		request.append("<terminal>");
		request.append(device);
		request.append("</terminal>");
		request.append("<clienteRequestTo>");
		request.append("<idCliente>");
		request.append(client.getId());
		request.append("</idCliente>");
		request.append("<nombreCompleto>");
		request.append(client.getCompleteName());
		request.append("</nombreCompleto>");
		request.append("<correo>");
		request.append(client.getMail());
		request.append("</correo>");
		request.append("</clienteRequestTo>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		res=null;
		param= new Parameter<String, String>(CLIENTS_PARAM,request.toString());
		req.addParameter(param);
		res= Connections.makeSecureSoapConnection(req,TIME_OUT);
		if (res==null) {
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			listener.sendMessage(ctx,msg,response);
			return;
		}
		parseEditResponse(Utility.fixSoapResponse(res.getResponse().toString()));
		if (operationCode.equalsIgnoreCase("0") || operationCode.equalsIgnoreCase("00")) {
			msg.setType(Message.OK);
			listener.sendMessage(ctx,msg, response);
			Connections.closeSoapConnection(res);
		}
		else{
			msg.setType(Message.ERROR);
			listener.sendMessage(ctx,msg,errorMessage);
			Connections.closeSoapConnection(res);
		}
	}
	private void parseEditResponse(String fixSoapResponse) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(fixSoapResponse));
		int eventType=parser.getEventType();
		String currentTag=null;
		
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:break;
			case XmlPullParser.END_DOCUMENT:break;
			case XmlPullParser.START_TAG:
				currentTag=parser.getName();
				
				break;
			case XmlPullParser.TEXT:
				
				if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
					operationCode=parser.getText();
				}
				else if (currentTag.equalsIgnoreCase(ERROR_TAG)){
					errorMessage=parser.getText();
				}
				break;
			case XmlPullParser.END_TAG:
				currentTag=null;
				break; 
			default:
				break;
			}
			eventType=parser.next();
		}
	}
	private void search() throws IOException, XmlPullParserException, TimeoutException, InterruptedIOException {
		// TODO Auto-generated method stub
		Message msg=new Message();
		request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append("btob");
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append("BuscaCliente");
		request.append("</canalEntrada>");
		request.append("<idComercio>");
		request.append(folio);
		request.append("</idComercio>");
		request.append("<terminal>");
		request.append(device);
		request.append("</terminal>");
		request.append("<clienteRequestTo>");
		request.append("<nombreCompleto>");
		request.append("");
		request.append("</nombreCompleto>");
		request.append("<correo>");
		request.append("");
		request.append("</correo> ");
		request.append("</clienteRequestTo>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		res=null;
		param= new Parameter<String, String>(CLIENTS_PARAM,request.toString());
		req.addParameter(param);
		res= Connections.makeSecureSoapConnection(req,TIME_OUT);
		if (res==null) {
			msg.setType(Message.EXCEPTION);
			msg.setTextMessage( R.string.connectionFails);
			listener.sendMessage(ctx,msg,response);
			return;
		}
		parseSearchResponse(Utility.fixSoapResponse(res.getResponse().toString()));
		Logger.log(Logger.MESSAGE,CLIENTS_JOB, "clients	 " + ((ArrayList<ClientBean>)this.response).toString());
		Logger.log(Logger.MESSAGE,CLIENTS_JOB, "operation Code: " + operationCode);
		if (operationCode.equalsIgnoreCase("0") || operationCode.equalsIgnoreCase("00")) {
			msg.setType(Message.OK);
			listener.sendMessage(ctx,msg, response);
			Connections.closeSoapConnection(res);
		}
		else{
			msg.setType(Message.ERROR);
			listener.sendMessage(ctx,msg,errorMessage);
			Connections.closeSoapConnection(res);
		}
	}
	private void parseSearchResponse(String fixSoapResponse) throws XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		XmlPullParser parser=Xml.newPullParser();
		parser.setInput(new StringReader(fixSoapResponse));
		int eventType=parser.getEventType();
		String currentTag=null;
		ClientBean currentClient=null;
		response= (T) new ArrayList<ClientBean>();
		while(eventType!=XmlPullParser.END_DOCUMENT){
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:break;
			case XmlPullParser.END_DOCUMENT:break;
			case XmlPullParser.START_TAG:
				currentTag=parser.getName();
			
				if (currentTag.equalsIgnoreCase(CLIENTS_TAG)) {
					currentClient=new ClientBean();
				}
				
				break;
			case XmlPullParser.TEXT:
				if (currentTag.equalsIgnoreCase(MAIL_TAG)) {
					currentClient.setMail(parser.getText());
				}
				else if (currentTag.equalsIgnoreCase(ID_CLIENT_TAG)){
					currentClient.setId(parser.getText());
				}
				else if (currentTag.equalsIgnoreCase(NAME_TAG)) {
					currentClient.setCompleteName(parser.getText());
				}
				else if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)){
					operationCode=parser.getText();
				}
				else if (currentTag.equalsIgnoreCase(ERROR_TAG)){
					errorMessage=parser.getText();
				}
				break;
			case XmlPullParser.END_TAG:
				String endTag=parser.getName();
				if (endTag.equalsIgnoreCase(CLIENTS_TAG)){
					((List<ClientBean>)response).add(currentClient);
					currentClient=null; 
				}
				currentTag=null;
				break; 
			default:
				break;
			}
			eventType=parser.next();
		}
	}
	public int getServiceType() {
		return serviceType;
	}
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}

}
