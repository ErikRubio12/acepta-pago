package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;

public class OperationsBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String OPERATION = "operation";
	private String operationDate;
	private String authorizedNumber;
	private String cardNUmber;
	private String amount;
	private String folio;
	private String reference;
	private String messageType;
	private String ConsultaDate;
	private String idTtransaccion;
	private String codeProcess;
	
	
	public String getConsultaDate() {
		return ConsultaDate;
	}
	public void setConsultaDate(String consultaDate) {
		ConsultaDate = consultaDate;
	}
	public String getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}
	public String getAuthorizedNumber() {
		return authorizedNumber;
	}
	public void setAuthorizedNumber(String authorizedNumber) {
		this.authorizedNumber = authorizedNumber;
	}
	public String getCardNUmber() {
		return cardNUmber;
	}
	public void setCardNUmber(String cardNUmber) {
		this.cardNUmber = cardNUmber;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getAmount() {
		return amount;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	
	public String getReference() {
		return reference;
	}
	public String getIdTtransaccion() {
		return idTtransaccion;
	}
	public void setIdTtransaccion(String idTtransaccion) {
		this.idTtransaccion = idTtransaccion;
	}
	public String getCodeProcess() {
		return codeProcess;
	}
	public void setCodeProcess(String codeProcess) {
		this.codeProcess = codeProcess;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer output=new StringBuffer();
		output.append("fecha de operacion: ");
		output.append(operationDate);
		output.append(" numero de autorizacion: ");
		output.append(authorizedNumber);
		output.append(" folio: ");
		output.append(folio);
		output.append(" referencia: ");
		output.append(reference);
		output.append(" monto: ");
		output.append(amount);
		output.append(" tipo de mensaje : ");
		output.append(messageType);
		output.append(" numero de tarjeta : ");
		output.append(cardNUmber);
		output.append("Id transaccion: ");
		output.append(idTtransaccion);
		output.append("Codigo processs ");
		output.append(codeProcess);
		return output.toString();
	}
}
