package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Xml;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.SoapRequest;
import mx.com.bancoazteca.network.SoapResponse;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class OperationListJob extends BaseService<Message, String>{
	
	private static final String OPERATIONS_METHOD = "BusinessToBusinessServiceListarOperacionesIphone";
	private static final String OPERATIONS_ACTION = null;
	private static final String OPERATIONS_NAMESPACE = "http://service.btb.com";
	private static final String OPERATIONS_URL = Urls.PAZ3 + "services/PuntoAztecaBtoB";
	private static final String OPERATIONS_PARAM = "xml";
	public static final int NO_OPERATIONS = 102;
	private static final String OPERATION_LIST = "operation list";
	private String folio;
	private List<OperationsBean> sales=new ArrayList<OperationsBean>();
	private List<OperationsBean> salesCancelacion=new ArrayList<OperationsBean>();
	private List<OperationsBean> salesDevolucion=new ArrayList<OperationsBean>();
	private MessageListener<Message, String> listener;
	private String lastDate;
	private String firstDate;
	private Context ctx;
	private String rfc;
	
	private static final String AUTHORIZED_NUMBRE_TAG = "autorizacion";
	private static final String DATE_TAG = "fecha";
	private static final String FOLIO_TAG = "folio";
	private static final String AMOUNT_TAG = "monto";
	private static final String REFERENCE_TAG = "referencia";
	private static final String MESSAGE_TYPE_TAG = "tipo_mensaje";
	private static final String OPERATION_PROCESS_TAG = "codigoProcesamiento";
	private static final String ID_TRANSACCION_TAG = "idTtransaccion";
	private static final String LAST4_DIGITS_TAG = "tarjeta";
	private static final String OPERATION_CODE_TAG = "codigo_operacion";
	private static final String DESCRIPTION_OPERATION_CODE_TAG = "descripcion_codigo";
	private static final String MENSAJE_SYS_TAG = "mensajeSistema";
	private static final String DATE_CONSULT_TAG = "fechaConsulta";

	private static final String SYSTEM_ERROR_TAG = "error_sistema";
	private static final String SALE_TAG = "cargos";
	private static final String SALE_TAG_CANCELACACION = "cancelaciones";
	private static final String SALE_CANCELACACION = "Cancelacion";

	private static final String SALE_DEVOLUCION = "Devolucion";
	private String fechaConsult;
	private String descriptionCode;

	public OperationListJob(Context ctx,String rfc,String folio,String firstDate,String lastDate,MessageListener<Message, String> listener) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		this.folio=folio;
		this.firstDate=firstDate;
		this.lastDate=lastDate;
		this.ctx=ctx;
		this.rfc=rfc;
		Logger.log(Logger.MESSAGE, OPERATION_LIST,"inicial: "+ firstDate+ "   final:"+ lastDate);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub

		StringBuilder request= new StringBuilder();
		request.append("<bancoAzteca>");
		request.append("<eservices>");
		request.append("<request>");
		request.append("<idservicio>");
		request.append("btob");
		request.append("</idservicio>");
		request.append("<canalEntrada>");
		request.append("PuntoAztecaListaOperacion");
		request.append("</canalEntrada>");
		request.append("<origen>");
		request.append("02");
		request.append("</origen>");
		request.append("<folioAzteca>");
		request.append(folio);
		request.append("</folioAzteca>");
		request.append("<rfc>");
		request.append(rfc);
		request.append("</rfc>");
		request.append("<fechaInicio>");
		request.append(firstDate);
		request.append("</fechaInicio>");
		request.append("<fechaFin>");
		request.append(lastDate);
		request.append("</fechaFin>");
		request.append("</request>");
		request.append("</eservices>");
		request.append("</bancoAzteca>");
		
		SoapRequest<String> req= new SoapRequest<String>(OPERATIONS_METHOD, OPERATIONS_ACTION);
		req.setNameSpace(OPERATIONS_NAMESPACE);
		req.setUrl(OPERATIONS_URL);
		Parameter<String,String> param= new Parameter<String, String>(OPERATIONS_PARAM, request.toString());
		req.addParameter(param);
		SoapResponse res = null;
		try {
			res = Connections.makeSecureSoapConnection(req, TIME_OUT);
			if (res==null) {
				listener.sendStatus(ctx,Message.EXCEPTION);
				return;
			}
			
			String xml=res.getResponse().toString();
			xml=Utility.fixSoapResponse(xml);
			Connections.closeSoapConnection(res);
			XmlPullParser parser=Xml.newPullParser();
			parser.setInput(new StringReader(xml));
			int eventType=parser.getEventType();
			String currentTag=null;
			OperationsBean currentOperationBean=null;
			while(eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:break;
				case XmlPullParser.END_DOCUMENT:break;
				case XmlPullParser.START_TAG:
					currentTag=parser.getName();
					if(currentTag.equalsIgnoreCase(SALE_TAG))
						currentOperationBean= new OperationsBean();
					else if(currentTag.equalsIgnoreCase(SALE_TAG_CANCELACACION))
						currentOperationBean= new OperationsBean();
					break;
				case XmlPullParser.TEXT:
					if (currentTag.equalsIgnoreCase(OPERATION_CODE_TAG)) 
						operationCode= parser.getText();
					else if (currentTag.equalsIgnoreCase(DESCRIPTION_OPERATION_CODE_TAG)) 
						descriptionCode=parser.getText();
					else if (currentTag.equalsIgnoreCase(SYSTEM_ERROR_TAG)) 
						systemError=parser.getText();
					else if (currentTag.equalsIgnoreCase(MENSAJE_SYS_TAG)) 
						systemError=parser.getText();
					else if(currentTag.equalsIgnoreCase(FOLIO_TAG))
						currentOperationBean.setFolio(parser.getText());
					else if(currentTag.equalsIgnoreCase(AUTHORIZED_NUMBRE_TAG))
						currentOperationBean.setAuthorizedNumber(parser.getText());
					else if(currentTag.equalsIgnoreCase(AMOUNT_TAG))
						currentOperationBean.setAmount(parser.getText());
					else if(currentTag.equalsIgnoreCase(REFERENCE_TAG))
						currentOperationBean.setReference(parser.getText());
					else if(currentTag.equalsIgnoreCase(MESSAGE_TYPE_TAG))
						currentOperationBean.setMessageType(parser.getText());
					else if(currentTag.equalsIgnoreCase(DATE_TAG))
						currentOperationBean.setOperationDate(parser.getText());
					else if(currentTag.equalsIgnoreCase(LAST4_DIGITS_TAG))
						currentOperationBean.setCardNUmber(parser.getText());
					else if (currentTag.equalsIgnoreCase(OPERATION_PROCESS_TAG)) 
						currentOperationBean.setCodeProcess(parser.getText());
					else if (currentTag.equalsIgnoreCase(ID_TRANSACCION_TAG)) 
						currentOperationBean.setIdTtransaccion(parser.getText());
					else if(currentTag.equalsIgnoreCase(DATE_CONSULT_TAG))
						fechaConsult=parser.getText();
					break;
				case XmlPullParser.END_TAG:
					currentTag=parser.getName();
					if(currentTag.equalsIgnoreCase(SALE_TAG))
						sales.add(currentOperationBean);	
					else if(currentTag.equalsIgnoreCase(SALE_TAG_CANCELACACION)){
						if (currentOperationBean.getCodeProcess().equalsIgnoreCase(SALE_DEVOLUCION)) {
							salesDevolucion.add(currentOperationBean);
						}else if (currentOperationBean.getCodeProcess().equalsIgnoreCase(SALE_CANCELACACION)) {
							salesCancelacion.add(currentOperationBean);	
						}
					}
					break; 
				default:
					break;
				}
				eventType=parser.next();
			}
			Logger.log(Logger.MESSAGE, OPERATION_LIST,"codigo operacion: "+ operationCode +" error sistema: "+ systemError);
			Logger.log(Logger.MESSAGE, OPERATION_LIST,"vantas: "+ sales.size() +  " cancelaciones: "+  salesCancelacion.size());
			if("00".equalsIgnoreCase(operationCode) || "0".equalsIgnoreCase(operationCode)){
				Message msg= new Message();
				msg.setType(Message.OK);
				ArrayList<List<OperationsBean>> operations= new ArrayList<List<OperationsBean>>(2);
				operations.add(sales);
				operations.add(salesCancelacion);
				operations.add(salesDevolucion);
	 			listener.sendMessage(ctx,msg, operations,fechaConsult);
			}
			else if("326".equals(operationCode)){
				listener.sendStatus(ctx,NO_OPERATIONS);
			}
			else
				listener.sendStatus(ctx,Message.CANCEL);

			
		}catch (InterruptedIOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, OPERATION_LIST,"InterruptedIOException"+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, OPERATION_LIST,"IOException"+e.getMessage());
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, OPERATION_LIST,"TimeoutException"+e.getMessage());
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, OPERATION_LIST,"XmlPullParserException"+e.getMessage());
		} 
		
	}

}
