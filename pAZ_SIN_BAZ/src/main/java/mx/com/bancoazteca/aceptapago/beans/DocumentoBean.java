package mx.com.bancoazteca.aceptapago.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by B931724 on 20/02/18.
 */

class DocumentoBean implements Parcelable {
    @SerializedName("documento")
    @Expose
    private String documento;
    @SerializedName("http")
    @Expose
    private String http;

    protected DocumentoBean(Parcel in) {
        documento = in.readString();
        http = in.readString();
    }

    public static final Creator<DocumentoBean> CREATOR = new Creator<DocumentoBean>() {
        @Override
        public DocumentoBean createFromParcel(Parcel in) {
            return new DocumentoBean(in);
        }

        @Override
        public DocumentoBean[] newArray(int size) {
            return new DocumentoBean[size];
        }
    };

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(documento);
        dest.writeString(http);
    }
}
