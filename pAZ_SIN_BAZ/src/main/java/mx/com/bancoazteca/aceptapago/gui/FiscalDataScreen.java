package mx.com.bancoazteca.aceptapago.gui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mx.com.bancoazteca.aceptapago.beans.AdditionalInfo;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.baz.apertura.UtilidadesEktDinero;
import mx.com.bancoazteca.aceptapago.baz.entidades.Colonia;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.ExtraInfoBean;
import mx.com.bancoazteca.aceptapago.db.daos.AddressDao;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class FiscalDataScreen extends BaseActivity{
	private static final int POPUP_VALID_FORM = 11;
	private final static int POPUP_SINGLE_CHOICE=9;
	protected static final int POPUP_VALID_CHARACTERS = 10;
	protected static final String TAG_FISCAL_DATA = "FiscalData";
	private static final int CONFIRM_CODE = 150;
	private Button accept;
	private EditText internalNumber;
	private EditText externalNUmber;
	private EditText state;
	private EditText poblacion;
	private EditText colony;
	private EditText zipCode;
	private EditText rfc;
	private EditText fiscalName;
	private EditText street;
	private EditText webPage;
	private EditText lada;
	private EditText telephone;
	private EditText extension;
	private FiscalDataBean fiscalData= new FiscalDataBean();
	private ExtraInfoBean extraInfo;
	private AddressDao addressDao;
	private ArrayAdapter<String> colonyAdapter;
	private Switch aSwitchSameAddress;
	private AddressBean customerAddress= new AddressBean();
	private boolean reset=false;
	private boolean sameAddress=false;
	private int spinnerType;
	private final static int SPINNER_COLONY=2;
	private final static String REQUIRED_FIELD="Campo requerido";
	public final static String CHARACTERES = "Caracter invalido";
	private ArrayList<Colonia> addresses;
	private Toolbar mtoolbar;
	private String zipC;
	private String rfcText;
	private RegistrationBean register;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		setContentView(R.layout.fiscal_data);

		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		char [] extraChars={'_','-','.'};
		accept=(Button) findViewById(R.id.acceptButton);
		InputFilter filters[];
		aSwitchSameAddress=(Switch) findViewById(R.id.switchSameAddress);
		street=(EditText) findViewById(R.id.streetEditText);
		filters= new InputFilter[street.getFilters().length +1];
		System.arraycopy(street.getFilters(), 0, filters,0,street.getFilters().length);
		filters[street.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS,extraChars);
		street.setFilters(filters);
		
		internalNumber=(EditText) findViewById(R.id.internalNumberEditText);
		filters= new InputFilter[internalNumber.getFilters().length +1];
		System.arraycopy(internalNumber.getFilters(), 0, filters,0,internalNumber.getFilters().length);
		filters[internalNumber.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS,extraChars);
		internalNumber.setFilters(filters);
		
		externalNUmber=(EditText) findViewById(R.id.externalNumberEditText);
		filters= new InputFilter[externalNUmber.getFilters().length +1];
		System.arraycopy(externalNUmber.getFilters(), 0, filters,0,externalNUmber.getFilters().length);
		filters[externalNUmber.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS,extraChars);
		externalNUmber.setFilters(filters);
		
		state=(EditText) findViewById(R.id.stateSpinner);
		poblacion= (EditText) findViewById(R.id.poblacionSpinner);
		colony=(EditText) findViewById(R.id.colonySpinner);
		zipCode=(EditText) findViewById(R.id.zipCodeSpinner);
		
		rfc=(EditText) findViewById(R.id.rfcEditText);
		filters= new InputFilter[rfc.getFilters().length +1];
		System.arraycopy(rfc.getFilters(), 0, filters,0,rfc.getFilters().length);
		filters[rfc.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC,extraChars);
		rfc.setFilters(filters);
		
		lada=(EditText) findViewById(R.id.ladaEditText);
		filters= new InputFilter[lada.getFilters().length +1];
		System.arraycopy(lada.getFilters(), 0, filters,0,lada.getFilters().length);
		filters[lada.getFilters().length]=new TextFilter(TextFilter.TYPE_LADA,extraChars);
		lada.setFilters(filters);
		lada.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				lada.setText("");
			}
		});
		lada.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				lada.setText("");
				telephone.setText("");
				return false;
			}
		});
		
		telephone=(EditText) findViewById(R.id.telephoneNumberEditText);
		filters= new InputFilter[telephone.getFilters().length +1];
		System.arraycopy(telephone.getFilters(), 0, filters,0,telephone.getFilters().length);
		filters[telephone.getFilters().length]=new TextFilter(TextFilter.TYPE_NUMERIC,extraChars);
		telephone.setFilters(filters);
		
		telephone.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				changePhoneLength();
			}
		});
		telephone.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				changePhoneLength();
				return false;
			}
		});
		
		extension=(EditText) findViewById(R.id.extNumberEditText);
		filters= new InputFilter[extension.getFilters().length +1];
		System.arraycopy(extension.getFilters(), 0, filters,0,extension.getFilters().length);
		filters[extension.getFilters().length]=new TextFilter(TextFilter.TYPE_NUMERIC,extraChars);
		extension.setFilters(filters);
		
		fiscalName=(EditText) findViewById(R.id.businessNameEditText);
		filters= new InputFilter[fiscalName.getFilters().length +1];
		System.arraycopy(fiscalName.getFilters(), 0, filters,0,fiscalName.getFilters().length);
		filters[fiscalName.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC,extraChars);
		fiscalName.setFilters(filters);
		
		webPage= (EditText)findViewById(R.id.webPageEditText);
		filters= new InputFilter[webPage.getFilters().length +1];
		System.arraycopy(webPage.getFilters(), 0, filters,0,webPage.getFilters().length);
		filters[webPage.getFilters().length]=new TextFilter(TextFilter.TYPE_EMAIL);
		webPage.setFilters(filters);
		
		accept.setOnClickListener(acceptListener);
		
		
		zipCode.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus)
					if (zipCode.length() == 5) {
						new TaskProcesoColonias().execute();
					}
			}

		});

		aSwitchSameAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
					fillAddress();
					String ladaStr= customerAddress.getLada();

					if(ladaStr != null && ladaStr.length()>0){
						lada.setText(ladaStr);
						telephone.setText("");
						changePhoneLength();
					}
				}else {
					emptyAddress();
					lada.setText("");
					telephone.setText("");
					changePhoneLength();
				}
			}
		});

	}
	private class TaskProcesoColonias extends AsyncTask<Void, Void, ArrayList<Colonia>> {

		@Override
		protected void onPreExecute() {
			popupTitle=R.string.popUpMessage;
			popupMsg=R.string.wait;
			showDialog(POPUP_MESSAGES_PROGRESS);
			zipC = zipCode.getText()
					.toString().trim();
		}

		@Override
		protected ArrayList<Colonia> doInBackground(Void... params) {
			try {
				
				UtilidadesEktDinero utilEkt = new UtilidadesEktDinero();
				addresses = utilEkt.traeColonia(zipC);
				if(addresses==null || addresses.size()==0)
					return null;				
				else{
					List<String> colonies= new ArrayList<String>();				
					for (Colonia currentAddress : addresses) {
						colonies.add(currentAddress.getColonia());
					}
//					colonyAdapter=new ArrayAdapter<String>
//					(getActivity(),android.R.layout.select_dialog_singlechoice,addressDao.queryColony(idState,idPoblacion));
					colonyAdapter=new ArrayAdapter<String>
					(FiscalDataScreen.this,android.R.layout.select_dialog_singlechoice,colonies);
					return addresses;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
		}

		@Override
		protected void onPostExecute(ArrayList<Colonia> result) {
			removeDialog(POPUP_MESSAGES_PROGRESS);
			if (result==null) {
				zipCode.setText("");
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.addressError));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
			} else {
				Colonia colony=result.get(0);
				
				fiscalData.getAddress().setZipCode(zipCode.getText().toString().trim());
				
				fiscalData.getAddress().setState(colony.getEstado());
				state.setText(colony.getEstado());
				
				fiscalData.getAddress().setPoblacion(colony.getPoblacion());
				poblacion.setText(colony.getPoblacion());
				
				
				popupTitle=R.string.colony;
				spinnerType=SPINNER_COLONY;
				showSingleChoicePopup().show();
			}

		}
	}
	private void changePhoneLength(){
		if(lada.length()==2){
			InputFilter filters[]=null;
			filters= new InputFilter[telephone.getFilters().length +1];
			System.arraycopy(telephone.getFilters(), 0, filters,0,telephone.getFilters().length);
			filters[telephone.getFilters().length]=new InputFilter.LengthFilter(8);
			telephone.setFilters(filters);
		}
		else if (lada.length()==3){
			InputFilter filters[]=null;
			filters= new InputFilter[telephone.getFilters().length +1];
			System.arraycopy(telephone.getFilters(), 0, filters,0,telephone.getFilters().length);
			filters[telephone.getFilters().length]=new InputFilter.LengthFilter(7);
			telephone.setFilters(filters);
		}
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		fiscalData.setAddress(customerAddress);
		if(params!=null){
			extraInfo=(ExtraInfoBean) params.getSerializable(ExtraInfoBean.EXTRA_INFO);
			customerAddress=(AddressBean) params.getSerializable(AddressBean.ADDRESS);
			rfcText = params.getString(CustomerBean.RFC);
			rfc.setText(rfcText);
			register =(RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
			if(params.getSerializable(FiscalDataBean.FISCAL_DATA)!=null){
				reset=true;
				fiscalData=(FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
				street.setText(fiscalData.getAddress().getStreet());
				internalNumber.setText(fiscalData.getAddress().getInternalNumber());
				externalNUmber.setText(fiscalData.getAddress().getExternalNumber());
				lada.setText(fiscalData.getAddress().getLada());
				telephone.setText(fiscalData.getAddress().getTelephone());
				extension.setText(fiscalData.getAddress().getExtention());
//				rfc.setText(fiscalData.getRfc());
				rfc.setText(rfcText);
				fiscalName.setText(fiscalData.getFiscalName());
				webPage.setText(extraInfo.getWebPage());
				
				String[] states= getResources().getStringArray(R.array.states);
				for (int i = 0; i < states.length; i++) {
					if(states[i].equals(fiscalData.getAddress().getState())){
						state.setText(states[i]);
						break;
					}
				}
			}
		}
		addressDao= new AddressDao(this);
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case POPUP_SINGLE_CHOICE:
			return  showSingleChoicePopup();
		case POPUP_VALID_FORM:
			popUp=new PopUp(this,popupTitle,popupMsg);
			popUp.create();
			return popUp.getDialog();
		case POPUP_VALID_CHARACTERS:
			StringBuffer text= new StringBuffer();
			text.append(getResources().getString(R.string.invalidCharacters));
			text.append("\nLos caracteres permitidos son: ");
			text.append(Utility.validCharsToString());
			text.append("\nY en el caso del correo tambien '@'");
			popUp=new PopUp(this, popupTitle,text.toString());
			popUp.setCancelable(true);
			popUp.create();
			return popUp.getDialog();
		default:
			return super.onCreateDialog(id);
		}
		
	}
	public boolean isValidCharacters(){
		boolean answer = true;
		
		if(!Utility.isvalidCharacters(street.getText().toString())){
			answer=false;
			street.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(externalNUmber.getText().toString())){
			answer=false;
			externalNUmber.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(rfc.getText().toString())){
			answer=false;
			rfc.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(fiscalName.getText().toString())){
			answer=false;
			fiscalName.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(telephone.getText().toString())){
			answer=false;
			telephone.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(lada.getText().toString())){
			answer=false;
			lada.setError(CHARACTERES);
		}
		
		
		return answer;
	}
	protected boolean validForm() {
		// TODO Auto-generated method stub
		boolean answer=true;
		fiscalData.getAddress().setStreet(street.getText().toString().toUpperCase(Locale.getDefault()));
		fiscalData.getAddress().setInternalNumber(internalNumber.getText().toString().toUpperCase(Locale.getDefault()));
		fiscalData.getAddress().setExternalNumber(externalNUmber.getText().toString().toUpperCase(Locale.getDefault()));
		fiscalData.getAddress().setLada(lada.getText().toString());
		fiscalData.getAddress().setTelephone(telephone.getText().toString());
		fiscalData.getAddress().setExtention(extension.getText().toString());
		fiscalData.getAddress().setState(state.getText().toString());
		fiscalData.getAddress().setPoblacion(poblacion.getText() !=null ?  poblacion.getText().toString() : "");
		fiscalData.getAddress().setColony(colony.getText()!=null ? colony.getText().toString() : "");
		fiscalData.getAddress().setZipCode(zipCode.getText() != null ? zipCode.getText().toString() : "");
		fiscalData.setRfc(rfc.getText().toString().toUpperCase(Locale.getDefault()));
		fiscalData.setFiscalName(fiscalName.getText().toString().toUpperCase(Locale.getDefault()));
		extraInfo.setWebPage(webPage.getText().toString().toUpperCase(Locale.getDefault()));
		if(street.getText().length()==0){
			street.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 1");
		}
			
		if(externalNUmber.getText().length()==0){
			externalNUmber.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 2");
		}
			
		if(state.getText()==null || state.getText().toString().length()==0 || state.getText().toString().equalsIgnoreCase("ESTADO")){
			state.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 3");
		}
		else
			state.setError(null);
		
			
			
//		if(rfc.getText().length()==0){
//			rfc.setError(REQUIRED_FIELD);
//			answer=false;
//			System.out.println("Campo 4");
//		}
		if(fiscalName.getText().length()==0){
			fiscalName.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 5");
		}
		if(colony.getText()==null || colony.getText().toString().length()==0 || colony.getText().toString().equalsIgnoreCase("COLONIA")){
			colony.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 6");
		}
		else
			colony.setError(null);
		if(zipCode.getText()==null || zipCode.getText().toString().length()==0 || zipCode.getText().toString().equalsIgnoreCase("C.P.")){
			zipCode.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 7");
		}
		else
			zipCode.setError(null);
		if(telephone.getText().length()==0){
			telephone.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 8");
		}
		if(lada.getText().length()==0){
			lada.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 9");
		}
		if(poblacion.getText()==null || poblacion.getText().toString().length()==0 || poblacion.getText().toString().equalsIgnoreCase("POBLACION")){
			poblacion.setError(REQUIRED_FIELD);
			answer=false;
			System.out.println("Campo 10");
		}
		else
			poblacion.setError(null);
		System.out.println("paso");
		if(!answer){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.emptyFields));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
			return false;
		}
		
		if(fiscalData.getRfc().length()<10){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.invalidRfcLength));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
			return false;
		}
		return true;
	}
	/*public void sameAddress(View v){
		if(v.getId()==R.id.businessInfoAddressQuestionButtonYes){
			if(!sameAddress){
				fillAddress();
				String ladaStr= customerAddress.getLada();
				
				if(ladaStr != null && ladaStr.length()>0){
					lada.setText(ladaStr);
					telephone.setText("");
					changePhoneLength();
				}
			}
				
		} else if(v.getId()==R.id.businessInfoAddressQuestionButtonNo){
			if(sameAddress){
				emptyAddress();

				
				lada.setText("");
				telephone.setText("");
				changePhoneLength();
			}
		}
		
		sameAddress=!sameAddress;
	}*/
	//*********ACTION LISTENERS*****************
	
	private OnClickListener acceptListener= new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 if(validForm()){
				intent.putExtra(ExtraInfoBean.EXTRA_INFO, extraInfo);
				intent.putExtra(FiscalDataBean.FISCAL_DATA, fiscalData);

				Logger.log(Logger.MESSAGE, TAG_FISCAL_DATA, "Datos: \n" + extraInfo + "\n"+ fiscalData);
				 register.setFiscalData(fiscalData);
//				 register.setFiscalData((FiscalDataBean)params.getSerializable(FiscalDataBean.FISCAL_DATA));
				 AdditionalInfo addInf= new AdditionalInfo(register.getFiscalData(), register.getCustomer());
				 register.setAdditionalInfo(addInf);

//
				 ArrayList<Serializable> params=new ArrayList<Serializable>();
				 params.add(register.getCustomer());
				 params.add(register);
				 ArrayList<String> paramNames=new ArrayList<String>();
				 paramNames.add(CustomerBean.CUSTOMER);
				 paramNames.add(RegistrationBean.REGISTER);
				 goForwardForResult(ConfirmationDataScreen.class, FiscalDataScreen.this, params,paramNames,CONFIRM_CODE);
				 Log.i("TAG_REgistration_menu", "extra_info_screen");
			}
		}
	};
	
	
	private  DialogInterface.OnClickListener colonyListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			Colonia selected=addresses.get(which);
			Logger.log(Logger.MESSAGE, TAG_FISCAL_DATA, "colony selected "+ selected);
			if(!"COLONIA".equalsIgnoreCase(selected.getColonia())){
				String ladaStr=null;
				if(selected.getIdEdo()!=null && !selected.getIdEdo().equals("") &&
				   selected.getIdPob()!=null && !selected.getIdPob().equals(""))
					ladaStr= addressDao.queryLada(1,Integer.parseInt(selected.getIdEdo()),Integer.parseInt(selected.getIdPob()) );
				Logger.log(Logger.MESSAGE, TAG_FISCAL_DATA, "lada encontrada: "+ ladaStr);
				if(ladaStr != null && ladaStr.length()>0){
					lada.setText(ladaStr);
					telephone.setText("");
					changePhoneLength();
				}
				colony.setText(selected.getColonia());
				fiscalData.getAddress().setColony(selected.getColonia());
			}
			else{
				zipCode.setText("");
				state.setText("");
				poblacion.setText("");				
				colony.setText("");
			}				
			dialog.dismiss();
		}
		
	};


	protected void fillAddress() {
		// TODO Auto-generated method stub
		if(customerAddress!=null){		
			zipCode.setEnabled(false);
			
			street.setText(customerAddress.getStreet());
			fiscalData.getAddress().setStreet(customerAddress.getStreet());
			internalNumber.setText(customerAddress.getInternalNumber());
			fiscalData.getAddress().setInternalNumber(customerAddress.getInternalNumber());
			externalNUmber.setText(customerAddress.getExternalNumber());
			fiscalData.getAddress().setExternalNumber(customerAddress.getExternalNumber());
			
			state.setText(customerAddress.getState());
			fiscalData.getAddress().setState(customerAddress.getState());
			poblacion.setText(customerAddress.getPoblacion());
			fiscalData.getAddress().setPoblacion(customerAddress.getPoblacion());
			colony.setText(customerAddress.getColony());
			fiscalData.getAddress().setColony(customerAddress.getColony());
			zipCode.setText(customerAddress.getZipCode());
			fiscalData.getAddress().setZipCode(customerAddress.getZipCode());
			
		}
	}
	protected void emptyAddress() {
		// TODO Auto-generated method stub
		
		zipCode.setEnabled(true);
		
		street.setText("");
		internalNumber.setText("");
		externalNUmber.setText("");
		state.setText("");
		poblacion.setText("");
		colony.setText("");
		zipCode.setText("");
		
		fiscalData.getAddress().setStreet(null);
		fiscalData.getAddress().setInternalNumber(null);
		fiscalData.getAddress().setExternalNumber(null);
		
		
		fiscalData.getAddress().setState(null);
		fiscalData.getAddress().setPoblacion(null);
		fiscalData.getAddress().setColony(null);
		fiscalData.getAddress().setZipCode(null);
				
	}
	public Dialog showSingleChoicePopup(){
		Builder builder = new AlertDialog.Builder(this);	
		builder.setTitle(popupTitle);		
		builder.setCancelable(false);	
		
		switch (spinnerType) {		
		case SPINNER_COLONY:
			if(colonyAdapter!=null && colonyAdapter.getCount()>0){
				builder.setSingleChoiceItems(colonyAdapter, 0, colonyListener);
				return builder.create();
			}
			else
				return null;		
		default:
			return null;
		
		}
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(data!=null) {
			params = data.getExtras();

		}
		Logger.log("result: "+ resultCode + "request: "+ requestCode);
		switch (requestCode) {


			case CONFIRM_CODE:
				if(resultCode==RESULT_OK){
					if(params!= null){
						RegistrationBean registerBean=(RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
						intent.putExtra(RegistrationBean.REGISTER,registerBean);
						setResult(RESULT_OK,intent);
						finish();
					}
				}
				break;

			default:
				break;

		}
	}
}
