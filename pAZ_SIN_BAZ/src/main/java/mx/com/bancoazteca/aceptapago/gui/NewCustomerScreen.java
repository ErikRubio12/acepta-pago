package mx.com.bancoazteca.aceptapago.gui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import mx.com.bancoazteca.aceptapago.beans.GenerateKeyBean;
import mx.com.bancoazteca.aceptapago.beans.GenerateRequest;
import mx.com.bancoazteca.aceptapago.beans.LoginRequest;
import mx.com.bancoazteca.aceptapago.beans.LoginServiceBean;
import mx.com.bancoazteca.aceptapago.util.HtmlUtils;
import mx.com.bancoazteca.aceptapago.ws.APIClient;
import mx.com.bancoazteca.aceptapago.ws.Constants;
import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.data.Parameter;
import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.messages.EmailBean;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.baz.apertura.UtilidadesEktDinero;
import mx.com.bancoazteca.aceptapago.baz.entidades.Colonia;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.db.daos.AddressDao;
import mx.com.bancoazteca.aceptapago.db.daos.CustomerDao;
import mx.com.bancoazteca.aceptapago.jobs.RegisterCustomerJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.ConfirmationDialog;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;
import okhttp3.Headers;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;

public class NewCustomerScreen extends BaseActivity implements MessageListener<Message, String>{
	protected static final String FOLIO = "folio";
	private static final int MIN_USER_LENGTH = 1;
	private static final int MAX_USER_LENGTH = 50;
	private static final int MAX_PASSWORD_LENGTH = 50;
	private static final int MIN_PASSWORD_LENGTH = 1;
	private static final int POPUP_VALID_FORM = 13;
	private static final int POPUP_INVALID_EMAIL = 14;
	private static final int POPUP_REGISTER = 15;
	protected static final int POPUP_INVALID_USER = 16;
	protected static final int POPUP_ERROR = 17;
	protected static final int POPUP_CONNECTION_ERROR = 18;
	private static final int POPUP_ONE_LAST_NAME = 19;
	private static final int POPUP_CONFIRM = 22;
	private final static int POPUP_SINGLE_CHOICE=10;
	protected static final String TAG_NEW_CUSTOMER = "ClienteNuevo";
	private int spinnerType;	
	private final static int SPINNER_COLONY=2;
	private final static int SPINNER_IDTYPE=4;
	private final static int SPINNER_MARITAL_SATUS=5;
	private final static int SPINNER_GENDER=7;
	private final static int SPINNER_CITIZENSHIP=8;





//  ********************************* DESARROLLO *************************************
//	private static final String passLogin = "l5r3/sgLqq/fUsq+IOgOp6Frs+4gkuQOtVYbnx8PshY=";
//  private static final String userLogin = "usrDispositivosMovilesDesa";


//  ********************************* PRODUCCIÓN *************************************
	private static final String passLogin = "cU+ngdbqrbe95DZ7EcNHmOB1CX3ViA6pT/Xs8I5TsQs=";
	private static final String userLogin = "usrDispositivosMoviles";


	private static final String keyAppLogin = "JVCiOS";
	private static final String dispositivoLogin = "gs1089882";
	private static final String infoHashActivationLogin = "eloy-2553a5586fS17c0a163d051b83";
	private static final String aplicacionLogin = "IOS-IPAD_JVC 38.0";




	private EditText gender;
	private EditText idType;
	private EditText maritalStatus;
	private EditText bornDate;
	private EditText citizenship;
	private EditText state;
	private EditText poblacion;
	private EditText colony;
	private EditText zipCode;
	private EditText name;
	private EditText lastName;
	private EditText lastName2;
	private EditText rfc;
	private EditText id;
	private EditText street;
	private EditText externalNumber;
	private EditText internalNumber;
	private EditText lada;
	private EditText telephone;
	private EditText extention;
	private EditText mail;
	private EditText user;
	private EditText pw;
	public final static String REQUIRED = "Campo obligatorio";
	public final static String CHARACTERES = "Caracter invalido";

	private Calendar cal=Calendar.getInstance();
	private CustomerBean customer=new CustomerBean();
	private LoginBean login;
	private AddressDao addressDao;
	private CustomerDao customerDao;
	private AddressBean address= new AddressBean();
	private ArrayAdapter<String> colonyAdapter;
	private List<Parameter<String, String>> citizenships;
	private List<Parameter<String, String>> ids;
	private ArrayAdapter<String> citizenshipAdapter;
	private ArrayAdapter<String> idsAdapter;
	private int year=0;
	private String tmpParameter=null;
	private boolean justOneLastName=false;
	private boolean isLastName2Available=true;
	private boolean isLastNameAvailable=true;
	private ArrayAdapter<String> genderAdapter;
	private ArrayAdapter<String> maritalAdapter;
	private ArrayList<Colonia> addresses;
	private Toolbar mtoolbar;
	private String zipC;
	private EditText rfcHomoclave;
	private String token;
	private String dateCurrent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		addressDao= new AddressDao(this);
		if(params!= null){
			
			customer=(CustomerBean) params.getSerializable(CustomerBean.CUSTOMER);
			login=(LoginBean)params.getSerializable(LoginBean.LOGIN);
			if(customer.getName()!=null){
				SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
				bornDate.setText(formatter.format(customer.getBornDate()));
				name.setText(customer.getName());
				lastName.setText(customer.getLastName());
				lastName2.setText(customer.getLasttName2());
				rfc.setText(customer.getRfc());
				id.setText(customer.getId());
				street.setText(customer.getAddress().getStreet());
				externalNumber.setText(customer.getAddress().getExternalNumber());
				internalNumber.setText(customer.getAddress().getInternalNumber());
				lada.setText(customer.getAddress().getTelephone());
				telephone.setText(customer.getAddress().getTelephone());
				extention.setText(customer.getAddress().getExtention());
				mail.setText(customer.getMail());
				user.setText(login.getUser());
//				password.setText(login.getPassword());
				pw.setText(login.getPassword());

				cal.setTime(customer.getBornDate());				
				checkRfc();
			}
		}
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		setContentView(R.layout.new_customer);
		char [] extraChars={'_','-','.','@'};
		InputFilter filters[]=null;
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
		gender=(EditText) findViewById(R.id.gendersEditText);
		bornDate=(EditText) findViewById(R.id.bornDateText);
		
		name=(EditText) findViewById(R.id.nameEditText);
		filters= new InputFilter[name.getFilters().length +1];
		System.arraycopy(name.getFilters(), 0, filters,0,name.getFilters().length);
		filters[name.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC);
		name.setFilters(filters);
		
		lastName=(EditText) findViewById(R.id.lasNameEditText);
		filters= new InputFilter[lastName.getFilters().length +1];
		System.arraycopy(lastName.getFilters(), 0, filters,0, lastName.getFilters().length);
		filters[lastName.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC);
		lastName.setFilters(filters);
		
		lastName2=(EditText) findViewById(R.id.lastName2EditText);
		filters= new InputFilter[lastName2.getFilters().length +1];
		System.arraycopy(lastName2.getFilters(), 0, filters,0, lastName2.getFilters().length);
		filters[lastName2.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC);
		lastName2.setFilters(filters);
		
		maritalStatus=(EditText) findViewById(R.id.marriedStatusEditText);
		citizenship=(EditText) findViewById(R.id.citizenshipEditText);
		
		rfc=(EditText) findViewById(R.id.rfcEditText);
		filters= new InputFilter[rfc.getFilters().length +1];
		System.arraycopy(rfc.getFilters(), 0, filters,0, rfc.getFilters().length);
		filters[rfc.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC);
		rfc.setFilters(filters);


		rfcHomoclave = (EditText) findViewById(R.id.rfcEditTextHomoclave);
		filters= new InputFilter[rfcHomoclave.getFilters().length +1];
		System.arraycopy(rfcHomoclave.getFilters(), 0, filters,0, rfcHomoclave.getFilters().length);
		filters[rfcHomoclave.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC);
		rfcHomoclave.setFilters(filters);
		InputFilter[] editFilters = rfcHomoclave.getFilters();
		InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
		System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);
		newFilters[editFilters.length] = new InputFilter.AllCaps();
		rfcHomoclave.setFilters(newFilters);

		id=(EditText) findViewById(R.id.idEditText);
		filters= new InputFilter[id.getFilters().length +1];
		System.arraycopy(id.getFilters(), 0, filters,0,id.getFilters().length);
		filters[id.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC);
		id.setFilters(filters);
		
		street=(EditText) findViewById(R.id.streetEditText);
		filters= new InputFilter[street.getFilters().length +1];
		System.arraycopy(street.getFilters(), 0, filters,0,street.getFilters().length);
		filters[street.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		street.setFilters(filters);
		
		externalNumber=(EditText) findViewById(R.id.externalNumberEditText);
		filters= new InputFilter[externalNumber.getFilters().length +1];
		System.arraycopy(externalNumber.getFilters(), 0, filters,0,externalNumber.getFilters().length);
		filters[externalNumber.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		externalNumber.setFilters(filters);
		
		internalNumber=(EditText) findViewById(R.id.internalNumberEditText);
		filters= new InputFilter[internalNumber.getFilters().length +1];
		System.arraycopy(internalNumber.getFilters(), 0, filters,0,internalNumber.getFilters().length);
		filters[internalNumber.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		internalNumber.setFilters(filters);
	
		state=(EditText) findViewById(R.id.stateEditText);
		poblacion=(EditText) findViewById(R.id.poblacionEditText);
		colony=(EditText) findViewById(R.id.colonyEditText);
		zipCode=(EditText) findViewById(R.id.zipCodeEditText);
		
		lada=(EditText) findViewById(R.id.ladaEditText);
		filters= new InputFilter[lada.getFilters().length +1];
		System.arraycopy(lada.getFilters(), 0, filters,0,lada.getFilters().length);
		filters[lada.getFilters().length]=new TextFilter(TextFilter.TYPE_LADA);
		lada.setFilters(filters);
		lada.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {lada.setText("");}
		});
		lada.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				lada.setText("");
				telephone.setText("");
				return false;
			}
		});
		telephone=(EditText) findViewById(R.id.telephoneNumberEditText);
		telephone.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				changePhoneLength();
			}
		});
		telephone.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				changePhoneLength();
				return false;
			}
		});
		filters= new InputFilter[telephone.getFilters().length +1];
		System.arraycopy(telephone.getFilters(), 0, filters,0,telephone.getFilters().length);
		filters[telephone.getFilters().length]=new TextFilter(TextFilter.TYPE_NUMERIC);
		telephone.setFilters(filters);
		
		extention=(EditText) findViewById(R.id.extNumberEditText);
		filters= new InputFilter[extention.getFilters().length +1];
		System.arraycopy(extention.getFilters(), 0, filters,0,extention.getFilters().length);
		filters[extention.getFilters().length]=new TextFilter(TextFilter.TYPE_NUMERIC);
		extention.setFilters(filters);
		
		mail=(EditText) findViewById(R.id.emailEditText);
		filters= new InputFilter[mail.getFilters().length +1];
		System.arraycopy(mail.getFilters(), 0, filters,0,mail.getFilters().length);
		filters[mail.getFilters().length]=new TextFilter(TextFilter.TYPE_EMAIL);
		mail.setFilters(filters);
		
		user=(EditText) findViewById(R.id.userEditText);
		filters= new InputFilter[user.getFilters().length +1];
		System.arraycopy(user.getFilters(), 0, filters,0,user.getFilters().length);
		filters[user.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC,extraChars);
		user.setFilters(filters);

		pw=(EditText) findViewById(R.id.pwEditText);
		filters= new InputFilter[pw.getFilters().length +1];
		System.arraycopy(pw.getFilters(), 0, filters,0,pw.getFilters().length);
		filters[pw.getFilters().length]=new TextFilter(TextFilter.TYPE_ALPHA_NUMERIC,extraChars);
		pw.setFilters(filters);
		
		
		customerDao= new CustomerDao(this);
		
		genderAdapter=new ArrayAdapter<String>(this,android.R.layout.select_dialog_singlechoice,customerDao.queryGenders());
		
		maritalAdapter =new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice,customerDao.queryMaritalStatus());
		
		idType= (EditText) findViewById(R.id.idTypeEditText);
		
		
		
		citizenships=customerDao.queryCitizenship();
		ids=customerDao.queryIds();
		
		citizenshipAdapter=new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
		idsAdapter=new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
		
		
		
		for (Parameter<String, String> value : citizenships)
			citizenshipAdapter.add(value.getValue());
		for (Parameter<String, String> value : ids)
			idsAdapter.add(value.getValue());
		
		zipCode.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus)
					if (zipCode.length() == 5) {
						new TaskProcesoColonias().execute();
					}
			}

		});

				
		citizenship.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.citizenship;
				popupMsg=R.string.selectCitizenship;
				spinnerType=SPINNER_CITIZENSHIP;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});				
		
		maritalStatus.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.marriedStatus;
				popupMsg=R.string.selectMarriedStatus;
				spinnerType=SPINNER_MARITAL_SATUS;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		
		idType.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.idType;
				popupMsg=R.string.selectIdType;
				spinnerType=SPINNER_IDTYPE;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		
		gender.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				popupTitle=R.string.gender;
				popupMsg=R.string.selectGender;
				spinnerType=SPINNER_GENDER;
				showDialog(POPUP_SINGLE_CHOICE);
			}
		});
		
		
		
		bornDate.setOnClickListener(pickerListener);
		
		
//		lastName.setOnFocusChangeListener(rfcListener);
//		lastName2.setOnFocusChangeListener(rfcListener);
//		name.setOnFocusChangeListener(rfcListener);
//		bornDate.setOnFocusChangeListener(rfcListener);
		lastName.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus)
					checkRfc();
			}
		});
		lastName2.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus)
					checkRfc();
			}
		});
		name.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus)
					checkRfc();
			}
		});


		Calendar calendar = Calendar.getInstance();
		Locale spanishLocale=new Locale("es", "ES");
		int currentYear = calendar.get(Calendar.YEAR);
		int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
		String month = calendar.getDisplayName(Calendar.MONTH,Calendar.LONG,spanishLocale);
		String monthCap = month.substring(0,1).toUpperCase() + month.substring(1).toLowerCase();

		dateCurrent = currentDay + " de " + monthCap + " del " + currentYear;

	}
	private class TaskProcesoColonias extends AsyncTask<Void, Void, ArrayList<Colonia>> {

		@Override
		protected void onPreExecute() {
			popupTitle=R.string.popUpMessage;
			popupMsg= R.string.wait;
			showDialog(POPUP_MESSAGES_PROGRESS);
			zipC = zipCode.getText().toString().trim();
		}

		@Override
		protected ArrayList<Colonia> doInBackground(Void... params) {
			try {
				
				UtilidadesEktDinero utilEkt = new UtilidadesEktDinero();
				addresses = utilEkt.traeColonia(zipC);
				if(addresses==null || addresses.size()==0)
					return null;				
				else{
					List<String> colonies= new ArrayList<String>();				
					for (Colonia currentAddress : addresses) {
						colonies.add(currentAddress.getColonia());
					}
//					colonyAdapter=new ArrayAdapter<String>
//					(getActivity(),android.R.layout.select_dialog_singlechoice,addressDao.queryColony(idState,idPoblacion));
					colonyAdapter=new ArrayAdapter<String>
					(NewCustomerScreen.this,android.R.layout.select_dialog_singlechoice,colonies);
					return addresses;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
		}

		@Override
		protected void onPostExecute(ArrayList<Colonia> result) {
			removeDialog(POPUP_MESSAGES_PROGRESS);
			if (result==null) {
				zipCode.setText("");
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.addressError));
				dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
			} else {
				Colonia colony=result.get(0);				
				address.setZipCode(zipCode.getText().toString().trim());				
				address.setState(colony.getEstado());
				state.setText(colony.getEstado());
				
				address.setPoblacion(colony.getPoblacion());
				poblacion.setText(colony.getPoblacion());
				
				
				popupTitle=R.string.colony;
				spinnerType=SPINNER_COLONY;
				showSingleChoicePopup().show();
			}

		}
	}
	private void changePhoneLength(){
		if(lada.length()==2){
			InputFilter filters[]=null;
			filters= new InputFilter[telephone.getFilters().length +1];
			System.arraycopy(telephone.getFilters(), 0, filters,0,telephone.getFilters().length);
			filters[telephone.getFilters().length]=new InputFilter.LengthFilter(8);
			telephone.setFilters(filters);
		}
		else if (lada.length()==3){
			InputFilter filters[]=null;
			filters= new InputFilter[telephone.getFilters().length +1];
			System.arraycopy(telephone.getFilters(), 0, filters,0,telephone.getFilters().length);
			filters[telephone.getFilters().length]=new InputFilter.LengthFilter(7);
			telephone.setFilters(filters);
		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton accept;
		Logger.log("Inicia onCreateDialog");
		switch (id) {
		case POPUP_SINGLE_CHOICE:
			return  showSingleChoicePopup();
		case POPUP_REGISTER:
			popUp= new PopUp(this, R.string.popUpMessage, R.string.registeringUser);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		default:
			return super.onCreateDialog(id);
		}
		
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putSerializable("login", login);
		outState.putString("parameter", tmpParameter);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		login=(LoginBean) savedInstanceState.getSerializable("login");
		tmpParameter=savedInstanceState.getString("tmpParameter");
	}
	private void updateDisplay() {
		
		bornDate.setText(
				new StringBuilder()
				// Month is 0 based so add 1
				.append(cal.get(Calendar.DAY_OF_MONTH)).append("/")
				.append(cal.get(Calendar.MONTH) + 1).append("/")
				.append(cal.get(Calendar.YEAR))
		);

	}
	public boolean isValidCharacters(){
		boolean answer = true;
		char extraCharacters[]= {'@'};
//		char specialCharacters[]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0',
//			    '1','2','3','4','5','6','7','8','9',' ','*','/','_','-',',',';',':','?','�','+','$','%','\\','.',')','(','[',']','=','�','{','}','@'};
		if(!Utility.isvalidCharacters(lastName.getText().toString())){
			answer=false;
			lastName.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(lastName2.getText().toString())){
			answer=false;
			lastName2.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(name.getText().toString())){
			answer=false;
			name.setError(CHARACTERES);
		}
		
		if(!Utility.isvalidCharacters(id.getText().toString())){
			answer=false;
			id.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(street.getText().toString())){
			answer=false;
			street.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(externalNumber.getText().toString())){
			answer=false;
			externalNumber.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(telephone.getText().toString())){
			answer=false;
			telephone.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(pw.getText().toString())){
			answer=false;
			pw.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(user.getText().toString())){
			answer=false;
			user.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(mail.getText().toString(), extraCharacters)){
			answer=false;
			mail.setError(CHARACTERES);
		}
		if(!Utility.isvalidCharacters(lada.getText().toString())){
			answer=false;
			lada.setError(CHARACTERES);
		}
		return answer;
	}
	public boolean validForm() {
		boolean answer=true;
		customer.setLastName(lastName.getText().toString().toUpperCase(Locale.getDefault()));
		customer.setLastName2(lastName2.getText().toString().toUpperCase(Locale.getDefault()));
		customer.setName(name.getText().toString().toUpperCase(Locale.getDefault()));
		customer.setBornDate(cal.getTime());
		customer.setRfc(rfc.getText().toString().toUpperCase(Locale.getDefault()) + rfcHomoclave.getText().toString().toUpperCase(Locale.getDefault()));
		customer.setCurp("");
		customer.setId(id.getText().toString());

		address.setStreet(street.getText().toString().toUpperCase(Locale.getDefault()));
		address.setExternalNumber(externalNumber.getText().toString().toUpperCase(Locale.getDefault()));
		address.setInternalNumber(internalNumber.getText().toString().toUpperCase(Locale.getDefault()));
		
		address.setLada(lada.getText().toString());
		address.setTelephone(telephone.getText().toString());
		address.setExtention(extention.getText().toString().length()==0 ? "0" : extention.getText().toString() );
		customer.setAddress(address);
		customer.setMail(mail.getText().toString().toUpperCase(Locale.getDefault()));
		int lengthPass = pw.length();
		char[] passw = new char[lengthPass];
		pw.getText().getChars(0,lengthPass,passw,0);
		login= new LoginBean(user.getText().toString().trim(), passw);
		
		if(lastName.length()==0){
			lastName.setError(REQUIRED);
		}
		if(lastName2.length()==0){
			lastName2.setError(REQUIRED);
		}
		if(name.getText().length()==0){
			name.setError(REQUIRED);
			answer=false;
		}
		if(id.length()==0){
			id.setError(REQUIRED);
			answer=false;
		}

//		if(rfcHomoclave.length()==0){
//			rfcHomoclave.setError(REQUIRED);
//			answer=false;
//		}
		if(street.length()==0){
			street.setError(REQUIRED);
			answer=false;
		}
		if(externalNumber.length()==0){
			externalNumber.setError(REQUIRED);
			answer=false;
		}
		if(state.getText().toString().length() == 0 || state.getText().toString().equalsIgnoreCase("ESTADO")){
			state.setError(REQUIRED);
			answer=false;
		}
		else 
			state.setError(null);
		if(poblacion.getText()==null || poblacion.getText().toString().length()==0 || poblacion.getText().toString().equalsIgnoreCase("POBLACION")){
			poblacion.setError(REQUIRED);
			answer=false;
		}
		else
			poblacion.setError(null);
		if(colony.getText()==null || colony.getText().toString().length()==0 || colony.getText().toString().equalsIgnoreCase("COLONIA")){
			colony.setError(REQUIRED);
			answer=false;
		}
		else
			colony.setError(null);
		if(zipCode.getText()==null || zipCode.getText().toString().length()==0 || zipCode.getText().toString().equalsIgnoreCase("C.P.")){
			zipCode.setError(REQUIRED);
			answer=false;
		}
		else
			zipCode.setError(null);
		if(telephone.length()==0){
			telephone.setError(REQUIRED);
			answer=false;
		}
		if(pw.length()==0){
			pw.setError(REQUIRED);
			answer=false;
		}
		if(user.length()==0){
			user.setError(REQUIRED);
			answer=false;
		}
		if(mail.length()==0){
			mail.setError(REQUIRED);
			answer=false;
		}
		if(lada.length()==0){
			lada.setError(REQUIRED);
			answer=false;
		}
		if(bornDate.getText().length()==0){
			bornDate.setError(REQUIRED);
			answer=false;
		}
		else 
			bornDate.setError(null);
		if(gender.getText().length()==0){
			gender.setError(REQUIRED);
			answer=false;
		}
		else
			gender.setError(null);
		if(maritalStatus.getText().length()==0){
			maritalStatus.setError(REQUIRED);
			answer=false;
		}
		else
			maritalStatus.setError(null);
		if(citizenship.getText().length()==0){
			citizenship.setError(REQUIRED);
			answer=false;
		}
		else
			citizenship.setError(null);
		if(idType.getText().length()==0){
			idType.setError(REQUIRED);
			answer=false;
		}
		else
			idType.setError(null);
			
		popupMsg= R.string.requiredFields;
		popupTitle=R.string.popUpError;
		
		if(!answer)
			return answer;
		else{
			isLastNameAvailable=true;
			isLastName2Available=true;
			justOneLastName=false;
			
			if(lastName.length()==0)
				isLastNameAvailable=false;
			else
				isLastNameAvailable=true;
			if(lastName2.length()==0)
				isLastName2Available=false;
			else
				isLastName2Available=true;
			
			
			if(!isLastName2Available & !isLastNameAvailable)
				return !answer;
			
			else if((!isLastName2Available & isLastNameAvailable) || (isLastName2Available & !isLastNameAvailable)){
				justOneLastName=true;
				return true;
			}
			
			Calendar cal= Calendar.getInstance();
			Log.i("New_Customer_scree", "año current" + year);
			Log.i("New_Customer_scree","año "+cal.get(Calendar.YEAR));
			if((cal.get(Calendar.YEAR)-year)<18){
				popupMsg=R.string.invalidAge;
				popupTitle=R.string.popUpMessage;
				return !answer;
			}
//			if(login.getUser().length()<MIN_USER_LENGTH || login.getPassword().length()>MAX_USER_LENGTH){
			if(login.getUser().length()<MIN_USER_LENGTH || login.getPassword().length()>MAX_USER_LENGTH){
				popupMsg=R.string.badUserPasswordLength;
				popupTitle=R.string.popUpMessage;
				return !answer;
			}
//			else if (login.getPassword().length()>MAX_PASSWORD_LENGTH || login.getPassword().length()<MIN_PASSWORD_LENGTH) {
			else if (login.getPassword().length()>MAX_PASSWORD_LENGTH || login.getPassword().length()<MIN_PASSWORD_LENGTH) {
				popupMsg=R.string.badUserPasswordLength;
				popupTitle=R.string.popUpMessage;
				return !answer;
			}
			return answer;
		}
		
	}

	private void startPickerDate() {
		// TODO Auto-generated method stub
		DatePickerDialog datePicker= new DatePickerDialog(this, dateListener,cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
		
		datePicker.show();
	}
	private void checkRfc(){
		if(name.getText().length()>0 && lastName.length()>0 && lastName2.length()>0 && bornDate.length()>0){
			String rfcStr=Utility.generateRfc(name.getText().toString(), lastName.getText().toString(), lastName2.getText().toString(), cal.getTime());
			rfc.setText(rfcStr);
			System.out.println("********* rfc long "+ rfc.getText().toString().length() + " RFC: " + rfcStr);
			
		}
	}
	//******************ACTION LISTENERS********************************
	private  DialogInterface.OnClickListener idTypeSelectedListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			customer.setCodIdent(Integer.parseInt(ids.get(which).getKey()));
			idType.setText(ids.get(which).getValue());
			removeDialog(POPUP_SINGLE_CHOICE);
			checkRfc();
		}
		
	};

	
	private  DialogInterface.OnClickListener genderSelectedListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			customer.setGender(which+1);
			gender.setText(genderAdapter.getItem(which));
			removeDialog(POPUP_SINGLE_CHOICE);
			checkRfc();
		}
		
	};
	
	private  DialogInterface.OnClickListener citizenshipSelectedListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			customer.setCitizenship(Integer.parseInt(citizenships.get(which).getKey()));
			citizenship.setText(citizenships.get(which).getValue());
			removeDialog(POPUP_SINGLE_CHOICE);
			checkRfc();
		}
	};
	
	private OnDateSetListener dateListener= new OnDateSetListener() {
		public void onDateSet(DatePicker view, int _year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			year=_year;
			cal.set(Calendar.YEAR, _year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDisplay();
			checkRfc();
			
		}
	};
	private OnClickListener pickerListener=new OnClickListener() {
		public void onClick(View v) {
			startPickerDate();
		}

	};
	private  DialogInterface.OnClickListener maritalListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			customer.setMaritalStatus(which+1);
			maritalStatus.setText(maritalAdapter.getItem(which));
			removeDialog(POPUP_SINGLE_CHOICE);
			checkRfc();
		}
		
	};
	public void acceptListener(View v){
		checkRfc();
		if(!validForm() ){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(popupTitle),getString(popupMsg));
			dialog.show(getSupportFragmentManager(),""+POPUP_VALID_FORM);
		}else if(!EmailBean.validEmail(mail.getText().toString())) {
			SimpleDialog dialog =SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.invalidEmail));
			dialog.show(getSupportFragmentManager(),""+POPUP_INVALID_EMAIL);
		}else{
			String b = "Se va ha realizar el registro con los siguientes datos.\n\nUsuario: " +
					user.getText().toString() +
					"\nCorreo: " +
					mail.getText() +
					"\n\n" +
					"¿Estas seguro que deseas continuar?";
			ConfirmationDialog confirmationDialog=ConfirmationDialog.newInstance(getString(R.string.popUpMessage), b);
			confirmationDialog.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
				@Override
				public void onAccept(DialogInterface dialog) {
					if (justOneLastName) {
						ConfirmationDialog confirmationDialog1 = ConfirmationDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.oneLastName));
						confirmationDialog1.setmAcceptListener(new ConfirmationDialog.AcceptListener() {
							@Override
							public void onAccept(DialogInterface dialog) {
								if (isLastName2Available) {
									String tempLastName = HtmlUtils.escapeHtml(lastName2.getText().toString().toUpperCase(Locale.getDefault()));
									customer.setLastName(tempLastName);
									String tempLastNameFinal = HtmlUtils.escapeHtml(customer.getLastName());
									lastName.setText(tempLastNameFinal);
								} else if (isLastNameAvailable) {
									String tempLastName = HtmlUtils.escapeHtml(lastName.getText().toString().toUpperCase(Locale.getDefault()));
									customer.setLastName(tempLastName);
									String tempLastNameFinal = HtmlUtils.escapeHtml(customer.getLastName());
									lastName2.setText(tempLastNameFinal);
								}
								checkRfc();
								customer.setRfc(rfc.getText().toString().toUpperCase(Locale.getDefault()));
								startRegister();
							}
						});
						confirmationDialog1.show(getSupportFragmentManager(), "" + POPUP_ONE_LAST_NAME);
					} else {
						startRegister();
					}
				}
			});
			confirmationDialog.show(getSupportFragmentManager(), "" + POPUP_CONFIRM);
		}
	}
	private void startRegister(){
		customer.setLogin(login);
		showDialog(POPUP_REGISTER);
		Logger.log(Logger.MESSAGE, TAG_NEW_CUSTOMER, "Datos:\n"+ customer);
		backgroundThread= new Thread(new RegisterCustomerJob(this,customer, this));
		backgroundThread.start();
//		try {
//			executeLoginService();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}


//	private OnFocusChangeListener rfcListener= new OnFocusChangeListener() {
//		public void onFocusChange(View v, boolean hasFocus) {
//			if(!hasFocus)
//				checkRfc();
//		}
//	};
	
	
	private DialogInterface.OnClickListener colonyListener= new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			Colonia selected=addresses.get(which);
			Logger.log(Logger.MESSAGE, TAG_NEW_CUSTOMER, "colony selected "+ selected);
			if(!"COLONIA".equalsIgnoreCase(selected.getColonia())){
				String ladaStr=null;
				if(selected.getIdEdo()!=null && !selected.getIdEdo().equals("") &&
				   selected.getIdPob()!=null && !selected.getIdPob().equals(""))
					ladaStr= addressDao.queryLada(1,Integer.parseInt(selected.getIdEdo()),Integer.parseInt(selected.getIdPob()) );
				Logger.log(Logger.MESSAGE, TAG_NEW_CUSTOMER, "lada encontrada: "+ ladaStr);
				if(ladaStr != null && ladaStr.length()>0){
					lada.setText(ladaStr);
					telephone.setText("");
					changePhoneLength();
				}
				colony.setText(selected.getColonia());
				address.setColony(selected.getColonia());
			}
			else{
				zipCode.setText("");
				state.setText("");
				poblacion.setText("");				
				colony.setText("");
			}
				
			dialog.dismiss();
		}
		
	};
	
	//*********************MESSAGE LISTENERS****************************
	@Override
	public void sendMessage(Context ctx,final Message message,final String parameter) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			public void run() {
				if(isFinishing())
					return;
				this_.removeDialog(POPUP_REGISTER);
				switch (message.getType()) {
				case Message.CANCEL:
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.invalidUser));
					dialog.show(getSupportFragmentManager(),""+POPUP_INVALID_USER);
					break;
				case Message.OK:
					customer.setCustomerId(parameter);
					intent.putExtra(CustomerBean.CUSTOMER, customer);
					setResult(RESULT_OK,intent);
					finish();
					break;
				case Message.ERROR:	
					tmpParameter=parameter;
					SimpleDialog dialog1=SimpleDialog.newInstance(getString(R.string.popUpError),tmpParameter);
					dialog1.show(getSupportFragmentManager(),""+POPUP_ERROR);
					break;

				default:
					break;
				}
			}
		});
		
		
	}
	@Override
	public void sendStatus(Context ctx,final int status) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			public void run() {
				if(isFinishing())
					return;
				this_.removeDialog(POPUP_REGISTER);
				SimpleDialog dialog =SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
				dialog.show(getSupportFragmentManager(),""+POPUP_CONNECTION_ERROR);
			}
		});
	}
	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
	
	public Dialog showSingleChoicePopup(){
		Builder builder = new AlertDialog.Builder(this);	
		builder.setTitle(popupTitle);		
		builder.setCancelable(false);	
		
		switch (spinnerType) {
		
		case SPINNER_COLONY:
			if(colonyAdapter!=null && colonyAdapter.getCount()>0){
				builder.setSingleChoiceItems(colonyAdapter, 0, colonyListener);
				return builder.create();
			}
			else
				return null;		
		case SPINNER_GENDER:
			if(genderAdapter!=null && genderAdapter.getCount()>0){
				builder.setSingleChoiceItems(genderAdapter,0,genderSelectedListener);
				return builder.create();
			}
			else
				return null;
		case SPINNER_IDTYPE:
			if(idsAdapter!= null && idsAdapter.getCount()>0){
				builder.setSingleChoiceItems(idsAdapter,0,idTypeSelectedListener);	
				return builder.create();
			}
		case SPINNER_MARITAL_SATUS:
			if(maritalAdapter!= null && maritalAdapter.getCount()>0){
				builder.setSingleChoiceItems(maritalAdapter,0,maritalListener);	
				return builder.create();
			}
			else
				return null;
		case SPINNER_CITIZENSHIP:
			if(citizenshipAdapter!=null &&  citizenshipAdapter.getCount()>0){
				builder.setSingleChoiceItems(citizenshipAdapter,0,citizenshipSelectedListener);	
				return builder.create();
			}
			else
				return null;
		default:
			return null;
		
		}
		
	}



	public void executeLoginService() throws IOException {


		//Creamos interfaz
		APIClient.ApiInterface service;
		//Ejecutamos el metodo para obtner el Cliente
		service = APIClient.getClient();

		Call<LoginServiceBean> callName = service.methodName(new LoginRequest(userLogin,passLogin,keyAppLogin,dispositivoLogin,infoHashActivationLogin,aplicacionLogin));
//		Call<LoginServiceBean> callName = service.methodName(new LoginRequest(userLogin,gen.genPassword(passLogin),keyAppLogin,dispositivoLogin,infoHashActivationLogin,aplicacionLogin));
		Log.d(Constants.GLOBAL_TAG,callName.request().toString());
		Log.d(Constants.GLOBAL_TAG,bodyToString(callName.request().body()));
		callName.enqueue(new Callback<LoginServiceBean>() {
			@Override
			public void onResponse(Call<LoginServiceBean> call, Response<LoginServiceBean> response) {
				Log.w(Constants.GLOBAL_TAG,new GsonBuilder().setPrettyPrinting().create().toJson(response));
				if(response.errorBody()!= null){
					Log.d(Constants.GLOBAL_TAG,"Error al realizar Login en WS");

//					AlertDialog.Builder alert = new AlertDialog.Builder(RegistrationMenu.this);
//					executeLoginService();
				}else{
					if(response.body().getCodigoOperacion() == 0){
						token = response.body().getTokenCode();
						//AQUI ME QUEDEEEE
						Headers headers = response.headers();

						executeWSContratos(headers.get("Set-Cookie"),name.getText().toString().toUpperCase(Locale.getDefault()),lastName.getText().toString().toUpperCase(Locale.getDefault()),lastName2.getText().toString().toUpperCase(Locale.getDefault()));
					}else{
						this_.removeDialog(POPUP_REGISTER);
						Log.d(Constants.GLOBAL_TAG,"Error al realizar Login en WS");
						android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(NewCustomerScreen.this);
						builder.setTitle("Mensaje")
								.setMessage("Error al registar los datos intentelo nuevamente")
								.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});
						android.support.v7.app.AlertDialog alert = builder.create();
						alert.show();

					}
				}
			}

			@Override
			public void onFailure(Call<LoginServiceBean> call, Throwable t) {

			}
		});
	}

	private void executeWSContratos(String cookieValue,String nomCte,String apePat,String apeMat) {

		//Creamos interfaz
		APIClient.ApiInterface service;
		//Ejecutamos el metodo para obtner el Cliente
		service = APIClient.getClient();


//


		GenerateRequest generateRequest =new GenerateRequest(nomCte, apePat,apeMat,dateCurrent,token);
//		GenerateRequest generateRequest =new GenerateRequest("gdfhjkl", "khdtfygku","ytyghougiy","20 de Febrero del 2018",token);
		Log.d(Constants.GLOBAL_TAG,generateRequest.toString());

		Call<GenerateKeyBean> call = service.generateKey(cookieValue,generateRequest);
		call.enqueue(new Callback<GenerateKeyBean>() {
			@Override
			public void onResponse(Call<GenerateKeyBean> call, Response<GenerateKeyBean> response) {
				Log.w(Constants.GLOBAL_TAG,new GsonBuilder().setPrettyPrinting().create().toJson(response));
				if(response.errorBody()!= null){
					Log.d(Constants.GLOBAL_TAG,"Error al realizar Login en WS");
				}else{
					if(response.body().getCodigoOperacion() == 0){
						customer.setKeyRegister(response.body().getLlave());
						backgroundThread= new Thread(new RegisterCustomerJob(NewCustomerScreen.this,customer, NewCustomerScreen.this));
						backgroundThread.start();

					}else{
						this_.removeDialog(POPUP_REGISTER);
						Log.d(Constants.GLOBAL_TAG,"Error al realizar Login en WS");
						Log.d(Constants.GLOBAL_TAG,"Error al realizar Login en WS");
						android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(NewCustomerScreen.this);
						builder.setTitle("Mensaje")
								.setMessage("Error al registar los datos intentelo nuevamente")
								.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});
						android.support.v7.app.AlertDialog alert = builder.create();
						alert.show();
					}
				}
			}

			@Override
			public void onFailure(Call<GenerateKeyBean> call, Throwable t) {

			}
		});


	}

	private String bodyToString(final RequestBody request) {
		try {
			final RequestBody copy = request;
			final Buffer buffer = new Buffer();
			if (copy != null)
				copy.writeTo(buffer);
			else
				return "";
			return buffer.readUtf8();
		} catch (final IOException e) {
			return "did not work";
		}
	}
}
