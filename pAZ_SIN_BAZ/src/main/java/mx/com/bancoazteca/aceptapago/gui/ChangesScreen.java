package mx.com.bancoazteca.aceptapago.gui;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.Configuration;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.jobs.CheckDevice;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;

public class ChangesScreen extends BaseActivity implements MessageListener<Message, String>{
	private FiscalDataBean fiscalData;
	private CustomerBean customer;
	private TextView nameText;
	private TextView razonSocialText;
	private TextView RFCText;
	private TextView addressText;
	private TextView afiliacionText;
	private TextView deviceNumberText;
	private Toolbar mtoolbar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.changes);
		mtoolbar=(Toolbar) findViewById(R.id.toolbar);
		nameText=(TextView) findViewById(R.id.configurationNameText);
		razonSocialText=(TextView) findViewById(R.id.configurationRazonSocialText);
		RFCText=(TextView) findViewById(R.id.configurationRFCText);
		addressText=(TextView) findViewById(R.id.configurationAddressText);
		afiliacionText=(TextView) findViewById(R.id.configurationAfiliacionText);
		deviceNumberText=(TextView) findViewById(R.id.configurationDeviceNumberText);

		setSupportActionBar(mtoolbar);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		Configuration.loadPreferences(this);				
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params!=null){
			customer= (CustomerBean)params.getSerializable(CustomerBean.CUSTOMER);
			fiscalData=(FiscalDataBean)params.getSerializable(FiscalDataBean.FISCAL_DATA);
			Configuration.setName(customer.getName() +" " +customer.getLastName()+ " "+customer.getLasttName2());
			Configuration.setSocialName(fiscalData.getFiscalName());
			Configuration.setRFC(fiscalData.getRfc());
			Configuration.setAddress(fiscalData.getAddress().getState()+ ", " +fiscalData.getAddress().getColony()+  ", " + fiscalData.getAddress().getZipCode() );
			addressText.setText(Configuration.getAddress());
			nameText.setText(Configuration.getName());
			razonSocialText.setText(Configuration.getSocialName());
			RFCText.setText(Configuration.getRFC());
			Configuration.setSwiped(false);
			Configuration.persistPreferences(this);
		}
	}


	@Override
	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		super.startBackgroundProcess();
		if (customer.getDevice()==null) {
			popupTitle=R.string.popUpMessage;
			popupMsg=R.string.downloadBusinessInfo;
			showDialog(POPUP_MESSAGES_PROGRESS);
			
			backgroundThread= new Thread(new CheckDevice(this,this,customer.getFolioPreap(),fiscalData.getRfc()));
			backgroundThread.start();
		}
		else{
			deviceNumberText.setText(customer.getDevice());
			afiliacionText.setText(customer.getAffiliate());
		}
	}
	
	//***********ACTION LISTENERS******************+
	
	
		@Override
		public void sendMessage(Context ctx,Message message,final String parameter) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void sendStatus(Context ctx,final int status) {
			if(isFinishing())
				return;
			runOnUiThread(new Runnable() {
				public void run() {
					this_.removeDialog(POPUP_MESSAGES_PROGRESS);
					if (status==Message.EXCEPTION){
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
						dialog.show(getSupportFragmentManager(),""+POPUP_CONNECTION_ERROR);
					}else if (status==Message.ERROR){
						SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.internalError));
						dialog.show(getSupportFragmentManager(), "" + POPUP_MESSAGES);
					}
						
				}
			});
			
		}
		@Override
		public void sendMessage(Context ctx,Message message, final Object... parameters) {
			// TODO Auto-generated method stub
			if(isFinishing())
				return;
			runOnUiThread(new Runnable() {
				public void run() {
					this_.removeDialog(POPUP_MESSAGES_PROGRESS);
					deviceNumberText.setText((String)parameters[0]);
					afiliacionText.setText((String)parameters[1]);
					
				}
			});
		}
}
