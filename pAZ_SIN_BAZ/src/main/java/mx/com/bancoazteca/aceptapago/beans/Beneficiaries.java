package mx.com.bancoazteca.aceptapago.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class Beneficiaries implements KvmSerializable, Serializable{

	private static final long serialVersionUID = -3571876494159488787L;
	private static final int FIELDS = 49;
	private static final int BENEFICIARIES = 4;
	private String folio;
	private String lastNames[];
	private String lastNames2[];
	private String streets[];
	private String colonies[];
	private String zipCodes[];
	private String states[];
	private String externalNumbers[];
	private String internalNumbers[];
	private String poblaciones[];
	private String percentages[];
	private String kin[];
	private String names[];
	private List<BeneficiaryBean> beneficiaries;
	public static final String LAST_NAMES2[]={"apMaterno1","apMaterno2","apMaterno3","apMaterno4"};
	public static final String LAST_NAMES[]={"apPaterno1","apPaterno2","apPaterno3","apPaterno4"};
	public static final String STREETS[]={"calle1","calle2","calle3","calle4"};
	public static final String COLONIES[]={"colonia1","colonia2","colonia3","colonia4"};
	public static final String ZIPCODES[]={"cp1","cp2","cp3","cp4"};
	public static final String STATES[]={"estado1","estado2","estado3","estado4"};
	public static final String FOLIO="folioPrea";
	public static final String NAMES[]={"nombre1","nombre2","nombre3","nombre4"};
	public static final String EXTERNAL_NUMBERS[]={"numExt1","numExt2","numExt3","numExt4"};
	public static final String INTERNAL_NUMBERS[]={"numInt1","numInt2","numInt3","numInt4"};
	public static final String KIN[]={"parentesco1","parentesco2","parentesco3","parentesco4"};
	public static final String POBLACIONES[]={"poblacion1","poblacion2","poblacion3","poblacion4"};
	public static final String PERCENTAGES[]={"porcentaje1","porcentaje2","porcentaje3","porcentaje4"};
	private static PropertyInfo PILastNames2[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PILastNames[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIStreets[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIColonies[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIZipCodes[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIStates[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIFolio= new PropertyInfo();
	private static PropertyInfo PINames[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIExternalNumbers[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIINternalNUmbers[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIKin[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIPoblaciones[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo PIPercentages[]= new PropertyInfo[BENEFICIARIES];
	private static PropertyInfo listPI[]= new PropertyInfo[FIELDS];
	static{
		for (int i = 0; i < BENEFICIARIES; i++) {
			PILastNames2[i]= new PropertyInfo();
			PILastNames2[i].name=LAST_NAMES2[i];
			PILastNames2[i].type=PropertyInfo.STRING_CLASS;
			
			PILastNames[i]= new PropertyInfo();
			PILastNames[i].name=LAST_NAMES[i];
			PILastNames[i].type=PropertyInfo.STRING_CLASS;
			
			PIStreets[i]= new PropertyInfo();
			PIStreets[i].name=STREETS[i];
			PIStreets[i].type=PropertyInfo.STRING_CLASS;
			
			PIColonies[i]= new PropertyInfo();
			PIColonies[i].name=COLONIES[i];
			PIColonies[i].type=PropertyInfo.STRING_CLASS;
			
			PIZipCodes[i]= new PropertyInfo();
			PIZipCodes[i].name=ZIPCODES[i];
			PIZipCodes[i].type=PropertyInfo.STRING_CLASS;
			
			PIStates[i]= new PropertyInfo();
			PIStates[i].name=STATES[i];
			PIStates[i].type=PropertyInfo.STRING_CLASS;
			
			PINames[i]= new PropertyInfo();
			PINames[i].name=NAMES[i];
			PINames[i].type=PropertyInfo.STRING_CLASS;
			
			PIExternalNumbers[i]= new PropertyInfo();
			PIExternalNumbers[i].name=EXTERNAL_NUMBERS[i];
			PIExternalNumbers[i].type=PropertyInfo.STRING_CLASS;
			
			PIINternalNUmbers[i]= new PropertyInfo();
			PIINternalNUmbers[i].name=INTERNAL_NUMBERS[i];
			PIINternalNUmbers[i].type=PropertyInfo.STRING_CLASS;
			
			PIKin[i]=new PropertyInfo();
			PIKin[i].name=KIN[i];
			PIKin[i].type=PropertyInfo.STRING_CLASS;
			
			PIPoblaciones[i]= new PropertyInfo();
			PIPoblaciones[i].name=POBLACIONES[i];
			PIPoblaciones[i].type=PropertyInfo.STRING_CLASS;
			
			PIPercentages[i]= new PropertyInfo();
			PIPercentages[i].name=PERCENTAGES[i];
			PIPercentages[i].type=PropertyInfo.STRING_CLASS;
		}
		PIFolio.name=FOLIO;
		PIFolio.type=PropertyInfo.STRING_CLASS;
		int i=0;
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PILastNames2[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PILastNames[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIStreets[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIColonies[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIZipCodes[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIStates[j];
			i++;
		}
		listPI[i]=PIFolio;
		i++;
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PINames[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIExternalNumbers[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIINternalNUmbers[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIKin[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIPoblaciones[j];
			i++;
		}
		for (int j = 0; j < BENEFICIARIES; j++) {
			listPI[i]=PIPercentages[j];
			i++;
		}
		
	}
	
	public Beneficiaries(){
		
	}
	public Beneficiaries(BeneficiaryBean[] list,String folio) {
		// TODO Auto-generated constructor stub
		this.beneficiaries=new ArrayList<BeneficiaryBean>(Arrays.asList(list));
		this.folio=folio;
		lastNames= new String[BENEFICIARIES];
		lastNames2= new String[BENEFICIARIES];
		streets= new String[BENEFICIARIES];
		colonies= new String[BENEFICIARIES];
		zipCodes= new String[BENEFICIARIES];
		states= new String[BENEFICIARIES];
		externalNumbers= new String[BENEFICIARIES];
		internalNumbers= new String[BENEFICIARIES];
		poblaciones= new String[BENEFICIARIES];
		percentages= new String[BENEFICIARIES];
		kin= new String[BENEFICIARIES];
		names= new String[BENEFICIARIES];
		for (int j = 0; j < BENEFICIARIES; j++) {
			lastNames[j]=list[j].getLastName();
			lastNames2[j]=list[j].getLastName2();
			streets[j]=list[j].getAddress().getStreet();
			colonies[j]=list[j].getAddress().getColony();
			zipCodes[j]=list[j].getAddress().getZipCode();
			states[j]=list[j].getAddress().getState();
			externalNumbers[j]=list[j].getAddress().getExternalNumber();
			internalNumbers[j]=list[j].getAddress().getInternalNumber();
			poblaciones[j]=list[j].getAddress().getPoblacion();
			percentages[j]=list[j].getPercentage();
			kin[j]=list[j].getKinship();
			names[j]=list[j].getName();
		}
	}
	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0:return lastNames2[0];
		case 1:return lastNames2[1];
		case 2:return lastNames2[2];
		case 3:return lastNames2[3];
		case 4:return lastNames[0];
		case 5:return lastNames[1];
		case 6:return lastNames[2];
		case 7:return lastNames[3];			
		case 8:return streets[0];
		case 9:return streets[1];
		case 10:return streets[2];
		case 11:return streets[3];
		case 12:return colonies[0];
		case 13:return colonies[1];
		case 14:return colonies[2];
		case 15:return colonies[3];
		case 16:return zipCodes[0];
		case 17:return zipCodes[1];
		case 18:return zipCodes[2];
		case 19:return zipCodes[3];
		case 20:return states[0];
		case 21:return states[1];
		case 22:return states[2];
		case 23:return states[3];
		case 24:return folio;
		case 25:return names[0];
		case 26:return names[1];
		case 27:return names[2];
		case 28:return names[3];
		case 29:return externalNumbers[0];
		case 30:return externalNumbers[1];
		case 31:return externalNumbers[2];
		case 32:return externalNumbers[3];
		case 33:return internalNumbers[0];
		case 34:return internalNumbers[1];
		case 35:return internalNumbers[2];
		case 36:return internalNumbers[3];
		case 37:return kin[0];
		case 38:return kin[1];
		case 39:return kin[2];
		case 40:return kin[3];
		case 41:return poblaciones[0];
		case 42:return poblaciones[1];
		case 43:return poblaciones[2];
		case 44:return poblaciones[3];
		case 45:return percentages[0];
		case 46:return percentages[1];
		case 47:return percentages[2];
		case 48:return percentages[3];
		default:return null;
		}
	}

	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return FIELDS;
	}

	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		arg2.name=listPI[arg0].name;
		arg2.type=listPI[arg0].type;
	}

	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0: lastNames2[0]=(String) arg1; break;
		case 1: lastNames2[1]=(String) arg1; break;
		case 2: lastNames2[2]=(String) arg1; break;
		case 3: lastNames2[3]=(String) arg1; break;
		case 4: lastNames[0]=(String) arg1; break;
		case 5: lastNames[1]=(String) arg1; break;
		case 6: lastNames[2]=(String) arg1; break;
		case 7: lastNames[3]=(String) arg1; break;			
		case 8: streets[0]=(String) arg1; break;
		case 9: streets[1]=(String) arg1; break;
		case 10: streets[2]=(String) arg1; break;
		case 11: streets[3]=(String) arg1; break;
		case 12: colonies[0]=(String) arg1; break;
		case 13: colonies[1]=(String) arg1; break;
		case 14: colonies[2]=(String) arg1; break;
		case 15: colonies[3]=(String) arg1; break;
		case 16: zipCodes[0]=(String) arg1; break;
		case 17: zipCodes[1]=(String) arg1; break;
		case 18: zipCodes[2]=(String) arg1; break;
		case 19: zipCodes[3]=(String) arg1; break;
		case 20: states[0]=(String) arg1; break;
		case 21: states[1]=(String) arg1; break;
		case 22: states[2]=(String) arg1; break;
		case 23: states[3]=(String) arg1; break;
		case 24: folio=(String) arg1; break;
		case 25: names[0]=(String) arg1; break;
		case 26: names[1]=(String) arg1; break;
		case 27: names[2]=(String) arg1; break;
		case 28: names[3]=(String) arg1; break;
		case 29: externalNumbers[0]=(String) arg1; break;
		case 30: externalNumbers[1]=(String) arg1; break;
		case 31: externalNumbers[2]=(String) arg1; break;
		case 32: externalNumbers[3]=(String) arg1; break;
		case 33: internalNumbers[0]=(String) arg1; break;
		case 34: internalNumbers[1]=(String) arg1; break;
		case 35: internalNumbers[2]=(String) arg1; break;
		case 36: internalNumbers[3]=(String) arg1; break;
		case 37: kin[0]=(String) arg1; break;
		case 38: kin[1]=(String) arg1; break;
		case 39: kin[2]=(String) arg1; break;
		case 40: kin[3]=(String) arg1; break;
		case 41: poblaciones[0]=(String) arg1; break;
		case 42: poblaciones[1]=(String) arg1; break;
		case 43: poblaciones[2]=(String) arg1; break;
		case 44: poblaciones[3]=(String) arg1; break;
		case 45: percentages[0]=(String) arg1; break;
		case 46: percentages[1]=(String) arg1; break;
		case 47: percentages[2]=(String) arg1; break;
		case 48: percentages[3]=(String) arg1; break;
		}
	}
	public void setBeneficiaries(List<BeneficiaryBean> beneficiaries) {
		this.beneficiaries = beneficiaries;
	}
	public List<BeneficiaryBean> getBeneficiaries() {
		return beneficiaries;
	}

}
