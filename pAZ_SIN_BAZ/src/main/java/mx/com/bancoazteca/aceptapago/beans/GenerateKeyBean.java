package mx.com.bancoazteca.aceptapago.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by B931724 on 20/02/18.
 */

public class GenerateKeyBean implements Parcelable{
    @SerializedName("folio")
    @Expose
    private String folio;
    @SerializedName("tokenCode")
    @Expose
    private String tokenCode;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("succes")
    @Expose
    private String succes;
    @SerializedName("respuesta")
    @Expose
    private RespuestaBean respuesta;
    @SerializedName("llave")
    @Expose
    private String llave;
    @SerializedName("codigoOperacion")
    @Expose
    private int codigoOperacion;

    protected GenerateKeyBean(Parcel in) {
        folio = in.readString();
        tokenCode = in.readString();
        descripcion = in.readString();
        succes = in.readString();
        llave = in.readString();
        codigoOperacion = in.readInt();
    }

    public static final Creator<GenerateKeyBean> CREATOR = new Creator<GenerateKeyBean>() {
        @Override
        public GenerateKeyBean createFromParcel(Parcel in) {
            return new GenerateKeyBean(in);
        }

        @Override
        public GenerateKeyBean[] newArray(int size) {
            return new GenerateKeyBean[size];
        }
    };

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getTokenCode() {
        return tokenCode;
    }

    public void setTokenCode(String tokenCode) {
        this.tokenCode = tokenCode;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSucces() {
        return succes;
    }

    public void setSucces(String succes) {
        this.succes = succes;
    }

    public RespuestaBean getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(RespuestaBean respuesta) {
        this.respuesta = respuesta;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public int getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(int codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(folio);
        dest.writeString(tokenCode);
        dest.writeString(descripcion);
        dest.writeString(succes);
        dest.writeString(llave);
        dest.writeInt(codigoOperacion);
    }
}
