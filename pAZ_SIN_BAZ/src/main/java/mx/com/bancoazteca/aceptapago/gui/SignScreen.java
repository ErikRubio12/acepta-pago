package mx.com.bancoazteca.aceptapago.gui;

import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.Path;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.bancoazteca.beans.CardBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.hw.LocationThread;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnResponse;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.ui.customviews.SignView;
import mx.com.bancoazteca.util.Logger;

public class SignScreen extends BaseActivity {
	public static final int DETAIL_SALE_CODE = 130;
	private SaleBean sale;
	private SignView firmArea;
	private CustomerBean customer;
	private FiscalDataBean fiscalData;
	private TextView amount;
	protected static final int MARGIN = 5;
	private OperationsBean operationSelected;
	private Location location;
	private boolean isCancellation=false;
	private OperationsBean operation;
	private String folioPreap;
	private CardBean card;
	private LoginBean login;
	private String afiliacion;
	private SaleReturnResponse saleReturnResponse=null;
	private final static String FIRM_SCREEN="FirmScreen";
	private TextView cardNumber;
	private ImageView cardType;
	private HashMap<String, Object>saved;
	
	public final static int SIGN_SALE_CODE=123;
	private Toolbar mtoolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.sign);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		amount = (TextView) findViewById(R.id.amountId);
		firmArea= (SignView) findViewById(R.id.firm);
		firmArea.setDrawingCacheEnabled(true);
		firmArea.setDrawingCacheQuality(2);
		cardNumber=(TextView) findViewById(R.id.card);
		cardType=(ImageView) findViewById(R.id.cardType);
		LayoutParams params=firmArea.getLayoutParams();
		params.height= (int) (getHeight()*0.50);
		firmArea.setLayoutParams(params);
		firmArea.setOnTouchListener(touchListener);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_sign_screen,menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id=item.getItemId();
		switch (id){
			case R.id.btn_menu_sign_next:
				signNext();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void handleBackToFront(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.handleBackToFront(savedInstanceState);
		if(getLastNonConfigurationInstance()!= null){
			saved=(HashMap<String, Object>) getLastNonConfigurationInstance();
			Path path=(Path)saved.get("path");	
			
			if(path != null){
				if(!isLandscape()){
					float sfx=((float)getWidth())/((float)getHeight());
					Matrix matrix= new Matrix();
					matrix.setScale(sfx,sfx);
					path.transform(matrix);
					path.moveTo(50, 90);
					LayoutParams params=firmArea.getLayoutParams();
					params.height= (int) (getHeight()*0.50);
					firmArea.setLayoutParams(params);
					
					firmArea.setPath(path);
					firmArea.invalidate();
				}
				else{
					LayoutParams params=firmArea.getLayoutParams();
					params.height= (int) (getHeight()*0.30);
					firmArea.setLayoutParams(params);
					firmArea.cleanScreen();
				}
			}
		}
		
	}
	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		// TODO Auto-generated method stub
		saved= new HashMap<String, Object>();
		if(firmArea!= null )
			saved.put("path", firmArea.getPath()) ;
		
		return saved;
	}
	@Override
	public void onBackPressed() {
		setResult(RESULT_OK);
		finish();
	}
	@Override
	public void back(View v) {
		// TODO Auto-generated method stub
		setResult(RESULT_OK);
		super.back(v);
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params !=null){
			isCancellation=params.getBoolean(OperationsDetailScreen.CANCELLATION,false);
			if(isCancellation){
				Logger.log(Logger.MESSAGE, FIRM_SCREEN, "va a ser cancelacion ");
				operation=(OperationsBean) params.getSerializable(OperationsBean.OPERATION);
				folioPreap=(String) params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
				fiscalData=(FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
				card=(CardBean) params.getSerializable(CardBean.CREDIT_CARD);
				login=(LoginBean) params.getSerializable(LoginBean.LOGIN);
				saleReturnResponse = (SaleReturnResponse) params.getSerializable(SaleScreen.RETURN_RESPONSE);
				afiliacion=(String) params.getSerializable(SaleBean.AFILIACION);
				operationSelected=(OperationsBean) params.getSerializable(OperationsBean.OPERATION);
				StringBuffer buffer = new StringBuffer(getString(R.string.amountToPay));
				buffer.append(" : $ ");
				String decimalesStr=null;
				String enterosStr=null;
				//Logger.log(Logger.MESSAGE, FIRM_SCREEN,"***************** MONTO REGRESADO CANCELACION = "+ saleReturnResponse.getAmountReturned());
				int index=saleReturnResponse.getAmountReturned().indexOf(".");
				if(index>=0){
					decimalesStr=saleReturnResponse.getAmountReturned().substring(index+1);
					enterosStr=saleReturnResponse.getAmountReturned().substring(0, index);
				}
				else{
					decimalesStr=saleReturnResponse.getAmountReturned().substring(saleReturnResponse.getAmountReturned().length()-2);
					enterosStr=saleReturnResponse.getAmountReturned().substring(0, saleReturnResponse.getAmountReturned().length()-2);
				}
				int entero=Integer.parseInt(enterosStr);
				String montoAut= entero+"."+decimalesStr;				
				buffer.append(montoAut);
								
				amount.setText(buffer.toString());
				cardNumber.setText( "**** "+ card.getMaskedPan().substring(card.getMaskedPan().length()-4, card.getMaskedPan().length()));
				//Logger.log(Logger.MESSAGE, FIRM_SCREEN, "folio: "+ folioPreap + " tarjeta: "+ card);
			}
			else{
				Logger.log(Logger.MESSAGE, FIRM_SCREEN, "va a ser venta");
				sale=(SaleBean) params.getSerializable(SaleBean.SALE);
				customer= (CustomerBean) params.getSerializable(CustomerBean.CUSTOMER);
				fiscalData=(FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
				location=params.getParcelable(LocationThread.LOCATION);
				cardNumber.setText( "**** "+ sale.getCard().getMaskedPan().substring(sale.getCard().getMaskedPan().length()-4, sale.getCard().getMaskedPan().length()));
				//Logger.log(Logger.MESSAGE, FIRM_SCREEN, " tarjeta: "+ sale.getCard().getMaskedPan());
				if (sale != null){
					StringBuffer buffer = new StringBuffer(getString(R.string.amountToPay));
					buffer.append("$ ");
					String decimalesStr=null;
					String enterosStr=null;
					//Logger.log(Logger.MESSAGE, FIRM_SCREEN,"***************** MONTO REGRESADO VENTA = "+ sale.getAmountReturned());
					int index=sale.getAmountReturned().indexOf(".");
					if(index>=0){
						decimalesStr=sale.getAmountReturned().substring(index+1);
						enterosStr=sale.getAmountReturned().substring(0, index);
					}
					else{
						decimalesStr=sale.getAmountReturned().substring(sale.getAmountReturned().length()-2);
						enterosStr=sale.getAmountReturned().substring(0, sale.getAmountReturned().length()-2);
					}
					int entero=Integer.parseInt(enterosStr);
					String montoAut= entero+"."+decimalesStr;
					buffer.append(montoAut);
					amount.setText(buffer.toString());
				}
				
			}
			System.gc();
			if( isCancellation && card.getMaskedPan().startsWith("4"))
				cardType.setImageResource(R.drawable.img_visa);
			else if(isCancellation && card.getMaskedPan().startsWith("5"))
				cardType.setImageResource(R.drawable.img_master_card);
			else if( !isCancellation && sale.getCard().getMaskedPan().startsWith("4"))
				cardType.setImageResource(R.drawable.img_visa);
			else if(!isCancellation && sale.getCard().getMaskedPan().startsWith("5"))
				cardType.setImageResource(R.drawable.img_master_card);				
		}
	}		
	@Override
	public void release() {
		// TODO Auto-generated method stub
		super.release();
		
		
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (isFinishing()) {
			return;
		}
		switch (requestCode) {
		case SaleScreen.SALE_CODE:
			if(resultCode==RESULT_OK){
				File f=FileManager.openFile(this, "sign.png");
				if(f!= null && f.exists()){
					f.delete();
					Logger.log(Logger.MESSAGE, FIRM_SCREEN, "se elimino firma en onActivityResult debe regresar a venta");
				}
				setResult(RESULT_OK);
				finish();
			}
			
			break;
		case SaleScreen.SETTINGS_CODE:
			if(resultCode==RESULT_OK){
				File f=FileManager.openFile(this, "sign.png");
				if(f!= null && f.exists()){
					f.delete();
					Logger.log(Logger.MESSAGE, FIRM_SCREEN, "se elimino firma en onActivityResult debe regresar a Settings");
				}
				setResult(RESULT_OK);
				finish();
			}
			break;
		default:
			break;
		}
	}
	//**************** LISTENERS  ****************
	private OnTouchListener touchListener=new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			
			float x = event.getX();
            float y = event.getY();
            
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    firmArea.moveDown(x, y);
                    firmArea.invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    firmArea.drag(x, y);
                    firmArea.invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    firmArea.moveUp();
                    firmArea.invalidate();
                    break;
            }
            return true;
		}
	};

	public void clean(View v){
		firmArea.cleanScreen();
		firmArea.invalidate();
	}
	
	private void signNext() {

		if (firmArea.isEmpty()) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.signCantBeEmpty));
			dialog.show(getSupportFragmentManager(),""+POPUP_MESSAGES);
		} else {
			File signFile = FileManager.saveBitmap(firmArea.getBitmap(), getBaseContext(), "sign", 100, FileManager.PNG, FileManager.EXTERNAL);
			if (!isCancellation) {
				final List<Serializable> params = new ArrayList<Serializable>();
				params.add(fiscalData);
				params.add(customer);
				params.add(sale);
				final List<String> paramNames = new ArrayList<String>();
				paramNames.add(FiscalDataBean.FISCAL_DATA);
				paramNames.add(CustomerBean.CUSTOMER);
				paramNames.add(SaleBean.SALE);
				sale.setSignPath(signFile.getAbsolutePath());
				firmArea.setBitmap(null);
				firmArea = null;
				System.gc();
				intent.putExtra(LocationThread.LOCATION, location);
				//Logger.log(Logger.MESSAGE, FIRM_SCREEN,"datos fiscales: "+fiscalData+" cliente: "+customer+  "venta: "+ sale+" imagen: "+ SaleBean.getImage() + " ubicacion: " + location );
				goForwardForResult(DetailSaleScreen.class, getBaseContext(), params, paramNames, SaleScreen.SALE_CODE);
			} else {
				ArrayList<Serializable> paramsValues = new ArrayList<Serializable>();
				paramsValues.add(operationSelected);
				paramsValues.add(fiscalData);
				paramsValues.add(card);
				paramsValues.add(login);
				paramsValues.add(folioPreap);
				paramsValues.add(afiliacion);
				paramsValues.add(saleReturnResponse);
				ArrayList<String> names = new ArrayList<String>();
				names.add(OperationsBean.OPERATION);
				names.add(FiscalDataBean.FISCAL_DATA);
				names.add(CardBean.CREDIT_CARD);
				names.add(LoginBean.LOGIN);
				names.add(CustomerBean.FOLIO_PREAPERTURA);
				names.add(SaleBean.AFILIACION);
				names.add(SaleScreen.RETURN_RESPONSE);
				intent.putExtra(LocationThread.LOCATION, location);
				Logger.log(Logger.MESSAGE, FIRM_SCREEN, "Devolucion: " + params.getBoolean(OperationsDetailScreen.REFUND, false));
				if (params.getBoolean(OperationsDetailScreen.REFUND, false))
					goForwardForResult(DetailSaleRefundScreen.class, getBaseContext(), paramsValues, names, SaleScreen.SETTINGS_CODE);
				else
					goForwardForResult(DetailSaleReturnScreen.class, getBaseContext(), paramsValues, names, SaleScreen.SETTINGS_CODE);
			}
		}
	}

}
