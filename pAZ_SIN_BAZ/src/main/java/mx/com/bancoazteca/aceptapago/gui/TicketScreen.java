package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;

public class TicketScreen extends BaseActivity{
private static final String NAME_TICKET = "ticketReenvio";
public static final String TICKET = "ticket";
private static final int LOADING_TICKET = 12;
private static final int POPUP_MEMORY = 10;
private static final String TICKET_SCREEN = "TicketScreen";
private ImageView ticket;
private String url;
private static File ticketFile;
private String rfc;
private String folioPreap;
private OperationsBean operation;
	private Toolbar mtoolbar;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.ticket_image);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		ticket=(ImageView) findViewById(R.id.ticket);	
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params!= null){
			url=(String) params.getSerializable("urlTicket");
			rfc= (String) params.getSerializable(FiscalDataBean.BUSINESS_RFC);
			folioPreap=(String)params.getSerializable(CustomerBean.FOLIO_PREAPERTURA);
			operation=(OperationsBean) params.getSerializable(OperationsBean.OPERATION);
			showDialog(LOADING_TICKET);
			Logger.log(Logger.MESSAGE, TICKET_SCREEN, url);
			new TicketLoader().execute(url);	
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_forward_ticket,menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id=item.getItemId();
		switch (id){
			case R.id.btn_menu_forward_ticket:
				send();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if(ticketFile != null )
			deleteTicket(ticketFile.getAbsolutePath());
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case LOADING_TICKET:
			popUp=new PopUp(this,R.string.popUpMessage,R.string.loadingTicket);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		default:
			return null;
		}
	}
	private void  deleteTicket(String path){
		File f= new File(path);
		if(f != null && f.exists())
			f.delete();
		int index=path.indexOf("pdf");
		if(index>=0){
			path=path.replace("pdf", "jpg");
			f= new File(path);
			if(f != null && f.exists())
				f.delete();
		}
		else if((index=path.indexOf("jpg"))>=0){
			path=path.replace("jpg", "pdf");
			f= new File(path);
			if(f != null && f.exists())
				f.delete();
		}
		
	}
	public void send(){

		List<Serializable> params= new ArrayList<Serializable>();
		params.add(rfc);
		params.add(folioPreap);
		params.add(operation);
		List<String> paramsName= new ArrayList<String>();
		paramsName.add(CustomerBean.RFC);
		paramsName.add(CustomerBean.FOLIO_PREAPERTURA);
		paramsName.add(OperationsBean.OPERATION);
		intent.putExtra(TICKET, ticketFile);
		goForwardForResult(EmalConfirmationScreen.class, getBaseContext(), params, paramsName, SaleScreen.SETTINGS_CODE);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		
		case SaleScreen.SETTINGS_CODE:
			if(resultCode==RESULT_OK){
				if(ticketFile != null )
					deleteTicket(ticketFile.getAbsolutePath());
				setResult(RESULT_FIRST_USER);
				finish();
			}
			else if (resultCode==SaleScreen.HOME_RESULT){
				setResult(SaleScreen.HOME_RESULT);
				finish();
			}
			break;
		default:
			break;
		}
	}
	private class TicketLoader extends AsyncTask<String, Integer, Bitmap>{
		
		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				return Utility.loadBitmap(params[0],(int) (getWidth()*0.60), (int) (getHeight()*0.70),1);
			} catch (OutOfMemoryError e) {
				// TODO: handle exception
				return null;
			}
			
		}
		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Logger.log(Logger.MESSAGE, TICKET_SCREEN, Thread.currentThread().getName());
			Logger.log(Logger.MESSAGE, TICKET_SCREEN, "onPostExecute");
			removeDialog(LOADING_TICKET);
			if(result!=null){
				ticket.setImageBitmap(result);
				ticketFile=FileManager.saveBitmap(result,NAME_TICKET,100,FileManager.JPEG);
			}
			else{
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.outOfMemory));
				dialog.show(getSupportFragmentManager(),""+POPUP_MEMORY);
			}
			
		}
	}
}

