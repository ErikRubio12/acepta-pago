package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;

import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.aceptapago.jobs.RegisterBusinessJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.ui.customviews.SignView;
import mx.com.bancoazteca.util.Logger;

public class ContractSignatureScreen extends BaseActivity implements MessageListener<Message, String> {
	private static final String CONTRACT_SIGNATURE_SCREEN = "Contract Signature";
	private SignView firmArea;
	private RegistrationBean register;
	private String tmpMsg;
	private static final int POPUP_EMPTY_FIRM = 14;
	private static final int POPUP_REGISTER_BUSINESS = 15;
	private static final int POPUP_ERROR = 17;
	protected static final int POPUP_EXCEPTION = 0;
	private Toolbar mtoolbar;

	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.contract_sign);
		firmArea= (SignView) findViewById(R.id.firm);
		firmArea.setDrawingCacheEnabled(true);
		firmArea.setDrawingCacheQuality(2);
		LayoutParams params=firmArea.getLayoutParams();
		params.height= (int) (getHeight()*0.50);
		firmArea.setLayoutParams(params);
		firmArea.setOnTouchListener(touchListener);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();					
		register=(RegistrationBean)params.getSerializable(RegistrationBean.REGISTER);
		Logger.log(Logger.MESSAGE, CONTRACT_SIGNATURE_SCREEN, "viene del contrato: " + register.getCustomer());
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("msg", tmpMsg);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		tmpMsg=savedInstanceState.getString("msg");
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case RegistrationMenu.CONTRACT_CODE:
			if(resultCode==RESULT_OK){
				setResult(RESULT_OK);
				finish();
			}
				
			break;
		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton button=null;
		DialogButton acceptButton=null;
		switch (id) {
		case POPUP_REGISTER_BUSINESS:
			popUp= new PopUp(this, R.string.popUpMessage,R.string.sendingNewCustomerInfo );
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		default:
			return null;
		}
	}
	public void makeRegistration(View v){

		if (firmArea.isEmpty()) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.signCantBeEmpty));
			dialog.show(getSupportFragmentManager(),""+POPUP_EMPTY_FIRM);
		}else{
			showDialog(POPUP_REGISTER_BUSINESS);
			backgroundThread= new Thread(new RegisterBusinessJob(this,register,this));
			backgroundThread.start();
		}

	}
	public void clean(View v){
		firmArea.cleanScreen();
		firmArea.invalidate();
	}
	private OnTouchListener touchListener=new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub

			float x = event.getX();
			float y = event.getY();

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				firmArea.moveDown(x, y);
				firmArea.invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				firmArea.drag(x, y);
				firmArea.invalidate();
				break;
			case MotionEvent.ACTION_UP:
				firmArea.moveUp();
				firmArea.invalidate();
				break;
			}
			return true;
		}
	};
	@Override
	public void sendMessage(final Context ctx, final Message message, final String parameter) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (isFinishing()) 
					return;
				if(message.getType()==Message.OK){
					Logger.log(Logger.MESSAGE, CONTRACT_SIGNATURE_SCREEN, "Servicio terminado el folio es: "+ parameter);
					register.getCustomer().setFolioPreap(parameter);
					this_.removeDialog(POPUP_REGISTER_BUSINESS);
					intent.putExtra(CustomerBean.FOLIO_PREAPERTURA, register.getCustomer().getFolioPreap());
					goForwardForResult(RegisterSucced.class, ctx,RegistrationMenu.CONTRACT_CODE);
				}
				if(message.getType()== RegisterBusinessJob.ERROR_SENDING_DATA){
					this_.removeDialog(POPUP_REGISTER_BUSINESS);
					tmpMsg=parameter;
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),tmpMsg);
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							setResult(RESULT_OK);
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUP_ERROR);
				}
			}
		});
	}
	@Override
	public void sendStatus(Context ctx,final  int status) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			public void run() {
				if (isFinishing()) 
					return;
				if(status==RegisterBusinessJob.EXCEPTION){
					this_.removeDialog(POPUP_REGISTER_BUSINESS);
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							setResult(RESULT_OK);
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_EXCEPTION);
				}
				
			}
		});
	}
	@Override
	public void sendMessage(Context ctx, Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
}