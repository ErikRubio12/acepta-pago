package mx.com.bancoazteca.aceptapago.db.daos;

import java.util.ArrayList;
import java.util.List;

import mx.com.bancoazteca.aceptapago.beans.SaleReturnTicketBean;
import mx.com.bancoazteca.aceptapago.db.Query;
import mx.com.bancoazteca.util.Logger;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class TicketsDao extends Dao{
	private final static String TICKETS_TABLE="SalesReturnTicket";
	private static final String DAO_TICKETS = "DaoTickets";
	public TicketsDao(Context context) {
		super(context);
	}
	public List<SaleReturnTicketBean> querySalesReturnTickets(String user ) {
		// TODO Auto-generated method stub
		ArrayList<SaleReturnTicketBean> pendingTickets= new ArrayList<SaleReturnTicketBean>();
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_SALE_RETURN_TICKETS, new String[]{String.valueOf(user)});
		SaleReturnTicketBean currentTicket=null;
		if(cursor.moveToFirst()){
			do {
				currentTicket= new SaleReturnTicketBean();
				currentTicket.setTicketPath(cursor.getString(0).trim());
				currentTicket.setMaskedPAN(cursor.getString(1).trim());
				currentTicket.setReference(cursor.getString(2).trim());
				currentTicket.setAuthorizationNumber(cursor.getString(3).trim());
				currentTicket.setAffiliate(cursor.getString(4).trim());
				currentTicket.setIdoc(cursor.getString(5).trim());
				currentTicket.setRfc(cursor.getString(6).trim());
				currentTicket.setFolio(cursor.getString(7).trim());
				currentTicket.setUser(user);
				pendingTickets.add(currentTicket);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return pendingTickets;
	}
	public SaleReturnTicketBean querySaleReturnTicket(SaleReturnTicketBean bean) {
		// TODO Auto-generated method stub
		SaleReturnTicketBean pendingTicket= null;
		Cursor cursor=db.getReadableDatabase().rawQuery(Query.FIND_SALE_RETURN_TICKET, new String[]{String.valueOf(bean.getTicketPath()),String.valueOf(bean.getMaskedPAN()),String.valueOf(bean.getReference()),String.valueOf(bean.getAuthorizationNumber()),String.valueOf(bean.getAffiliate()),String.valueOf(bean.getIdoc()),String.valueOf(bean.getRfc()),String.valueOf(bean.getFolio()),String.valueOf(bean.getUser())});
		if(cursor.moveToFirst()){
			Logger.log(Logger.MESSAGE, DAO_TICKETS,"********* encontro el ticket");
			pendingTicket= new SaleReturnTicketBean();
			pendingTicket.setTicketPath(cursor.getString(0).trim());
			pendingTicket.setMaskedPAN(cursor.getString(1).trim());
			pendingTicket.setReference(cursor.getString(2).trim());
			pendingTicket.setAuthorizationNumber(cursor.getString(3).trim());
			pendingTicket.setAffiliate(cursor.getString(4).trim());
			pendingTicket.setIdoc(cursor.getString(5).trim());
			pendingTicket.setRfc(cursor.getString(6).trim());
			pendingTicket.setFolio(cursor.getString(7).trim());
			pendingTicket.setUser(bean.getUser());
		}
		cursor.close();
		db.close();
		return pendingTicket;
	}
	public long insertSaleReturnTicket(SaleReturnTicketBean ticket) {
		long res=-1;
		ContentValues values= new ContentValues(9);
		values.put("ticketPath", ticket.getTicketPath());
		values.put("maskedPan", ticket.getMaskedPAN());
		values.put("reference", ticket.getReference());
		values.put("authorizationNumber", ticket.getAuthorizationNumber());
		values.put("afiliacion", ticket.getAffiliate());
		values.put("idDoc", ticket.getIdoc());
		values.put("rfc", ticket.getRfc());
		values.put("folio", ticket.getFolio());
		values.put("user", ticket.getUser());
		res=db.getReadableDatabase().insert(TICKETS_TABLE, null, values);
		db.close();
		
		return res;
	}
	public long deleteSaleReturnTicket(SaleReturnTicketBean ticket) {
		long res=-1;
		String values[]= new String[9];
		values[0]= ticket.getTicketPath();
		values[1]= ticket.getMaskedPAN();
		values[2]= ticket.getReference();
		values[3]= ticket.getAuthorizationNumber();
		values[4]= ticket.getAffiliate();
		values[5]= ticket.getIdoc();
		values[6]= ticket.getRfc();
		values[7]= ticket.getFolio();
		values[8]= ticket.getUser();
		
		res=db.getReadableDatabase().delete(TICKETS_TABLE, "ticketPath=? AND maskedPan=? AND reference=? AND authorizationNumber=? AND afiliacion=? AND idDoc=? AND rfc=? AND folio=? AND user=?", values);
		db.close();
		
		return res;
	}
}
