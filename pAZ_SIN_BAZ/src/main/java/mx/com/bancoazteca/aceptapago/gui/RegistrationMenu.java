package mx.com.bancoazteca.aceptapago.gui;


import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.Serializable;
import java.util.ArrayList;
import mx.com.bancoazteca.beans.AddressBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.AdditionalInfo;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.ExtraInfoBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.util.Logger;



public class RegistrationMenu extends BaseActivity {
	private static final int NEW_CUSTOMER_CODE = 0;
	private static final int EXTRA_INFO_SCREEN_CODE = 11;
	public static final int CONTRACT_CODE = 12;
	private static final int MAP_CODE = 14;


	private TextView userData;
	private TextView businessData;
	//	private TextView contractData;
	public RegistrationBean register= new RegistrationBean();
	private boolean userDataComplete=false;
	private boolean businessDataComplete=false;
	private boolean contractDataComplete=false;
	private CustomerBean customerIncomplete;
	private Toolbar mtoolbar;


	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.registration_menu);
		userData= (Button) findViewById(R.id.userData);
		businessData=(Button) findViewById(R.id.businessData);
		businessData=(Button) findViewById(R.id.businessData);
//		contractData=(Button) findViewById(R.id.contractData);
		businessData.setEnabled(false);
//		contractData.setEnabled(false);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}



	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if (params != null) {
			customerIncomplete= (CustomerBean) params.getSerializable("customerIncomplete");
			if (customerIncomplete != null) {
				userData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.n_icono_registro_usuario, 0, R.drawable.tick, 0);
				userData.setEnabled(false);
				userDataComplete=true;
				businessData.setEnabled(true);
				businessData.requestFocus();
				register.setCustomer(customerIncomplete);
			}

		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(data!=null)
			params=data.getExtras();
		//Logger.log("result: "+ resultCode + "request: "+ requestCode);
		switch (requestCode) {
			case NEW_CUSTOMER_CODE:
				if(resultCode==RESULT_OK){
					if(params!= null){
						userData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.n_icono_registro_usuario, 0, R.drawable.tick, 0);
						userData.setEnabled(false);
						userDataComplete=true;
						businessData.setEnabled(true);
						businessData.requestFocus();
						register.setCustomer((CustomerBean) params.getSerializable(CustomerBean.CUSTOMER));

						//Log.ii("TAG_REgistration_menu","New_customer_code");



					}
				}
				break;

			case EXTRA_INFO_SCREEN_CODE:
				if(resultCode==RESULT_OK){
					if(params!=null){
						businessDataComplete=true;
//						businessData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.n_icono_registro_datos, 0, R.drawable.tick, 0);
						userData.setEnabled(false);
//					contractData.setEnabled(true);
//					contractData.requestFocus();
						setResult(RESULT_OK);
						finish();



					}
				}
				break;

//			case CONTRACT_CODE:
//				//Log.ii("TAG_REgistration_menu", "contract_code");
//				if(resultCode==RESULT_OK){
//					businessData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.n_icono_registro_datos, 0, R.drawable.tick, 0);
////				contractDataComplete=true;
////				contractData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.n_icono_registro_contrato, 0, R.drawable.tick, 0);
////				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////				goForward(VideoScreen.class,this);
//					setResult(RESULT_OK);
//					finish();
//				}
//				break;
//
//			case CONTRACT_CODE:
//
//				break;

			default:
				break;
		}
	}





    //************* ACTION LISTENERS  ***********************


	public void pressEvent(View viewDispatchedEvent){
		if(viewDispatchedEvent.getId()==R.id.userData){
			if(register.getCustomer()!=null){
				intent.putExtra(CustomerBean.CUSTOMER,register.getCustomer());
				intent.putExtra(LoginBean.LOGIN, register.getCustomer().getLogin());
			}
			goForwardForResult(NewCustomerScreen.class,this, NEW_CUSTOMER_CODE);
		}
		else if(viewDispatchedEvent.getId()==R.id.businessData){
			if(register.getFiscalData()!=null){
				intent.putExtra(FiscalDataBean.FISCAL_DATA,register.getFiscalData());
				intent.putExtra(ExtraInfoBean.EXTRA_INFO,register.getCustomer().getExtraInfo());
			}
			intent.putExtra(AddressBean.ADDRESS,register.getCustomer().getAddress());
			intent.putExtra(CustomerBean.RFC,register.getCustomer().getRfc());
			intent.putExtra(RegistrationBean.REGISTER,register);
			goForwardForResult(ExtraInfoScreen.class,this,EXTRA_INFO_SCREEN_CODE);
		}
//		else if(viewDispatchedEvent.getId()==R.id.muestraMapa){
//			goForwardForResult(MapTicketScreen.class, this, MAP_CODE);
//		}
	}



}
