package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;

import mx.com.bancoazteca.aceptapago.beans.GenerateKeyBean;
import mx.com.bancoazteca.aceptapago.beans.GenerateRequest;
import mx.com.bancoazteca.aceptapago.beans.LoginRequest;
import mx.com.bancoazteca.aceptapago.beans.LoginServiceBean;
import mx.com.bancoazteca.aceptapago.jobs.SendPrivacityKeyJob;
import mx.com.bancoazteca.aceptapago.ws.APIClient;
import mx.com.bancoazteca.aceptapago.ws.Constants;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.RegistrationBean;
import mx.com.bancoazteca.aceptapago.db.daos.CustomerDao;
import mx.com.bancoazteca.aceptapago.jobs.RegisterBusinessJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Logger;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import mx.com.bancoazteca.pagoazteca.beans.RegistrationBean;

public class ConfirmationDataScreen extends BaseActivity implements MessageListener<Message, String> {
	private static final int CONFIRM_CODE = 14;
	private Button accept;
	private CustomerBean customer;
	public final static String CONFIRMATION_SCREEN="confirmationScreen";
	private static final int POPUP_REGISTER_BUSINESS = 15;
	protected static final int POPUP_EXCEPTION = 0;
	private static final int POPUP_ERROR = 17;

	private TextView name;
	private TextView lastName;
	private TextView lastName2;
	private TextView gender;
	private TextView maritalStatus;
	private TextView bornDate;
	private TextView citizenship;
	private TextView rfc;
	private TextView curp;
	private TextView id;
	private TextView idNumber;
	private TextView street;
	private TextView colony;
	private TextView state;
	private TextView externalNumber;
	private TextView internalNUmber;
	private TextView zipCode;
	private TextView city;
	private TextView lada;
	private TextView telephone;
	private TextView extention;
	private TextView mail;
	private CustomerDao dao;
	private Toolbar mtoolbar;
	private RegistrationBean register;
	private String tmpMsg;


	@Override
	protected void loadUiComponents() {

		// TODO Auto-generated method stub
		setContentView(R.layout.confirmation);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		name=(TextView) findViewById(R.id.nameTextView);
		lastName=(TextView) findViewById(R.id.lastNameTextView);
		lastName2=(TextView) findViewById(R.id.lastName2TextView);
		gender=(TextView) findViewById(R.id.gendersTextView);
		maritalStatus=(TextView) findViewById(R.id.maritalStatusTextView);
		bornDate=(TextView) findViewById(R.id.bornDateText);
		citizenship=(TextView) findViewById(R.id.citizenshipTextView);
		rfc=(TextView) findViewById(R.id.rfcTextView);
		curp=(TextView) findViewById(R.id.curpTextView);
		id=(TextView) findViewById(R.id.idTextView);
		idNumber=(TextView) findViewById(R.id.idNumberTextView);
		street=(TextView) findViewById(R.id.streetTextView);
		externalNumber=(TextView) findViewById(R.id.externalNumberTextView);
		internalNUmber=(TextView) findViewById(R.id.internalNumberTextView);
		colony=(TextView) findViewById(R.id.colonyTextView);
		zipCode=(TextView) findViewById(R.id.zipCodeTextView);
		state=(TextView) findViewById(R.id.stateTextView);
		city=(TextView) findViewById(R.id.cityTextView);
		lada=(TextView) findViewById(R.id.ladaTextView);
		telephone=(TextView) findViewById(R.id.teleponeTextView);
		extention=(TextView) findViewById(R.id.extentionTextView);
		mail=(TextView) findViewById(R.id.mailTextView);
		accept=(Button) findViewById(R.id.acceptButton);
//		accept.setOnClickListener(acceptListener);

	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
		if(params != null){
			dao= new CustomerDao(this);
			customer=(CustomerBean)params.getSerializable(CustomerBean.CUSTOMER);
			register = (RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
			name.setText(customer.getName());
			lastName.setText(customer.getLastName());
			lastName2.setText(customer.getLasttName2());
			gender.setText(customer.getGender()==1 ? getResources().getString(R.string.female) : getResources().getString(R.string.male));
			maritalStatus.setText(dao.queryMaritalStatus(customer.getMaritalStatus()));
			SimpleDateFormat formatter= new SimpleDateFormat("dd/MM/yyyy");
			bornDate.setText(formatter.format(customer.getBornDate()));
			citizenship.setText(dao.queryCitizenship(customer.getCitizenship()));
			rfc.setText(customer.getRfc());
			curp.setText(customer.getCurp());
			id.setText(dao.queryIds(customer.getCodIdent()));
			idNumber.setText(customer.getId());
			street.setText(customer.getAddress().getStreet());
			externalNumber.setText(customer.getAddress().getExternalNumber());
			internalNUmber.setText(customer.getAddress().getInternalNumber());
			colony.setText(customer.getAddress().getColony());
			zipCode.setText(customer.getAddress().getZipCode());
			state.setText(customer.getAddress().getState());
			city.setText(customer.getAddress().getPoblacion());
			lada.setText(customer.getAddress().getLada());
			telephone.setText(customer.getAddress().getTelephone());
			extention.setText(customer.getAddress().getExtention());
			mail.setText(customer.getMail());
		}
	}
//	private OnClickListener acceptListener=new OnClickListener() {
//
//		@Override
//		public void onClick(View v) {
//			// TODO Auto-generated method stub
//			makeRegistration();
//
//		}
//	};

	public void makeRegistration(View v){
		showDialog(POPUP_REGISTER_BUSINESS);
		backgroundThread= new Thread(new RegisterBusinessJob(this,register,this));
		backgroundThread.start();
	}

	@Override
	public void sendMessage(final Context ctx, final Message msg, final String parameter) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (isFinishing())
					return;
				//SI AQUI ESTABAAAAA ACUERDATE
				if(msg.getType()==Message.OK){
					//Logger.log(Logger.MESSAGE, CONFIRMATION_SCREEN, "Servicio terminado el folio es: "+ parameter);
					register.getCustomer().setFolioPreap(parameter);
					this_.removeDialog(POPUP_REGISTER_BUSINESS);
					intent.putExtra(CustomerBean.FOLIO_PREAPERTURA, register.getCustomer().getFolioPreap());

					sendKey();

				}
				if(msg.getType()==Message.OKKey){
					intent.putExtra(RegistrationBean.REGISTER,register);
					goForwardForResult(RegisterSucced.class, ctx,CONFIRM_CODE);
//					sendKey();

				}
//				if(msg.getType()==Message.OKKey){
//					Logger.log(Logger.MESSAGE, CONFIRMATION_SCREEN, "Llave mandada, la llave es: "+ register.getKeyRegister());
//					Logger.log(Logger.MESSAGE, CONFIRMATION_SCREEN, "El mensaje que regresa Jason es: "+ parameter);
//
//				}

				if(msg.getType()== RegisterBusinessJob.ERROR_SENDING_DATA){
					this_.removeDialog(POPUP_REGISTER_BUSINESS);
					tmpMsg=parameter;
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),tmpMsg);
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
//							setResult(RESULT_OK);
							dialog.dismiss();
//							finish();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUP_ERROR);
				}
			}
		});
	}

	private void sendKey() {
		showDialog(POPUP_REGISTER_BUSINESS);
		backgroundThread= new Thread(new SendPrivacityKeyJob(this,register,this));
		backgroundThread.start();
	}




	@Override
	public void sendStatus(Context ctx, final int status) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (isFinishing())
					return;
				if(status==RegisterBusinessJob.EXCEPTION){
					this_.removeDialog(POPUP_REGISTER_BUSINESS);
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.connectionFails));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
//							setResult(RESULT_OK);
							dialog.dismiss();
//							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_EXCEPTION);
				}

			}
		});
	}

	@Override
	public void sendMessage(Context ctx, Message message, Object... parameters) {

	}




	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton button=null;
		DialogButton acceptButton=null;
		switch (id) {
			case POPUP_REGISTER_BUSINESS:
				popUp= new PopUp(this, R.string.popUpMessage,R.string.sendingNewCustomerInfo );
				popUp.setCancelable(false);
				popUp.setType(PopUp.PROGRESS_INFINIT);
				popUp.create();
				return popUp.getDialog();
			default:
				return null;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(data!=null)
			params=data.getExtras();
		//Logger.log("result: "+ resultCode + "request: "+ requestCode);
		switch (requestCode) {
			case CONFIRM_CODE:
				if(resultCode==RESULT_OK){
					RegistrationBean registerBean=(RegistrationBean) params.getSerializable(RegistrationBean.REGISTER);
					intent.putExtra(RegistrationBean.REGISTER,registerBean);
					setResult(RESULT_OK,intent);
					finish();
				}

				break;
		}
	}


}
