package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import mx.com.bancoazteca.aceptapago.gps.SingleShotLocationProvider;
import mx.com.bancoazteca.beans.CardBean;
import mx.com.bancoazteca.beans.FiscalDataBean;
import mx.com.bancoazteca.beans.LoginBean;
import mx.com.bancoazteca.data.files.FileManager;
import mx.com.bancoazteca.hw.LocationService;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.messages.EmailBean;
import mx.com.bancoazteca.messages.Mail;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.beans.ClientBean;
import mx.com.bancoazteca.aceptapago.beans.CustomerBean;
import mx.com.bancoazteca.aceptapago.beans.SaleBean;
import mx.com.bancoazteca.aceptapago.beans.SaleReturnResponse;
import mx.com.bancoazteca.aceptapago.db.daos.CardDao;
import mx.com.bancoazteca.aceptapago.jobs.ClientsJob;
import mx.com.bancoazteca.aceptapago.jobs.UploadImageTicketJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.ui.customviews.CustomImageView;
import mx.com.bancoazteca.ui.customviews.LineSignView;
import mx.com.bancoazteca.util.GenericException;
import mx.com.bancoazteca.util.Logger;
import mx.com.bancoazteca.util.Utility;


public class DetailSaleReturnScreen extends BaseActivity implements MessageListener<Message, String>,OnMapReadyCallback {
	private Location location;
	private boolean isTicketSaved=false;
	private static final long TIME_TO_LOAD = 1000*7;
	private static final int POPUP_IMAGE_NOT_LOADED = 11;
	private static final int POPUP_SIGN_NOT_LOADED = 12;
	private static final int POPUP_TICKET_NOT_LOADED = 13;
	private static final int POPUP_TICKET_NOT_LOADED2 = 14;
	private static final int POPUP_MAP = 15;
	private static final int POPUP_EMPTY_FIELDS=16;
	private final static int POPUP_ERROR=17;
	public static final String FILE_PATH = "ticket";
	private static final String DETAIL_SALE_RETURN_SCREEN = "DetailSaleReturnScreen";
	private LinearLayout container;
	private static File ticketFile;
	protected static final String NAME_TICKET = "ticketCancelacion";
	private String afiliacion;
	private CardBean card;
	private TextView amount;
	private LinearLayout saleDetailLayout,signLayout;
	private TextView cardNumber;
	private TextView  ticketRegisterNumber;
	private TextView  lugar;
	private TextView validityCard;
	private TextView typeCard;
	private LineSignView line;
	private TextView folioTicket;
	private TextView reference;
	private TextView ticketCardText;
	private TextView ticketBankText;
	private ImageView productImage;
	private TextView ticketFolioPreap;
	private TextView folio;
	private TextView ticketBusinessText;
	private CardDao dao;
	private CustomImageView firmImage;
	private TextView ticketDate;
	private LoginBean login;
	private String folioPreap;
	private FiscalDataBean fiscalData;
	private String signPath;
	private static final int POPUP_PROCESSING = 0;
	private static final int ZOOM_LEVEL = 19;
	private static boolean isTicketOk=true;
	private Thread backgroundThread;
	private SaleReturnResponse saleReturnResponse;
	private ClientBean clientSelected;
	private  EditText businessClientName;
	private  EditText businessClientEmail;
	private EmailBean email= new EmailBean();
	private TextView businessName;
	private TextView businessFolio;
	private TextView businessDate;
	private TextView businessTime;
	private TextView businessPrice;
	private TextView businessCard;
	private CustomImageView businessFirm;
	private PopupWindow ticketScreen;
	private LinearLayout aidContent;
	private LinearLayout arqcContent;
	private TextView aid;
	private TextView arqc;
	private TextView txtTipoVenta;
	private Button shareButton;
	private LinearLayout layoutDescription;
	private TextView txtSignTitle;
	private Toolbar mtoolbar;

//	private ImageView imageView;
//
//
//	private GoogleMap googleMap;

	private ImageView mapImage;

	private LatLng KYIV;
	private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
	private MapView mMapView;
	private int mMapWidth = 600;
	private int mMapHeight = 400;
	private GoogleMapOptions options;
	private Bundle mapViewBundle;


	@Override
	protected void onCreate(Bundle icicle) {
		// TODO Auto-generated method stub
		super.onCreate(icicle);
		this_=this;

		mapImage = (ImageView) findViewById(R.id.mapImage);
		mapViewBundle = null;
		if (icicle != null) {
			mapViewBundle = icicle.getBundle(MAP_VIEW_BUNDLE_KEY);
		}
		popupTitle=R.string.popUpMessage;
//		popupMsg= R.string.wait;
//		showDialog(POPUP_MESSAGES_PROGRESS);
		SingleShotLocationProvider.requestSingleUpdate(this,
				new SingleShotLocationProvider.LocationCallback() {
					@Override
					public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
						Log.d("Location", "my location is " + location.latitude + location.longitude);
						KYIV = new LatLng(location.latitude,location.longitude);

						options = new GoogleMapOptions()
								.compassEnabled(false)
								.mapToolbarEnabled(false)
								.camera(CameraPosition.fromLatLngZoom(KYIV,15))
								.liteMode(true);
						mMapView = new MapView(DetailSaleReturnScreen.this, options);
						mMapView.onCreate(mapViewBundle);

						mMapView.getMapAsync(new OnMapReadyCallback() {
							@Override
							public void onMapReady(GoogleMap googleMap) {
								googleMap.addMarker(new MarkerOptions()
										.position(KYIV)
										.title("Kyiv"));
								mMapView.measure(View.MeasureSpec.makeMeasureSpec(mMapWidth, View.MeasureSpec.EXACTLY),
										View.MeasureSpec.makeMeasureSpec(mMapHeight, View.MeasureSpec.EXACTLY));
								mMapView.layout(0, 0, mMapWidth, mMapHeight);

								googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
									@Override
									public void onMapLoaded() {
										mMapView.setDrawingCacheEnabled(true);
										mMapView.measure(View.MeasureSpec.makeMeasureSpec(mMapWidth, View.MeasureSpec.EXACTLY),
												View.MeasureSpec.makeMeasureSpec(mMapHeight, View.MeasureSpec.EXACTLY));
										mMapView.layout(0, 0, mMapWidth, mMapHeight);
										mMapView.buildDrawingCache(true);
										Bitmap b = Bitmap.createBitmap(mMapView.getDrawingCache());
										mMapView.setDrawingCacheEnabled(false);
										mapImage.setImageBitmap(b);
//										removeDialog(POPUP_MESSAGES_PROGRESS);
									}



								});



							}
						});
					}
				});
//		this.loadUiComponents(icicle);
	}
	public void home(View v){
		showDialog(POPUP_PROCESSING);
		new Wait().execute(Integer.valueOf(v.getId()));
	}
	protected void handleBackToFront(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		loadUiComponents();
		processData();
	}
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		setContentView(R.layout.detail_sale);
		mtoolbar=(Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		ActionBar actionBar=getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDefaultDisplayHomeAsUpEnabled(false);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		loadNewTicketComponents();
		shareButton=(Button) findViewById(R.id.shareButton);
		shareButton.setVisibility(View.GONE);
		txtSignTitle =(TextView)findViewById(R.id.signTitle);
		container =(LinearLayout) findViewById(R.id.detailLayout);
		LayoutParams params = container.getLayoutParams();
		params.height=getHeight();
		params.width=getWidth();
		container.setLayoutParams(params);
		productImage= (ImageView) container.findViewById(R.id.productImage);
		firmImage=(CustomImageView) container.findViewById(R.id.firmImage);
		saleDetailLayout=(LinearLayout) container.findViewById(R.id.saleDetailLayout);
		line=(LineSignView) container.findViewById(R.id.lineFirmView);
		signLayout=(LinearLayout) container.findViewById(R.id.signLayout);
		amount=(TextView) container.findViewById(R.id.amountTicketTextView);
		folio=(TextView) container.findViewById(R.id.ticketFolio);
		reference=(TextView) container.findViewById(R.id.referenceTextView);
		folioTicket=(TextView) container.findViewById(R.id.folioTicketTextView);
		cardNumber=(TextView) container.findViewById(R.id.cardNumberTextView);
		ticketDate=(TextView) container.findViewById(R.id.ticketDate);
		ticketRegisterNumber=(TextView) container.findViewById(R.id.ticketRegisterNumberId);
		lugar = (TextView) container.findViewById(R.id.location);
		ticketBusinessText=(TextView) container.findViewById(R.id.ticketBusinessText);
		ticketBankText=(TextView) container.findViewById(R.id.ticketBankTextId);
		ticketCardText=(TextView) container.findViewById(R.id.ticketCardTextId);
		txtTipoVenta=(TextView)container.findViewById(R.id.txt_tipo_venta);
		validityCard=(TextView) container.findViewById(R.id.validityCard);
		typeCard =(TextView) container.findViewById(R.id.typeCard);
		ticketFolioPreap=(TextView) container.findViewById(R.id.ticketFolioPreap);
		layoutDescription=(LinearLayout)container.findViewById(R.id.layoutDescription);
		layoutDescription.setVisibility(View.GONE);
		aidContent=(LinearLayout) container.findViewById(R.id.aidContent);
		arqcContent=(LinearLayout) container.findViewById(R.id.arqcContent);

		aid=(TextView) container.findViewById(R.id.aid);
		arqc=(TextView) container.findViewById(R.id.arqc);


		params=productImage.getLayoutParams();
		params.height=(int) (getHeight()*0.23);
		params.width=getWidth()/2;
		productImage.setLayoutParams(params);


		params=saleDetailLayout.getLayoutParams();
		params.width=getWidth()/2;
		params.height=(int) (getHeight()*0.30);
		saleDetailLayout.setLayoutParams(params);

		params=signLayout.getLayoutParams();
		params.width=getWidth()/2;
		params.height=(int) (getHeight()*0.30);
		signLayout.setLayoutParams(params);


		//EGR(New Map Snapshot)

		//EGR (new Map Snapshot)




		//EGR(Old Map Snapshot)

//		imageView = (ImageView) container.findViewById(R.id.mapImage);
//
//		location = LocationService.getMostRecentLocation();
//		Intent service = new Intent(this, LocationService.class);
//		stopService(service);
//
//		if (googleMap == null) {
//			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
//		}
//
//		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//
//
//		LatLng coordinate = new LatLng(location.getLatitude(), location.getLongitude());
//		CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 11);
//		googleMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));
//		googleMap.animateCamera(yourLocation);
//
//		googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
//			@Override
//			public void onMapLoaded() {
//				googleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
//					@Override
//					public void onSnapshotReady(Bitmap bitmap) {
//						imageView.setImageBitmap(bitmap);
//					}
//				});
//			}
//		});

		Bitmap b=null;
		File f;
		f=FileManager.openFile(this,SaleBean.getImage(),FileManager.EXTERNAL,FileManager.MODE_APPEND ^ FileManager.MODE_RW);
		if(f != null && f.exists()){
			b=Utility.decodeImage(this,SaleBean.getImage(),FileManager.EXTERNAL,  getWidth()/2, (int) (getHeight()*0.25));
			if(b != null){

				Matrix m= new Matrix();
				RectF drawableRect = new RectF(0, 0, b.getWidth(), b.getHeight());
				RectF viewRect = new RectF(0, 0,getWidth()/2, (int) (getHeight()*0.25));
				m.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
				b= Bitmap.createBitmap(b, 0, 0, b.getWidth(),b.getHeight(),m, true);
				m= new Matrix();
				m.setRotate(90f);
				b= Bitmap.createBitmap(b, 0, 0, b.getWidth(),b.getHeight(),m, true);
				productImage.setImageBitmap(b);
				productImage.setBackgroundColor(Color.BLACK);
				Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN, "w3: "+ productImage.getWidth() + " h3:" + productImage.getHeight());

				b=null;
				System.gc();
			}
			else{
				showDialog(POPUP_IMAGE_NOT_LOADED);
				Logger.log(Logger.EXCEPTION, DETAIL_SALE_RETURN_SCREEN,"no se pudo cargar la imagen del producto");
			}
		}
		f=FileManager.openFile(this,"sign.png",FileManager.EXTERNAL,FileManager.MODE_APPEND);
		if (f != null && f.exists()) {
			try {
				b=BitmapFactory.decodeFile(f.getAbsolutePath());
				if(b != null){
					Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"ancho: "+ b.getWidth() + " alto: "+ b.getHeight());
					b= Bitmap.createScaledBitmap(b, (int)(b.getWidth()*0.3),(int)(b.getHeight()*0.3), false);
					System.gc();
					Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"ancho: "+ b.getWidth() + " alto: "+ b.getHeight());
					firmImage.setImageBitmap(b);
					b= Bitmap.createScaledBitmap(b, (int)(b.getWidth()*0.7),(int)(b.getHeight()*0.7), false);
					Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"ancho: "+ b.getWidth() + " alto: "+ b.getHeight());
					businessFirm.setImageBitmap(b);
					System.gc();
				}
				else{
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.signNotEnoughMemory));
					dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
					Logger.log(Logger.EXCEPTION, DETAIL_SALE_RETURN_SCREEN,"no se pudo cargar la firma del ticket");
				}
			} catch (OutOfMemoryError e) {
				// TODO: handle exception
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.signNotEnoughMemory));
				dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
				Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"OutOfMemoryError no se pudo cargar la firma del ticket: "+e);
			}
			catch (Exception e) {
				// TODO: handle exception
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.signNotEnoughMemory));
				dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
				Logger.log(Logger.EXCEPTION, DETAIL_SALE_RETURN_SCREEN,"Exception no se pudo cargar la firma del ticket: "+e);
			}

		}
		else{
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.signNotEnoughMemory));
			dialog.show(getSupportFragmentManager(), "" + POPUP_SIGN_NOT_LOADED);
			Logger.log(Logger.EXCEPTION, DETAIL_SALE_RETURN_SCREEN,"Exception no se pudo cargar la firma del ticket");
		}
		b=null;
		System.gc();

	}



	public void showTicket(View v){
		Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN, "showTicket");

		goForward(ImageTicketScreen.class, ticketFile, FILE_PATH);
	}

	private void loadNewTicketComponents() {
		// TODO Auto-generated method stub
		businessClientEmail=(EditText) findViewById(R.id.businessClientEmail);
		businessClientName=(EditText) findViewById(R.id.businessClientName);
		businessName=(TextView) findViewById(R.id.businessName);
		businessFolio=(TextView) findViewById(R.id.businessFolio);
		businessDate=(TextView) findViewById(R.id.businessDate);
		businessTime=(TextView) findViewById(R.id.businessTime);
		businessPrice=(TextView) findViewById(R.id.businessPrice);
		businessCard=(TextView) findViewById(R.id.businessCard);
		businessFirm=(CustomImageView) findViewById(R.id.businessFirm);
		businessFirm.setWatermark(false);

	}
	@Override
	protected void processData(){
		Bundle params= getIntent().getExtras();
		if(params!=null){
			dao= new CardDao(this);
			card=(CardBean) params.getSerializable(CardBean.CREDIT_CARD);
			fiscalData=(FiscalDataBean) params.getSerializable(FiscalDataBean.FISCAL_DATA);
//			location=params.getParcelable(LocationThread.LOCATION);
			location= LocationService.getMostRecentLocation();
			Intent service = new Intent(this, LocationService.class);
			stopService(service);

			//GoogleMap mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

			//mapFragment.getMapAsync(this);
			if (location != null) {
				if (location.getLatitude() != 0) {

				} else {
					//mapFragment.setUserVisibleHint(false);
				}
			}

			login=(LoginBean) params.getSerializable(LoginBean.LOGIN);
			folioPreap=params.getString(CustomerBean.FOLIO_PREAPERTURA);
			afiliacion=(String) params.getSerializable(SaleBean.AFILIACION);
			signPath=(String) params.getSerializable(SaleBean.SIGN);
			saleReturnResponse= (SaleReturnResponse)params.getSerializable(SaleScreen.RETURN_RESPONSE);
			if(card.getChipData()!=null){
				//Logger.log(Message.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"fue chip: " +  saleReturnResponse.getAid() + "  -  " + saleReturnResponse.getArqc());
				aidContent.setVisibility(View.VISIBLE);
				arqcContent.setVisibility(View.GONE);
				aid.setText(saleReturnResponse.getAid());
				arqc.setText(saleReturnResponse.getArqc());
			}
			Date saleReturnDate;
			if(saleReturnResponse.getDate()==null || saleReturnResponse.getDate().length()==0)
				saleReturnDate= new Date();
			else
				saleReturnDate=Utility.dateFromFormat(saleReturnResponse.getDate(), "yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat dateFormatter= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
			SimpleDateFormat hourFormatter= new SimpleDateFormat("HH:mm:ss",Locale.getDefault());
			ticketDate.setText(ticketDate.getText().toString() + dateFormatter.format(saleReturnDate) + " Hora: "+ hourFormatter.format(saleReturnDate));
			txtTipoVenta.setText(getString(R.string.ticketOperationType)+ " "+getString(R.string.operationTypeSaleReturn));
			txtSignTitle.setText(getString(R.string.operationTypeSaleReturn));


			String decimalesStr=null;
			String enterosStr=null;
			//Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"***************** MONTO REGRESADO CANCELACION = "+ saleReturnResponse.getAmountReturned());
			int index=saleReturnResponse.getAmountReturned().indexOf(".");
			if(index>=0){
				decimalesStr=saleReturnResponse.getAmountReturned().substring(index+1);
				enterosStr=saleReturnResponse.getAmountReturned().substring(0, index);
			}
			else{
				decimalesStr=saleReturnResponse.getAmountReturned().substring(saleReturnResponse.getAmountReturned().length()-2);
				enterosStr=saleReturnResponse.getAmountReturned().substring(0, saleReturnResponse.getAmountReturned().length()-2);
			}
			int entero=Integer.parseInt(enterosStr);
			String montoAut= entero+"."+ decimalesStr;


			amount.setText(montoAut);

			cardNumber.setText("*** *** *** "+ card.getMaskedPan().substring(card.getMaskedPan().length()-4));
			validityCard.setText(validityCard.getText()+ card.getStringValidity().replace("/", " - "));

			if(card.getMaskedPan().startsWith("4"))
				typeCard.setText(typeCard.getText()+ getResources().getString(R.string.visa));
			else if(card.getMaskedPan().startsWith("5"))
				typeCard.setText(typeCard.getText() +getResources().getString(R.string.masterCard));

			if(card.getCardOwner()!=null)
				line.setText(card.getCardOwner());
			ticketBusinessText.setText(fiscalData.getFiscalName());
			folio.setText(folio.getText()+saleReturnResponse.getFolio());
			folioTicket.setText(saleReturnResponse.getAuthorization());
			reference.setText(saleReturnResponse.getReference());
			ticketRegisterNumber.setText(ticketRegisterNumber.getText().toString()+ " "+ afiliacion);
			ticketFolioPreap.setText(ticketFolioPreap.getText()+ " "+ folioPreap);
			lugar.setText(fiscalData.getAddress().getState()+" "+fiscalData.getAddress().getPoblacion());
			try {
				ticketCardText.setText(ticketCardText.getText()+dao.queryCardType(card.getMaskedPan().substring(0,6)));
				ticketBankText.setText(ticketBankText.getText()+dao.queryBankCard(Long.parseLong(card.getMaskedPan().substring(0,6))));
			} catch (NumberFormatException e) {
				// TODO: handle exception
				Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"NumberFormatException error al parsear la tarjeta: " + e);
				ticketCardText.setText("Internacional");
				ticketBankText.setText("Internacional");
			}
			catch (Exception e) {
				// TODO: handle exception
				Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"Exception error al parsear la tarjeta: " + e);
				ticketCardText.setText("Internacional");
				ticketBankText.setText("Internacional");
			}

			StringBuilder address= new StringBuilder(fiscalData.getAddress().getStreet());
			address.append(" ");
			address.append(fiscalData.getAddress().getExternalNumber());
			address.append(" ");
			address.append(fiscalData.getAddress().getColony());

			firmImage.setAuthorizationNumber(saleReturnResponse.getAuthorization());

			businessName.setText(fiscalData.getFiscalName());
			businessFolio.setText(businessFolio.getText()+": "+  saleReturnResponse.getFolio());
			businessDate.setText(businessDate.getText()+": "+  Utility.formatDate(saleReturnDate, "yyyy/MM/dd"));
			businessTime.setText(businessTime.getText() + ": " + Utility.formatDate(saleReturnDate, "HH:mm:ss"));
//			businessDescription.setText(description.getText());
			businessPrice.setText("$"+ montoAut);
			businessCard.setText(cardNumber.getText());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_detail_sale, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.btn_menu_finish_sale:
				showDialog(POPUP_PROCESSING);
				new Wait().execute(Integer.valueOf(item.getItemId()));
				return true;
		}

		return super.onOptionsItemSelected(item);
	}

	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		showDialog(POPUP_MAP);
		Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"startBackgroundProcess");
		new CountDownTimer(TIME_TO_LOAD, TIME_TO_LOAD) {
			public void onTick(long millisUntilFinished) {}

			public void onFinish() {
				if (isFinishing())
					return;
				if(popUp != null)
					this_.removeDialog(POPUP_MAP);
				Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"::::::::::::::::::::::::-------------------------------------:::::::::::::::::::");
				sendTicket();
			}
		}.start();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch (id) {
		case POPUP_MAP:
			popUp= new PopUp(this, R.string.popUpMessage, R.string.downloadingMap);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.setCancelable(false);
			popUp.create();
			return popUp.getDialog();
		case POPUP_PROCESSING:
			popUp= new PopUp(this, R.string.popUpMessage, R.string.processingTicket);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.setCancelable(false);
			popUp.create();
			return popUp.getDialog();
		default:
			return null;
		}
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	private boolean sendMail(boolean isPDF){
		try {
			File pdfFile= null;
			if("".equalsIgnoreCase(businessClientEmail.getText().toString()) || isPDF || "".equalsIgnoreCase(businessClientName.getText().toString())){
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.emptyFields));
				dialog.show(getSupportFragmentManager(),""+POPUP_EMPTY_FIELDS);
				return false;
			}
			if(clientSelected==null){
				clientSelected= new ClientBean();
				clientSelected.setCompleteName(businessClientName.getText().toString());
				clientSelected.setMail(businessClientEmail.getText().toString());
			}
			//Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"clientSelected "+ clientSelected);
			email.setSubject("Banco Azteca, ticket de cancelacion");
			email.setMessage(clientSelected.getCompleteName() + " se te ha enviado tu ticket de cancelacion");
			email.addAttached(ticketFile);
			email.addDestiny(clientSelected.getMail());
			if(!isTicketOk){
				showDialog(POPUP_PROCESSING);
				new Wait().execute(0);
			}
			else
				sendEmail();
			return true;
		} catch (GenericException e) {
			// TODO Auto-generated catch block
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.mailSendError));
			dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
			return false;
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case SaleScreen.SETTINGS_CODE:
			if(resultCode==RESULT_OK){
				if( data != null && data.getExtras()!= null){
					Bundle info=data.getExtras();
					clientSelected= (ClientBean) info.getSerializable(Clients.CLIENT_SELECTED);
					Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN, "cliente seleccionado: "+ clientSelected );
					if(clientSelected != null ){
						businessClientEmail.setText(clientSelected.getMail());
						businessClientName.setText(clientSelected.getCompleteName());
					}
				}
			}
			else if(resultCode==SaleScreen.HOME_RESULT){
				setResult(RESULT_OK);
				finish();
			}
			break;

		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN, "Mapa Ready");
		BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.overlay);
		LatLng locationSale;

		if (location != null) {
			locationSale = new LatLng(location.getLatitude(), location.getLongitude());
		} else {
			locationSale = new LatLng(19.296836, -99.1900087);
		}

		googleMap.addMarker(new MarkerOptions()
				.position(locationSale))
				.setIcon(icon);
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationSale, ZOOM_LEVEL));



	}

	private void  deleteTicket(String path){
		File f= new File(path);
		if(f != null && f.exists())
			f.delete();
		int index=path.indexOf("pdf");
		if(index>=0){
			path=path.replace("pdf", "jpg");
			f= new File(path);
			if(f != null && f.exists())
				f.delete();
		}
		else if((index=path.indexOf("jpg"))>=0){
			path=path.replace("jpg", "pdf");
			f= new File(path);
			if(f != null && f.exists())
				f.delete();
		}

	}

	private void sendTicket() {
		Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"Inicia envio de correo");
		if (!isTicketSaved) {
			try {
				ticketScreen= new PopupWindow(container,(int)( getWidth()*0.9),(int) (getHeight()*0.9));
				ticketScreen.setContentView(container);
				container.invalidate();
				Bitmap bitmapTicket=Bitmap.createBitmap(getWidth(),getHeight(), Bitmap.Config.ARGB_8888);
				Canvas canvas=new Canvas(bitmapTicket);
				canvas.drawColor(Color.WHITE);
				ticketScreen.getContentView().draw(canvas);
				StringBuffer dirTicket=new StringBuffer(Utility.formatDate(new Date(), "dd-MM-yy"));
				FileManager.createDirectory(dirTicket.toString(),FileManager.EXTERNAL);
				dirTicket.append(FileManager.separator);
				dirTicket.append(NAME_TICKET);
				dirTicket.append(Utility.formatDate(new Date(), "-yyMMddHHmmss"));
				ticketFile=FileManager.saveBitmap(bitmapTicket,this,dirTicket.toString(),60,FileManager.JPEG,FileManager.EXTERNAL);

				if(ticketFile==null){
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.dontSendByMemoryOrMounted));
					dialog.show(getSupportFragmentManager(), "" + POPUP_TICKET_NOT_LOADED);
					return;
				}
				Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"El ticket se ha almacenado: "+ticketFile.getAbsolutePath());
				bitmapTicket=null;
				canvas=null;
				System.gc();
			} catch (OutOfMemoryError e) {
				// TODO: handle exception
				Logger.log(Logger.EXCEPTION, DETAIL_SALE_RETURN_SCREEN, "No se pudo enviar el ticket OutOfMemoryError " + e);
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.dontSendByOutOfMemory));
				dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
					@Override
					public void onAccept(DialogInterface dialog) {
						setResult(RESULT_OK);
						finish();
					}
				});
				dialog.show(getSupportFragmentManager(), "" + POPUP_TICKET_NOT_LOADED);
				return;
			}
			catch (Exception e) {
				// TODO: handle exception
				Logger.log(Logger.EXCEPTION, DETAIL_SALE_RETURN_SCREEN,"No se pudo enviar el ticket Exception "+e);
				SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.dontSendByOutOfMemory));
				dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
					@Override
					public void onAccept(DialogInterface dialog) {
						setResult(RESULT_OK);
						finish();
					}
				});
				dialog.show(getSupportFragmentManager(), "" + POPUP_TICKET_NOT_LOADED);
				return;
			}

		}
		else{
			ticketFile= FileManager.openFile(this,NAME_TICKET,FileManager.EXTERNAL, FileManager.MODE_RW);
			Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN,"El ticket ya estaba almacenado: "+ (ticketFile !=null ? ticketFile.getAbsolutePath() : "no hay ticket"));
		}
		isTicketSaved=true;
		backgroundThread = new Thread(new UploadImageTicketJob(this,this,saleReturnResponse.getFolio(), saleReturnResponse.getAuthorization(),login.getUser(), fiscalData.getRfc(), saleReturnResponse.getReference(), afiliacion, ticketFile,card.getMaskedPan(),"344"));
		backgroundThread.start();
	}

	public void send(View v){
		if(sendMail(false) && v.getId()==R.id.addSend)
			saveClient();
	}
	private void saveClient() {
		// TODO Auto-generated method stub
		ClientBean client= new ClientBean();
		client.setCompleteName(businessClientName.getText().toString().toUpperCase(Locale.getDefault()));
		client.setMail(businessClientEmail.getText().toString());
		new ClientsJob<String>(this,folioPreap, fiscalData.getRfc(), client, this, ClientsJob.CREATE_CLIENT).start();

	}
	public void search(View v){
		ArrayList<Serializable> params=new ArrayList<Serializable>();
		params.add(folioPreap);
		params.add(fiscalData.getRfc());
		params.add(saleReturnResponse);
		params.add(email);
		ArrayList<String> paramNames=new ArrayList<String>();
		paramNames.add(CustomerBean.FOLIO_PREAPERTURA);
		paramNames.add(CustomerBean.RFC);
		paramNames.add(SaleScreen.RETURN_RESPONSE);
		paramNames.add(EmailBean.EMAIL);

		goForwardForResult(Clients.class, this, params, paramNames, SaleScreen.SETTINGS_CODE);
	}
	private static final String MAIL_CONFIG = "mailConfig.properties";
	private Properties configProps= new Properties();

	private void sendEmail(){


		try {
			removeDialog(POPUP_PROCESSING);
			if(email.getEmailFrom() == null)
			{
				try {
					configProps.load(Utility.loadFileAsset(getApplicationContext(),MAIL_CONFIG));
					email.setEmailFrom(configProps.getProperty(Mail.USER));
				} catch (IOException e) {
					e.printStackTrace();

				}
			}
			Mail.sendByIntent(email, DetailSaleReturnScreen.this);
		} catch (GenericException e) {
			// TODO Auto-generated catch block
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.mailSendError));
			dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
		}catch (ActivityNotFoundException e) {
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpError),getString(R.string.noAppForEmail));
			dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
		}
	}

	@Override
	public void sendMessage(Context ctx, Message message, String parameter) {
		// TODO Auto-generated method stub

	}
	@Override
	public void sendStatus(final Context ctx,final int status) {
		// TODO Auto-generated method stub
		Logger.log(Logger.MESSAGE, DETAIL_SALE_RETURN_SCREEN, "Contesto el servicio de digitalizacion");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(isFinishing())
					return;
				if(status==Message.OK)
					isTicketOk=true;
				else
					isTicketOk=false;
			}
		});
	}
	@Override
	public void sendMessage(Context ctx, Message message, Object... parameters) {
		// TODO Auto-generated method stub
	}
	private class Wait extends AsyncTask<Integer, Integer, Integer>{

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		@Override
		protected Integer doInBackground(Integer... arg0) {
			// TODO Auto-generated method stub
			while(backgroundThread!=null && backgroundThread.isAlive() && !backgroundThread.isInterrupted()){}
			return arg0[0];
		}
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if(result==0){
				sendEmail();
			}
			else{
				removeDialog(POPUP_PROCESSING);
				setResult(RESULT_OK);
				finish();
			}
		}
	}
}
