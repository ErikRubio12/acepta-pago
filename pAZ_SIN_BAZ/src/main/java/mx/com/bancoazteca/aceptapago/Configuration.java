package mx.com.bancoazteca.aceptapago;

import android.content.Context;
import android.content.SharedPreferences;

public class Configuration {

	public final static String NAME="configurationName";
	public final static String SOCIAL_NAME="socialName";
	public final static String RFC="rfc";
	public final static String ADDRESS="configurationAddress";
	public final static String AFILIACION="configurationAfiliacion";
	public final static String DEVICE_NUMBER="configurationDeviceNUmber";
	public final static String EMAIL="configurationEmail";
	public final static String BUSINESS_TYPE="configurationBusinessType";
	public final static String TAKE_SHOOT="configurationTakeShoot";
	public final static String LOCATE="configurationLocate";
	public final static String ZEBRA="configurationZebra";
	public final static String SWIPED="configurationSwiped";
	private static final String PREFS_FILE_NAME ="pagoAztecaPreferences";
	private static String name="";
	private static String socialName="";
	private static String rfc="";
	private static String address="";
	private static String deviceNumber="";
	private static String email="";
	private static String businessType="";
	private static boolean takeShoot=true;
	private static boolean locate=true;
	private static boolean swiped=true;
	
	

	public static String getName() {
		return name;
	}
	public static void setName(String newName) {
		name = newName;
	}
	public static String getSocialName() {
		return socialName;
	}
	public static void setSocialName(String newSocialName) {
		socialName = newSocialName;
	}
	public static String getRFC() {
		return rfc;
	}
	public static void setRFC(String newRFC) {
		rfc = newRFC;
	}
	public static String getAddress() {
		return address;
	}
	public static void setAddress(String newAddress) {
		address = newAddress;
	}

	public static String getDeviceNumber() {
		return deviceNumber;
	}
	public static void setDeviceNumber(String newDeviceNumber) {
		deviceNumber = newDeviceNumber;
	}
	public static String getEmail() {
		return email;
	}
	public static void setEmail(String newEmail) {
		email = newEmail;
	}

	public static boolean isTakeShoot() {
		return takeShoot;
	}
	public static void setTakeShoot(boolean newTakeShoot) {
		takeShoot = newTakeShoot;
	}
	public static boolean isLocate() {
		return locate;
	}
	public static void setLocate(boolean newLocate) {
		locate = newLocate;
	}

	public static boolean isSwiped() {
		return swiped;
	}
	public static void setSwiped(boolean swiped_) {
		swiped = swiped_;
	}
	
	public static void persistPreferences(Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(TAKE_SHOOT,takeShoot);
		editor.putBoolean(LOCATE, locate);
		editor.putBoolean(SWIPED, swiped);
		editor.putString(NAME, name);
		editor.putString(SOCIAL_NAME, socialName);
		editor.putString(RFC, rfc);
		editor.putString(ADDRESS, address);
		editor.putString(DEVICE_NUMBER, deviceNumber);
		editor.putString(EMAIL, email);
		editor.putString(BUSINESS_TYPE, businessType);
		editor.commit();

	}
	
	public static void loadPreferences(Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_FILE_NAME, 0);
		name = settings.getString(NAME, "");
		socialName = settings.getString(SOCIAL_NAME, "");
		rfc = settings.getString(RFC, "");
		address = settings.getString(ADDRESS, "");
		deviceNumber = settings.getString(DEVICE_NUMBER, "");
		email = settings.getString(EMAIL, "");
		businessType = settings.getString(BUSINESS_TYPE, "");
		takeShoot = settings.getBoolean(TAKE_SHOOT, true);
		locate = settings.getBoolean(LOCATE, true);
		swiped=settings.getBoolean(SWIPED,true);
	}

	
}
