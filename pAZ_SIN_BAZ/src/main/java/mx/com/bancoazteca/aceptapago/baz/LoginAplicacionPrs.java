package mx.com.bancoazteca.aceptapago.baz;

import java.io.IOException;

import mx.com.bancoazteca.aceptapago.baz.entidades.FirmaAplicacion;
import mx.com.bancoazteca.aceptapago.baz.ws.parsers.UtilidadesParseo;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LoginAplicacionPrs {
	UtilidadesParseo util = new UtilidadesParseo();

	public FirmaAplicacion parsearArchivoXml(String llave) throws SAXException,
			IOException {
		FirmaAplicacion unaFirma = new FirmaAplicacion();
		try {
			util = new UtilidadesParseo();
			util.setXml(llave);
			Element docEle = util.parseaTexto();
			// if(util.obtieneCodigoOperacion(docEle) <0){
			// return null;
			// }
			NodeList nl = docEle.getElementsByTagName("idaplicacion");
			if (nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {
					Element elemento = (Element) nl.item(i);
					unaFirma = obtieneFirma(elemento);

				}
			}
			unaFirma.setCodigoOperacion(Integer.parseInt(util.obtenerTexto(
					docEle, "codigo_operacion")));
			unaFirma.setMensaje(util.obtenerTexto(docEle, "descripcion_codigo"));
			return unaFirma;
		} catch (Exception pce) {
			pce.printStackTrace();
			unaFirma = new FirmaAplicacion();
			unaFirma.setCodigoOperacion(-1);
			return unaFirma;
		}
	}

	public FirmaAplicacion obtieneFirma(Element elemento) {
		FirmaAplicacion firma = new FirmaAplicacion();
		firma.setLlave(elemento.getAttribute("cipher"));
		return firma;
	}
}
