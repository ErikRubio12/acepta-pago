package mx.com.bancoazteca.aceptapago.jobs;

import java.io.IOException;
import java.net.ConnectException;
import java.util.StringTokenizer;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpStatus;

import android.content.Context;

import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.network.BaseService;
import mx.com.bancoazteca.network.Connections;
import mx.com.bancoazteca.network.HttpConnection;
import mx.com.bancoazteca.network.HttpHeaders;
import mx.com.bancoazteca.aceptapago.beans.OperationsBean;
import mx.com.bancoazteca.util.Logger;

public class DownloadTicketJob extends BaseService<Message, String> implements Runnable{
	private static final String DIGITIZING_URL = Urls.DIGITALIZACION + "iPhone/DigSvlGetUrl";
	private String key;
	private HttpConnection conn;
	private boolean isCancellation=false;
	private OperationsBean operation;
	public final static int DOWNLOADED=324;
	public final static int DOWNLOADED_ERROR=245;
	private static final String DOWNLOAD_TICKET = "DownloadTicketJob";
	public DownloadTicketJob(Context ctx,MessageListener<Message, String> listener,String key, boolean isCancellation, OperationsBean operation) {
		// TODO Auto-generated constructor stub
		this.listener=listener;
		this.key=key;
		this.ctx=ctx;
		this.isCancellation=isCancellation;
		this.operation=operation;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Message msg= new Message();
		HttpHeaders headers= new HttpHeaders();
		StringBuilder url= new StringBuilder(DIGITIZING_URL);
		url.append("?");
		url.append("llave=");
		url.append(this.key);
		url.append("&");
		url.append("producto=PAGOAZTECA");
		Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"url descarga : "+ url);
		try {
			conn= Connections.makeHttpConnection(url.toString(), headers, null,TIME_OUT);
			Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"conexion : "+ conn);
			if(conn==null){
				listener.sendStatus(ctx,Message.EXCEPTION);
				return;
			}
			else if (conn.getResponse().getStatusLine()!=null && conn.getResponse().getStatusLine().getStatusCode()==HttpStatus.SC_OK) {
				Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"Si existe ticket");
				String response=conn.getResponseContent();
				Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"response = "+ response);
				
				if("0".equalsIgnoreCase(response.trim())){
					listener.sendStatus(ctx,DOWNLOADED_ERROR);
					return;
				}
				response= response.replace(" ", "|");
				response= response.replace("\n", "|");
				Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"nuevo response: " + response);
				StringTokenizer tokens= new StringTokenizer(response,"|");
				int numTokens=tokens.countTokens();
				Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"numero tokens=" + numTokens);
				if(tokens.countTokens()<3){
					listener.sendStatus(ctx,Message.CANCEL);
					return;
				}
				String firstID= tokens.nextToken();
				String urlTicket=null;
				Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"primer token = "+ firstID);
				
				if(isCancellation && "344".equals(firstID)){
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"segundo token = "+ tokens.nextToken());
					urlTicket=tokens.nextToken();
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"tercer token = "+ urlTicket);
				}
				else if(isCancellation && !"344".equals(firstID) && numTokens>3){
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"segundo token = "+ tokens.nextToken());
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"tercer token = "+ tokens.nextToken());
					String secondID=tokens.nextToken();
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"cuarto token segundo ID = "+ secondID);
					if("344".equalsIgnoreCase(secondID)){
						Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"quinto token = "+ tokens.nextToken());
						urlTicket=tokens.nextToken();
						Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"sexto token = "+ urlTicket);
					}
					else{
						Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"no hay ticket cancelacion 1");
						listener.sendStatus(ctx,DOWNLOADED_ERROR);
						return;
					}
					
				}
				else if(isCancellation && !"344".equals(firstID) && numTokens<=3){
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"no hay ticket cancelacion 2");
					listener.sendStatus(ctx,DOWNLOADED_ERROR);
					return;
				}
				else if(!isCancellation && "516".equals(firstID)){
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"segundo token = "+ tokens.nextToken());
					urlTicket=tokens.nextToken();
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"tercer token = "+ urlTicket);
				}
				else if(!isCancellation && !"516".equals(firstID) && numTokens>3){
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"segundo token = "+ tokens.nextToken());
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"tercer token = "+ tokens.nextToken());
					String secondID=tokens.nextToken();
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"cuarto token segundo ID = "+ secondID);
					if("516".equalsIgnoreCase(secondID)){
						Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"quinto token = "+ tokens.nextToken());
						urlTicket=tokens.nextToken();
						Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"sexto token = "+ urlTicket);
					}
					else{
						Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"no hay ticket venta 1");
						listener.sendStatus(ctx,DOWNLOADED_ERROR);
						return;
					}
				}
				else if(!isCancellation && !"516".equals(firstID) && numTokens<=3){
					Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"no hay ticket venta 2");
					listener.sendStatus(ctx,DOWNLOADED_ERROR);
					return;
				}
				StringBuilder builder = new StringBuilder(Urls.DIGITALIZACION.substring(0, Urls.DIGITALIZACION.indexOf(':', 5)));
				builder.append(urlTicket.substring(urlTicket.indexOf('/', 7))); 
				Logger.log(Logger.MESSAGE, DOWNLOAD_TICKET,"url = "+builder );
				
				msg.setType(DOWNLOADED);
				listener.sendMessage(ctx,msg, builder.toString() );
				
			}
			else{
				listener.sendStatus(ctx,Message.CANCEL);
				return;
			}
		} catch (ConnectException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, DOWNLOAD_TICKET,"ConnectException: "+ e.getMessage());
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, DOWNLOAD_TICKET,"TimeoutException: "+ e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			listener.sendStatus(ctx,Message.EXCEPTION);
			Logger.log(Logger.EXCEPTION, DOWNLOAD_TICKET,"IOException: "+ e.getMessage());
		}
	}

}
