package mx.com.bancoazteca.aceptapago.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;

import mx.com.bancoazteca.filters.TextFilter;
import mx.com.bancoazteca.listeners.Message;
import mx.com.bancoazteca.listeners.MessageListener;
import mx.com.bancoazteca.aceptapago.R;
import mx.com.bancoazteca.aceptapago.jobs.RecoverPasswordJob;
import mx.com.bancoazteca.ui.BaseActivity;
import mx.com.bancoazteca.ui.DialogButton;
import mx.com.bancoazteca.ui.PopUp;
import mx.com.bancoazteca.ui.alertDialog.SimpleDialog;
import mx.com.bancoazteca.util.Utility;

public class PasswordRecoverScreen extends BaseActivity implements MessageListener<Message, String>{
	
	private static final int POPUP_EMPTY_FIELDS = 11;
	private static final int POPUP_RECOVER_PASSWORD = 12;
	protected static final int POPUP_PASSWORD_RECOVERED = 13;
	protected static final int POPUP_CONNECTION_ERROR = 14;
	protected static final int POPUP_ERROR = 15;
	private static final int POPUP_PASSWORD_RECOVER_MSG = 16;
	private static final int POPUP_VALID_CHARACTERS = 17;
	private EditText user;
	private String tmpText;
	private String tmpParameter;
	public final static String CHARACTERES = "Caracter invalido";
	private Toolbar mtoolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this_=this;
	}
	@Override
	protected void loadUiComponents() {
		// TODO Auto-generated method stub
		super.loadUiComponents();
		setContentView(R.layout.password_recover);
		InputFilter filters[]= null;
		
		user=(EditText) findViewById(R.id.userEditText);
		
		filters= new InputFilter[user.getFilters().length +1];
		System.arraycopy(user.getFilters(), 0, filters,0,user.getFilters().length);
		filters[user.getFilters().length]=new TextFilter(TextFilter.TYPE_CHARACTERS);
		user.setFilters(filters);
		mtoolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mtoolbar);
		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowTitleEnabled(false);
		}
		
	}
	@Override
	protected void processData() {
		// TODO Auto-generated method stub
		super.processData();
	}
	@Override
	protected void startBackgroundProcess() {
		// TODO Auto-generated method stub
		super.startBackgroundProcess();
		SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.passwordRecoverPopupText));
		dialog.show(getSupportFragmentManager(),""+POPUP_PASSWORD_RECOVER_MSG);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		DialogButton btn;
		super.onCreateDialog(id);
		switch (id) {
		case POPUP_RECOVER_PASSWORD:
			popUp= new PopUp(this,R.string.popUpMessage,R.string.sendingEmail);
			popUp.setCancelable(false);
			popUp.setType(PopUp.PROGRESS_INFINIT);
			popUp.create();
			return popUp.getDialog();
		default:
			return null;
		}
		
	}
	@Override
	protected boolean validForm() {
		// TODO Auto-generated method stub
		if (user.getText().toString().length() == 0 ){
			SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),getString(R.string.emptyFields));
			dialog.show(getSupportFragmentManager(),""+POPUP_EMPTY_FIELDS);
			return false;
		}
		return true;
	}
	public boolean isValidCharacters(){
		boolean answer = true;
		if(!Utility.isvalidCharacters(user.getText().toString())){
			answer=false;
			user.setError(CHARACTERES);
		}
		
		
		return answer;
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("text", tmpText);
		outState.putString("parameter", tmpParameter);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		tmpText=savedInstanceState.getString("text");
		tmpParameter=savedInstanceState.getString("parameter");
	}
	//***********ACTION LISTNERS**************************

	public void accept(View v){
		if(validForm()){
			showDialog(POPUP_RECOVER_PASSWORD);
			backgroundThread= new Thread(new RecoverPasswordJob(this,user.getText().toString(), this));
			backgroundThread.start();
		}
	}
	@Override
	public void sendMessage(final Context ctx,final Message message, final String parameter) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			public void run() {
				if (isFinishing()) 
					return;
				this_.dismissDialog(POPUP_RECOVER_PASSWORD);
				if(message.getType()==Message.OK){
					StringBuffer text= new StringBuffer(getResources().getString(R.string.passwordRecover));
					text.append(" ");
					text.append(parameter);
					text.append(".");
					tmpText=text.toString();
					SimpleDialog dialog=SimpleDialog.newInstance(getString(R.string.popUpMessage),tmpText);
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(),""+POPUP_PASSWORD_RECOVERED);
				}else if (message.getType()==Message.EXCEPTION) {
					SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage), getString(R.string.connectionFails));
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_CONNECTION_ERROR);
				}else{
					tmpParameter=parameter;
					SimpleDialog dialog = SimpleDialog.newInstance(getString(R.string.popUpMessage),tmpParameter);
					dialog.setmAcceptListener(new SimpleDialog.AcceptListener() {
						@Override
						public void onAccept(DialogInterface dialog) {
							finish();
						}
					});
					dialog.show(getSupportFragmentManager(), "" + POPUP_ERROR);
				}	
			}
		});
	}

	@Override
	public void sendStatus(Context ctx,int status) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMessage(Context ctx,Message message, Object... parameters) {
		// TODO Auto-generated method stub
		
	}
}
